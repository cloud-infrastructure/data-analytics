stages:
  - build-base-image   
  - test-components
  - build-images
  - test-qa
  - test-prod


############################################################
#####              BUILD IMAGES                        #####
############################################################

.definition_build_image: &template_build_image
  image: # NB enable shared runners and do not specify a CI tag
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder # CERN version of the Kaniko image
    entrypoint: [""]
  script:
    - echo "current commit is ${CI_COMMIT_SHA:0:8}"
    - echo "current branch is ${CI_COMMIT_BRANCH}"
    - echo "current tag is ${CI_COMMIT_TAG}"
    - if [[ -z $DOCKERFILE ]]; then echo "ERROR variable DOCKERFILE is not defined "; exit 1; fi
    - if [[ -z $CONTEXT ]]; then echo "ERROR variable CONTEXT is not defined "; exit 1; fi
    - if [[ -z $IMAGE_NAME ]]; then echo "ERROR variable IMAGE_NAME is not defined "; exit 1; fi
    - if [[ -z $IMAGE_TAG ]]; then echo "ERROR variable IMAGE_TAG is not defined "; exit 1; fi
    - export DESTINATIONS="--destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_BRANCH}-${CI_COMMIT_SHA:0:8}"
    - echo "DESTINATIONS $DESTINATIONS"
    # Prepare Kaniko configuration file
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # Build and push the image from the Dockerfile at the root of the project.
    # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
    # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
    - /kaniko/executor --context $CONTEXT --dockerfile $DOCKERFILE $DESTINATIONS

#------------------------------------
# Build image to be used in tox tests
build_tox_image:
    stage: build-base-image
    before_script:
        - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/tox/Dockerfile
        - export CONTEXT=$CI_PROJECT_DIR/docker-images/tox
        - export IMAGE_NAME=tox
        - export IMAGE_TAG=latest
    <<: *template_build_image
    only:
      refs:
        - master
      changes:
        - docker-images/tox/*

#------------------------------------
# Build base image for the spark notebook based on swan image
build_sparknotebook_base_image:
    stage: build-base-image
    before_script:
        - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/sparknotebook/Dockerfile-swan-base
        - export CONTEXT=$CI_PROJECT_DIR/docker-images/sparknotebook
        - export IMAGE_NAME=sparknotebook_base
        - export IMAGE_TAG=v1.0
    <<: *template_build_image
    only:
      refs:
        - qa
      changes:
        - docker-images/sparknotebook/Dockerfile-swan-base

#-------------------------------------------------------------------------------------
# Build image that containts docker compose to
# test pipelines in docker compose
# in privileged runner
build_compose_image:
    stage: build-base-image
    before_script:
        - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/compose/Dockerfile
        - export CONTEXT=$CI_PROJECT_DIR/docker-images/compose
        - export IMAGE_NAME=compose
        - export IMAGE_TAG=latest
    <<: *template_build_image
    rules:
       - if: '$CI_COMMIT_BRANCH =~ /^qa.*$/'
         changes:
          - docker-images/compose/*
       - if: '$CI_COMMIT_TAG =~ /^v.*$/'
      

############################################################
#####              BUILD QA IMAGES                     #####
############################################################

#-------------------------------------------------------------------------------------
# Build image that runs jupyter notebook with the data-analytics libraries installed
# The same image can be used interactively to start a jupyter notebook
build_jupyter_image:
    stage: build-images
    before_script:
        - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/jupyter/Dockerfile
        - export CONTEXT=$CI_PROJECT_DIR
        - export IMAGE_NAME=jupyter
        - export IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
    <<: *template_build_image
    rules:
       - if: '$CI_COMMIT_BRANCH =~ /^qa.*$/ || $CI_COMMIT_TAG =~ /^v.*$/'
#-------------------------------------------------------------------------------------
# Build fluentd image with plugins
build_fluentd_image:
    stage: build-images
    before_script:
      - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/fluentd/Dockerfile
      - export CONTEXT=$CI_PROJECT_DIR/docker-images/fluentd
      - export IMAGE_NAME=fluentd
      - export IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
    <<: *template_build_image
    rules:
       - if: '$CI_COMMIT_BRANCH =~ /^qa.*$/ || $CI_COMMIT_TAG =~ /^v.*$/'

#-------------------------------------------------------------------------------------
# Build image that runs swan spark notebook with the data-analytics libraries installed
# The same image can be used interactively to query spark
build_spark_image:
    stage: build-images
    before_script:
      - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/sparknotebook/Dockerfile
      - export CONTEXT=$CI_PROJECT_DIR/
      - export IMAGE_NAME=sparknotebook
      - export IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
    <<: *template_build_image
    rules:
       - if: '$CI_COMMIT_BRANCH =~ /^qa.*$/ || $CI_COMMIT_TAG =~ /^v.*$/'

#-------------------------------------------------------------------------------------
# Build image that runs airflow image
build_airflow_image:
    stage: build-images
    before_script:
      - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/docker-airflow/Dockerfile
      - export CONTEXT=$CI_PROJECT_DIR/docker-images/docker-airflow/
      - export IMAGE_NAME=docker-airflow
      - export IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
    <<: *template_build_image
    rules:
       - if: '$CI_COMMIT_BRANCH =~ /^qa.*$/ || $CI_COMMIT_TAG =~ /^v.*$/'


############################################################
#####              TEST COMPONENTS                     #####
############################################################

.pep8:
  stage: test-components
  image: $CI_REGISTRY_IMAGE/tox:latest
  except:
    - tags
  script:
    - cd $CI_PROJECT_DIR
    - scl enable rh-python36 'tox -epep8'

.coverage:
  stage: test-components
  image: $CI_REGISTRY_IMAGE/tox:latest
  except:
    - tags
  script:
    - cd $CI_PROJECT_DIR
    - scl enable rh-python36 'tox -ecover'
  artifacts:
    paths:
      - cover


###################################
#####        TESTS            #####
###################################

# ----------------------------------------------------
# Test data extraction from grafana using grafana_etl
qa_test_grafana_etl: &template_test_grafana_etl
  stage: test-qa
  image:
    name: $CI_REGISTRY_IMAGE/jupyter:${CI_COMMIT_BRANCH}
    entrypoint: [""]
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa.*$/
  script:
    - echo $GRAFANA_KEY > $CI_PROJECT_DIR/tests/grafana_etl/secrets/grafana_token.txt
    - ls -l
    - pwd
    - cd $CI_PROJECT_DIR/tests/grafana_etl/
    - jupyter nbconvert --to notebook --execute test_ETL.ipynb
  artifacts:
    paths:
        - $CI_PROJECT_DIR/tests/grafana_etl/test_ETL.nbconvert.ipynb
    expire_in: 1 week
    when: always

# Need to duplicate test because image name cannot be template to
# $CI_REGISTRY_IMAGE/jupyter:${CI_COMMIT_TAG:-${CI_COMMIT_BRANCH}}
prod_test_grafana_etl:
  <<: *template_test_grafana_etl
  stage: test-prod
  image:
    name: $CI_REGISTRY_IMAGE/jupyter:${CI_COMMIT_TAG}
    entrypoint: [""]
  only:
    variables:
      - $CI_COMMIT_TAG =~ /^v.*$/

# -----------------------------------------------
# Test data extraction from spark using spark_etl
spark_etl: &template_test_spark_etl
  stage: test-qa
  tags:
    - data-analytics-spark-ci # for private runner
  image:
    name: $CI_REGISTRY_IMAGE/compose:v0.1
    entrypoint: [""]
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^qa.*$/ || $CI_COMMIT_TAG =~ /^v.*$/'
  script:
    - . ${CI_PROJECT_DIR}/tests/spark_etl/integration/ci_test_script.sh
    - start_docker_compose
  after_script:
    - . ${CI_PROJECT_DIR}/tests/spark_etl/integration/ci_test_script.sh
    - stop_docker_compose
  artifacts:
    paths:
        - $CI_PROJECT_DIR/tests/spark_etl/integration/*
    expire_in: 1 week
    when: always

# -----------------------------------------------------
# Test fluentd pipeline to push data into Elasticsearch
pipeline_anomaly_to_ES: &template_pipeline_anomaly_to_ES
  stage: test-qa
  tags:
    - data-analytics-spark-ci # for private runner
  image:
    name: $CI_REGISTRY_IMAGE/compose:v0.1
    entrypoint: [""]
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^qa.*$/ || $CI_COMMIT_TAG =~ /^v.*$/'
  before_script:
    - docker ps -a
    - . $CI_PROJECT_DIR/tests/anomaly_to_ES/ci_run_script.sh
    - start_docker_compose
    - echo "before_script is finished"
  script:
    - . $CI_PROJECT_DIR/tests/anomaly_to_ES/ci_run_script.sh
    - run_reader
    - echo "script is finished"
  after_script:
    - . $CI_PROJECT_DIR/tests/anomaly_to_ES/ci_run_script.sh
    - stop_docker_compose
    - echo "after_script is finished"
  artifacts:
    paths:
        - $CI_PROJECT_DIR/tests/anomaly_to_ES/myfluent.log
        - $CI_PROJECT_DIR/tests/anomaly_to_ES/compose.log
    expire_in: 1 week
    when: always

# -----------------------------------------------
# Test adcern
qa_adcern:
  stage: test-qa
  tags:
    - data-analytics-spark-ci # for private runner
  image:
    name: $CI_REGISTRY_IMAGE/compose:v0.1
    entrypoint: [""]
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^qa.*$/ || $CI_COMMIT_TAG =~ /^v.*$/'
  before_script:
    - docker ps -a
    - . ${CI_PROJECT_DIR}/tests/adcern/integration/ci_test_script.sh
    - start_docker_compose
    - echo "before_script is finished"
  script:
    - . ${CI_PROJECT_DIR}/tests/adcern/integration/ci_test_script.sh
    - comparison
    - echo "script is finished"
  after_script:
    - . ${CI_PROJECT_DIR}/tests/adcern/integration/ci_test_script.sh
    - stop_docker_compose
    - echo "after_script is finished"
  artifacts:
    paths:
        - $CI_PROJECT_DIR/tests/adcern/integration/*
    expire_in: 1 week
    when: always
