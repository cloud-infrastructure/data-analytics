import numpy as np
import pandas as pd
from random import randint
from etl.spark_etl.dataprep import WindowDispatcher
from sklearn.preprocessing import normalize


class Mixer(object):

    def __init__(self, set_anomalies, set_normal,
                 noramlizer_id, scaler_fitted):
        """Create datasets of artificial anomalies injecting shared cells.

        The idea is to inject few hosts from shared cells in the hosts of
        batch cells so that we can threat them as anomalies. Because they
        fundamentally belong to two different classes. By taking inspiration
        from the following paper [1].

        Initialize by passing a two set of CollectdDatasets.

        Parameters
        ----------
        set_anomalies : list[CollectdDataset]
            List of dataset from Shared cells
        set_normal : list[CollectdDataset]
            List of dataset from Batch cells

        Attributes
        ----------
        point_difficulty: float, [0, 1]
            How much the injected hosts are similar to the batch hosts?
            This value set the similarity threshold above under which the
            anomalies will be injected.
            e.g. shared host with similarity 0.15 with batch dataset will be
            more difficult than a host with similarity 0.83
        relative_frequency: float, [0, 1]
            How many injections do we perform? Expressed as a fraction in [0,1]
        semantic_variation: float, [0, 1]
            How different are the two generating processes?
            If we cluster the shared and the batch how fare are those two
            clusters?
        relevant_features: int
            How many features do we want to consider?

        References
        ----------
        [1] A. F. Emmott, S. Das, T. Dietterich, A. Fern, W. Wong
        "Systematic Construction of Anomaly Detection Benchmarks from Real
        Data"
        """
        self.set_anomalies = set_anomalies
        self.set_normal = set_normal
        self.noramlizer_id = noramlizer_id
        self.scaler_fitted = scaler_fitted
        self.set_options()
        pass

    def set_options(self,
                    relative_frequency=0.05,
                    history_len=8,
                    prediction_len=0):
        """Set the attributes explained in the class header."""
        self.relative_frequency = relative_frequency
        self.history_len = history_len
        self.prediction_len = prediction_len

    def generate(self, rnd_seed=42,
                 index_ds_anomaly=0, index_ds_normal=0):
        """Generate the new test set with anomalies and labels.

        Parameters
        ----------
        rnd_seed : int
            For reproducible purpose

        Returns
        -------
        test_set : pandas Dataframe
            Each row contains the history window (all the features) of a
            host in a specific timestep.
            Columns tell which plugin and timestep that column is from
        provenance_time: numpy array
            Each row gives info about the timestep.
        provenance_orgin: numpy array
            Each row gives info about the host.
        artificial_anomaly: numpy array
            Vecor of 0 (from batch class) or 1 (from shared class)
        group_analysis: numpy array
            Each row has a number and represent the machines that has to be
            considered in the same timewindow for prediciton.
        """
        # get the two datasets to merge
        clds_shared = self.set_anomalies[index_ds_anomaly]
        clds_batch = self.set_normal[index_ds_normal]

        # create windows
        wd_s = WindowDispatcher(clds_shared)
        X_s, y_s, prov_s = wd_s.window_creation(history_len=self.history_len,
                                                prediction_len=self.prediction_len,  # noqa
                                                scaler=self.scaler_fitted,
                                                scaler_id=self.noramlizer_id)  # noqa

        wd_b = WindowDispatcher(clds_batch)
        X_b, y_b, prov_b = wd_b.window_creation(history_len=self.history_len,
                                                prediction_len=self.prediction_len,  # noqa
                                                scaler=self.scaler_fitted,
                                                scaler_id=self.noramlizer_id)  # noqa

        # copy the original
        X_modified = X_b.copy()
        y_modified = y_b.copy()
        prov_modified = prov_b.copy()

        # create a unique id for each window
        prov_window = prov_modified[1].astype('category').cat.codes

        # compute how many injections to do
        nr_samples_batch = len(prov_b)
        nr_samples_shared = len(prov_s)
        nr_injections = int(nr_samples_batch * self.relative_frequency)

        # randomly sample from shared
        index_shared = randint(0, nr_samples_shared - 1)
        index_shared = np.random.randint(nr_samples_shared, size=nr_injections)
        print(index_shared)
        # selected_shared = prov_s.iloc[index_shared].values
        # print(selected_shared)

        # randomly sample from batch
        index_batch = randint(0, nr_samples_batch - 1)
        index_batch = np.random.choice(np.arange(nr_samples_batch),
                                       nr_injections, replace=False)
        print(index_batch)
        # selected_batch = prov_b.iloc[index_batch].values
        # print(selected_batch)

        # create the vector anomalies
        anomaly_series = pd.DataFrame(np.zeros((nr_samples_batch, 1)))

        # copy shared to the selected positions in modified version of batch
        X_modified.iloc[index_batch] = X_s.iloc[index_shared].values
        prov_modified.iloc[index_batch, 0] = prov_s.iloc[index_shared, 0].values  # noqa
        y_modified.iloc[index_batch] = y_s.iloc[index_shared].values
        anomaly_series.iloc[index_batch] = 1

        test_set = X_modified
        provenance_time = prov_modified[1].values
        provenance_orgin = prov_modified[0].values
        artificial_anomaly = anomaly_series.values
        group_analysis = prov_window.values
        return test_set, provenance_time, provenance_orgin, \
            artificial_anomaly, group_analysis

    def save(self, filename):
        """Save the result of the generation process in a csv.

        The csv represent the pandas dataframe in which the results array
        are stacked horizontally.

        Parameters
        ----------
        filename : str
            name of the file, it shoud include csv
        """
        pass

    def load(self, filename):
        """Read the csv file previously saved.

        The csv represent the pandas dataframe in which the results array
        are stacked horizontally. The internal attributes are repopulated
        after this function call.

        Parameters
        ----------
        filename : str
            name of the file
        """
        pass


class WindowMixer(object):
    """Mix two datasets from Batch and Shared cells.

    Note that the data as input have to be already normalized.


    Attributes
    ----------
    point_difficulty: float, [0, 1]
        How much the injected hosts are similar to the batch hosts?
        This value set the similarity threshold above under which the
        anomalies will be injected.
        e.g. shared host with similarity 0.15 with batch dataset will be more
        difficult than a host with similarity 0.83
    relative_frequency: float, [0, 1]
        How many injections do we perform? Expressed as a fraction in [0,1]
    semantic_variation: float, [0, 1]
        How different are the two generating processes?
        If we cluster the shared and the batch how fare are those two clusters?
    relevant_features: int

    """

    def __init__(self, filename=None):
        if filename is not None:
            self.load(filename=filename)

    def set_options(self,
                    X_normal, prov_normal,
                    X_anomalous, prov_anomalous,
                    nr_timesteps=48,
                    relative_frequency=0.05,
                    subsample=48):
        """Set the attributes explained in the class header.

        Parameters
        ----------
        X_normal : Pandas Dataframe
            columns: cpu_h0, ..., cpu_h48, load_h0, ... etc
        prov_normal : Pandas Dataframe
            columns: hostname | ts (datetime) | timestamp (int)

        X_anomalous : Pandas Dataframe
            columns: cpu_h0, ..., cpu_h48, load_h0, ... etc
        prov_anomalous : Pandas Dataframe
            columns: hostname | ts (datetime) | timestamp (int)
        nr_timesteps: int
            history to consider
        subsample: int
            number of windows to skip every two consecutie evaluations
        """
        self.X_normal = X_normal
        self.prov_normal = prov_normal
        self.X_anomalous = X_anomalous
        self.prov_anomalous = prov_anomalous
        self.nr_timesteps = nr_timesteps
        self.subsample = subsample
        self.relative_frequency = relative_frequency

    def generate(self, rnd_seed=42):
        """Generate the new test set with anomalies and labels.

        Parameters
        ----------
        rnd_seed : int
            For reproducible purpose

        Returns
        -------
        test_set : pandas DataFrame
            Each row contains the history window (all the features) of a
            host in a specific timestep.
            Columns tell which plugin and timestep that column is from
        prov_test: pandas DataFrame
            Each row gives info about the provenance
            columns:
                hostname | ts (datetime) | timestamp (int) | anomaly | group
            anomaly: Vecor of 0 (from normal class) or 1 (from anomalous class)
            group:
            Each row has a number and represent the machines that has to be
            considered in the same timewindow for prediciton.
        """
        all_test_set = pd.DataFrame(columns=self.X_normal.columns)
        new_prov_columns = \
            list(self.prov_normal.columns) + ["anomaly", "group"]
        all_prov_test = pd.DataFrame(columns=new_prov_columns)

        # control random factor
        np.random.seed(rnd_seed)

        # compute all timesta
        timestamps = sorted(self.prov_normal["ts"].unique())[::self.subsample]
        print("TIMESTAMPS:")
        [print("-> ", t) for t in timestamps]

        # analyse the batch wndows sequentially
        for i_group, ts in enumerate(timestamps):
            # get batch window in this timestep
            mask = self.prov_normal["ts"] == ts
            X_w = self.X_normal[mask]
            prov_w = self.prov_normal[mask]
            nr_normal_instances = len(X_w)

            # plot Window's info
            print("=" * 50)
            print("New window with %i normal machines" % nr_normal_instances)
            print("group %i - timestamp: %s" % (i_group, ts))
            not_null_values = list((~np.isnan(X_w.values)).sum(axis=1))
            nr_host_all_nan = not_null_values.count(0)

            if nr_host_all_nan > 0:
                print("Nr machines with all null values: ", nr_host_all_nan)
                print("Window dropped. Next one.")
                continue

            # compute the nr of machines that we need from the anomalous
            nr_anomaly_to_add = \
                int(nr_normal_instances * self.relative_frequency)
            print("nr_anomaly_to_add: ", nr_anomaly_to_add)

            # get machines randomly from the anomalous data
            nr_tot_anomalies_available = len(self.X_anomalous)
            print("nr_tot_anomalies_available: %i (overall)"
                  % nr_tot_anomalies_available)

            # if it is a valid window
            if (nr_normal_instances == 0):
                print("No machines go to nex window.")
                continue
            if nr_anomaly_to_add > nr_tot_anomalies_available:
                print("Not enought Machine in the pool of anomalous to"
                      "match the requirements: "
                      "required: %s, available: %s" %
                      (nr_anomaly_to_add, nr_tot_anomalies_available))
                continue

            # Experiment Creation
            print("Experiment creation...")
            # extraction procedure
            # RANDOM
            # mask_anomalous = np.full(nr_tot_anomalies_available, False)
            # mask_anomalous[:nr_anomaly_to_add] = True
            # np.random.shuffle(mask_anomalous)
            anomalous_test_selection = self.X_anomalous
            anomalous_prov_test_selection = self.prov_anomalous
            # similarity based
            # compare every candidate share with all the batch in this window
            # thak the one most similar on average
            # dot product as similarity

            candidates = \
                normalize(np.nan_to_num(anomalous_test_selection.values,
                                        nan=0,
                                        posinf=10,
                                        neginf=-10), axis=1)
            batch_to_imitate = normalize(np.nan_to_num(X_w.values,
                                                       nan=0,
                                                       posinf=10,
                                                       neginf=-10))
            print("batch_to_imitate col: ", batch_to_imitate[0, :3])
            print("candidates row: ", candidates[0, :3])
            similarity_matrix = np.matmul(candidates, batch_to_imitate.T)
            # print("SIMILARITY: ", similarity_matrix[0, :])
            similarity_every_candidate = np.median(similarity_matrix, axis=1)
            best_candidates_pos = \
                np.argsort(similarity_every_candidate)[::-1][:int(nr_anomaly_to_add)]  # noqa
            print("best_candidates position: ", best_candidates_pos)
            print("similarity of best_candidates: ",
                  similarity_every_candidate[best_candidates_pos])
            anomalous_test_selection = \
                anomalous_test_selection.iloc[best_candidates_pos]
            anomalous_prov_test_selection = \
                anomalous_prov_test_selection.iloc[best_candidates_pos]

            normal_test_selection = X_w
            normal_prov_test_selection = prov_w

            print("normal_test_selection: ",
                  normal_test_selection.shape)
            print("anomalous_test_selection: ",
                  anomalous_test_selection.shape)

            # create the test set by merging
            test_set = pd.concat([normal_test_selection,
                                  anomalous_test_selection],
                                 axis=0)
            prov_test_set = pd.concat([normal_prov_test_selection,
                                       anomalous_prov_test_selection],
                                      axis=0)
            ground_truth = \
                [False] * len(normal_test_selection) + \
                [True] * len(anomalous_test_selection)

            # Show final measures
            print("test_set: ", test_set.shape)
            print("prov_test_set: ", prov_test_set.shape)

            # compose artificial window
            prov_test_set["anomaly"] = ground_truth
            prov_test_set["group"] = np.full(len(ground_truth), i_group)

            print(prov_test_set.shape)

            all_test_set = pd.concat([all_test_set, test_set])
            all_prov_test = pd.concat([all_prov_test, prov_test_set])
            # break

        # store them in case want to save to file
        self.all_test_set = all_test_set
        self.all_prov_test = all_prov_test

        return self.all_test_set, self.all_prov_test

    def save(self,
             filename,
             folder="/tmp/vm-datalake/experiments/",):
        """Save the result of the generation process in a csv.

        The csv represent the pandas dataframe in which the results array
        are stacked horizontally.

        Parameters
        ----------
        filename : str
            name of the file, it shoud include csv
        """
        self.all_test_set.to_csv(folder + filename + "_data.csv", index=False)
        self.all_prov_test.to_csv(folder + filename + "_prov.csv", index=False)

    def load(self,
             filename,
             folder="/tmp/vm-datalake/experiments/"):
        """Read the csv file previously saved.

        The csv represent the pandas dataframe in which the results array
        are stacked horizontally. The internal attributes are repopulated
        after this function call.

        Parameters
        ----------
        filename : str
            name of the file
        """
        self.all_test_set = pd.read_csv(folder + filename + "_data.csv")
        self.all_prov_test = pd.read_csv(folder + filename + "_prov.csv")
        return self.all_test_set, self.all_prov_test
