import matplotlib  # noqa : F401
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.spatial import distance
from scipy.cluster import hierarchy
from statsmodels.graphics.tsaplots import plot_acf

import collections


class Explorer(object):
    """Object to explore CERN data aka Exploratory Data Analysis (EDA)."""

    def __init__(self, collectd_dataset):
        """Initialize with CERN data (as CollectdDataset).

        collectd_dataset: metrics data of the machines in a cell.
        """
        if collectd_dataset is None:
            raise Exception("You passed an empty object as dataset.")
        data = collectd_dataset.get_data()
        metadata = collectd_dataset.get_metadata()
        # print(metadata["granularity_min"])
        self.data = data
        self.metadata = metadata
        if "start_date" in metadata.keys() and \
                "end_date" in metadata.keys():
            self.set_range(metadata["start_date"], metadata["end_date"])
        else:
            inferred_start = self.data.index.min().strftime("%Y-%m-%d")
            inferred_end = self.data.index.max().strftime("%Y-%m-%d")
            self.set_range(inferred_start, inferred_end)
        self.img_width = 20

    def set_image_width(self, img_width):
        """Choose the max width of your diagrams (default 18)."""
        self.img_width = img_width

    def describe(self):
        """Describe the data acquired with some statistis and key info."""
        pass

    def report_missing_values(self, on_metrics=[]):
        """Show info about missing values and key statistics."""
        nr_missing = self.data[self.data["mean_value"].isnull()].shape[0]
        total_points = self.data.shape[0]
        perc = (float(nr_missing) / total_points) * 100
        print("Missing values: %i (%i) in percentage: %.2f %%\n"
              % (nr_missing, total_points, perc))

    def set_range(self, start_date: str, end_date: str):
        """Define the range of your exploration.

        start_date: start date of your dataset (format: "YYYY-mm-dd")
        end_date: end date of your dataset (format: "YYYY-mm-dd")
        """
        self.start = start_date
        self.end = end_date
        self.snapshot = self.data[self.start:self.end]

    def plot(self, on_metrics=None, on_machines=None, short=1, **pltargs):
        """Plot the timeseries of the machines contained in this dataset.

        on_metrics: the list of signal/metrics names that you want to plot,
        use the names that you have chosen in the dictionary as signal_name.
        If not set all the plugin will be plt one after the other.
        on_machine: the list of hosts/machines names that you want to plot.
        short: a multiplier from 0 to 1 to shrink the height of your plots.
        **pltargs: arguments that will be passed to the pandas plot function
        of the DataFrame object
        """
        if on_metrics is None:
            on_metrics = list(self.snapshot.plugin.unique())
        for plugin_name in sorted(on_metrics):
            pdf_plugin = \
                self.snapshot[self.snapshot["plugin"] == plugin_name]
            pdf_swarm = pdf_plugin.pivot_table(columns="host",
                                               index="temporal_index",
                                               values="mean_value")
            if on_machines is not None:
                pdf_swarm = pdf_swarm[[h for h in pdf_swarm.columns
                                       if h in on_machines]]
            height = 10 * short
            ax = pdf_swarm.plot(figsize=(self.img_width, height), **pltargs)
            ax.set_title(plugin_name, fontsize=20)
            plt.show()

    def plot_percentile(self, on_metrics=None, on_machines=None,
                        percentiles=[.1, .25, .5, .75, .9], **pltargs):
        """Plot the percentile of the machine behaviour in a cell.

        on_machines: the list of host names that you want to plot.
        on_metrics: the list of signal/metrics names that you want to plot,
        use the names that you have chosen in the dictionary as signal_name.
        percentiles: the percentiles you want to compute.
        """
        if on_metrics is None:
            on_metrics = list(self.snapshot.plugin.unique())
        for metric in sorted(on_metrics):
            fig, axes = plt.subplots(2, 1, figsize=(self.img_width, 10),
                                     gridspec_kw={'height_ratios': [1, 5]})
            fig.suptitle("Percentile on metric: %s" % metric, fontsize=22)
            df = self.snapshot
            if on_machines is not None:
                # filter data for this machine
                df = df[df.host.isin(on_machines)]
            df_metric = df[df["plugin"] == metric]
            # plot hinstogram of data numerosity
            df = df_metric
            df = df.groupby(by="temporal_index").count()
            # nr_max_machines = df["mean_value"].max()
            # nr_min_machines_required = nr_max_machines * 0.1
            # print(nr_max_machine)
            s = df["mean_value"]
            ax_hist = s.plot(kind="bar", figsize=(self.img_width, 1),
                             width=1.0, ax=axes[0],
                             **pltargs)
            n = int(len(s) / 4)
            ticks = ax_hist.xaxis.get_ticklocs()
            ticklabels = \
                [label.get_text() for label in ax_hist.xaxis.get_ticklabels()]
            ax_hist.xaxis.set_ticks(ticks[::n])
            ax_hist.set_xticklabels(ticklabels[::n], rotation=0)

            # Useful for future data cleaning
            # indices_to_remove = df[df["mean_value"]
            #                        < nr_min_machines_required].index
            # print(list(indices_to_remove))

            df = df_metric
            # Future way for data cleaning
            # df = df[~(df.index.isin(indices_to_remove))]
            # df["temporal_index"] = pd.to_datetime(df["temporal_index"])
            df_grouped = df.groupby(by="temporal_index")
            df = df_grouped["mean_value"].quantile(percentiles)
            df = df.reset_index(drop=False)
            df = df.pivot(index="temporal_index", columns="level_1",
                          values="mean_value")
            df.plot(figsize=(self.img_width, 8), ax=axes[1], **pltargs)
            df = df.transpose()
        return axes

    def correlation_machines(self, on_metrics=None, corr_method="pearson",
                             linkage="average", max_cluster=10,
                             hierarchical_metric="euclidean",
                             **pltargs):
        """Plot heatmap correlation between every pair of host time series.

        Compute the correlation between every couple of time series (two
        different machines/hosts) on the same metric.
        Repeat this procedure for every pair, and compute a matrix; finally
        show it as a heatmap.
        Compute also a dendrogam on the column of the correlation matrix and
        plot the resulting clusters.
        Repeat this process for each metric in the list.

        on_metrics: the list of signal/metrics names that you want to plot,
            use the names that chosen in the dictionary as signal_name.
        corr_method: for the correlation of different machines' timeseries. It
            is implemented with pandas.DataFrame.corr
        linkage: the method to perform likage during hierarchical clustering
            it can be: 'single', 'average', 'complete', 'weighted', 'centroid'.
            It is implemented with scipy.cluster.hierarchy.linkage
        hierarchical_metric: metric to use for hierarchical clustering
            it can be: ‘braycurtis’, ‘canberra’, ‘chebyshev’, ‘cityblock’,
            ‘correlation’, ‘cosine’, ‘dice’, ‘euclidean’, ‘hamming’, ‘jaccard’,
            ‘jensenshannon’, ‘kulsinski’, ‘mahalanobis’, ‘matching’,
            ‘minkowski’, ‘rogerstanimoto’, ‘russellrao’, ‘seuclidean’,
            ‘sokalmichener’, ‘sokalsneath’, ‘sqeuclidean’, ‘yule’
        max_cluster: max number of cluster that you want to have. It is
            implemented with scipy.cluster.hierarchy.fcluster
        """
        if on_metrics is None:
            on_metrics = list(self.snapshot.plugin.unique())
        for metric in sorted(on_metrics):
            df = self.snapshot
            df = df[df["plugin"] == metric]
            df = df.pivot(index="temporal_index", columns="host",
                          values="mean_value")
            # COMPUTE CORRELATION
            correlations = df.corr(method=corr_method)
            matrix_corr = np.asarray(df.corr())
            # COMPUTE LINKAGE
            row_linkage = hierarchy.linkage(
                distance.pdist(matrix_corr, hierarchical_metric),
                method=linkage)
            col_linkage = hierarchy.linkage(
                distance.pdist(matrix_corr.T, hierarchical_metric),
                method=linkage)
            # CLUSTERMAP
            sns.clustermap(correlations,
                           row_linkage=row_linkage, col_linkage=col_linkage,
                           method=linkage,
                           figsize=(self.img_width, 16), cmap="Greys")
            # DENDROGRAM
            plt.figure(figsize=(self.img_width, 6))
            hierarchy.dendrogram(row_linkage)
            plt.show()
            # PLOT SINGLE CLUSTERS
            clustering = hierarchy.fcluster(row_linkage, max_cluster,
                                            criterion='maxclust',
                                            monocrit=None)
            cluster_assigns = pd.Series(clustering, index=df.columns)
            for i in np.arange(1, max_cluster + 1):
                machines = list(cluster_assigns[cluster_assigns == i].index)
                if (len(machines) > 0):
                    print("Cluster ", str(i), ": ( N =",
                          len(cluster_assigns[cluster_assigns == i].index),
                          ")", ", ".join(machines))
                    self.plot(on_metrics=[metric], on_machines=machines,
                              short=.2, **pltargs)

    def plot_autocorrelation(self, on_machines=None, on_metrics=None,
                             lags=50, short=1):
        """Plot autocorrelation plot for metrics and machine selected.

        on_machines: the list of host names that you want to plot.
        on_metrics: the list of signal/metrics names that you want to plot,
            use the names that chosen in the dictionary as signal_name.
        lags: nr lags you want to plot
        short: a multiplier from 0 to 1 to shrink the height of your plots.
        """
        if on_metrics is None:
            on_metrics = list(self.snapshot.plugin.unique())
        for plugin_name in sorted(on_metrics):
            df = self.snapshot[self.snapshot["plugin"] == plugin_name]
            if on_machines is not None:
                # filter data for this machine
                df = df[df.host.isin(on_machines)]
            height = 6 * short
            fig = plot_acf(df["mean_value"], lags=lags)
            fig.suptitle('Autocorrelation on %s' % plugin_name, fontsize=20)
            fig.set_size_inches(self.img_width, height)
            plt.show()

    def pairplot_metrics(self, on_machines=None):
        """Plot scatterplots to see the relationship between 2 metrics.

        Create the scatterplot between every couple of time series (two
        different metrics) on the same machine.
        Repeat this procedure for every pair of metrics.
        on_machines: the list of host names that you want to plot.
        method: correlation method for computing the corr matrix.
        """
        if on_machines is not None:
            # plot all the
            df = self.snapshot
            # filter data for this machine
            df = df[df.host.isin(on_machines)]
            g = self._plot_pairplot(df)
        else:
            # plot just one for everything
            g = self._plot_pairplot(self.snapshot)
        return g

    def _plot_pairplot(self, df):
        """Plot pairplot of the given DF, every column a different metric."""
        # PREPRARE THE DATASET
        metrics = list(df.plugin.unique())

        # filter non finite
        point_before = len(df)
        df = df[np.isfinite(df['mean_value'])]
        point_after = len(df)
        tot_dropped = point_before - point_after
        fract_dropped = (float(tot_dropped) / point_before) * 100
        print("Points dropped to plot not finite: %i (%.2f%s)"
              % (tot_dropped, fract_dropped, "%"))

        df = df.pivot_table(index=["temporal_index", "host"], columns="plugin",
                            values="mean_value")
        df = df.reset_index(drop=False)
        # MAP THE HOUR OF THE DAY
        df['temporal_index'] = pd.to_datetime(df['temporal_index'])
        df['hour'] = df['temporal_index'].dt.hour // 4
        df['hour_range'] = df['hour'].map({0: "00:00 - 04:00",
                                           1: "04:00 - 08:00",
                                           2: "08:00 - 12:00",
                                           3: "12:00 - 16:00",
                                           4: "16:00 - 20:00",
                                           5: "20:00 - 00:00"})
        # REMOVE THE NULL VALUE TO USE HUE COLOR
        point_before = len(df)
        df = df.dropna()
        point_after = len(df)
        tot_dropped = point_before - point_after
        fract_dropped = (float(tot_dropped) / point_before) * 100
        print("Points dropped to plot hue color: %i (%.2f%s)"
              % (tot_dropped, fract_dropped, "%"))
        # PLOT PAIR PLOTS
        g = sns.pairplot(data=df,
                         vars=metrics,
                         diag_kind='hist',
                         hue="hour_range",
                         palette=sns.color_palette("husl", 6),
                         plot_kws={'alpha': 0.6, 's': 80, 'edgecolor': 'k'})
        # Map a density plot to the lower triangle
        # g = g.map_lower(sns.kdeplot)
        g.fig.set_size_inches(20, 20)
        return g

    def show_gni(self):
        """Show info coming from the gni dataset."""
        print("GNI Analysis Coming soon.")
        pass

    def plot_ensemble_outliers(self,
                               on_machines,
                               scores,
                               on_metrics=None,
                               window_start=None,
                               window_end=None,
                               nr_machines_in_legend=4,
                               color_outlier="red",
                               short=1, **pltargs):
        """Plot the selected machines, color indicates anomaly score.

        on_machines: the list of host names that you want to plot.
        on_metrics: the list of signal/metrics names that you want to plot,
            use the names that you have chosen in the dictionary as
            signal_name.
            If not set all the plugin will be plt one after the other.
            on_machine: the list of hosts/machines names that you want to plot.
        scores: the anomaly scores (HIGHER is MORE CRITICAL/RED). It must
            have the same order of the machines.
        short: a multiplier from 0 to 1 to shrink the height of your plots.
        color_outlier: ("red") color of your timeseries: "green", "blue",
            "orange", "purple".
        **pltargs: arguments that will be passed to the pandas plot function
            of the DataFrame object

        return: list of machines from the most critical to the lowest and
            list of their scores.
        """
        dict_cmap = {}
        dict_cmap["red"] = matplotlib.cm.Reds
        dict_cmap["blue"] = matplotlib.cm.Blues
        dict_cmap["green"] = matplotlib.cm.Greens
        dict_cmap["orange"] = matplotlib.cm.Oranges
        dict_cmap["purple"] = matplotlib.cm.Purples

        on_machines = list(on_machines)

        # Outlier preprocessing
        # reorder the anomalies:
        # plot first normal data (go to background)
        # plot outlier at the end (on top of the others)
        ordered_index = np.argsort(scores)
        ordered_score = [scores[x] for x in ordered_index]
        ordered_machines = [on_machines[x] for x in ordered_index]
        print("Candidates outliers:")
        print(ordered_machines[-nr_machines_in_legend:])

        # print(on_machines)
        if on_metrics is None:
            on_metrics = list(self.snapshot.plugin.unique())

        # create plots spaces
        n_rows = len(sorted(on_metrics)) + 1
        height = n_rows * 10 * short
        # fig, axes = plt.subplots(ncols=2, nrows= n_rows,
        #                          dpi=100, figsize=(self.img_width, height))

        fig = plt.figure(figsize=(self.img_width, height))
        widths = [1, 20]
        print(widths)
        spec5 = fig.add_gridspec(ncols=2, nrows=n_rows, width_ratios=widths)

        # plot score distribution
        ax = fig.add_subplot(spec5[0, 1])
        ax.set_title("Distribution of the anomaly scores", fontsize=20)
        sns.distplot(ordered_score, ax=ax,
                     color=color_outlier,
                     kde=False)
        row_index = 1
        for plugin_name in sorted(on_metrics):

            pdf_plugin = \
                self.snapshot[self.snapshot["plugin"] == plugin_name]

            # with this filter we will not consider all those machines
            # that do not have this particular plugin plugin_name
            # therefore we have to be aware that we will plot only a subgroup
            # of the machines
            entities_with_this_plugins = list(set(pdf_plugin["host"]))

            pdf_swarm = pdf_plugin.pivot_table(columns="host",
                                               index="temporal_index",
                                               values="mean_value")

            if on_machines is not None:
                pdf_swarm = pdf_swarm[[h for h in pdf_swarm.columns
                                       if h in on_machines]]

            norm = matplotlib.colors.Normalize(vmin=min(scores),
                                               vmax=max(scores))
            cmap = matplotlib.cm.ScalarMappable(norm=norm,
                                                cmap=dict_cmap[color_outlier])
            cmap.set_array([])

            ax = fig.add_subplot(spec5[row_index, 1])
            i = 0
            for score, host in zip(ordered_score, ordered_machines):
                # skip machines that o not have this plugin
                if host not in entities_with_this_plugins:
                    print("Host ", host,
                          " doesn't have this plugin:", plugin_name,
                          " so no plot for it")
                    continue
                # plot current timeseries
                df_ts = pdf_swarm[host]
                # print legend only for a subset
                if (len(ordered_machines) - i) <= nr_machines_in_legend:
                    lebel_plus_score = ("(%.2f) - %s" % (score, host))
                    ax = df_ts.plot(c=cmap.to_rgba(score),
                                    ax=ax,
                                    label=lebel_plus_score,
                                    **pltargs)
                else:
                    ax = df_ts.plot(c=cmap.to_rgba(score),
                                    ax=ax,
                                    label='_nolegend_',
                                    **pltargs)
                i += 1
            ax.set_title(plugin_name, fontsize=20)
            if window_start is not None:
                ax.axvline(pd.to_datetime(window_start), color='red',
                           linestyle='--')
            if window_end is not None:
                ax.axvline(pd.to_datetime(window_end), color='blue',
                           linestyle='--')
            if window_end is not None and window_start is not None:
                ax.axvspan(pd.to_datetime(window_start),
                           pd.to_datetime(window_end),
                           alpha=0.5, color='yellow')

            row_index += 1

        # add legend - in a dedicated ax
        # move the legend outside
        # and reverse the order (most critical first)
        handles, labels = ax.get_legend_handles_labels()
        ax = fig.add_subplot(spec5[0, 0])
        ax.axis('off')
        plt.legend(handles[::-1], labels[::-1],
                   title='(Score) - Most anomalous machines',
                   bbox_to_anchor=(0, 0), loc="lower center")

        fig.tight_layout(pad=3.0)
        ax = fig.add_subplot(spec5[1, 0])
        fig.colorbar(cmap, cax=ax)
        plt.show()

    def plot_clusters(self,
                      on_machines,
                      labels,
                      smaller_most_critical=False,
                      on_metrics=None,
                      window_start=None,
                      window_end=None,
                      short=1, **pltargs):
        """Plot the selected machines, color indicates the cluster.

        on_machines: the list of host names that you want to plot.
        on_metrics: the list of signal/metrics names that you want to plot,
            use the names that you have chosen in the dictionary as
            signal_name.
            If not set all the plugin will be plt one after the other.
        on_machine: the list of hosts/machines names that you want to plot.
        smaller_most_critical: default = False. If true the smaller cluster
            have a dasker shade of red. While the bigger will be very
            light.
        labels: the labels defived by the cluster algorithm. One integer
            value for each different cluster.
        short: a multiplier from 0 to 1 to shrink the height of your plots.
        **pltargs: arguments that will be passed to the pandas plot function
            of the DataFrame object
        """
        # Cluster preprocessing
        # count how many sample in each cluster
        counter = collections.Counter(labels)
        labels_freq_big_first = counter.most_common()
        # list of tuples like -> ("label", frequency)
        print("Cluster partitioning: %s" % labels_freq_big_first)

        on_machines = list(on_machines)
        # print(on_machines)
        if on_metrics is None:
            on_metrics = list(self.snapshot.plugin.unique())
        for plugin_name in sorted(on_metrics):
            pdf_plugin = \
                self.snapshot[self.snapshot["plugin"] == plugin_name]
            pdf_swarm = pdf_plugin.pivot_table(columns="host",
                                               index="temporal_index",
                                               values="mean_value")
            if on_machines is not None:
                pdf_swarm = pdf_swarm[[h for h in pdf_swarm.columns
                                       if h in on_machines]]

            height = 10 * short
            fig, ax = plt.subplots(dpi=100,
                                   figsize=(self.img_width, height))
            ax.set_title(plugin_name, fontsize=20)

            NUM_COLORS = len(labels_freq_big_first)
            if smaller_most_critical:
                cm = plt.get_cmap('Reds')
            else:
                cm = plt.get_cmap('gist_rainbow')
            colors = [cm(1. * i / NUM_COLORS) for i in range(NUM_COLORS)]
            i = 0
            # get the list of cluster from the biggest to the smalles
            for (label, freq) in labels_freq_big_first:
                machines_in_cluster = [on_machines[x]
                                       for x in range(len(labels))
                                       if labels[x] == label]
                for host in machines_in_cluster:
                    # plot current timeseries
                    df_ts = pdf_swarm[host]
                    ax = df_ts.plot(ax=ax,
                                    c=colors[i],
                                    label='_nolegend_',
                                    **pltargs)
                i += 1
            plt.show()
        return ax

    def plot_matrices(self, window_dataset, hosts_present,
                      plugin_names, on_machines=None):
        """Plot the window on different metrics as a greyscale image.

        Returns the axes of the plot. Ordering of labels fof plugin names
        assumed to be alphabetical
        Parameters
        ----------
        window_dataset : ndarry,  shape (n_hosts, n_metrics * n_window_size)
            Windowed data of my object.
        hosts_present : list of hosts present, they are ordered as they appear
            in the window_dataset. Each row of window_dataset is a host of this
            list, in this specific order.
        plugin_names : list, plugin names in the order they appera in the
            matrix.
        on_machines: the list of host names that you want to plot.
        Returns
        -------
        ax : ax of the plot
        """
        if not isinstance(window_dataset, np.ndarray):
            raise Exception("Expecting dataset as numpy array")

        if on_machines is not None:
            mask = [True if x in on_machines else False for x in hosts_present]
            selected_machines = window_dataset[mask, :]
        else:
            selected_machines = window_dataset

        nr_metrics = len(plugin_names)

        axes = []
        # for all row of the window dataset create an image
        lines_indices = list(range(selected_machines.shape[0]))
        for i in lines_indices:
            image_matrix = window_dataset[i].reshape((nr_metrics, -1))
            fig, ax = plt.subplots()
            img = ax.imshow(image_matrix, cmap='Greys',
                            interpolation='nearest')
            ax.set_yticks(range(len(plugin_names)))
            ax.set_yticklabels(sorted(plugin_names))
            fig.set_size_inches(30, 5)
            fig.colorbar(img)
            plt.show()
            axes.append(ax)
        return axes
