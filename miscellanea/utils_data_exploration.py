"""Utility functions for Data Exploration.

AGE + NUMEROSITY + COLUMNS OF COLLECTED PLUGIN DATA

You have functions to explore your data and create DataFrames
to answer questions such as:
- how many hypervisors have a specific collectd plugin installed?
- when was that plugin installed in those hypervisors?
- which metrics are collected by a plugin? (i.e. columns exploration)

NB. All questions are restircted to the data belonging to the chosen
MACRO_HOSTGROUP (e.g. 'cloud_compute/level2/')

"""


from datetime import datetime

import json
import copy
import pandas as pd
import seaborn as sns  # noqa
import matplotlib  # noqa
import matplotlib.pyplot as plt
from pyspark.sql.utils import AnalysisException

from etl.spark_etl.cluster_utils import get_elements_in_dir
from etl.spark_etl.cluster_utils import MACRO_HOSTGROUP  # noqa
from etl.spark_etl.cluster_utils import scan_plugin_folder
from etl.spark_etl.cluster_utils import TIMESTAMP_TO_CONSIDER  # noqa


# DATA EXPLORATION - HOW OLD MY DATA ARE / HOW MANY MACHINES -----------------


def find_oldest_date(spark, plugin_name, full_check=False,
                     earliest_year_possible=2010):
    """Find oldest data collected by this plugin.

    Look inside the folder at the following path:
    /project/monitoring/collectd/<plugin_name>
    Open all parquet files and find the oldest data
    (aka the minimum timestamp).
    Full check = true: loads all the data at once and check
    the earliest date
    Full check = false: loads the data smartly. Starting
    from the oldest data month by month, and looking for
    machines in MACRO_HOSTGROUP, if no machine is there,
    a more recent month is checked and so on.
    earliest_year_possible is used only with full_check =
    false to avoid data that are too old.
    """
    global MACRO_HOSTGROUP  # pylint: disable=global-statement
    global TIMESTAMP_TO_CONSIDER  # pylint: disable=global-statement
    # columns = filtered_df.schema.names
    # print("This parquet files has the following schema \n %s" % columns)
    if full_check:
        # not recommended - folders_to_scan is not supported
        folders_to_scan = []
        in_path = "/project/monitoring/collectd/" + plugin_name + "/*/*/*"
        full_df = spark.read.parquet(in_path)
        # oldest date
        filtered_df = full_df.filter(
            (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))
        min_timestamp = filtered_df.agg({TIMESTAMP_TO_CONSIDER: "min"}) \
                                   .collect()[0][0]
    else:
        min_timestamp = None
        folders_to_scan = scan_plugin_folder(plugin_name)
        # remove folders that are too old
        folders_to_scan = [x for x in folders_to_scan
                           if int(x.split("/")[-2]) > earliest_year_possible]
        print("to scan: %s" % folders_to_scan)
        while (min_timestamp is None) and (len(folders_to_scan) > 0):
            # check in the oldest folder
            in_path = folders_to_scan[0]
            print("Checking: %s" % in_path)
            full_df = spark.read.parquet(in_path + "/*")
            # oldest date
            filtered_df = full_df.filter(
                (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))
            min_timestamp = filtered_df.agg({TIMESTAMP_TO_CONSIDER: "min"}) \
                                       .collect()[0][0]
            if (min_timestamp is None):
                # pop remove the checked
                folders_to_scan.pop(0)

    return (min_timestamp, folders_to_scan)


def find_nr_of_hostgroups(spark, plugin_name, year="2020", month="03"):
    """Find nr hostgroups that have this plugin installed.

    Look inside the folder at the following path:
    /project/monitoring/collectd/<plugin_name>
    Open all parquet files and find the nr of
    hostgroups that are saved in those data.
    """
    global MACRO_HOSTGROUP  # pylint: disable=global-statement
    in_path = ("/project/monitoring/collectd/" + plugin_name +
               "/" + year + "/" + month + "/*")
    try:
        full_df = spark.read.parquet(in_path)
    except AnalysisException:
        return 0, []
    filtered_df = full_df.filter(
        (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))

    # nr hostgroup
    projected_df = filtered_df.select('submitter_hostgroup').distinct()
    hostgroups_presents = projected_df.collect()
    hostgroups_presents = [x[0] for x in hostgroups_presents]
    nr_hostgroups = len(hostgroups_presents)
    return nr_hostgroups, hostgroups_presents


def analyze_all_plugins(spark, swan_outfolder):
    """Summarize plugins characteristics in json files.

    Loop over all the plugins in the collectd folder
    and for each compute (always referring only to
    machines of cloud compute level 2):
    - oldest timestamp
    - list of months
    - nr of hostrgroups
    - list of hostgroups
    """
    all_folders = get_elements_in_dir("/project/monitoring/collectd",
                                      return_absolute_path=False)
    plugin_list = [x for x in all_folders if (x[0] != ".")]
    # print(plugin_list)
    # return
    # all_folders
    for plugin in plugin_list[19:]:
        print("----------------------------")
        print("JOBS FOR %s" % plugin)
        # check oldest date
        min_timestamp, folders_after = find_oldest_date(spark, plugin)
        time_data = dict()
        time_data['oldest_timestamp'] = min_timestamp
        time_data['folders_availables'] = folders_after
        if min_timestamp is None:
            time_data['str_oldest_timestamp'] = "None"
        else:
            ts_in_ms = min_timestamp / 1000
            dt_object = datetime.fromtimestamp(ts_in_ms)
            time_data['str_oldest_timestamp'] = str(dt_object)
        # save findings
        with open(swan_outfolder + "/" +
                  plugin + '_months.json', 'w') as outfile:
            json.dump(time_data, outfile)

        # check nr hostgroups
        nr_hostgroups, hostgroups_presents = \
            find_nr_of_hostgroups(spark, plugin)
        hostgroup_data = dict()
        hostgroup_data['nr_hostgroups'] = nr_hostgroups
        hostgroup_data['hostgroups_presents'] = hostgroups_presents
        # save findings
        with open(swan_outfolder + "/" +
                  plugin + '_hostgroups.json', 'w') as outfile:
            json.dump(hostgroup_data, outfile)


def costruct_pdf_statisitc_table(swan_outfolder="statistics-about-plugins"):
    """Create a pandas dataframe to visualize the json just created.

    ASSUMPTION: you have executed analyze_all_plugins() before
    Given a folder containing json results from the analysis
    it creates a joint table.
    """
    all_folders = get_elements_in_dir("/project/monitoring/collectd",
                                      return_absolute_path=False)
    plugin_list = [x for x in all_folders if x[0] != "."]
    df = pd.DataFrame()
    # print(df.head())
    for plugin in plugin_list:
        print("----------------------------")
        print("READING %s" % plugin)
        try:
            with open(swan_outfolder + "/" +
                      plugin + '_months.json') as json_file:
                data_time = json.load(json_file)
                # print(data)
            with open(swan_outfolder + "/" +
                      plugin + '_hostgroups.json') as json_file:
                data_nr = json.load(json_file)
                # print(data)
            if (data_time["oldest_timestamp"] is not None):
                just_names = [x.split("/")[-2:]
                              for x in data_nr['hostgroups_presents']]
                df_row = pd.DataFrame({
                    "name": [plugin],
                    "timestamp": [data_time['oldest_timestamp']],
                    "date": [data_time['str_oldest_timestamp']],
                    "nr_hostgroups_in_March_month": [data_nr['nr_hostgroups']],
                    "list_hostgroups_batch": [sorted([x for x in just_names
                                                      if x[0] == "batch"])],
                    "list_hostgroups_main": [sorted([x for x in just_names
                                                     if x[0] == "main"])],
                    "list_hostgroups_other": [sorted([x for x in just_names
                                                      if (not (x[0] == "main") and  # noqa : E501
                                                          not (x[0] == "batch"))])]  # noqa : E501
                })
                df = df.append(df_row)
                print("OK")
            else:
                print("Empty files")
        except FileNotFoundError:
            print("File not found")
    return df


# DATA EXPLORATION - WHICH METRICS / COLUMNS ---------------------------------


def get_metrics_of_plugin(spark, plugin_name="cpu", year="2020", month="03"):
    """Create a json file with the attributes and type_instance of the plugin.

    Get the metrics/attributes that are collected by a
    single collectd plugin. Usually type instance is a good
    indicator for that.
    It returns the list of the metrics.
    """
    global MACRO_HOSTGROUP  # pylint: disable=global-statement
    result_dict = {}
    # naming conventions discussion: python -> snake_case
    # https://stackoverflow.com/questions/5543490/json-naming-convention
    result_dict["metrics_count"] = 0
    result_dict["metrics_names"] = []
    result_dict["columns"] = []

    # read and filter
    in_path = ("/project/monitoring/collectd/" + plugin_name +
               "/" + year + "/" + month + "/*")
    try:
        full_df = spark.read.parquet(in_path)
        result_dict["columns"] = full_df.schema.names
    except AnalysisException:
        print("Data found - plugin %s doesn't have the data" /
              " for this period: %s/%s" % (plugin_name, year, month))
        return result_dict
    filtered_df = full_df.filter(
        (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))

    # collect metrics
    projected_df = filtered_df.select('type_instance').distinct()
    metrics_presents = projected_df.collect()
    result_dict["metrics_names"] = [x[0] for x in metrics_presents]
    result_dict["metrics_count"] = len(metrics_presents)
    return result_dict


def collect_metrics_all_plugins(spark,
                                swan_outfolder="statistics-about-plugins",
                                out_file_name="all_metrics",
                                override=False, year="2020", month="03"):
    """Collect the metrics for every plugin.

    It creates a file json containing the metrics of all the plugins.
    Practically, it loops over all the plugins in the collectd folder
    and for each we get the metrics (always referring only to machines
    of cloud compute level 2).
    NB: we look only at the specified year and month data to speed up
    the process. It is assumed that older data contains the same info.
    If override = False it appens all the result in a file.
    """
    all_folders = get_elements_in_dir("/project/monitoring/collectd",
                                      return_absolute_path=False)
    plugin_list = [x for x in all_folders if (x[0] != ".")]
    print(plugin_list)
    if override:
        with open(swan_outfolder + "/" + out_file_name + ".json", 'w') as f:
            json.dump({}, f)
    # return all_folders
    for plugin_name in plugin_list[:]:
        print("----------------------------")
        print("JOBS FOR %s" % plugin_name)
        plugin_dict = {}
        plugin_dict[plugin_name] = get_metrics_of_plugin(spark, plugin_name,
                                                         year, month)
        # check if previous file is there: read it
        try:
            with open(swan_outfolder + "/" + out_file_name + ".json") as f:
                previous_data = json.load(f)
        # otherwise: create an empty dictionary and dump that afterwards
        except FileNotFoundError:
            previous_data = {}
        previous_data.update(plugin_dict)
        new_data = previous_data
        with open(swan_outfolder + "/" + out_file_name + ".json", 'w') as f:
            json.dump(new_data, f)


def inspect_collected_metrics(swan_folder="statistics-about-plugins",
                              file_name="all_metrics"):
    """Visualize the collected metrics all together.

    ASSUMPTION: you have executed collect_metrics_all_plugins() before
    Given a folder containing json results from the analysis
    it prints the json content of the analysis
    """
    with open(swan_folder + "/" + file_name + ".json") as f:
        data = json.load(f)
    print(json.dumps(data, indent=4, sort_keys=True))


# PLOTTING -------------------------------------------------------------------


def plot_each_plugin(local_file_path, **pltargs):
    """Plot the timeseries in the csv, one plot per plugin.

    The plotting function is the one of Pandas, pass kwy argument that can be
    interpreted by its api DataFrame.plot().
    """
    pdf_all_data = pd.read_csv(local_file_path)
    for plugin_name in list(pdf_all_data.plugin.unique()):
        pdf_plugin = pdf_all_data[pdf_all_data["plugin"] == plugin_name]
        pdf_swarm = pdf_plugin.pivot(columns="host",
                                     index="temporal_index",
                                     values="mean_value")
        ax = pdf_swarm.plot(figsize=(22, 10), **pltargs)
        ax.set_title(plugin_name)
        plt.show()


def plot_window(X_window, pandas_normalization, on_metrics=None, **pltargs):
    """Plot the window of data with their real scale.

    Params
    ------
    X_window: Pandas DataFrame
        Contains the data from the same timestamp, each record is representing
        the full window history of a single host.
        You need to have headers with the plugin name. It is assumed thwy have
        the format metric_name_h[0-9]* and on the left it starts with recent
        history timesteps:
        (e.g. metric_name_h0, metric_name_h1, ..., metric_name_h48)
    pandas_normalization: Pandas DataFrame
        Containing the normalization coefficients (mean and stddev)
        for this hosts to plot the values at their ofiginal scale and variance
    on_metrics: list of str
        list of metrics as they are in the header. (e.g. metric_name)
    hostgroup: str
        full paths for the hostrgoroup this data belong to. It is needed to
        retireve the right normalization coefficients form the pandas
        normalization DataFrame.
    outlier_scores: list of float/int
        these are the outier scores of the hosts in this window, they must have
        a correspondence with the record in the X_window.

    Return
    ------
    Plot the various window
    """
    pass


def apply_reverse_transformation_for_loop(one_window, norm_df,
                                          add_mean=True, multiply_scale=True):
    """Reconstruct original window from the normalization coeffs."""
    denormalized_window = copy.deepcopy(one_window)
    norm_df = norm_df.drop_duplicates()
    for c in one_window.columns:
        name_plugin = c[:c.rfind("_h")]
        # print(name_plugin)
        norm_coeff_this_plg = norm_df[norm_df["plugin"] == name_plugin]
        if norm_coeff_this_plg.shape[0] > 1:
            # if multiple different entries get the first
            norm_coeff_this_plg = norm_coeff_this_plg.head(1)
        mean = float(norm_coeff_this_plg["mean"])
        stddev = float(norm_coeff_this_plg["stddev"])
        # print(mean)
        # print(stddev)
        if multiply_scale and add_mean:
            denormalized_window[c] = (one_window[c] * stddev) + mean
        else:
            if multiply_scale:
                denormalized_window[c] = (one_window[c] * stddev)
            elif add_mean:
                denormalized_window[c] = one_window[c] + mean
    return denormalized_window


def plot_plugin_data(plugin_key, ax,
                     X_window, prov_window,
                     ordered_scores, ordered_hosts,
                     aggregate_min=10,
                     nr_machines_in_legend=5,
                     cmap=None):
    """Plot the window of data for a specific plugin.

    Params
    ------
    plugin_key: str
        Exact serialized plugin name (with underscore) as it appears in the
        dictionary of selected plugins
    ax: matplotlib ax
        to plot your time series
    X_window: Pandas DataFrame
        Contains the data from the same timestamp, each record is representing
        the full window history of a single host.
        You need to have headers with the plugin name. It is assumed thwy have
        the format metric_name_h[0-9]* and on the left it starts with recent
        history timesteps:
        (e.g. metric_name_h0, metric_name_h1, ..., metric_name_h48)
    ordered_scores: list(float)
        ordered oulier scores from the highest to the lowest
    ordered_hosts: list (str)
        hostnames corresponding to the ordered_scores
    aggregate_min: int
        every how many minutes your data are aggregated
    nr_machines_in_legend: int
        how many machines you want in the legend
    Return
    ------
    Plot the plugin data in the ax provided
    """
    # get current timestamp
    ts = int(prov_window["timestamp"].iloc[0])
    # get data for this plugin only
    plugin_columns = [name for name in X_window.columns
                      if plugin_key in name]
    plugin_selected = X_window[plugin_columns]
    host_as_col = \
        plugin_selected.reset_index(drop=True).T.reset_index(drop=True)
    # create the timestamps for the history steps
    history_of_ts = \
        [ts - (60 * aggregate_min * i) for i in range(len(host_as_col))]
    host_as_col["ts"] = \
        pd.to_datetime(history_of_ts, unit='s')
    host_as_col["ts"] = \
        host_as_col["ts"].dt.tz_localize('UTC')\
        .dt.tz_convert('Europe/Zurich')
    print("After: ", host_as_col["ts"].iloc[0])
    host_as_col.set_index("ts", inplace=True)
    # rename the column after the pivot
    host_as_col.columns = prov_window.iloc[:, 0].values
    # prepare the colorrange for the plotting
    if cmap is None:
        norm = matplotlib.colors.Normalize(vmin=min(ordered_scores),
                                           vmax=max(ordered_scores))
        cmap = matplotlib.cm.ScalarMappable(norm=norm,
                                            cmap=matplotlib.cm.Reds)
        cmap.set_array([])

    i = 0
    entities_with_this_plugins = host_as_col.columns
    # plot the less anomalous first
    # print("nr of host unique: ", len(set(ordered_hosts)))
    # print("nr of host all: ", len((ordered_hosts)))
    for score, host in zip(ordered_scores[::-1], ordered_hosts[::-1]):
        # print(".", host)
        # break
        # skip machines that o not have this plugin
        if host not in entities_with_this_plugins:
            print("Host ", host,
                  " doesn't have this plugin:", plugin_key,
                  " so no plot for it")
            continue
        # plot current timeseries
        df_ts = host_as_col[host]
        # print legend only for a subset
        if (len(ordered_hosts) - i) <= nr_machines_in_legend:
            lebel_plus_score = ("(%.2f) - %s" % (score, host))
            ax = df_ts.plot(c=cmap.to_rgba(score),
                            ax=ax,
                            label=lebel_plus_score)
        else:
            ax = df_ts.plot(c=cmap.to_rgba(score),
                            ax=ax,
                            label='_nolegend_')
        i += 1
    # date_form = DateFormatter("%Y-%m-%d %H:%M")
    # ax.xaxis.set_major_formatter(date_form)
    ax.set_title(plugin_key)
    # plt.xticks(rotation=90)
    return ax


def plot_outliers(X_window,
                  prov_window,
                  ordered_scores,
                  ordered_hosts,
                  on_metrics=None,
                  nr_machines_in_legend=5,
                  color_outlier="red",
                  short=1, **pltargs):
    """Plot the selected machines, color indicates anomaly score.

    ordered_scores: the anomaly scores (HIGHER is MORE CRITICAL/RED). It must
        have the same order of the machines. The shoul be in decreasing order.
    short: a multiplier from 0 to 1 to shrink the height of your plots.
    color_outlier: ("red") color of your timeseries: "green", "blue",
        "orange", "purple".
    **pltargs: arguments that will be passed to the pandas plot function
        of the DataFrame object

    return: list of machines from the most critical to the lowest and
        list of their scores.
    """
    dict_cmap = {}
    dict_cmap["red"] = matplotlib.cm.Reds
    dict_cmap["blue"] = matplotlib.cm.Blues
    dict_cmap["green"] = matplotlib.cm.Greens
    dict_cmap["orange"] = matplotlib.cm.Oranges
    dict_cmap["purple"] = matplotlib.cm.Purples
    dict_cmap["diverging"] = matplotlib.cm.cool

    # Outlier preprocessing
    # reorder the anomalies:
    # plot first normal data (go to background)
    # plot outlier at the end (on top of the others)
    print("Candidates outliers:")
    print(ordered_hosts[:nr_machines_in_legend])

    if on_metrics is None:
        on_metrics = \
            list(set([c[:c.rfind("_h")] for c in X_window.columns]))

    # create plots spaces
    n_rows = len(sorted(on_metrics)) + 1
    height = n_rows * 10 * short

    fig = plt.figure(figsize=(18, height))
    widths = [1, 20]
    spec5 = fig.add_gridspec(ncols=2, nrows=n_rows, width_ratios=widths)

    # plot score distribution
    ax = fig.add_subplot(spec5[0, 1])
    ax.set_title("Distribution of the anomaly scores", fontsize=20)
    sns.distplot(ordered_scores, ax=ax,
                 color="grey",
                 kde=False, bins=40)
    row_index = 1

    norm = matplotlib.colors.Normalize(vmin=min(ordered_scores),
                                       vmax=max(ordered_scores))
    cmap = matplotlib.cm.ScalarMappable(norm=norm,
                                        cmap=dict_cmap[color_outlier])
    cmap.set_array([])

    for plugin_name in sorted(on_metrics):
        ax = fig.add_subplot(spec5[row_index, 1])
        ax.set_title(plugin_name, fontsize=20)
        print("Plugin:", plugin_name)
        plot_plugin_data(plugin_key=plugin_name,
                         ax=ax,
                         X_window=X_window,
                         prov_window=prov_window,
                         ordered_scores=ordered_scores,
                         ordered_hosts=ordered_hosts,
                         nr_machines_in_legend=nr_machines_in_legend,
                         cmap=cmap)
        row_index += 1

    # add legend - in a dedicated ax
    # move the legend outside
    # and reverse the order (most critical first)
    handles, labels = ax.get_legend_handles_labels()
    ax = fig.add_subplot(spec5[0, 0])
    ax.axis('off')
    plt.legend(handles[::-1], labels[::-1],
               title='(Score) - Most anomalous machines',
               bbox_to_anchor=(0, 0), loc="lower center")

    fig.tight_layout(pad=3.0)
    ax = fig.add_subplot(spec5[1, 0])
    fig.colorbar(cmap, cax=ax)
    plt.show()
