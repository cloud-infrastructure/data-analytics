"""
Class with utilities to compare different methods predictions.

Comparison among themselves (Kendall's tau) or wih the ground truth (ROC).
"""

import scipy as scy
import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import RobustScaler


def heatmap_accordance_kendall(rankings, labels):
    """Compute the heatmap comparing all pairs of rankings of detectors."""
    nr_detectors = len(rankings)
    matrix_coeff = np.zeros((nr_detectors, nr_detectors))
    matrix_p_values = np.zeros((nr_detectors, nr_detectors))
    for i, rank_row in enumerate(rankings):
        for j, rank_col in enumerate(rankings):
            matrix_coeff[i][j], matrix_p_values[i][j] = \
                scy.stats.kendalltau(x=rank_row, y=rank_col)
    fig, axes = plt.subplots(nrows=1, ncols=2)
    fig.set_size_inches(18.5, 4)
    axes[0].set_title("Kendall tau coefficient", fontsize=20)
    sns.heatmap(data=pd.DataFrame(matrix_coeff,
                                  index=labels, columns=labels),
                annot=True, ax=axes[0], cmap="gray")
    axes[1].set_title("P-value", fontsize=20)
    sns.heatmap(data=pd.DataFrame(matrix_p_values,
                                  index=labels, columns=labels),
                annot=True, ax=axes[1], cmap="gray")
    plt.plot()


def plot_correlation_outlier_scores(raw_outlier_scores, labels,
                                    ground_truth=None):
    """Compute the coorelation between outlier scores.

    Params
    ------
    raw_outlier_scores: list of outlier scores lists [aka list of floats]
        list containing list of raw outlier scores, every sublist represents
        a different methods
    labels: list of str
        name of the methods represented by the list of outliers. "labels" as
        the labels of a plot.
    ground_truth: list of int
        list representing the if the samples are anomalies (value = 1) or
        noraml data (value = 0)
    Return
    ------
    Plot the matrix of correspondence of all combinations of methods
    """
    nr_methods = len(labels)
    # scale all the raw scores
    scaled_os = [list(RobustScaler()
                      .fit_transform(np.array(list_of_scores).reshape(-1, 1)))
                 for list_of_scores in raw_outlier_scores]

    fig, axes = plt.subplots(nrows=nr_methods, ncols=nr_methods,
                             sharex=False, sharey=False)
    fig.set_size_inches((25, 25))
    plt.tight_layout(pad=3)
    for row, (scores_row, name_row) \
            in enumerate(zip(scaled_os, labels)):
        for col, (scores_col, name_col) \
                in enumerate(zip(scaled_os, labels)):
            ax = axes[row][col]
            ax.set_xlabel(name_row)
            ax.set_ylabel(name_col)
            sns.scatterplot(x=scores_row, y=scores_col,
                            hue=ground_truth,
                            ax=ax)
    pass


def plot_roc(fpr, tpr, roc_auc, ax, **pltargs):
    """Plot the ROC curve.

    Params
    ------
    frp: float
        FALSE positive rate
    trp: float
        TRUE positive rate
    roc_auc: float
        Area Under The Curve of ROC (Receiving Operating Curve)
    ax: matplotlib ax
        where to plot the diagram
    """
    lw = 2
    ax.plot(fpr, tpr, color='darkorange',
            lw=lw, **pltargs)
    ax.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.05])
    ax.set_xlabel('FPR')
    ax.set_ylabel('TPR')
    ax.set_title('ROC curve (area = %0.2f)' % roc_auc)
