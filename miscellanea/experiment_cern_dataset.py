"""
Download annotations, create CERN dataset, perform experiments on it.

Annotations are downloaded via Grafana (we need the secret in the folder:
data_analytics/grafana_etl/secrets/grafana_token.txt and explicitely state it
as argument to the function query_for_annotations as
file_path_token = "../grafana_etl/secrets/grafana_token.txt").
"""

import requests
import time
import re
from datetime import datetime
from datetime import timedelta
import pandas as pd
import numpy as np

import os

import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt

from collections import Counter
from collections import OrderedDict
from etl.spark_etl.utils import read_local_window_dataset


BASE_URL = "https://monit-grafana.cern.ch/api/annotations"


def create_url_to_get(
        base_url,
        time_from=None,
        time_to=None,
        alert_id=None,
        dashboard_id=None,
        panel_id=None,
        tags=None,
        limit=None):
    """Create the url to query the Grafana Annoation API endpoint.

    Return
    ------
    list_annotations_path: (str)
        e.g. "?tags=tag1&limit=100"
    """
    list_annotations_path = base_url
    params = []

    if time_from:
        params.append("time_from=%s" % time_from)
    if time_to:
        params.append("time_to=%s" % time_to)
    if alert_id:
        params.append("alertId=%s" % alert_id)
    if dashboard_id:
        params.append("dashboardID=%s" % dashboard_id)
    if panel_id:
        params.append("panelId=%s" % panel_id)
    if isinstance(tags, list):
        for t in tags:
            params.append("tags=%s" % t)
    else:
        if tags:
            params.append("tags=%s" % tags)

    if limit:
        params.append("limit=%s" % limit)

    list_annotations_path += "?"
    list_annotations_path += "&".join(params)
    return list_annotations_path


def query_for_annotations(hostgroups,
                          file_path_token="/opt/repositories/notebook_private/grafana_token.txt"):  # noqa
    """Query the enpoint and get the annotations in json/dictionary format.

    Params
    ------
    hostgroups: list(str)
        list of hostgroups to get
        e.g. = ['cloud_compute/level2/main/gva_shared_012',
                'cloud_compute/level2/batch/gva_project_014']
    Return
    ------
    jres: list of dict
        every anotation is a python dictionary
    """
    start = time.time()
    token = open(file_path_token, 'r').readline()
    headers = {'Authorization': 'Bearer %s' % token,
               'Content-Type': 'application/json'}
    hostgroups_enriched = \
        ["hostgroup:" + h for h in hostgroups]

    print(hostgroups_enriched)
    new_url = create_url_to_get(BASE_URL,
                                tags=hostgroups_enriched)
    # e.g. new_url =
    # https://monit-grafana.cern.ch/api/annotations?tags=tag1&limit=100
    new_url += "&limit=1000000000"
    print(new_url)
    res = requests.get(new_url, headers=headers)
    end = time.time()
    duration = end - start
    jres = res.json()
    print("Query Time: %.3f sec" % duration)
    print("Nr Annotations found: ", len(jres))
    return jres


def convert_to_dataframe(json_annotations):
    """Convert json representation to a dataframe.

    Params
    ------
    jres: list of dict
        every anotation is a python dictionary
    Return
    ------
    df_annotations: Pandas DataFrame
        columns:
        hostname | hostgroup | ts_start_milli | ts_end_milli (int) | ..
        is_anomalous (int 0/1) | author (str cern email) | description | ..
        dt_start (datetime) | dt_end (datetime) |
    """
    hg_reg = re.compile('^hostgroup:cloud_compute/level2')
    host_reg = re.compile('^host:[a-z][0-9]*[a-z][0-9]*$')
    # start | end | 0=normal, 1=anomaly
    annotations = json_annotations
    my_dataset = []
    for ann in annotations:
        record = {}
        tags = ann["tags"]
        hostgroups = [s for s in tags if hg_reg.match(s)]
        hostnames = [s for s in tags if host_reg.match(s)]
        if len(hostnames) == 0:
            continue
        record["hostname"] = hostnames[0]
        record["hostgroup"] = hostgroups[0]
        # remove prefix
        record["hostname"] = remove_prefix(record["hostname"], "host:")
        record["hostgroup"] = remove_prefix(record["hostgroup"], "hostgroup:")
        record["ts_start_milli"] = ann["time"]
        record["ts_end_milli"] = ann["timeEnd"]
        record["is_anomalous"] = \
            int("anomaly_test" in tags or "anomaly" in tags)
        record["author"] = ann["email"]
        record["description"] = ann["text"]
        record["dt_start"] = \
            datetime.fromtimestamp(record["ts_start_milli"] / 1000.0)
        record["dt_end"] = \
            datetime.fromtimestamp(record["ts_end_milli"] / 1000.0)
        my_dataset.append(record)
    df_annotation = pd.DataFrame(my_dataset)
    # convert to int
    df_annotation["ts_start_milli"] = \
        df_annotation["ts_start_milli"].astype(int)
    df_annotation["ts_end_milli"] = \
        df_annotation["ts_end_milli"].astype(int)
    return df_annotation


def discretize_start_end(df_annotations, window_length_in_min):
    """Discretize the start and end datetime based on the window length.

    Note that the dt_start and dt_end will be overritten
    e.g. window length = 8 h
    dt_start = 2020-06-19 05:16:14.967 -> 2020-06-19 08:00:00
    dt_end = 2020-08-16 17:32:11.162 -> 2020-08-16 16:00:00
    Params
    ------
    df_annotations: Pandas DataFrame
        columns:
        hostname | hostgroup | ts_start_milli | ts_end_milli (int) | ..
        is_anomalous (int 0/1) | author (str cern email) | description | ..
        dt_start (datetime) | dt_end (datetime) |
    window_length_in_min: int
        minutes of length of a window
    Return
    ------
    df_discretized : Pandas DataFrame
        columns:
        hostname | hostgroup | ts_start_milli | ts_end_milli (int) | ..
        is_anomalous (int 0/1) | author (str cern email) | description | ..
        dt_start (datetime) | dt_end (datetime) |
    """
    df = df_annotations.copy()
    # round the intervals to sarting date
    df.loc[:, "dt_start"] = \
        df["dt_start"].apply(
            lambda dt: get_closest_window_boundary_following(
                dt, nr_min_in_a_window=window_length_in_min))
    df.loc[:, "dt_end"] = \
        df["dt_end"].apply(
            lambda dt: get_closest_window_boundary_preceeding(
                dt, nr_min_in_a_window=window_length_in_min))
    df_discretized = df
    return df_discretized


def enrich_with_duration(df_annotations):
    """Add extra fields for duration in seconds, minutes, hours.

    Params
    ------
    df_annotation: Pandas DataFrame
        required columns:
    Return
    ------
    df_annotation: Pandas DataFrame
        new columns: duration_sec | duration_min | duration_hours
    """
    df_annotations = df_annotations.copy()
    df_annotations["duration_sec"] = \
        ((df_annotations["ts_end_milli"] - df_annotations["ts_start_milli"]) / (1000)).astype(int)  # noqa
    df_annotations["duration_min"] = \
        (df_annotations["duration_sec"] / 60).astype(int)
    df_annotations["duration_hours"] = \
        (df_annotations["duration_sec"] / 3600).astype(int)
    return df_annotations


def get_closest_window_boundary_preceeding(dt, nr_min_in_a_window):
    """Given a datetime find the closest preceeding boundary.

    Note that the returned datetime will always be smaller.
    """
    min_input = dt.hour * 60 + dt.minute
    tot_min_output = min_input - (min_input % nr_min_in_a_window)
    hour_output = int(tot_min_output / 60)
    min_output = tot_min_output % 60
    return datetime(year=dt.year, month=dt.month, day=dt.day,
                    hour=hour_output, minute=min_output)


def get_closest_window_boundary_following(dt, nr_min_in_a_window):
    """Given a datetime find the closest following boundary.

    Note that the returned datetime will always be bigger.
    """
    min_input = dt.hour * 60 + dt.minute
    # if the annotation is aleady precise on the boundary, do not move it.
    if min_input // nr_min_in_a_window == 0:
        offset = 0  # do ot move precise annotations
    else:
        offset = nr_min_in_a_window
    tot_min_output = min_input + offset - (min_input % nr_min_in_a_window)
    hour_output = int(tot_min_output / 60)
    # add a day if you reach the midnight of the next day
    offset_day = timedelta(hours=0)  # by default
    if hour_output > 23:
        hour_output = 0
        offset_day = timedelta(hours=24)
    min_output = tot_min_output % 60
    dt_out = datetime(year=dt.year, month=dt.month, day=dt.day,
                      hour=hour_output, minute=min_output) + offset_day
    # print("(in) %s -> %s (out)" % (dt, dt_out))
    return dt_out


def remove_prefix(string, prefix):
    """Remove the prefix from the stirng."""
    return string[len(prefix):]


def remove_suffix(string, suffix):
    """Remove the suffix from the stirng."""
    start_of_suffix = string.find(suffix)
    return string[:start_of_suffix]


def create_window_labels(df_discretized_annotations, nr_min_in_a_window):
    """Create a database of windows for every window.

    Params
    ------
    nr_min_in_a_window: int
        minutes of length of a window
    df_discretized_annotations : Pandas DataFrame
        columns:
        hostname | hostgroup | ts_start_milli | ts_end_milli (int) | ..
        is_anomalous (int 0/1) | author (str cern email) | description | ..
        dt_start (datetime) | dt_end (datetime) |
    """
    df = df_discretized_annotations.copy()
    # get list of hosts
    hosts = df["hostname"].unique()
    # create an empty list for every host
    labels_dict = {}
    for host in hosts:
        labels_dict[host] = []
    # get the global min and the closest next window start
    global_start_dt = df["dt_start"].min()
    global_end_dt = df["dt_end"].max()

    # iterate over all the intervals
    # from the min to the max
    window_span = timedelta(minutes=nr_min_in_a_window)
    current_start = global_start_dt
    current_end = global_start_dt + window_span
    print("global_start_dt: ", global_start_dt)
    print("global_end_dt: ", global_end_dt)

    window_infos = []
    counter = 0
    while current_end < global_end_dt:
        counter += 1
        if counter == 100000000000:
            break
        # iterate over all the line/hosts
        # if the annotation contains the current window
        df_subset_to_check = \
            df[(df["dt_start"] <= current_start) & (df["dt_end"] >= current_end)]  # noqa
        window_infos.append(current_end)

        host_not_updated = list(hosts)
        for i in range(len(df_subset_to_check)):
            current_row = df_subset_to_check.iloc[i]
            hostname = current_row["hostname"]
            anomaly = current_row["is_anomalous"]
            # if anomaly == 1
            # append 1 to the list of that host
            # if anomaly == 0
            # append 0 to the list of that host
            if hostname in host_not_updated:
                labels_dict[hostname].append(anomaly)
                # remove hosts for which I added something
                host_not_updated.remove(hostname)

        for host in host_not_updated:
            labels_dict[host].append(np.nan)
        # go to next window
        current_start = current_end
        current_end = current_end + window_span

    # create the matrix
    rows = []
    for key_hostname in labels_dict.keys():
        rows.append(labels_dict[key_hostname])

    matrix_dataset = np.vstack(rows)
    df_labels = pd.DataFrame(index=window_infos,
                             columns=labels_dict.keys(),
                             data=matrix_dataset.T,
                             dtype='float')
    return df_labels


def count_per_interval(df_raw_annotations, nr_min_in_a_window=48 * 10):
    """Count for every host how many anomalous/noraml annotations per interval.

    Params
    ------
    nr_min_in_a_window: int
        minutes of length of a window
    df_raw_annotations : Pandas DataFrame
        columns:
        hostname | hostgroup | ts_start_milli | ts_end_milli (int) | ..
        is_anomalous (int 0/1) | author (str cern email) | description | ..
        dt_start (datetime) | dt_end (datetime) |
        Note that real (not discretized) intervals are present here

    Return
    ------
    matrix_count : numpy matrix
        each row is a host, each column is a discrete window, and the values
        in the matrix represent how many anomalous/normal annotations
        were present in that interval.
    """
    df = df_raw_annotations.copy()
    df["dt_start"] = pd.to_datetime(df["dt_start"])
    df["dt_end"] = pd.to_datetime(df["dt_end"])
    # get list of hosts
    hosts = df["hostname"].unique()
    # create an empty list for every host
    labels_dict = {}
    for host in hosts:
        labels_dict[host] = []
    # get the global min and the closest next window start
    global_start_dt =  \
        get_closest_window_boundary_preceeding(
            df["dt_start"].min(),
            nr_min_in_a_window=nr_min_in_a_window)
    global_end_dt = \
        get_closest_window_boundary_following(
            df["dt_end"].max(),
            nr_min_in_a_window=nr_min_in_a_window)

    # iterate over all the intervals
    # from the min to the max
    window_span = timedelta(minutes=nr_min_in_a_window)
    current_start = global_start_dt
    current_end = global_start_dt + window_span
    print("global_start_dt: ", global_start_dt)
    print("global_end_dt: ", global_end_dt)

    window_infos = []
    while current_end < global_end_dt:

        # iterate over all the line/hosts
        # if the annotation contains the current window

        # the annotation can be:
        # 1. entirely in this window
        # 2. starts in this window
        # 3. ends in this winodow
        # 4. bigger than the window and including the window
        df_subset_to_check = \
            df[((df["dt_start"] >= current_start) &
                (df["dt_end"] <= current_end)) |
               ((df["dt_start"] >= current_start) &
                (df["dt_start"] <= current_end)) |
               ((df["dt_end"] >= current_start) &
                (df["dt_end"] <= current_end)) |
               ((df["dt_start"] < current_start) &
                (df["dt_end"] > current_end))]

        # check if there are annotations that are normal or anomalous
        df_presents = \
            df_subset_to_check[["hostname", "is_anomalous"]].drop_duplicates()

        # check for every host which is its situation in this case
        for hostname in hosts:
            # get annotations only for this host
            df_presents_this_host = \
                df_presents[df_presents["hostname"] == hostname]
            nr_different_annotations = len(df_presents_this_host)
            # an interval can have either normal annotations (is anomalous = 0)
            # or abnormal annotations (is_anomalous = 1) in this
            # df_presents_this_host we do not have duplicates so we can only
            # have at aximum one row for an anomaly and one row for a normal
            # they just tell us about the presence in this window of
            # normal or anomalous annotated intervals form the exper
            # note that empty dataframe means no annotations at all
            if nr_different_annotations == 0:
                labels_dict[hostname].append(np.NaN)
            elif nr_different_annotations == 2:
                # if 2 annotatins we have for sure an anomaly
                labels_dict[hostname].append(2)
            elif (nr_different_annotations == 1 and
                  list(df_presents_this_host["is_anomalous"] == 1)[0]):
                # this conditions check if this single row is an anomaly (== 1)
                labels_dict[hostname].append(1)
            elif (nr_different_annotations == 1 and
                  list(df_presents_this_host["is_anomalous"] == 0)[0]):
                # this conditions check if this single row is a
                # normal data (== 0)
                labels_dict[hostname].append(0)
        # save info about provenance
        window_infos.append(current_end)
        # go to next window
        current_start = current_end
        current_end = current_end + window_span

    # create the matrix
    rows = []
    for hostname in hosts:
        rows.append(labels_dict[hostname])

    matrix_dataset = np.vstack(rows)
    df_labels = pd.DataFrame(index=window_infos,
                             columns=labels_dict.keys(),
                             data=matrix_dataset.T,
                             dtype='float')
    return df_labels


def change_time_for_scores(df_scores,
                           start_change="2020-04-04 22:00:00",
                           end_change="2020-11-01 00:00:00"):
    """Move the scores to the time of label data.

    In practice add one hour in the interval after the start and before the
    end, so that everything is confrming to the december time
    (called legal time (? not sure).
    Note that you need to use that format and it acts on the index only
    (if not datetime it will be converted).

    Params
    ------
    df_scores: pandas Dataframe
        index = strings of time (we will call datetime index on it)
    Return
    ------
    df_scores_legal_time: pandas Dataframe
        index = datatime index (with correct legal time for all records)
    """
    df_scores_legal_time = df_scores.copy()
    # convert to datetime index
    df_scores.index = pd.to_datetime(df_scores.index)
    # add one hour to all data after this data
    new_index = []
    for i in df_scores.index:
        dt_change_start = \
            datetime.strptime(start_change, '%Y-%m-%d %H:%M:%S')
        dt_change_end = \
            datetime.strptime(end_change, '%Y-%m-%d %H:%M:%S')
        if i > dt_change_start and i < dt_change_end:
            new_i = i + timedelta(hours=1)
            new_index.append(new_i)
        else:
            new_index.append(i)
    df_scores_legal_time.index = new_index
    return df_scores_legal_time


# VISUALIZATION - PLOTTING


def visualize_heatmap_annotations(
        df_labels,
        figsize=(
            10,
            20),
        group_name="Unknown",
        ax=None):
    """Visualize a heatmap of the windows annotations.

    Plot a heatmapt in which you can see how many windows are:
    anomalous, normal, mixed, empty.
    Params
    ------
    df_labels: pandas DataFrame
        columns = hypervisors names
        index = timestamp
        values = 0 for normal, 1 for anomaly, 2 for mixed, NaN for empty
    """
    if ax is None:
        # visualize the heatmap
        fig, ax = plt.subplots()
        fig.set_size_inches(*figsize)

    cmap_anoamly = matplotlib.colors.ListedColormap(
        ['#377eb8', '#ff7f00', 'black'])
    try:
        df_labels.index = df_labels.index.astype('string')
    except Exception as e:
        print(e)
    sns.heatmap(df_labels, cmap=cmap_anoamly, ax=ax)
    ax.set_xlabel("Hypervisors (same group)")
    ax.set_ylabel("Windows (time)")

    locs, labels = plt.xticks()
    plt.xticks(rotation=90)
    ax.set_xticks(ticks=locs)
    ax.set_xticklabels(labels=df_labels.columns)
    ax.set_title("Annotations of " + group_name)
    print("Colorblind colors")


def _get_all_windows_as_str(df_labels):
    """Convert a portion of label dataframe in a vector of string.

    Every string is representative of a speific type of window for that
    server.

    Params
    ------
    df_labels: pandas DataFrame
        columns = hypervisors names
        index = timestamp
        values = 0 for normal, 1 for anomaly, 2 for mixed, NaN for empty
    """
    df_replaced = df_labels.astype(str)
    df_replaced = df_replaced.replace(to_replace="nan", value="Empty")
    df_replaced = df_replaced.replace(to_replace="0.0", value="Normal")
    df_replaced = df_replaced.replace(to_replace="1.0", value="Anomaly")
    df_replaced = df_replaced.replace(to_replace="2.0", value="Mix")
    all_windows = np.squeeze(np.reshape(df_replaced.values, (1, -1)))
    return all_windows


def add_value_labels(ax, spacing=5, round_int=False):
    """Add labels to the end of each bar in a bar chart.

    credits: https://stackoverflow.com/a/48372659/13585425
    Arguments:
        ax (matplotlib.axes.Axes): The matplotlib object containing the axes
            of the plot to annotate.
        spacing (int): The distance between the labels and the bars.
    """
    # For each bar: Place a label
    for rect in ax.patches:
        # Get X and Y placement of label from rect.
        y_value = rect.get_height()
        x_value = rect.get_x() + rect.get_width() / 2

        # Number of points between bar and label. Change to your liking.
        space = spacing
        # Vertical alignment for positive values
        va = 'bottom'

        # If value of bar is negative: Place label below bar
        if y_value < 0:
            # Invert space to place label below
            space *= -1
            # Vertically align label at top
            va = 'top'

        # Use Y value as label and format number with one decimal place
        label = "{:.1f}".format(y_value)
        if round_int:
            label = "{:.0f}".format(y_value)

        # Create annotation
        ax.annotate(
            label,                      # Use `label` as label
            (x_value, y_value),         # Place label at end of the bar
            xytext=(0, space),          # Vertically shift label by `space`
            textcoords="offset points",
            ha='center',                # Horizontally center label
            va=va)                   # Vertically align label differently for
        # positive and negative values.


def plot_statistics(df_labels, group_name="unknown"):
    """Plot a pie plot and a barplot with the statistics of annotations.

    df_labels: pandas DataFrame
        columns = hypervisors names
        index = timestamp
        values = 0 for normal, 1 for anomaly, 2 for mixed, NaN for empty
    """
    all_windows = _get_all_windows_as_str(df_labels=df_labels)
    unord_frequencies = dict(Counter(all_windows))
    frequencies = \
        OrderedDict(sorted(unord_frequencies.items(), key=lambda t: t[0]))
    x = np.char.array(list(frequencies.keys()))
    y = np.array(list(frequencies.values()))
    colors = []
    if 'Anomaly' in x:
        colors.append(sns.color_palette()[1])
    if 'Empty' in x:
        colors.append('white')
    if 'Mix' in x:
        colors.append('black')
    if 'Normal' in x:
        colors.append(sns.color_palette()[0])
    porcent = 100. * y / y.sum()

    # create two axes
    fig, axes = plt.subplots(ncols=2)
    ax_pie = axes[1]
    ax_bar = axes[0]
    # BAR PLOT
    ax_bar.bar(x=x, height=y, color=colors)
    add_value_labels(ax_bar, spacing=5, round_int=True)
    ax_bar.set_ylim(0, max(y) * 1.2)
    ax_bar.set_title(group_name + " - Count of Types of Annotations (8 hours)")
    # PIE PLOT
    ax_pie.set_title(
        group_name +
        " - Percentage of Types of Annotations (8 hours)")
    fig.set_size_inches(14, 5)

    wedgeprops = {"edgecolor": "k",
                  'linewidth': 1,
                  'linestyle': 'solid',
                  'antialiased': True}

    patches, texts = ax_pie.pie(y,
                                colors=colors,
                                startangle=90,
                                radius=1.2,
                                wedgeprops=wedgeprops)
    labels = ['{0} - {1:1.2f} %'.format(i, j) for i, j in zip(x, porcent)]

    sort_legend = True
    if sort_legend:
        patches, labels, dummy = zip(*sorted(zip(patches, labels, y),
                                             key=lambda x: x[2],
                                             reverse=True))

    ax_pie.legend(patches, labels, loc='upper left', bbox_to_anchor=(-0.1, 1.),
                  fontsize=8)
    print("Start period: %s" % min(df_labels.index))
    print("End period: %s" % max(df_labels.index))


def _get_prediction_algo(algo_name,
                         score_directory="/opt/data_repo_volume/vm-datalake/outlier_score_bank"):  # noqa
    """Get all prediction/outlier scores (from CSV of the same algorithm)."""
    only_files = [os.path.join(score_directory, f)
                  for f in os.listdir(score_directory)
                  if os.path.isfile(os.path.join(score_directory, f))]
    only_files_this_algo = [f for f in only_files
                            if algo_name in f]
    [print(n) for n in only_files_this_algo[:3]]
    # Read the dataframe relate to this
    overall_df = pd.DataFrame()
    list_dfs = []
    for file in only_files_this_algo:
        df_file = pd.read_csv(file)
        df_file["timestamp"] = pd.to_datetime(
            df_file["timestamp"] + 60 * 60, unit='s')
        list_dfs.append(df_file)
    overall_df = pd.concat(list_dfs)
    print(overall_df.shape)
    # load from the csv files all the predictions
    df_all_predictions = overall_df.copy()
    return df_all_predictions


def get_matrix_scores(df_labels,
                      algo_name,
                      score_directory):
    """Construct a matrix of scores.

    Note that we keep the same ordering we have in the labels.

    """
    df_all_predictions = \
        _get_prediction_algo(algo_name=algo_name,
                             score_directory=score_directory)
    df_pivot = df_all_predictions.pivot_table(
        index="timestamp", columns="hostname", values="score")
    # return the ordered version
    # and only the hosts in the labels
    hostnames_in_labels = \
        [h + ".cern.ch" for h in list(df_labels.columns)]

    return df_pivot[hostnames_in_labels]


def get_matrix_scores_form_single_csv(single_csv_path,
                                      df_labels):
    """Create the score matrix from a singel CSV."""
    df_all_scores = pd.read_csv(single_csv_path)
    df_all_scores["timestamp"] = \
        pd.to_datetime(df_all_scores["timestamp"] + 60 * 60, unit='s')
    # create matrix
    df_matrix_score_all_hosts = \
        df_all_scores.pivot_table(index="timestamp",
                                  columns="hostname",
                                  values="score")
    # return the ordered version
    # and only the hosts in the labels
    hostnames_in_labels = \
        [h + ".cern.ch" for h in list(df_labels.columns)]
    df_matrix_score = df_matrix_score_all_hosts[hostnames_in_labels]
    return df_matrix_score


def visualize_heatmap_scores(df_scores, figsize=(10, 20), ax=None, vmax=None):
    """Visualize a heatmap of the windows' scores.

    Plot a heatmapt in which you can see the score for each window
    Params
    ------
    df_scores: pandas DataFrame
        columns = hypervisors names
        index = timestamp
        values = values for the scores
    """
    # visualize the heatmap
    if ax is None:
        fig, ax = plt.subplots()
        fig.set_size_inches(*figsize)

    df_scores = df_scores.copy()
    try:
        df_scores.index = df_scores.index.astype('string')
    except Exception as e:
        print(e)
    if vmax is not None:
        sns.heatmap(df_scores, cmap="Reds", ax=ax, vmax=vmax)
    else:
        sns.heatmap(df_scores, cmap="Reds", ax=ax)
    ax.set_xlabel("Hypervisors (same group)")
    ax.set_ylabel("Windows")

    # locs, labels = plt.xticks()
    # plt.xticks(rotation=90)
    # ax.set_xticks(ticks=locs)
    # ax.set_xticklabels(labels=df_scores.columns)

    # locs, labels = plt.yticks()
    # ax.set_yticks(ticks=locs)
    # ax.set_yticklabels(df_scores.index.strftime('%d-%m-%Y'))


def show_heatmap(df_labels, df_scores,
                 above=None, below=None, anomalous=True,
                 figsize=(10, 20)):
    """Visualize the windows that are at the two tail anomlous or normal)."""
    if df_labels.shape[0] != df_scores.shape[0]:
        i_scores = df_scores.index
        i_labels = df_labels.index
        df_labels = df_labels[i_labels.isin(i_scores)]
        print("We adapted the labels to the dimentsions of the scores: ",
              df_labels.shape)
    title = ""
    # PREPARE SCORES
    df_scores = df_scores.copy()
    if above is not None:
        title += "only above " + str(above)
        # keep only high scores
        df_scores[df_scores <= above] = np.NaN
    if below is not None:
        # keep only lower scores
        title += "only below " + str(below)
        df_scores[df_scores >= below] = np.NaN
    # keep only anomaly or normal
    matrix_labels = df_labels.astype("float").values
    if anomalous:
        title += " - only anomaly"
        df_scores[(matrix_labels == 0) | (matrix_labels == 2)] = np.NaN
    else:
        title += " - only normal"
        df_scores[(matrix_labels == 1) | (matrix_labels == 2)] = np.NaN
    fig, ax = plt.subplots()
    fig.set_size_inches(*figsize)
    ax.set_title(title)
    visualize_heatmap_scores(df_scores, ax=ax)
    return df_scores.copy()


def plot_precision_recall_curve(precisions, recalls, thresholds, ax=None):
    """Plot precision recall."""
    if ax is None:
        fig, ax = plt.subplots()
    ax.plot(recalls, precisions)
    ax.set_xlabel("Recall")
    ax.set_ylabel("Precision")
    ax.set_xlim(left=0)
    ax.set_ylim(bottom=0)
    return ax


def plot_vs_thresholds(metric, metric_name, thresholds, ax=None):
    """Plot a metric versus threshold."""
    if ax is None:
        fig, ax = plt.subplots()
    ax.plot(thresholds, metric, label=metric_name)
    ax.set_xlabel("Thresholds")
    # ax.set_ylabel(metric_name)
    return ax


# DATA FOLDER ACCESS - VISUALISATION


def parse_4_dates(filename):
    """Parse the 4 dates from the filename."""
    filename = filename.split("/")[-1]
    just_name = remove_suffix(filename, ".metadata")
    last_4_string_dates = just_name.split("_")[-4:]
    dates = [datetime.strptime(date_string, '%Y-%m-%d')
             for date_string in last_4_string_dates]
    return dates


def check_dates_same_week_normalization(dates):
    """Check that his dates are two-two the same."""
    return len(set(dates)) == 2


def is_contained(dt_to_check, two_dates_list):
    """Check if a datatime is included in other two.

    Params
    ------
    two_dates_list: list of datetime
        pass just two datetime (don't worry about the order)
    """
    return (dt_to_check > min(two_dates_list) and
            dt_to_check < max(two_dates_list))


def get_filename_containing_this_window(timestamp_dt_end,
                                        data_folder,
                                        self_normalized=True):
    """Find the filename of the file containing this window.

    Params
    ------
    timestamp_dt_end: datetime
        timestamp signaling the end of a window
    data_folder: str
        folder name containing data
    self_normalized: boolen
        if True you check only origianl data, normalized on themselves
        you do not look into data that have been normalized on previous
        intervals (e.g. previous week).
    """
    only_files = [os.path.join(data_folder, f)
                  for f in os.listdir(data_folder)
                  if os.path.isfile(os.path.join(data_folder, f))]
    for filename in only_files:
        dates = parse_4_dates(filename)
        # check they are in the same week
        if self_normalized == check_dates_same_week_normalization(dates=dates):
            # keep the first two dates that represent the actual data period
            first_two_dates = dates[:2]
            # if my timestamp is in this file -> return this filename
            if is_contained(dt_to_check=timestamp_dt_end,
                            two_dates_list=first_two_dates):
                return filename
    raise Exception("We did not find data with this timestamp")


def get_window(filename_metadata, timestamp_dt_end_window):
    """Get the data from this window.

    Params
    ------
    filename_metadata: str
        name of the file ".metadata" containing this window
    timestamp_dt_end_window: datatime
        timestamp of the window we are intereste in
    Return
    ------
    X_w: pandas DataFrame
        actual data. columns = plugin name and temporal steps
    prov_w: pandas DataFrame
        provenance of this window hostname | ts (dt) | timestamp (int)
    """
    # Open this dataframe file
    X_train, prov_train, nr_timeseries = \
        read_local_window_dataset(config_filepath=filename_metadata)
    # look for this timewindow
    ts_to_find = datetime.timestamp(timestamp_dt_end_window)
    # if not present try to change time zone (add one hour)
    # and search again
    if ts_to_find not in list(prov_train["timestamp"]):
        ts_to_find = \
            datetime.timestamp(timestamp_dt_end_window + timedelta(hours=1))
    # finally check if present in one of the two cases
    if ts_to_find in list(prov_train["timestamp"]):
        mask = prov_train["timestamp"] == ts_to_find
        prov_w = prov_train[mask]
        X_w = X_train[mask]
    else:
        raise Exception("timestamp %s not found in %s" %
                        (str(ts_to_find), filename_metadata))
    return X_w, prov_w


def get_row(df_scores_matrix, row):
    """Select a row of the scores marix.

    Return
    -----
    timestamp_dt_end: datatime
        representing the window on that row
    ordered_scores: list of float
        anomaly scores of the machines in that row. they are ordered the
        highest first.
    ordered_hosts: list of str
        hostnames ending in ".cern.ch" of the machines in that row. they
        correspond to the ordered score
    """
    # get the timestamp
    ts_pandas = df_scores_matrix.index[row]
    timestamp_dt_end = ts_pandas.to_pydatetime()
    print("Timestamp looked for: ", timestamp_dt_end)
    # get the scores
    scores = list(df_scores_matrix.iloc[row])
    # get the hosts
    hosts = list(df_scores_matrix.columns)
    # get the ordered version of scores and hosts couple
    pairs = zip(scores, hosts)
    ordered_pairs = sorted(pairs, reverse=True)
    ordered_scores, ordered_hosts = zip(*ordered_pairs)
    return timestamp_dt_end, ordered_scores, ordered_hosts
