import numpy as np
import sklearn
import json
from hashlib import blake2b
import pickle


class NpEncoder(json.JSONEncoder):
    """JSON encoder to support numpy datatypes."""

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


class NormalizerBank(object):

    def __init__(self,
                 normalization_folder):
        """Normalize your data.

        Parameters
        ---------
        normalization_folder: str
            path where is your bank stored
        """
        self.normalization_folder = normalization_folder

    def fit_normalizer(self,
                       scaler,
                       list_of_datasets,
                       replace_infinity_with=0):
        """Fit a normalizer on the given datasets.

        Parameters
        ----------
        scaler : object exposing the sklear scaler api
        list_of_datasets : list of CollectdDatasets. All the datasets should
            contain the same plugins.
        replace_infinity_with : int, optional
            To apply normalizatin we can't stand values that are infinite, so
            provide a value to replace them
        Returns
        -------
        scaler : fitted on the dataset passed
        reference_plugins : list of str
            names of the plugins, their order tells us the mapping of the
            various coeffiecients learned by the scaler
        list_of_cells: list of dict
            each dict represent the following info of the cell:
            dict_cell["cell_name"]
            dict_cell["start_date"]
            dict_cell["end_date"]
            dict_cell["granularity_min"]
        """
        reference_plugins = None
        list_of_cells = []
        # get the dataset in each plugin
        for clds in list_of_datasets:
            df_clds = clds.get_data()
            # create feature representation
            features_df = df_clds.pivot_table(index="temporal_index",
                                              columns="plugin",
                                              values="mean_value")
            # fill infinity. needed for the scaler
            features_df.replace(to_replace=[np.inf, -np.inf],
                                value=replace_infinity_with,
                                inplace=True)
            # get order metrics
            columns = list(features_df.columns)
            if reference_plugins is None:
                reference_plugins = columns

            # check that all the datasets contain the same plugins
            columns.sort()
            reference_plugins.sort()
            if columns != reference_plugins:
                print("Different plugins in the given datasets. Fix it.")
                return
            # get metadata about the list cells
            meta = clds.get_metadata()
            dict_cell = {}
            dict_cell["cell_name"] = meta["cell_name"]
            dict_cell["start_date"] = meta["start_date"]
            dict_cell["end_date"] = meta["end_date"]
            dict_cell["granularity_min"] = meta["granularity_min"]
            list_of_cells += [dict_cell]
            # fit the scaler
            scaler.fit(features_df.values)
        return scaler, reference_plugins, list_of_cells

    def save_normalizer(self,
                        scaler_name,
                        scaler_fitted,
                        metrics_order,
                        list_cells):
        """Push your normalizer to the bank.

        This will save your scaler in a json to represent all the data
        used to fit the scaler (reproducibility). And it will also save a
        pickle object of the fittet scaler to fast use.

        Parameters
        ----------
        scaler_name : str
            name of the scaler logic (e.g. "standard", "minmax", "robust")
        scaler_fitted : scaler object fitted on the datasets passed after
        metrics_order : list of str
            names of the plugins, their order tells us the mapping of the
            various coeffiecients learned by the scaler
        list_cells: list of dict
            Cells used to fit the scaler
            each dict represent the following info of the cell:
            dict_cell["cell_name"]
            dict_cell["start_date"]
            dict_cell["end_date"]
            dict_cell["granularity_min"]
        Return
        ------
        id: str
            id of the normalizer just saved
        """
        normalization = {}
        normalization["scaler"] = str(scaler_name)
        normalization["sklearn_ver"] = str(sklearn.__version__)
        normalization["params"] = scaler_fitted.__dict__
        normalization["metrics_order"] = metrics_order
        normalization["provenance"] = sorted(list_cells)

        # create a str version
        json_str_for_hash = json.dumps(normalization,
                                       indent=4,
                                       sort_keys=True,
                                       cls=NpEncoder)

        # compute the id
        HASH_LENGTH = 10
        h_normalization = blake2b(digest_size=HASH_LENGTH)
        h_normalization.update(str.encode(json_str_for_hash))
        id_normalization = h_normalization.hexdigest()

        # add id to the dictionary
        normalization["id"] = str(id_normalization)

        # get string final file
        json_str = json.dumps(normalization,
                              indent=4,
                              sort_keys=True,
                              cls=NpEncoder)

        # dump to file
        with open(self.normalization_folder + "/"
                  + id_normalization + ".json", "w") as outfile:
            outfile.write(json_str)

        # dump the pickle file
        with open(self.normalization_folder + "/"
                  + id_normalization + ".pkl", "wb") as pklfile:
            pickle.dump(scaler_fitted, pklfile)

        # dump in the std out to go to ES
        # print(json_str)
        print("id_normalization created: %s" % id_normalization)
        return id_normalization

    def get_normalizer(self,
                       id_normalization):
        """Get the normalizer with the given id.

        Parameters
        ----------
        id_normalization: str
            id of the normalizer you want to load.
        Return
        ------
        reproducibility_dict: dict
            contains the info necessary to re fit the scaler (reproducibility)
        scaler_fitted : scaler object fitted on the
        """
        # load reproducibility_dict
        with open(self.normalization_folder + "/"
                  + id_normalization + ".json", "r") as jsonfile:
            reproducibility_dict = json.load(jsonfile)

        # dump the pickle file
        with open(self.normalization_folder + "/"
                  + id_normalization + ".pkl", "rb") as pklfile:
            scaler_object = pickle.load(pklfile)
        return reproducibility_dict, scaler_object
