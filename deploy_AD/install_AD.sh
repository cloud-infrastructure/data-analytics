#!/bin/bash -e

# Script to install pkgs and configure the Anomaly Detection System
# To be run as root

function uninstall(){
    # remove the system
    rm -R /opt/ad_system/

    # remove soft link to control room
    rm /opt/control_ad_system
}

function install_centos7() {
    # INSTALL WGET
    yum -y install wget git python3-pip\
         kstart screen emacs

    # INSTALL DOCKER
    yum-config-manager \
        --add-repo \
        https://download.docker.com/linux/centos/docker-ce.repo
    yum -y install docker-ce docker-ce-cli containerd.io
 
    install_eos
}

function install_eos(){
    # INSTALL EOS
    # https://cern.service-now.com/service-portal?id=kb_article&n=KB0003846
    # For CERN CentOS 7 and CentOS8 desktops, please be sure that the EPEL repository is enabled and then use (as root):

    echo -e "install EOS: this can require few minutes"
    locmap --enable eosclient
    locmap --configure eosclient

}

function install_common(){
    # INSTALL DOCKER COMPOSE
    # https://docs.docker.com/compose/install/
    curl -L "https://github.com/docker/compose/releases/download/1.28.6/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
    [ ! -e /usr/bin/docker-compose ] && ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
    docker-compose --version
}

function install_ad(){

    # STOP DOCKER
    systemctl stop docker

    groupmod -g 50000 docker

    AD_USER=airflow
    # Add user airflow
    useradd -g docker -u 50000 ${AD_USER}

    # CHANGE SYSTEM GID / UID of DOCKER and AIRFLOW to match the
    # internal docker airflow configuration
    #groupmod -g 1001 airflow  # test
    # update the permission on the main docker file launcher
    #chmod 660 /var/run/docker.sock
    #chgrp docker /var/run/docker.sock
    # add user airflow in the group docker
    #usermod -aG docker ${AD_USER}

    # START DOCKER
    systemctl start docker

    # PREPARE MAIN FOLDER
    # main folder with files of the system
    [ ! -e ${BASE_AD_DIR} ] && mkdir -p ${BASE_AD_DIR}

    # go to the main folder
    # download the repository file FROM QA
    # and create a folder
    cd ${BASE_AD_DIR}
    INSTALL_AD_DIR=${BASE_AD_DIR}/data-analytics-$branch
    if [[ -e ${INSTALL_AD_DIR} ]];then
        echo "Folder ${INSTALL_AD_DIR} already exists."   
        INSTALL_AD_DIR=${INSTALL_AD_DIR}_$RANDOM
        echo "Using folder ${INSTALL_AD_DIR}"
    fi
    #wget https://gitlab.cern.ch/cloud-infrastructure/data-analytics/-/archive/$branch/data-analytics-$branch.tar.gz -O - | tar -xz
    git clone -b $branch --single-branch https://gitlab.cern.ch/cloud-infrastructure/data-analytics.git ${INSTALL_AD_DIR}
    [[ "$?" -ne 0 ]] && echo "git clone did not succeed. EXIT" && exit 1

    chown -R ${AD_USER} ${BASE_AD_DIR}
    # create the symlink to give a standard directory for the dags
    [[ -L "${CONTROL_AD_DIR}" && -d "${CONTROL_AD_DIR}" ]] && rm ${CONTROL_AD_DIR}
    sudo -u ${AD_USER} ln -s ${INSTALL_AD_DIR}/deploy_AD/airflow-compose ${CONTROL_AD_DIR}
    echo " "
}

function set_firewall(){
    # Set firewall rules to close some ports
    # and open the port to communicate with Spark Cluster

    if [ `iptables-save | grep DOCKER-USER | grep -c '\-\-dport 5003 -j DROP'` -eq 0 ];
    then
        echo "defining firewall rule to close port 5003 used by filebrowser and give only localhost access"
        iptables -I DOCKER-USER ! -s 127.0.0.1/32 -i eth0 -p tcp -m tcp --dport 5003 -j DROP
        /sbin/service iptables save
    fi

    if [ `iptables-save | grep DOCKER-USER | grep -c '\-\-dport 6555 -j DROP'` -eq 0 ];
    then
        echo "defining firewall rule to close port 6555 used by airflow and give only localhost access"
        iptables -I DOCKER-USER ! -s 127.0.0.1/32 -i eth0 -p tcp -m tcp --dport 6555 -j DROP
        /sbin/service iptables save
    fi

    if [ `iptables-save | grep DOCKER-USER | grep -c '\-\-dport 8080 -j DROP'` -eq 0 ];
    then
        echo "defining firewall rule to close port 8080 used by airflow Web UI and give only localhost access"
        iptables -I DOCKER-USER ! -s 127.0.0.1/32 -i eth0 -p tcp -m tcp --dport 8080 -j DROP
        /sbin/service iptables save
    fi

    if [ `iptables-save | grep DOCKER-USER | grep -c '\-\-dport 8793 -j DROP'` -eq 0 ];
    then
        echo "defining firewall rule to close port 8793 used by airflow and give only localhost access"
        iptables -I DOCKER-USER ! -s 127.0.0.1/32 -i eth0 -p tcp -m tcp --dport 8793 -j DROP
        /sbin/service iptables save
    fi

    if [ `iptables-save | grep DOCKER-USER | grep -c '\-\-dport 24224 -j DROP'` -eq 0 ];
    then
        echo "defining firewall rule to close port 24224 used by fluentd and give only localhost access"
        iptables -I DOCKER-USER ! -s 127.0.0.1/32 -i eth0 -p tcp -m tcp --dport 24224 -j DROP
        /sbin/service iptables save
    fi

    if [ `iptables-save | grep INPUT | grep -c '\-p tcp \-m multiport \-\-dports 5001:6000 \-m comment \-\-comment "00200 firewall for hadoop jobs" \-j ACCEPT'` -eq 0 ];
    then
        echo "defining firewall rule to open the Spark ports"
        iptables -I INPUT -p tcp -m multiport --dports 5001:6000 -m comment --comment "00200 firewall for hadoop jobs" -j ACCEPT
        /sbin/service iptables save
    fi

    if [ `iptables-save | grep DOCKER-USER | grep -c '\-p tcp \-m multiport \-\-dports 5001:6000 \-m comment \-\-comment "00200 firewall for hadoop jobs" \-j ACCEPT'` -eq 0 ];
    then
        echo "defining firewall rule to make the Spark ports accessible for docker"
        iptables -I DOCKER-USER -p tcp -m multiport --dports 5001:6000 -m comment --comment "00200 firewall for hadoop jobs" -j ACCEPT
        /sbin/service iptables save
    fi

    iptables -L DOCKER-USER

}

function install_all(){
    set -x #to display commands to be executed

    if [ `hostnamectl | grep Oper | grep -c -w -i 'CentOS Linux 7'` -gt 0 ];
    then
        install_centos7
    else
        echo "ERROR! This script is only supported on CentOS 7"
        exit 1
    fi

    install_common
    set_firewall
    install_ad
    root_cronjobs
    run_as_airflow
    print_instructions
    cd $HOME
}

function run_as_airflow(){

    cp $SCRIPT /home/airflow/
    chown airflow /home/airflow/install_AD.sh
    su - airflow -c "source /home/airflow/install_AD.sh; set_ktoken"
    su - airflow -c "source /home/airflow/install_AD.sh; airflow_cronjobs"
}

function airflow_cronjobs(){
    source ${CONTROL_AD_DIR}/secret.sh
    export KRB5CCNAME
    # cleanup cron - if we run dag 3 times a day for the data of previous day, we want to always rerun the inference part with fresh data
    line="0 12,20 * * * ( date; export KRB5CCNAME=$KRB5CCNAME; for i in \`ls ${AD_EOS_BASE_DIR}/Airflow/v0.4 | grep AD_\`; do find ${AD_EOS_BASE_DIR}/Airflow/v0.4/\$i/local_cache_inference/ -name *\$(date +\%Y-\%m-\%d)* -exec rm -rf {} \; ;done ) &> /var/tmp/inference_cleanup.log"
    echo -e "going to insert the following line in airflow's crontab \n$line"
    (crontab -l; echo "$line" ) | crontab -
    # onreboot action
    line="PATH=/usr/sbin:/sbin:/usr/bin:/bin"
    echo -e "going to insert the following line in airflow's crontab \n$line"
    (crontab -l; echo "$line" ) | crontab -
    line="@reboot (sleep 120; ${CONTROL_AD_DIR}/start_ad_system.sh -e prod) &> /var/tmp/onreboot_cron_airflow.txt"
    echo -e "going to insert the following line in airflow's crontab \n$line"
    (crontab -l; echo "$line" ) | crontab -

}

function root_cronjobs(){
    source ${CONTROL_AD_DIR}/secret.sh
    export KRB5CCNAME
    INSTALL_AD_DIR=${BASE_AD_DIR}/data-analytics-$branch

    line="PATH=/usr/sbin:/sbin:/usr/bin:/bin"
    echo -e "going to insert the following line in roots's crontab \n$line"
    (crontab -l; echo "$line" ) | crontab -

    # reboot action
    line="15 23 * * * (. ${CONTROL_AD_DIR}/start_ad_system.sh -e prod; stop_compose_AD; reboot now) &> /var/tmp/reboot_cron.txt"
    echo -e "going to insert the following line in roots's crontab \n$line"
    (crontab -l; echo "$line" ) | crontab -
    # onreboot action
    line="@reboot (sleep 60;service docker start; . ${INSTALL_AD_DIR}/deploy_AD/install_AD.sh; set_firewall) &> /var/tmp/onreboot_cron_root.txt"
    echo -e "going to insert the following line in roots's crontab \n$line"
    (crontab -l; echo "$line" ) | crontab -

}

function set_ktoken() {
    UHOME=/home/airflow
    source ${CONTROL_AD_DIR}/secret.sh
    export KRB5CCNAME
    read -p 'Which account will provide kerberos access? [srvdaana]: ' kaccount
    kaccount=${kaccount:-srvdaana}
    echo "The selected account is $kaccount"
    echo "now insert the password for that $kaccount in the following requests"
    echo "cern-get-keytab --keytab $HOME/ACCOUNT.kt --user --login $kaccount"
    echo -e "\n
    (*)     documentation in https://cern.service-now.com/kb_view.do?sysparm_article=KB0003405
    "
    cern-get-keytab --keytab $UHOME/ACCOUNT.kt --user --login $kaccount

    echo "going to run kinit"
    kinit -k -t $UHOME/ACCOUNT.kt -c $KRB5CCNAME $kaccount@CERN.CH
    #export KINIT_PROG=eosfusebind
    #k5start -f $UHOME/ACCOUNT.kt -L -K 30 -k ${KRB5CCNAME} -b -U -t
    #ps -f --pid `pgrep k5start` 2 > /dev/null
    # eosfusebind needs to be renewed (using cron to workaround this)
    line="0 * * * * (date; export KRB5CCNAME=$KRB5CCNAME ;klist; eosfusebind; ls ${AD_EOS_BASE_DIR}) &> /tmp/cron_eosfusebind.txt"
    echo -e "going to insert the following line in crontab \n$line"
    (crontab -l; echo "$line" ) | crontab -
    line="0 1 * * * (date; export KRB5CCNAME=$KRB5CCNAME ;klist; kinit -k -t $UHOME/ACCOUNT.kt -c $KRB5CCNAME $kaccount@CERN.CH; klist) &> /tmp/cron_refresh_token.txt"
    echo -e "going to insert the following line in crontab \n$line"
    (crontab -l; echo "$line" ) | crontab -
    crontab -l
    
}

function print_instructions() {
    set +x
    echo -e "\nInstallation finished
    1. To run Airflow change user to airflow user
            su - airflow

    2. Before starting the docker-compose of Anomaly Detection System
       You may want to change dummy passwords in ${CONTROL_AD_DIR}/secret.sh
       Then run:

            ${CONTROL_AD_DIR}/start_ad_system.sh

    3. Sometimes the firewall rules for Docker are overriden after the start of docker-compose
       If the ports enabling connection with spark are closed, the jobs will fail.
       Run again (as root)
       . ./install_AD.sh; set_firewall

    4. 'screen' and 'emacs' are available

    5. To print again these instructions, source the install_AD.sh script and execute
           print_instructions
    "
}


BASE_AD_DIR=/opt/ad_system
CONTROL_AD_DIR=${BASE_AD_DIR}/control_ad_system

if [[ $0 != $BASH_SOURCE ]];
then
    echo "Script is being sourced"
    SCRIPT=$(readlink -f $BASH_SOURCE)
else
    echo "Script is being run"
    SCRIPT=$(readlink -f $0)
    install_all
fi
echo $SCRIPT
