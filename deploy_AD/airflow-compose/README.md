# Anomaly Detection System driven by Airflow

**NB**:
In these examples there are dummy passwords stored in the **secret.sh** file.
Those passwords are here only as example for a simple local test.
**!!! Do not commit real production passwords !!!**

To start/stop Airflow docker-compose use the following commands:

1. Initialize: `source ./start_ad_system.sh`
1. Reset (!!!CAVEAT: this removes all stored DAG executions and logs!!!): `start_compose_AD prune`
1. Stop AD Airflow: `stop_compose_AD`
1. Stop AD Airflow and remove volumes (same effect as in 2.): `stop_compose_AD --volumes`
1. Start/Re-start AD Airflow (w/o removing volumes): `start_compose_AD` 

## Troubleshooting

This section collects some expedients applied in case of issues. 
Not necessary all of them will be always valid. This section needs a recurrent review.

### docker-compose network not starting

Creating network "airflow-compose_default" with the default driver
```
ERROR: Failed to Setup IP tables: Unable to enable SKIP DNAT rule:  
(iptables failed: iptables --wait -t nat -I DOCKER -i br-37e312e2428d -j 
RETURN: iptables: No chain/target/match by that name.
(exit status 1))
```

```
    systemctl restart docker
    systemctl restart iptables
    iptables -t filter -N DOCKER

```
