#!/bin/bash

# This script drives the start of docker-compose services
# for the Anomaly Detection System based on Airflow

export AD_SOURCE_DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
echo "AD_SOURCE_DIR $AD_SOURCE_DIR"

env="test" #expects "test" or "prod" value
prune=false

while getopts "e:p:" flag;
do
case $flag in
        e ) env=$OPTARG;;
        p ) prune=$OPTARG;;
esac
done

echo "Environment variable: ${env}"
echo "Prune variable: ${prune}"

function stop_compose_AD() {
    if [[ "$1" == "-h" ]]; then
        echo "stop docker-compose and remove-orphans. if --volumes is passed, also volumes are removed"
        return 0
    fi

    env="test" #expects "test" or "prod" value
    volume_opt=""

    local OPTIND
    while getopts "e:p" flag;
    do
    case $flag in
            e ) env=$OPTARG;;
            p ) volume_opt="--volumes"    ;;
    esac
    done


    echo "Here volume_opt!"
    echo $volume_opt
    if [[ "${volume_opt}" == "--volumes" ]];
    then
        volume_opt="--volumes"
        echo "List of volumes before docker-compose down"
        docker volume ls
    fi

    [[ -e deployment_cache.txt ]] && export THIS_DEPLOYMENT_CACHE=`cat deployment_cache.txt`
    ls $THIS_DEPLOYMENT_CACHE

    echo -e "\nStop previous docker-compose...\n"
    if [ "$env" = "prod" ]; then
        docker-compose --env-file ${AD_SOURCE_DIR}/secret.sh -f ${AD_SOURCE_DIR}/docker-compose.yml down --remove-orphans ${volume_opt}
        docker-compose --env-file ${AD_SOURCE_DIR}/secret.sh -f ${AD_SOURCE_DIR}/docker-compose.yml rm
    else
        docker-compose --env-file ${AD_SOURCE_DIR}/secret.sh -f ${AD_SOURCE_DIR}/docker-compose.yml -f ${AD_SOURCE_DIR}/docker-compose-test.yml down --remove-orphans ${volume_opt}
        docker-compose --env-file ${AD_SOURCE_DIR}/secret.sh -f ${AD_SOURCE_DIR}/docker-compose.yml -f ${AD_SOURCE_DIR}/docker-compose-test.yml rm
    fi

    echo "List of volumes after docker-compose down"
    docker volume ls 

    echo -e "\nKill processes still using cvmfs\n"
    [ `pgrep -f cvmfs -c` -gt 0 ] && kill -9 $(pgrep -f cvmfs)
}

function start_compose_AD(){
    . ${AD_SOURCE_DIR}/secret.sh

    # folders to store data locally
    [[ ! -e ${AD_LOGS_PATH} ]] && mkdir -p ${AD_LOGS_PATH}

    env="test" #expects "test" or "prod" value
    prune=false

    local OPTIND
    while getopts "e:p" flag;
    do
    case $flag in
            e ) env=$OPTARG;;
            p ) prune=true;;
    esac
    done


    if [ "$prune" == true ]; then
	    stop_compose_AD -p -e ${env}
    else
	    stop_compose_AD -e ${env}
    fi

    # Recipe: need to access ${AD_EOS_BASE_DIR} once from outside containers
    # to make eosfusebind work
    ls ${AD_EOS_BASE_DIR} > /dev/null

    # Copying the set of utility functions in the deployment cache # Fixme should use library
    export THIS_DEPLOYMENT_CACHE=${AD_DEPLOYMENTS_CACHE}/ #`date +%F_%T`_$RANDOM
    echo ${THIS_DEPLOYMENT_CACHE} > deployment_cache.txt
    mkdir -p $THIS_DEPLOYMENT_CACHE
    cp -r config_files $THIS_DEPLOYMENT_CACHE

    echo -e "\nStart new docker-compose...\n"
    if [ "$env" = "prod" ]; then
        docker-compose --env-file ${AD_SOURCE_DIR}/secret.sh -f ${AD_SOURCE_DIR}/docker-compose.yml up airflow-init
        docker-compose --env-file ${AD_SOURCE_DIR}/secret.sh -f ${AD_SOURCE_DIR}/docker-compose.yml up -d --remove-orphans --renew-anon-volumes # --abort-on-container-exit # --force-recreate
    else
        docker-compose --env-file ${AD_SOURCE_DIR}/secret.sh -f ${AD_SOURCE_DIR}/docker-compose.yml -f ${AD_SOURCE_DIR}/docker-compose-test.yml up airflow-init
        docker-compose --env-file ${AD_SOURCE_DIR}/secret.sh -f ${AD_SOURCE_DIR}/docker-compose.yml -f ${AD_SOURCE_DIR}/docker-compose-test.yml up -d --remove-orphans --renew-anon-volumes # --abort-on-container-exit # --force-recreate
    fi

    echo -e "\nYou can inspect logs running\n
    docker-compose -f ${AD_SOURCE_DIR}/docker-compose.yml logs -f
    "
}

if [[ $0 != $BASH_SOURCE ]];
then
    echo "Script is being sourced"
    echo -e "to stop compose run \n\t stop_compose_AD -e <env test/prod (default test)> [-p to prune existing volume]"
    echo -e "to start compose run \n\t start_compose_AD -e <env test/prod (default test)> [-p to prune existing volume]"
else
    echo "Script is being run"
    start_compose_AD -e ${env} 
fi


