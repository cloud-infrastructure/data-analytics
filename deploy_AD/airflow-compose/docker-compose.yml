version: '3.3'

x-environment:
    &airflow-common-env
         KRB5CCNAME: ${KRB5CCNAME}
         REDIS_HOST: redis
         REDIS_PASSWORD: ${REDIS_PASSWORD}
         THIS_DEPLOYMENT_CACHE: ${THIS_DEPLOYMENT_CACHE}
         
         # This list changes the default_airflow.cfg using the syntax
         # AIRFLOW_[component_in_the_cfg]_<parameter_in_the_cfg>

         AIRFLOW__WEBSERVER__DAG_DEFAULT_VIEW: graph
         AIRFLOW__WEBSERVER__DAG_ORIENTATION: TB # dag layout top-bottom
         AIRFLOW__CORE__EXECUTOR: CeleryExecutor
         AIRFLOW__CORE__SQL_ALCHEMY_CONN: postgresql+psycopg2://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}/${POSTGRES_DB}
         AIRFLOW__CORE__FERNET_KEY: ${FERNET_KEY}
         AIRFLOW__CORE__DAGS_ARE_PAUSED_AT_CREATION: 'true'
         AIRFLOW__CORE__LOAD_EXAMPLES: 'false'
         AIRFLOW__CELERY__RESULT_BACKEND: db+postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}/${POSTGRES_DB}
         AIRFLOW__CELERY__BROKER_URL: redis://:${REDIS_PASSWORD}@redis:${REDIS_PORT}/${REDIS_DBNUM}

         # These are global env variables passed to the AIRFLOW objects
         AIRFLOW_VAR_KRB5CCNAME: ${KRB5CCNAME}

x-airflow-common:
    &airflow-common
    image: gitlab-registry.cern.ch/cloud-infrastructure/data-analytics/docker-airflow:${AIRFLOW_IMAGE_TAG}
    volumes:
          - ${DAG_PATH}:/opt/airflow/dags
          - ${AD_LOGS_PATH}:/opt/airflow/logs
          - $KRB5CCNAME:$KRB5CCNAME
          - /eos:/eos
          - /var/run/docker.sock:/var/run/docker.sock
          #- ${KUBECONFIG}:/opt/airflow/.kube/config
    user: "${AIRFLOW_UID:-50000}:${AIRFLOW_GID:-50000}"
    depends_on:
      redis:
        condition: service_healthy
      postgres:
        condition: service_healthy
    restart: always

services:
########################################################
### support services
  srv_cvmfs:
      image: gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-builder/cvmfs-image:${CVMFS_IMAGE_TAG}
      command: -r sft.cern.ch -t /tmp/traces
      privileged: true
      volumes:
         - /cvmfs:/cvmfs:shared
      network_mode: host
      restart: always

  srv_fluentd:
      image: gitlab-registry.cern.ch/cloud-infrastructure/data-analytics/fluentd:${FLUENTD_IMAGE_TAG}
      volumes:
         - ./fluentd_conf/REAL_fluent_to_monit.conf:/fluentd/etc/fluent.conf
      ports:
         - "24224:24224"
         - "24224:24224/udp"
      restart: always

########################################################

  redis:
    image: redis:6.2.1
    command: redis-server --requirepass ${REDIS_PASSWORD}
    ports:
      - 6379:6379
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
      interval: 5s
      timeout: 30s
      retries: 50
    restart: always

  postgres:
    image: postgres:13
    environment:
      - POSTGRES_USER=${POSTGRES_USER}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
      - POSTGRES_DB=${POSTGRES_DB}
    volumes:
      - postgres-db-volume:/var/lib/postgresql/data
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "airflow"]
      interval: 5s
      retries: 5
    restart: always


  airflow_web:
    <<: *airflow-common
    environment:
          <<: *airflow-common-env
    privileged: true
    ports:
      - 8080:8080
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:8080/health"]
      interval: 10s
      timeout: 10s
      retries: 5
    command: webserver

  airflow_scheduler:
    <<: *airflow-common
    environment:
          <<: *airflow-common-env
          AD_SRV: scheduler
    command: scheduler
    privileged: true

  airflow_worker:
    <<: *airflow-common
    environment:
          <<: *airflow-common-env
    command: celery worker
    privileged: true

  airflow-init:
    <<: *airflow-common
    command: version
    environment:
      <<: *airflow-common-env
      _AIRFLOW_DB_UPGRADE: 'true'
      _AIRFLOW_WWW_USER_CREATE: 'true'
      _AIRFLOW_WWW_USER_USERNAME: ${_AIRFLOW_WWW_USER_USERNAME:-airflow}
      _AIRFLOW_WWW_USER_PASSWORD: ${_AIRFLOW_WWW_USER_PASSWORD:-airflow}

  airflow_flower:
    <<: *airflow-common
    environment:
          <<: *airflow-common-env
    command: celery flower
    privileged: true
    ports:
      - 5555:5555
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:5555/"]
      interval: 10s
      timeout: 10s
      retries: 5

volumes:
  postgres-db-volume:
      driver: local
  esdata66:
      driver: local
