---
# ----------------------------------------------------------------
#  CONFIGURATION FILE TO DEFINE WHICH DATA TO EXTRACT FORM HDFS
# ----------------------------------------------------------------

# the variables having value __templated__ are
# replaced by the BashOperator using the Jinja templates

# ----------------------------------------------------------------
#                         HOSTGROUP INFO
# ----------------------------------------------------------------

# Absolute path identifier of the cell/hostgroup that you want to mine.
# Note that it is in a list format, but only one hostgroup is supported so far.
hostgroups:
- cloud_compute/level2/main/gva_shared_012

# The pattern of the names of your data folders and ".metadata" files.
# This is the template name, each variable in brachets is replaced by the
# corresponding values via jinja tempalting
# Note that this name is the same use both for HDFS and your local copies.
code_project_name: "__templated__"

# ----------------------------------------------------------------
#                               LOCAL
# ----------------------------------------------------------------

# Local area of your VM where to save your data and metadata
# data are saved in folders with one parquet only
# metadata are saved in file with the same name of the resepctive foler
# plus the ".metadata" extension
local_cache_folder: "__templated__"

# ----------------------------------------------------------------
#                               HDFS
# ----------------------------------------------------------------

# HDFS Area where Spark saves the aggregated data of your cell
# Note that the saving can create multiple file depending on the number of
# partitions that the workers were using.
hdfs_out_folder: "__templated__"

# HDFS Area where Spark saves the aggregated data of your cell
# Note that here we force it to be one partiotion only
hdfs_cache_folder: "__templated__"

# HDFS Area where Spark saves the normalization coefficients computed on the
# normalziation chunk of data between:
# date_start_normalization and date_end_normalization_excluded
normalization_out_folder: "__templated__"

# Wether you want to overwrite (true) or not (false) the raw data in HDFS
# If not sure leave true
overwrite_on_hdfs: true

# Wether you want to overwrite (true) or not (false) the noramlization
# coefficeints in HDFS. If not sure leave true
overwrite_normalization: true

# ----------------------------------------------------------------
#                       TEMPORAL DETAILS
# ----------------------------------------------------------------

# The level of aggregation of your raw time series data.
# The aggregator is typically the mean operator.
# e.g. if 5 it means that we summarize the data every 5 min, and the values
# with timestamp 7.45 will represent the mean of the previous 5 minutes from
# 7.40 to 7.45 but that value will have 7.45 as timestamp
aggregate_every_n_minutes: 30

# The length of your windows of data
# e.g. if aggregate_every_n_minutes = 10 and history_steps = 6 it means that
# every windows is summarizing 6 * 10 = 60 minutes
history_steps: 8

# The number of step you want to move your window.
# e.g. if aggregate_every_n_minutes = 10 and history_steps = 2 it means that
# you will get a window of data that is translated of 10 * 2 = 20 min with
# respect to the previous.
# Note that if slide_steps has the same value of history_steps you have non-
# overlapping windows
slide_steps: 8

# In order to extend keep hystory_steps=slide_steps for both train and inference.
# To append on the contrary use slide_steps = 1 FOR BOTH and set 
# select_slide_steps_cols = True! 
# (if you use slide_steps != 1 is ok anyway, you will do an hybrid version).
select_slide_steps_cols: false

# Used to create windows with future steps.
# If not sure keep this to 0
future_steps: 0

# Dates representing the start/end of the data and noramlization chunks.
# - start_date -> the starting date of data chunk of ETL
# - end_date -> the ending date of data chunk of ETL
# - start_date_normalization -> the starting date of the chunk of data used
#   to learn noramlization coefficeints (typically this chunk preceeds the
#   chunk of data)
# - end_date_normalization -> the ending date of the chunk of data used
#   to learn noramlization coefficeints
# Note that the upper extremum is excluded (i.e. data will stop at the 23:59
# of the day preeceeding the date_end_excluded)
date_start: "__templated__"
date_end_excluded: "__templated__"
date_start_normalization: "__templated__"
date_end_normalization_excluded: "__templated__"

# ----------------------------------------------------------------
#                               METRICS
# ----------------------------------------------------------------

# List of plugins to mine.
# Note that it is a dictionary where every key represents the name your plugin
# have and the value is a dictionary with:
# 'plugin_data_path': the hdfs location where the plugin is
# 'plugin_filter': the plugin filter to select the interesting data
selected_plugins:
  load_longterm:
    plugin_data_path: "/project/monitoring/collectd/load"
    plugin_filter: "value_instance == 'longterm'"
  contextswitch_involuntary:
    plugin_data_path: "/project/monitoring/collectd/cloud"
    plugin_filter: "type_instance == 'involuntary' and type == 'contextswitch'"
  disk_io_time:
    plugin_data_path: "/project/monitoring/collectd/cloud"
    plugin_filter:  "value_instance == 'io_time'"
  memory_free:
    plugin_data_path: "/project/monitoring/collectd/memory"
    plugin_filter: "type_instance == 'free'"
  disk_pending_operations:
    plugin_data_path: "/project/monitoring/collectd/cloud"
    plugin_filter: "type == 'pending_operations' and plugin == 'disk'"
  cpu_system:
    plugin_data_path: "/project/monitoring/collectd/cpu"
    plugin_filter: "type_instance == 'system'"
...
