#!/bin/bash

env

echo "ad_config_train values after jinja rendering $ad_config_train"
echo "ad_config_inference values after jinja rendering $ad_config_inference"
echo "ad_config_experiment values after jinja rendering $ad_config_experiment"

# This python code replaces the template file ad_config_file with 
# a new file that will be encoded and passed to the docker container
cat > prepare_json_config_from_template.py << 'EOF'
import json
import sys 
import yaml

ad_config_from_dag_train = json.loads(sys.argv[1])
ad_config_from_dag_inference = json.loads(sys.argv[2])
ad_config_from_dag_experiment = json.loads(sys.argv[3])
ad_config_ref_file_train = ad_config_from_dag_train["template_config_file"]
ad_config_ref_file_inference = ad_config_from_dag_inference["template_config_file"]
ad_config_ref_file_experiment = ad_config_from_dag_experiment["template_config_file"]

print("ad_config_ref_file_train %s" % ad_config_ref_file_train)
print("ad_config_from_dag_train dict %s" % ad_config_from_dag_train)
print("ad_config_ref_file_inference %s" % ad_config_ref_file_inference)
print("ad_config_from_dag_inference dict %s" % ad_config_from_dag_inference)
print("ad_config_ref_file_experiment %s" % ad_config_ref_file_experiment)
print("ad_config_from_dag_experiment dict %s" % ad_config_from_dag_experiment)

with open(ad_config_ref_file_train, "r", encoding="utf-8") as f:
    ad_config_ref_dict_train = yaml.safe_load(f.read())
    f.close()
with open(ad_config_ref_file_inference, "r", encoding="utf-8") as f:
    ad_config_ref_dict_inference = yaml.safe_load(f.read())
    f.close()
with open(ad_config_ref_file_experiment, "r", encoding="utf-8") as f:
    ad_config_ref_dict_experiment = yaml.safe_load(f.read())
    f.close()

print(ad_config_ref_dict_train)
print(ad_config_ref_dict_inference)
print(ad_config_ref_dict_experiment)

replaced_dict_train = dict( 
                list(ad_config_ref_dict_train.items()) 
              + list(ad_config_from_dag_train.items())
               )
replaced_dict_inference = dict( 
                list(ad_config_ref_dict_inference.items()) 
              + list(ad_config_from_dag_inference.items())
               )
replaced_dict_experiment = dict( 
                list(ad_config_ref_dict_experiment.items()) 
              + list(ad_config_from_dag_experiment.items())
               )
print("ad_final_config_train json.dumps\n%s" % json.dumps(replaced_dict_train,indent=4))
print("ad_final_config_inference json.dumps\n%s" % json.dumps(replaced_dict_inference,indent=4))
print("ad_final_config_experiment json.dumps\n%s" % json.dumps(replaced_dict_experiment,indent=4))

with open("ad_final_config_train.json","w") as f:
     f.write(json.dumps(replaced_dict_train))
     f.close()
with open("ad_final_config_inference.json","w") as f:
     f.write(json.dumps(replaced_dict_inference))
     f.close()
with open("ad_final_config_experiment.json","w") as f:
     f.write(json.dumps(replaced_dict_experiment))
     f.close()

# put in an environment variable the image to be used
with open("ad_running_image.txt","w") as f:
     f.write(replaced_dict_train["running_image"])
     f.close()
EOF

python3 prepare_json_config_from_template.py "$ad_config_train" "$ad_config_inference" "$ad_config_experiment"

echo "Verification: dumping again the replaced config"
echo -e "\n------- ad_final_config_train.json ----------\n"
cat ad_final_config_train.json
echo -e "\n------- ad_final_config_inference.json ----------\n"
cat ad_final_config_inference.json
echo -e "\n------- END ----------\n"
echo -e "\n------- ad_final_config_experiment.json ----------\n"
cat ad_final_config_experiment.json
echo -e "\n------- END ----------\n"
config_base_64_train=`cat ad_final_config_train.json| base64 -w 0`
config_base_64_inference=`cat ad_final_config_inference.json| base64 -w 0`
config_base_64_experiment=`cat ad_final_config_experiment.json| base64 -w 0`

running_image=`cat ad_running_image.txt`
echo -e "\nVerification: running_image is $running_image\n"

cat > docker_run_script.sh <<'EOF' 
#!/bin/bash
env
pwd;
id
export KRB5CCNAME
ls -l {{var.value.KRB5CCNAME}}
echo ${config_base_64_train} | base64 -d > ad_config_train_file.json;
echo ${config_base_64_inference} | base64 -d > ad_config_inference_file.json;
echo ${config_base_64_experiment} | base64 -d > ad_config_experiment_file.json;
echo -e "\n------- ad_config_train_file.json ----------\n"
cat ad_config_train_file.json;
echo -e "\n------------ END  ---------------------\n"
echo -e "\n------- ad_config_inference_file.json ----------\n"
cat ad_config_inference_file.json;
echo -e "\n------------ END  ---------------------\n"
echo -e "\n------- ad_config_experiment_file.json ----------\n"
cat ad_config_experiment_file.json;
echo -e "\n------------ END  ---------------------\n"

klist -c {{var.value.KRB5CCNAME}}; 

source set_spark_analytix.sh; 
ls /eos/project-i/it-cloud-data-analytics/       #FIXME remove hardcoded path

{{params.ad_task}};
EOF

script_base_64=`cat docker_run_script.sh| base64 -w 0`

echo -e "\n------- docker_run_script.sh ----------\n"
cat docker_run_script.sh
echo -e "\n------------ END  ---------------------\n"

echo -e "\n list of files generated in `pwd`\n"
ls -l `pwd`

echo -e "\n... and now docker ...\n"

sleep 5
docker run --rm --name={{ task_instance_key_str }} \
    --log-driver=fluentd --log-opt tag=data-analytics.manual \
    -e KRB5CCNAME={{var.value.KRB5CCNAME}} \
    -e config_base_64_train=${config_base_64_train} \
    -e config_base_64_inference=${config_base_64_inference} \
    -e config_base_64_experiment=${config_base_64_experiment} \
    -e script_base_64=${script_base_64} \
    -v {{var.value.KRB5CCNAME}}:{{var.value.KRB5CCNAME}} \
    -v /cvmfs:/cvmfs:shared \
    -v /eos/project-i/it-cloud-data-analytics/Airflow/v0.5:/eos/project-i/it-cloud-data-analytics/Airflow/v0.5 \
    --net host \
    ${running_image} \
    bash -c 'cd; echo ${script_base_64} | base64 -d > docker_run_script.sh; chmod a+x docker_run_script.sh; ./docker_run_script.sh;'


