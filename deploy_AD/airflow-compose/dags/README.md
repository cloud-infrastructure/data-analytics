# Available Airflow DAGs (or Pipelines)

**work in progress**

## DAG description

We have four main types of DAGs representing the three steps in the benchmark procedure plus a combination suitable for continuous monitoring:
1. batch_1_etl / shared_1_etl : it downloads the data periodically from HDFS, aggregate them and save them in EOS. You control what to download via the two configuration yaml files: CONFIG_TEMPLATE_FILE_NAME_TEST and CONFIG_TEMPLATE_FILE_NAME_TRAIN. Note that this is done in Spark and the number of concurrent DAG you run influences the memory of the local VM, so be careful not to run too many of them concurrently (max_active_runs parameter of the dag).
1. batch_2_experiment / shared_2_experiment : it reads the EOS data and run the predefined algorithms in the analysis.yaml file (refer to CONFIG_TEMPLATE_ANALYSIS variable). This step can be on the current VM or on the K8s cluster by using the Airflow operators, DockerToPythonOperator and PythonInK8sOperator respectively. Note that this step publish data both in the MONIT infrastructure (via fluentd) and in the score folder (typically in EOS) as specified in the analysis.yaml file (refer to CONFIG_TEMPLATE_ANALYSIS variable).
1. batch_3_evaluation / shared_3_evaluation : it read the locally produced scores (in EOS), downloads the labeled data from Grafana and compute the performance of every algorithm in the benchmark period. It produces the results in the form of diagrams in the relative result folder defined in the analysis.yaml file (refer to CONFIG_TEMPLATE_ANALYSIS variable).
1. batch_4_always_on / shared_4_always_on: One additional kind of pipeline combines the previous two steps (ETL + production of scores in the MONIT) and run continuously.


## Our Operators know nothing about the AD algorithms!

We exploit the **BashOperator** to run a docker container, generally based on the **sparknotebook** container available in the gitlab registry of this same project.
All the intelligence implemented in the **adcern lib** runs in the *sparknotebook* container. This choice allow to run the same processes independently from Airflow. Pipelines can be therefore implemented and tested outside Airflow, and in particular can be also tested in Jupyter notebooks, if that notebook starts from the image *sparknotebook*. An example of this approach is provided in the CI [test](../../../tests/adcern/integration) of adcern.

The reason why we don't use the [official DockerOperator](https://airflow.apache.org/docs/stable/_api/airflow/operators/docker_operator/index.html) of Airflow, and we prefer to pass the *docker run* command to the BashOperator via a [script](./scripts/anomaly_detection_tasks.sh) is due to a limitation of the DockerOperator:<br>
the **--log-driver** Docker attribute is not supported. We use the log-driver to configure the **fluentd logging** service that collects the anomaly detection scores. Implementing directly the docker run command we can configure . Every Docker container will log its output to the [Fluentd daemon](https://docs.docker.com/config/containers/logging/fluentd/) so that results can be then redirected wherever you want (e.g. Elasticsearch of the Cern MONIT Infrastructure).

In order to guarantee the authentication to the **Cern Spark cluster** and to **EOS** the Kerberos ticket cache is percolated from the Airflow docker-compose to the running *sparknotebook* container.


Note:
- Documentation about docker operator can be found at
  - https://airflow.apache.org/docs/stable/_modules/airflow/operators/docker_operator.html#DockerOperator
  - https://marclamberti.com/blog/how-to-use-dockeroperator-apache-airflow/
- Docker swarm operator
  - https://airflow.apache.org/docs/stable/_modules/airflow/contrib/operators/docker_swarm_operator.html#DockerSwarmOperator._run_image


# Run on Kubernetes (THIS PART REFERS TO OLD APPROACH. TO BE REVIEWED)

Steps to run algorithm train on the Kubernetes (K8s) cluster are described here
1. Set up k8s cluster, following documentation reported [here](../k8s/README.md) 
2. Give instruction to Airflow how to reach the cluster via the configuration file (under the hood it uses the [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/) command). In this way the container that run Airflow (scheduler, etc.) can access the cluster.
<br> In the **secret.sh** file of airflow containing all pass and insert this environment variable
```
export KUBECONFIG=<absolute_path_where_you_created_the_config_file>/config
```
3. Use Pod operators<br>
In your DAG you will then be able to run all the Pod Operators you want. (Remeber to restart your docker compose so that Airflow can pick up the new secrets).<br>
   - Example of pod operator in your DAG:
```python
t_bash_ls = KubernetesPodOperator(namespace='default',
                                    image="python:3.6-stretch",
                                    cmds=["sh", "-c"],
                                    arguments=["ls /sys"],
                                    #cmds=["python","-c"],
                                    #arguments=["print('hello world')"],
                                    labels={"foo": "bar"},
                                    name="bash-ls-task",
                                    task_id="bash-ls-task",
                                    is_delete_operator_pod=True,
                                    in_cluster=False,
                                    dag=dag
                                    )
```

   - Example of pod operator in your DAG using EOS.
Note that you need to run a Helm chart developed by Spyros and Ricardo here (https://gitlab.cern.ch/helm/charts/cern/-/tree/master/eosxd).

```python

from kubernetes.client import models as k8s

# - CREATE VOLUME
# IT MUST HAVE BEEN ALREADY CREATED BY THE HELM OF EOS
# https://gitlab.cern.ch/helm/charts/cern/-/tree/master/eosxd

# - DEFINE VOLUME
# This is needed to have a reference to pass to the PodOperator
# so that it knows which volume to connect.
volume_eos = k8s.V1Volume(
    name='eos',
    host_path=k8s.V1HostPathVolumeSource(path='/var/eos')
)


# - VOLUME MOUNTING
eos_volume_mount = k8s.V1VolumeMount(mount_path='/eos',
                                     name='eos',
                                     mount_propagation='HostToContainer')

# - SECURITY
secrity_eos = k8s.V1PodSecurityContext(
    se_linux_options=k8s.V1SELinuxOptions(
        type='spc_t'
    )
)

t_eos_ls = KubernetesPodOperator(namespace='default',
                                    image="python:3.6-stretch",
                                    cmds=["sh", "-c"],
                                    arguments=["ls /eos"],
                                    name="eos-ls-task",
                                    task_id="eos-ls-task",
                                    is_delete_operator_pod=True,
                                    in_cluster=False,
                                    volumes=[volume_eos],
                                    volume_mounts=[eos_volume_mount],
                                    security_context=secrity_eos,
                                    dag=dag
                                    )
```

4. Run EOS in K8s
To use eos in K8s you need to have the be authenticated in the kinit.
The proper way to do that in K8s is using the secrets.
<br>
Follow these steps:
   1. Install the following Helm chart on the K8s so that every container in the cluster can access the EOS folders it is allowed to see:
https://gitlab.cern.ch/helm/charts/cern/-/tree/master/eosxd
   1. Create a secret definition a file named **user_secret.yaml** with this content:
    ```
    apiVersion: v1
    kind: Secret
    metadata:
    name: my-kinit-secret
    type: kubernetes.io/basic-auth
    stringData:
    username: <your-username-kinit>
    password: <your-password>
    ```
   1. Save the secret in the cluster k8s, by running:
    ```
    kubectl apply -f user_secret.yaml
    ```
