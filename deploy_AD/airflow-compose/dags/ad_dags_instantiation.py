from airflow.models import DAG, Variable
import ad_dags_definition as ADdags
from datetime import timedelta, datetime

import pendulum
local_tz = pendulum.timezone("Europe/Amsterdam")

# Batch -> "bi/condor/tzero/ams", "bi/condor/tzero/atlas", "bi/condor/tzero/cms"

#-------------------------------------------------------------------------------------------------------------------
# HELLO WORLD

dag_override_params={
    'default_args': {
        'start_date': datetime(2022, 2, 8, 15, 10, tzinfo=local_tz),
        },
    'schedule_interval': '*/10 * * * *', # Run every 10 minutes
    'tags': ['Hello_World'] 
}

dag_id = 'Hello_World'
print("[ad_dags_instantiation] creating dag with dag_id %s" % dag_id)

globals()[dag_id] = ADdags.hello_world_dag(dag_id, dag_override_params)

#-------------------------------------------------------------------------------------------------------------------
# ALL THE HOSTGROUPS ONE MODEL TRAINED ON GVA_SHARED_012
SINGLE_DAGS = False
if SINGLE_DAGS:
    try:
        hostgroups =  Variable.get("hostgroups", deserialize_json=True)
        print("[ad_dags_instantiation] issue in loading variable hostgroups")
    except:
        hostgroups = []
        for i in range(9, 27):
            hostgroups.append("cloud_compute/level2/main/gva_shared_" + "%03d" % (i,))

    print("[ad_dags_instantiation] list of hostgroups to process %s" % hostgroups)
    for hostgroup in hostgroups:
        short_hostgroup = hostgroup.split('/')[-1]
        dag_override_params={
        'default_args': {
            'ad_config_train': {
                "hostgroups": ["cloud_compute/level2/main/gva_shared_012"]
            },
            'ad_config_inference': {
                "hostgroups": [hostgroup]
            },
            'ad_config_experiment': {
                "hostgroup_abs_path": hostgroup,
                "publish_per_windows": 5,
                "algo_and_params": {
                    "IForest_200_shared_012": {
                        "import_path": "pyod.models.iforest.IForest",
                        "family": "Traditional",
                        "train_on_test": False,
                        "subsample_for_train": -1,
                        "parameters": {
                            "n_estimators": 200,
                            "max_samples": 0.9,
                            "random_state": 42
                        }
                    }
                }
            },
            'start_date': datetime(2022, 9, 1, tzinfo=local_tz),
            },
        'schedule_interval': '0 */8 * * *',
        'tags': ['etl_' + short_hostgroup + 'SINGLE'] 
        }
        end_date = datetime(2022, 5, 12)      
        delta = timedelta(days=7)
        start = str(end_date - delta)[:10]
        end = str(end_date)[:10]
        dag_override_params['default_args']['ad_config_train']['code_project_name'] = '_Airflow_{{dag.dag_id}}_' + end
        dag_override_params['default_args']['ad_config_train']["date_start"] = start
        dag_override_params['default_args']['ad_config_train']["date_end_excluded"] = end
        dag_override_params['default_args']['ad_config_train']["date_start_normalization"] = start
        dag_override_params['default_args']['ad_config_train']["date_end_normalization_excluded"] = end
        dag_override_params['default_args']['ad_config_inference']["date_start_normalization"] = start
        dag_override_params['default_args']['ad_config_inference']["date_end_normalization_excluded"] = end
        dag_override_params['default_args']['ad_config_inference']["date_start"] = '{{ macros.ds_add(execution_date.in_timezone(\'Europe/Amsterdam\').strftime(\'%Y-%m-%d\'), -1) }}'
        dag_override_params['default_args']['ad_config_inference']["date_end_excluded"] = '{{ macros.ds_add(execution_date.in_timezone(\'Europe/Amsterdam\').strftime(\'%Y-%m-%d\'), 0) }}'
        dag_id = 'AD_' + short_hostgroup + '_SINGLE'
        print("[ad_dags_instantiation] creating dag with dag_id %s" % dag_id)
        globals()[dag_id] = ADdags.ad_dag(dag_id, dag_override_params)

#-------------------------------------------------------------------------------------------------------------------
# One DAG for all the cloud.

dag_override_params={
'default_args': {
    'ad_config_train': {
        "hostgroups": ["cloud_compute/level2/main/gva_shared_[0-9]+$"]
    },
    'ad_config_inference': {
        "hostgroups": ["cloud_compute/level2/main/gva_shared_[0-9]+$"]
    },
    'ad_config_experiment': {
        "hostgroup_abs_path": "cloud_compute/level2/main/gva_shared_[0-9]+$",
        "publish_per_windows": 20,
        "algo_and_params": {
            "IForest_200_ALL_CLOUD": {
                "import_path": "pyod.models.iforest.IForest",
                "family": "Traditional",
                "train_on_test": False,
                "subsample_for_train": -1,
                "parameters": {
                    "n_estimators": 200,
                    "max_samples": 0.9,
                    "random_state": 42
                }
            },
            "AE_GRU_ALL_CLOUD": {
                "import_path": "adcern.analyser_deep.AEGruTF2",
                "family": "Deep",
                "train_on_test": False,
                "subsample_for_train": -1,
                "parameters": {
                    "nr_timesteps": 8,
                    "gru_units": 1,
                    "epochs": 50,
                    "verbose": 1,
                    "dropout_rate": 0.25,
                }
            },
            "AE_LSTM_ALL_CLOUD": {
                "import_path": "adcern.analyser_deep.AELstmTF2",
                "family": "Deep",
                "train_on_test": False,
                "subsample_for_train": -1,
                "parameters": {
                    "nr_timeseries": 6,
                    "nr_timesteps": 8,
                    "epochs": 50,
                    "verbose": 1,
                    "nr_lstm_units": 1,
                    "dropout_rate": 0.25,
                }
            }
        }
    },
    'start_date': datetime(2022, 2, 15, tzinfo=local_tz),
    },
'schedule_interval': '0 */8 * * *', # Run every 8 hours
    'tags': ['AD_' + "IFOR_GRU_LSTM_" + 'ALL_CLOUD'] 
}

end_date = datetime(2022, 8, 1)      


delta = timedelta(days=30)
start = str(end_date - delta)[:10]
end = str(end_date)[:10]

dag_override_params['default_args']['ad_config_train']['code_project_name'] = '_Airflow_{{dag.dag_id}}_' + end
dag_override_params['default_args']['ad_config_train']["date_start"] = start
dag_override_params['default_args']['ad_config_train']["date_end_excluded"] = end
dag_override_params['default_args']['ad_config_train']["date_start_normalization"] = start
dag_override_params['default_args']['ad_config_train']["date_end_normalization_excluded"] = end
dag_override_params['default_args']['ad_config_inference']["date_start_normalization"] = start
dag_override_params['default_args']['ad_config_inference']["date_end_normalization_excluded"] = end

# We want to run Airflow every day but using data of the previous day, so let's change the dates of inference
dag_override_params['default_args']['ad_config_inference']["date_start"] = '{{ macros.ds_add(execution_date.in_timezone(\'Europe/Amsterdam\').strftime(\'%Y-%m-%d\'), -1) }}'
dag_override_params['default_args']['ad_config_inference']["date_end_excluded"] = '{{ macros.ds_add(execution_date.in_timezone(\'Europe/Amsterdam\').strftime(\'%Y-%m-%d\'), 0) }}'

dag_id = 'AD_' + "IFOR_GRU_LSTM_" + 'ALL_CLOUD'
dag_obj = 'AD_' + "IFOR_GRU_LSTM_" + 'ALL_CLOUD'
print("[ad_dags_instantiation] creating dag with dag_id %s" % dag_id)


globals()[dag_id] = ADdags.ad_dag(dag_id, dag_override_params) # New Operational DAG

dag_override_params['default_args']['start_date'] =  datetime(2022, 9, 1, tzinfo=local_tz)
dag_override_params['default_args']['end_date'] =  datetime(2024, 2, 16, tzinfo=local_tz)
dag_id = 'AD_' + "IFOR_GRU_LSTM_" + 'ALL_CLOUD_OLD_DATA'
globals()[dag_id] = ADdags.ad_dag(dag_id, dag_override_params) # OLD Data DAG
