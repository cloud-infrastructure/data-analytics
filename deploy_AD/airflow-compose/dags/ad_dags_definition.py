from airflow.models import DAG
from airflow.utils.dates import days_ago
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.utils.state import State
from airflow.utils.task_group import TaskGroup

from builtins import range
from datetime import timedelta, datetime

import json
from mergedeep import merge, Strategy


def final_status(**kwargs):
    print(kwargs)
    for task_instance in kwargs['dag_run'].get_task_instances():
        print("task_id %30s \t current_state  %20s" % (task_instance.task_id,task_instance.current_state()))
        
    this_task = kwargs['task']
    print('this task_id is %s \t upstream task_ids %s' % (this_task.task_id, this_task.upstream_task_ids))
    
    total_upstreams = len(this_task.upstream_task_ids)
    counter=0
    for task_instance in kwargs['dag_run'].get_task_instances():
        if task_instance.task_id in this_task.upstream_task_ids and \
           task_instance.current_state() != State.SUCCESS:
            print('Checking parents: parent with task_id %s has current_state %s' % (task_instance.task_id, 
                                                                task_instance.current_state()))
            counter+=1
            if counter == total_upstreams:  #It means all upstream failed
                raise Exception("Task {} has collected failures. Failing this DAG run".format(task_instance.task_id))
    return True

def ad_dag(dag_id='give_me_a_name', override_params={}):
    ''' 
    default_params: DAG default parameters passed to DAGs 
    instantiated from this function
    
    override_params: allows to pass only the parameters 
                     that will be replaced respect to the default

    ad_config: default_params include the configuration data for the
               Anomaly Detection libs that will start via docker submission
    '''
    default_params={
        'default_args': {
            'owner': 'Airflow',
            'start_date': datetime(2023, 1, 1),
            # 'end_date': datetime(2023, 4, 1), # We don't set end_time so AirFlow runs always
            'ad_config_train': {
                'running_image': 'gitlab-registry.cern.ch/cloud-infrastructure/data-analytics/sparknotebook:qa-v0.5',

                'template_config_file': '/opt/airflow/dags/scripts/anomaly_detection_tasks_cfg_train.yml',
                'hostgroups': ['cloud_compute/level2/batch/gva_project_013'],
                'code_project_name': 'Airflow_{{dag.dag_id}}_{{execution_date.in_timezone(\'Europe/Amsterdam\').strftime(\'%Y-%m-%d\')}}',
                'local_cache_folder': '/eos/project-i/it-cloud-data-analytics/Airflow/v0.5/{{dag.dag_id}}/local_cache_train/',
                'hdfs_out_folder': '/project/it_cloud_data_analytics/Airflow/v0.5/{{dag.dag_id}}/raw_parquet_train/',
                'hdfs_cache_folder': '/project/it_cloud_data_analytics/Airflow/v0.5/{{dag.dag_id}}/compressed_train/',
                'normalization_out_folder': '/project/it_cloud_data_analytics/Airflow/v0.5/{{dag.dag_id}}/normalization',
                'date_start': '{{ macros.ds_add(ds, -7) }}',
                'date_end_excluded': '{{ ds }}',
                'date_start_normalization': '{{ macros.ds_add(ds, -7) }}',
                'date_end_normalization_excluded': '{{ ds }}',
            },
            'ad_config_inference': {
                'template_config_file': '/opt/airflow/dags/scripts/anomaly_detection_tasks_cfg_inference.yml',
                'hostgroups': ['cloud_compute/level2/batch/gva_project_013'],
                'code_project_name': 'Airflow_{{dag.dag_id}}_{{execution_date.in_timezone(\'Europe/Amsterdam\').strftime(\'%Y-%m-%d\')}}',
                'local_cache_folder': '/eos/project-i/it-cloud-data-analytics/Airflow/v0.5/{{dag.dag_id}}/local_cache_inference/',
                'hdfs_out_folder': '/project/it_cloud_data_analytics/Airflow/v0.5/{{dag.dag_id}}/raw_parquet_inference/',
                'hdfs_cache_folder': '/project/it_cloud_data_analytics/Airflow/v0.5/{{dag.dag_id}}/compressed_inference/',
                'normalization_out_folder': '/project/it_cloud_data_analytics/Airflow/v0.5/{{dag.dag_id}}/normalization',
                'date_start': '{{ macros.ds_add(ds, -7) }}',
                'date_end_excluded': '{{ ds }}',
                'date_start_normalization': '{{ macros.ds_add(ds, -7) }}',
                'date_end_normalization_excluded': '{{ ds }}',
            },
            'ad_config_experiment': {
                'template_config_file': '/opt/airflow/dags/scripts/anomaly_detection_tasks_cfg_experiment.yaml',
                'folder_time': '/eos/project-i/it-cloud-data-analytics/Airflow/v0.5/{{dag.dag_id}}/time/',
                'folder_models': '/eos/project-i/it-cloud-data-analytics/Airflow/v0.5/{{dag.dag_id}}/models/',
                'folder_local_scores': '/eos/project-i/it-cloud-data-analytics/Airflow/v0.5/{{dag.dag_id}}/scores/',
                'hostgroup_abs_path': 'cloud_compute/level2/batch/gva_project_013'
            },
        },
        'schedule_interval': '0 0 1 * *',
        'max_active_runs': 1, # number active runs
        'concurrency': 1, # total tasks concurrency 
        'dagrun_timeout': timedelta(minutes=120),
        'tags': ['Airflow_Production'],
       
    }

    # Merging/replacing dag arguments
    # in particular the ones that will 
    # be passed to the Bash Operator
    dag_args = merge({'dag_id':dag_id},
                     default_params,
                     override_params)

    print('DAG Args %s' % dag_args)

    # Create the DAG with the dictionary of args
    dag = DAG(**dag_args)

    dag_exit_status = PythonOperator(task_id='dag_exit_status', 
                                     provide_context=True,
                                     python_callable=final_status,
                                     trigger_rule='all_done',
                                     dag=dag)

    def return_configured_BashOperator(task_id='give_me_a_task_id', ad_task='define_a_task',trigger_rule='all_success', task_group=None):
        return BashOperator(
            task_id=task_id,
            bash_command='scripts/anomaly_detection_tasks.sh',
            # Here I make sure that the values of ad_config become env vars 
            # so that the env
            env={'ad_config_train': '%s' % json.dumps(dag_args['default_args']['ad_config_train']),
                 'ad_config_inference': '%s' % json.dumps(dag_args['default_args']['ad_config_inference']),
                 'ad_config_experiment': '%s' % json.dumps(dag_args['default_args']['ad_config_experiment'])}, 
            params={'ad_task': ad_task},
            trigger_rule=trigger_rule,
            task_group=task_group,
            dag=dag,
            task_concurrency=3
        )

    # List of tasks to define based on a BashOperator
    # Each entry includes the tuple: (task_id, step of the AD pipeline to call from inside the BashOperator script, trigger rule) 
    ad_tasks_train = [
        ('check_local_data_presence_TRN',    'data_mining data_presence          --resource_file ad_config_train_file.json',          'all_success'),
        ('spark_check_norm_presence_TRN',    'data_mining normalization_presence --resource_file ad_config_train_file.json',           'all_failed'),
        ('spark_compute_norm_TRN',           'data_mining compute_normalization  --resource_file ad_config_train_file.json',           'all_failed'),
        ('spark_transform_data_TRN',         'data_mining transform_data         --resource_file ad_config_train_file.json',          'one_success'),
        ('spark_mv_data_to_local_TRN',       'data_mining copy_locally           --resource_file ad_config_train_file.json',          'all_success'),
    ]

    ad_task_train_models = [
        ('train_models',                     'data_mining training               --analysis_file_path ad_config_experiment_file.json', 'one_success')
    ]

    ad_tasks_inference = [
        ('check_local_data_presence_INF',    'data_mining data_presence          --resource_file ad_config_inference_file.json',       'one_success'),
        ('spark_check_norm_presence_INF',    'data_mining normalization_presence --resource_file ad_config_inference_file.json',        'all_failed'),
        ('spark_compute_norm_INF',           'data_mining compute_normalization  --resource_file ad_config_inference_file.json',        'all_failed'),
        ('spark_transform_data_INF',         'data_mining transform_data         --resource_file ad_config_inference_file.json',       'one_success'),
        ('spark_mv_data_to_local_INF',       'data_mining copy_locally           --resource_file ad_config_inference_file.json',       'all_success'),
    ]

    ad_task_inference_scores = [
        ('inference_scores',                 'data_mining inference              --analysis_file_path ad_config_experiment_file.json', 'one_success')
    ]

    # Instantiate the AD tasks
    tg_train = TaskGroup(group_id='TRN', dag=dag)
    tg_train_models = TaskGroup(group_id='TRN_', dag=dag)
    tg_inference = TaskGroup(group_id='INF', dag=dag)   
    tg_inference_scores = TaskGroup(group_id='INF_', dag=dag)

    tg_train >> tg_train_models >> tg_inference >> tg_inference_scores

    for atask in ad_tasks_train:
        globals()[atask[0]] = return_configured_BashOperator(*atask, task_group=tg_train)

    for atask in ad_task_train_models:
        globals()[atask[0]] = return_configured_BashOperator(*atask, task_group=tg_train_models)

    for atask in ad_tasks_inference:
        globals()[atask[0]] = return_configured_BashOperator(*atask, task_group=tg_inference)

    for atask in ad_task_inference_scores:
        globals()[atask[0]] = return_configured_BashOperator(*atask, task_group=tg_inference_scores)

    # Define the task dependency flow

    # --------------------------------------------------------------------------TRAIN
    # Start checking the local data presence and in case break pipeline
    check_local_data_presence_TRN >> train_models
    # Otherwise if datapresence fails, check the normalization presence
    check_local_data_presence_TRN >> spark_check_norm_presence_TRN
    # if missing the normalization compute it and then download data 
    spark_check_norm_presence_TRN >> spark_compute_norm_TRN >> spark_transform_data_TRN
    # if normalization presence succeeds start immediately downloading data
    spark_check_norm_presence_TRN >> spark_transform_data_TRN
    # Finally move data to local folder (can also be eos)
    spark_transform_data_TRN >> spark_mv_data_to_local_TRN

    spark_mv_data_to_local_TRN >> train_models

    # ---------------------------------------------------------------------TRAIN MODEL
    train_models >> check_local_data_presence_INF

    # -----------------------------------------------------------------------INFERENCE
    # Start checking the local data presence and in case break pipeline
    check_local_data_presence_INF >> inference_scores
    # Otherwise if datapresence fails, check the normalization presence
    check_local_data_presence_INF >> spark_check_norm_presence_INF
    # if missing the normalization compute it and then download data 
    spark_check_norm_presence_INF >> spark_compute_norm_INF >> spark_transform_data_INF
    # if normalization presence succeeds start immediately downloading data
    spark_check_norm_presence_INF >> spark_transform_data_INF
    # Finally move data to local folder (can also be eos)
    spark_transform_data_INF >> spark_mv_data_to_local_INF

    spark_mv_data_to_local_INF >> inference_scores 

    # ----------------------------------------------------------------INFERENCE SCORES
    inference_scores >> dag_exit_status

    return dag

def hello_world_dag(dag_id='hello_world', override_params={}):
    default_params={
        'default_args': {
            'owner': 'Airflow',
            'start_date': datetime(2023, 1, 1),
        },
        'schedule_interval': '0 0 1 * *',
        'max_active_runs': 1, # number active runs
        'concurrency': 1, # total tasks concurrency 
        'dagrun_timeout': timedelta(minutes=120),
        'tags': ['hello_world'],
       
    }

    dag_args = merge({'dag_id':dag_id},
                     default_params,
                     override_params)

    print('DAG Args %s' % dag_args)

    dag = DAG(**dag_args)

    dag_exit_status = PythonOperator(task_id='dag_exit_status', 
                                     provide_context=True,
                                     python_callable=final_status,
                                     trigger_rule='all_done',
                                     dag=dag)
    
    task = ('hello_world',    'python3 -c \'print("hello world")\'')

    globals()[task[0]] = BashOperator(task_id=task[0], bash_command=task[1], dag=dag, task_concurrency=1)

    hello_world >> dag_exit_status

    return dag
