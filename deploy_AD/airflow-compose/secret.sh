#NB:
# In these examples there are dummy passwords, in the secret.sh file
# those passwords are here only as example for a simple local test.

# !!! Do not commit real production passwords !!!


###############################
#   FOLDERS
###############################
AD_STORE_DIR=/tmp/ad_system_store
AD_LOGS_PATH=${AD_STORE_DIR}/logs
AD_DATALAKE_DIR=${AD_STORE_DIR}/datalake/project_cache
AD_CONFIG_DIR=${AD_STORE_DIR}/configurations
AD_EOS_BASE_DIR=/eos/project/i/it-cloud-data-analytics/

# This is temporary needed to understand how to dispatch
# utility functions used by DAGs that are not in a lib
# and need to be shared via EOS
AD_DEPLOYMENTS_CACHE=${AD_EOS_BASE_DIR}/deployments

DAG_PATH=${AD_SOURCE_DIR}/dags

###############################
#   IMAGE TAGS
###############################
FLUENTD_IMAGE_TAG=qa-v0.5
SPARKNB_IMAGE_TAG=qa-v0.5
CVMFS_IMAGE_TAG=v1.0
AIRFLOW_IMAGE_TAG=qa-v0.5

###############################
#   AUTHENTICATION
###############################

# Kerberos ticket cache
KRB5CCNAME=/tmp/krb5cc_anomaly_detection

# Secret to encrypt
# Secret key to save connection passwords in the db.
# To generate a key look at # https://bcb.github.io/airflow/fernet-key
FERNET_KEY=YYWRfGG7GpTkbCYUuSEv5ycpY5Rz_a2PC_vlTOInfkg=   # This is FAKE!!! Change it

_AIRFLOW_WWW_USER_USERNAME=airflow    # This is FAKE!!! Change it
_AIRFLOW_WWW_USER_PASSWORD=airflow    # This is FAKE!!! Change it

######################
# Postgres details

# From airflow.cfg:
# The Celery result_backend. When a job finishes, it needs to update the
# metadata of the job. Therefore it will post a message on a message bus,
# or insert it into a database (depending of the backend)
# This status is used by the scheduler to update the state of the task
# The use of a database is highly recommended
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#task-result-backend-settings

POSTGRES_USER="airflow"     # This is FAKE!!! Change it
POSTGRES_PASSWORD="myairflowpass" # This is FAKE!!! Change it
POSTGRES_DB="airflow"
POSTGRES_HOST="postgres"
POSTGRES_EXTRAS=""
POSTGRES_PORT=5432

####################
# Redis details

# From airflow.cfg:
# The Celery broker URL. Celery supports RabbitMQ, Redis and experimentally
# a sqlalchemy database. Refer to the Celery documentation for more
# information.
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#broker-settings

REDIS_PASSWORD="myredispass" # This is FAKE!!! Change it
REDIS_PORT="6379"
REDIS_DBNUM="1"


# Fix Redis
#srv_redis_1          | 1:M 16 Jun 2020 15:45:34.145 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
#srv_redis_1          | 1:M 16 Jun 2020 15:45:34.145 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after
# a reboot. Redis must be restarted after THP is disabled.

#######################
## K8s

# SECTION TO GIVE INSTRUCTION TO AIRFLOW ON HOW TO REACH YOUR KUBERNETES CLUSTER
# Insert the configuration to use KUBECTL from within the Airflow containers
# this config file is created with the following steps:
# 1. go to openstack.cern.ch, go to the deveoper project (because we have more computational power) and create a cluster named: my-cluster-name
# 2. download from the top right the .sh script to connect to that project (tool > Download Openstack RC V3 bla bla)
# 3. source <just_downloaded_script>.sh
# 4. use this command to create the config: openstack coe cluster config my-cluster-name
# 5. Refer to that config file (absolute path) in the KUBECONFIG environmental variable
# Note that to use openstack on the VM you need to download via pip the following packages
# pip install python-openstackclient
# pip install python-magnumclient
# SMALL CLUSTER
# KUBECONFIG=/opt/data_repo_volume/repositories/k8s/config
# BIG CLUSTER
#KUBECONFIG=/opt/data_repo_volume/repositories/k8s/big-anomaly/config
