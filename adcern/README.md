# Anomaly Detection @CERN Algorithms

Usually the models come from the pyod library, however in the project other models are used (AutoEncoder, Forecaster ...); you can find such models in this folder and use this space to create you own AD Machine Learning Model.