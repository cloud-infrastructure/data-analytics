"""Using Ensemble Methods for Outlier Detection."""
# Author: Matteo Paltenghi <matteo.paltenghi@cern.ch> # noqa

import scipy  # noqa
import numpy as np

from pyod.models.combination import aom  # noqa
from pyod.models.combination import moa  # noqa
from pyod.models.combination import average
from pyod.models.combination import maximization
from pyod.models.combination import median
from pyod.models.combination import majority_vote  # noqa

from adcern.analyser import BaseOutlierAnalyser


class Ensembler(BaseOutlierAnalyser):
    """Ensembler detector using many independent PyODWrapper detectors.

    Params
    ------
    analysers: list of pyod.models.base.BaseDetector, mandatory
        the detctors that you want to use, they must be PyODWrapper themselves
        so that they expose the necessary methods (e.g. predict_std)
    contamination: (int), default 0.1
        number of outliers expected to be found in the data
    strategy: Strategy object
        it determines with which strategy to aggregate the data.
    """

    def __init__(self, analysers=[], strategy=None):
        """Create the enesmbler with its detectors and strategy."""
        if strategy is None:
            raise Exception("Strategy must be set")
        if len(analysers) == 0:
            raise Exception("Can't proceed without analysers")
        self.analysers = analysers
        self.algo_names = \
            [algo.__repr__()[:algo.__repr__().find("(")] for algo in analysers]
        self.strategy = strategy

    def fit(self, X, prov, y=None, value_for_missing=0, forward_prov=False):
        """Fit the model using X as training data."""
        X_cleaned = X.fillna(value_for_missing)
        # fit all the algos
        for pyod_detector in self.analysers:
            if forward_prov:
                pyod_detector.fit(X_cleaned, prov=prov)
            else:
                pyod_detector.fit(X_cleaned)
        # compute the normalization coefficients
        # from the train outlier scores
        self._compute_all_coefficients_std_score()
        # train also the strategy (if it implements this ability)
        # e.g. simple linear regression with labels
        train_score_matrix = \
            self._predict_scores_matrix(X=X_cleaned, prov=prov)
        self.strategy.fit(X_scores=train_score_matrix, y=y)

    def _predict_scores_matrix(self, X, prov):
        """Create the matrix of scores given by the algos for this test data.

        score_matrix: numpy matrix (nr_records, nr_algos)
            it is already standardized with respect to algo specific
            coefficients.
        """
        arrays_of_scores = []
        for i_algo, pyod_detector in enumerate(self.analysers):
            algo_score = pyod_detector.decision_function(X)
            algo_score_std = \
                self.standardize_with_train(scores=algo_score,
                                            algo_index=i_algo)
            arrays_of_scores.append(np.array(algo_score_std))
        matrix = np.vstack(arrays_of_scores)
        score_matrix = matrix.T
        return score_matrix

    def predict(self, X, prov, y=None, value_for_missing=0):
        print("Calling predic_std,"
              "ensemble just have this standardized option")
        return self.predict_std(X=X, prov=prov, y=y,
                                value_for_missing=value_for_missing)

    def predict_std(self, X, prov, y=None, value_for_missing=0):
        """Predict the model using X as testig data."""
        print("NB: every algo is standardized w.r.t. itself. "
              "No standardization standardization is performed after the"
              "ensemble.")
        X_cleaned = X.fillna(value_for_missing)
        self.score_matrix = self._predict_scores_matrix(X=X_cleaned, prov=prov)
        print("score_matrix:", self.score_matrix.shape)
        self.std_scores = self.strategy.predict(X_scores=self.score_matrix)
        self.host_names = [h for h in prov.iloc[:, 0].values.flatten()]
        return self.std_scores

    def _compute_all_coefficients_std_score(self):
        """Compute the standardization coefficients form training."""
        self.mean_vector = []
        self.std_vector = []
        for pyod_detector in self.analysers:
            train_scores = pyod_detector.decision_scores_
            mean_train = np.mean(train_scores)
            std_train = np.std(train_scores)
            self.mean_vector.append(mean_train)
            self.std_vector.append(std_train)

    def standardize_with_train(self, scores, algo_index):
        """Standardize with the training coefficients.

        Algo index is the index of the algorithm in the list self.alaysers
        """
        mean_train = self.mean_vector[algo_index]
        std_train = self.std_vector[algo_index]
        return (scores - mean_train) / std_train

    def get_scores_and_ranking_of_algo(self, algo_index):
        """Get the scores and ranking computed with the given algorithm.

        Algo index is the index of the algorithm in the list self.alaysers
        """
        outlier_scores = list(self.score_matrix[:, algo_index])
        ranking = np.argsort(outlier_scores)
        return outlier_scores, ranking

    def get_single_algorithms(self):
        """Get the detectors part of the ensemble, original object exposed."""
        return self.analysers

    def get_detectors_names(self):
        """Get the names of the detectors in the ensemble."""
        return self.algo_names

    def __repr__(self):
        """Create representation based on PyOD object."""
        representation = ("Ens-%s (%s)" %
                          (self.strategy, ", ".join(self.algo_names)))
        return representation


class Strategy(object):

    def __init__(self):
        """Initialize a strategy."""
        pass

    def fit(self, X_scores, y=None):
        """Fit the strategy via a learning prcedure.

        Given the scores of the single detectors learn how to assemble them.
        Params
        ------
        X_scores: numpy array (nr_samples, nr_detectors) mandatory
            for each example we have the score given by the single detector.
        y: numpy array (nr_samples, ) optional
            annotated labels to know it the sample is or not an anomaly.
        """
        pass

    def predict(self, X_scores):
        """Apply the strategy and aggregate the anomaly score.

        Params
        ------
        X_scores: numpy array (nr_samples, nr_detectors) mandatory
            for each example we have the score given by the single detector.
        """
        pass


class MaxStrategy(Strategy):

    def predict(self, X_scores):
        """Take the maximum of the anomaly scores.

        Params
        ------
        X_scores: numpy array (nr_samples, nr_detectors) mandatory
            for each example we have the score given by the single detector.
        """
        outlier_scores = maximization(X_scores)
        return outlier_scores

    def __repr__(self):
        """Return representation."""
        return "Max"


class MedianStrategy(Strategy):

    def predict(self, X_scores):
        """Take the median of the anomaly scores.

        Params
        ------
        X_scores: numpy array (nr_samples, nr_detectors) mandatory
            for each example we have the score given by the single detector.
        """
        outlier_scores = median(X_scores)
        return outlier_scores

    def __repr__(self):
        """Return representation."""
        return "Med"


class AveragingStrategy(Strategy):

    def predict(self, X_scores):
        """Take the average of the anomaly scores.

        Params
        ------
        X_scores: numpy array (nr_samples, nr_detectors) mandatory
            for each example we have the score given by the single detector.
        """
        outlier_scores = average(X_scores)
        return outlier_scores

    def __repr__(self):
        """Return representation."""
        return "Avg"
