"""Using Deep Auto Encoder with Outlier Detection."""
# Author: Matteo Paltenghi <matteo.paltenghi@cern.ch> # noqa

import scipy  # noqa
import numpy as np
import random
import keras.backend as K

# new
import tensorflow as tf  # noqa
from tensorflow.keras.losses import mean_squared_error
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.regularizers import l2
from tensorflow.keras.callbacks import EarlyStopping

from sklearn.preprocessing import StandardScaler
from sklearn.utils import check_array
from sklearn.utils.validation import check_is_fitted

from pyod.models.base import BaseDetector
from pyod.utils.utility import check_parameter
from pyod.utils.stat_models import pairwise_distances_no_broadcast

# LSTM
from tensorflow.keras.models import Model
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import TimeDistributed

# CNN
from tensorflow.keras.models import Model  # noqa
from tensorflow.keras.layers import Input  # noqa
from tensorflow.keras.layers import Conv2D  # noqa
from tensorflow.keras.layers import MaxPooling2D  # noqa
from tensorflow.keras.layers import UpSampling2D  # noqa

# GRU
from tensorflow.keras.models import Model
from tensorflow.keras.layers import GRU
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import TimeDistributed

def fix_seeds(seed):
    random.seed(seed)
    np.random.seed(seed)
    tf.random.set_seed(seed)
    #session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
    #sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
    #K.set_session(sess)  

class AEDenseTF2(BaseDetector):

    def __init__(self, hidden_neurons=None,
                 hidden_activation='relu', output_activation='sigmoid',
                 loss=mean_squared_error, optimizer='adam',
                 epochs=100, batch_size=32, dropout_rate=0.2,
                 l2_regularizer=0.1, validation_size=0.1, preprocessing=True,
                 verbose=1, random_state=None, contamination=0.1):
        """Autoencoder compatible with Tensorflow 2.

        Auto Encoder (AE) is a type of neural networks for learning useful data
        representations unsupervisedly. Similar to PCA, AE could be used to
        detect outlying objects in the data by calculating the reconstruction
        errors.

        Parameters
        ----------
        hidden_neurons : list, optional (default=[64, 32, 32, 64])
            The number of neurons per hidden layers.
        hidden_activation : str, optional (default='relu')
            Activation function to use for hidden layers.
            All hidden layers are forced to use the same type of activation.
            See https://keras.io/activations/
        output_activation : str, optional (default='sigmoid')
            Activation function to use for output layer.
            See https://keras.io/activations/
        loss : str or obj, optional (default=keras.losses.mean_squared_error)
            String (name of objective function) or objective function.
            See https://keras.io/losses/
        optimizer : str, optional (default='adam')
            String (name of optimizer) or optimizer instance.
            See https://keras.io/optimizers/
        epochs : int, optional (default=100)
            Number of epochs to train the model.
        batch_size : int, optional (default=32)
            Number of samples per gradient update.
        dropout_rate : float in (0., 1), optional (default=0.2)
            The dropout to be used across all layers.
        l2_regularizer : float in (0., 1), optional (default=0.1)
            The regularization strength of activity_regularizer
            applied on each layer. By default, l2 regularizer is used. See
            https://keras.io/regularizers/
        validation_size : float in (0., 1), optional (default=0.1)
            The percentage of data to be used for validation.
        preprocessing : bool, optional (default=True)
            If True, apply standardization on the data.
        verbose : int, optional (default=1)
            Verbosity mode.
            - 0 = silent
            - 1 = progress bar
            - 2 = one line per epoch.
            For verbosity >= 1, model summary may be printed.
        random_state : random_state: int, RandomState instance or None,
            optional (default=None)
            If int, random_state is the seed used by the random
            number generator; If RandomState instance, random_state is the
            random number generator; If None, the random number generator is
            the RandomState instance used by `np.random`.
        contamination : float in (0., 0.5), optional (default=0.1)
            The amount of contamination of the data set, i.e.
            the proportion of outliers in the data set. When fitting this is
            used to define the threshold on the decision function.
        Attributes
        ----------
        encoding_dim_ : int
            The number of neurons in the encoding layer.
        compression_rate_ : float
            The ratio between the original feature and
            the number of neurons in the encoding layer.
        model_ : Keras Object
            The underlying AutoEncoder in Keras.
        history_: Keras Object
            The AutoEncoder training history.
        decision_scores_ : numpy array of shape (n_samples,)
            The outlier scores of the training data.
            The higher, the more abnormal. Outliers tend to have higher
            scores. This value is available once the detector is
            fitted.
        threshold_ : float
            The threshold is based on ``contamination``. It is the
            ``n_samples * contamination`` most abnormal samples in
            ``decision_scores_``. The threshold is calculated for generating
            binary outlier labels.
        labels_ : int, either 0 or 1
            The binary labels of the training data. 0 stands for inliers
            and 1 for outliers/anomalies. It is generated by applying
            ``threshold_`` on ``decision_scores_``.
        """
        super(AEDenseTF2, self).__init__(contamination=contamination)
        self.hidden_neurons = hidden_neurons
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation
        self.loss = loss
        self.optimizer = optimizer
        self.epochs = epochs
        self.batch_size = batch_size
        self.dropout_rate = dropout_rate
        self.l2_regularizer = l2_regularizer
        self.validation_size = validation_size
        self.preprocessing = preprocessing
        self.verbose = verbose
        self.random_state = random_state

        # default values
        if self.hidden_neurons is None:
            self.hidden_neurons = [64, 32, 32, 64]

        # Verify the network design is valid
        if not self.hidden_neurons == self.hidden_neurons[::-1]:
            print(self.hidden_neurons)
            raise ValueError("Hidden units should be symmetric")

        self.hidden_neurons_ = self.hidden_neurons

        check_parameter(dropout_rate, 0, 1, param_name='dropout_rate',
                        include_left=True)

    def _build_model(self):  # REFACTORED
        model = Sequential()
        # Input layer
        model.add(Dense(
            self.hidden_neurons_[0], activation=self.hidden_activation,
            input_shape=(self.n_features_,),
            activity_regularizer=l2(self.l2_regularizer)))
        model.add(Dropout(self.dropout_rate))

        # Additional layers
        for i, hidden_neurons in enumerate(self.hidden_neurons_, 1):
            model.add(Dense(
                hidden_neurons,
                activation=self.hidden_activation,
                activity_regularizer=l2(self.l2_regularizer)))
            model.add(Dropout(self.dropout_rate))

        # Output layers
        model.add(Dense(self.n_features_, activation=self.output_activation,
                        activity_regularizer=l2(self.l2_regularizer)))

        # Compile model
        model.compile(loss=self.loss, optimizer=self.optimizer)
        if self.verbose >= 1:
            print(model.summary())
        return model

    # noinspection PyUnresolvedReferences
    def fit(self, X, y=None):
        """Fit detector. y is ignored in unsupervised methods.

        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The input samples.
        y : Ignored
            Not used, present for API consistency by convention.
        Returns
        -------
        self : object
            Fitted estimator.
        """
        # validate inputs X and y (optional)
        X = check_array(X)
        self._set_n_classes(y)

        # Verify and construct the hidden units
        self.n_samples_, self.n_features_ = X.shape[0], X.shape[1]

        # Standardize data for better performance
        if self.preprocessing:
            self.scaler_ = StandardScaler()
            X_norm = self.scaler_.fit_transform(X)
        else:
            X_norm = np.copy(X)

        # Shuffle the data for validation as Keras do not shuffling for
        # Validation Split
        np.random.shuffle(X_norm)

        # Validate and complete the number of hidden neurons
        if np.min(self.hidden_neurons) > self.n_features_:
            raise ValueError("The number of neurons should not exceed "
                             "the number of features")
        self.hidden_neurons_.insert(0, self.n_features_)

        # Calculate the dimension of the encoding layer & compression rate
        self.encoding_dim_ = np.median(self.hidden_neurons)
        self.compression_rate_ = self.n_features_ // self.encoding_dim_

        # Build AE model & fit with X
        self.model_ = self._build_model()
        self.history_ = self.model_.fit(X_norm, X_norm,
                                        epochs=self.epochs,
                                        batch_size=self.batch_size,
                                        shuffle=True,
                                        validation_split=self.validation_size,
                                        verbose=self.verbose).history
        # Reverse the operation for consistency
        self.hidden_neurons_.pop(0)
        # Predict on X itself and calculate the reconstruction error as
        # the outlier scores. Noted X_norm was shuffled has to recreate
        if self.preprocessing:
            X_norm = self.scaler_.transform(X)
        else:
            X_norm = np.copy(X)

        pred_scores = self.model_.predict(X_norm)
        self.decision_scores_ = pairwise_distances_no_broadcast(X_norm,
                                                                pred_scores)
        self._process_decision_scores()
        return self

    def decision_function(self, X):
        """Predict raw anomaly score of X using the fitted detector.

        The anomaly score of an input sample is computed based on different
        detector algorithms. For consistency, outliers are assigned with
        larger anomaly scores.
        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The training input samples. Sparse matrices are accepted only
            if they are supported by the base estimator.
        Returns
        -------
        anomaly_scores : numpy array of shape (n_samples,)
            The anomaly score of the input samples.
        """
        check_is_fitted(self, ['model_', 'history_'])
        X = check_array(X)

        if self.preprocessing:
            X_norm = self.scaler_.transform(X)
        else:
            X_norm = np.copy(X)

        # Predict on X and return the reconstruction errors
        pred_scores = self.model_.predict(X_norm)
        return pairwise_distances_no_broadcast(X_norm, pred_scores)


class AECnnTF2(BaseDetector):

    def __init__(self, nr_timeseries, nr_timesteps, receptive_field_len=3,
                 hidden_activation='relu', output_activation='sigmoid',
                 loss=mean_squared_error, optimizer='adam',
                 epochs=100, batch_size=32, dropout_rate=0.2,
                 l2_regularizer=0.1, validation_size=0.1,
                 verbose=1, random_state=None, contamination=0.1,
                 preprocessing=True):
        """Autoencoder CNN that is sliding acros multiple timeseries.

        Params
        ------
        nr_timeseries: (int), mandatory
            The number of time series taken as input.
        nr_timesteps: (int), optional
            the number of timesteps present for each timeseries. this can be
            inferred by the length of the input and the nr of timeseries
        receptive_field_len: (int), default 3
            filter dimension.
        """
        super(AECnnTF2, self).__init__(contamination=contamination)
        # for CNN
        self.nr_timeseries = nr_timeseries
        self.nr_timesteps = nr_timesteps
        self.receptive_field_len = receptive_field_len
        self.plugin_names = None
        # from AE
        # self.hidden_neurons = hidden_neurons
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation
        self.loss = loss
        self.optimizer = optimizer
        self.epochs = epochs
        self.batch_size = batch_size
        self.dropout_rate = dropout_rate
        self.l2_regularizer = l2_regularizer
        self.validation_size = validation_size
        self.preprocessing = preprocessing
        self.verbose = verbose
        self.random_state = random_state
        self.contamination = contamination      
  

    def fit(self, X, y=None):
        """Fit the CNN detector. y is ignored in unsupervised methods.

        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The input samples.
            The input for each entity should consist in a concatenation of the
            n timeseries related to the same entity. Each time series must
            start with the most recent timestamp.
            e.g. Given two timeseries of the same entity: ts1 and ts2
            where ts1_0 is the most recent data of ts1 and ts1_n is the n-th
            most recent data of ts1, we want the input to be like:
            ts1_0, ts1_1, ts1_2, ..., ts1_n, ts2_0, ts2_1, ts2_2, ..., ts2_n
        y : Ignored
            Not used, present for API consistency by convention.
        Returns
        -------
        self : object
            Fitted estimator.
        """
        columns = X.columns
        # remove the suffix from column name
        if self.plugin_names is None:
            self.plugin_names = list(set([x[:x.rfind("_")] for x in columns]))
        if len(self.plugin_names) != self.nr_timeseries:
            print("Nr timeseries (%i) doesn't match the actial "
                  "number of plugins" %
                  self.nr_timeseries)
            [print("> ", plg) for plg in self.plugin_names]

        # reshape the images
        X_image = np.reshape(X.values,
                             (len(X),
                              self.nr_timeseries, self.nr_timesteps, 1))
        X_image = np.nan_to_num(X_image, nan=0, posinf=0, neginf=0)
        # Standardize data for better performance
        # https://stackoverflow.com/questions/42460217/how-to-normalize-a-4d-numpy-array
        if self.preprocessing:
            # compute the max and min for every image
            self.v_min = X_image.min(axis=(0, 2, 3), keepdims=True)
            self.v_max = X_image.max(axis=(0, 2, 3), keepdims=True)
            denominator = (self.v_max - self.v_min)
            denominator[denominator == 0] = 1
            X_image = (X_image - self.v_min) / denominator

        # train
        self.model_ = self._build_model(self.receptive_field_len)
        self.model_.compile(optimizer=self.optimizer,
                            loss=self.loss)

        self.history_ = self.model_.fit(X_image, X_image,
                                        epochs=self.epochs,
                                        batch_size=self.batch_size,
                                        shuffle=True,
                                        validation_split=self.validation_size,
                                        verbose=self.verbose).history

        pred_scores = self.model_.predict(X_image)
        nr_rows = pred_scores.shape[0]
        pred_scores_flat = np.reshape(pred_scores,
                                      (nr_rows,
                                       self.nr_timeseries * self.nr_timesteps))
        original_images = np.reshape(X_image,
                                     (nr_rows,
                                      self.nr_timeseries * self.nr_timesteps))

        # print("pred_scores_flat:", pred_scores_flat.shape)
        # print("original_images:", original_images.shape)
        self.decision_scores_ = \
            pairwise_distances_no_broadcast(original_images, pred_scores_flat)
        self._process_decision_scores()
        return self

    def _build_model(self, receptive_field=3):
        """Create the CNN model."""
        input_img = Input(shape=(self.nr_timeseries, self.nr_timesteps, 1))
        # adapt this if using `channels_first` image data format

        x = Conv2D(4, (self.nr_timeseries, self.receptive_field_len),
                   activation=self.hidden_activation, padding='same')(input_img)  # A # noqa
        x = MaxPooling2D((1, 2), padding='same')(x)  # B
        x = Conv2D(2, (self.nr_timeseries, self.receptive_field_len),
                   activation=self.hidden_activation, padding='same')(x)  # C
        encoded = MaxPooling2D((1, 4), padding='same')(x)  # D

        # at this point the representation is (12, 1, 2) i.e. 24-dimensional

        x = Conv2D(2, (self.nr_timeseries, self.receptive_field_len),
                   activation=self.hidden_activation, padding='same')(encoded)  # M # noqa

        x = UpSampling2D((1, 4))(x)  # D
        x = Conv2D(4, (self.nr_timeseries, self.receptive_field_len),
                   activation=self.hidden_activation, padding='same')(x)  # C
        x = UpSampling2D((1, 2))(x)  # B
        decoded = Conv2D(1, (self.nr_timeseries, self.receptive_field_len),
                         activation=self.output_activation, padding='same')(x)  # A # noqa

        autoencoder = Model(input_img, decoded)
        autoencoder.summary()
        return autoencoder

    def decision_function(self, X):
        """Predict raw anomaly score of X using the fitted detector.

        The anomaly score of an input sample is computed based on different
        detector algorithms. For consistency, outliers are assigned with
        larger anomaly scores.
        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The training input samples. Sparse matrices are accepted only
            if they are supported by the base estimator.
        Returns
        -------
        anomaly_scores : numpy array of shape (n_samples,)
            The anomaly score of the input samples.
        """
        check_is_fitted(self, ['model_', 'history_'])
        X = check_array(X)

        X_image = np.reshape(X,
                             (len(X),
                              self.nr_timeseries, self.nr_timesteps, 1))

        if self.preprocessing:
            denominator = (self.v_max - self.v_min)
            denominator[denominator == 0] = 1
            X_image = (X_image - self.v_min) / denominator
        # Predict on X and return the reconstruction errors
        pred_scores = self.model_.predict(X_image)
        nr_rows = pred_scores.shape[0]
        pred_scores_flat = np.reshape(pred_scores,
                                      (nr_rows,
                                       self.nr_timeseries * self.nr_timesteps))
        original_images = np.reshape(X_image,
                                     (nr_rows,
                                      self.nr_timeseries * self.nr_timesteps))
        # print("pred_scores_flat:", pred_scores_flat.shape)
        # print("original_images:", original_images.shape)
        return pairwise_distances_no_broadcast(original_images,
                                               pred_scores_flat)


class AELstmTF2(BaseDetector):

    def __init__(self, nr_timeseries, nr_timesteps, nr_lstm_units=1,
                 patience=None,
                 lstm_activation='tanh',
                 recurrent_activation='sigmoid',
                 output_activation='sigmoid',
                 loss=mean_squared_error, optimizer='adam',
                 epochs=100, batch_size=32, dropout_rate=0.25,
                 l2_regularizer=0.1, validation_size=0.1,
                 verbose=1, random_state=None, contamination=0.1,
                 preprocessing=True):
        """Autoencoder LSTM that is sliding acros multiple timeseries.

        Params
        ------
        nr_timeseries: (int), mandatory
            The number of time series taken as input.
        nr_timesteps: (int), optional
            the number of timesteps present for each timeseries. this can be
            inferred by the length of the input and the nr of timeseries
        nr_lstm_units: (int), default 10
            nr of lstm cells in every layer encoding / decoding.
        """
        super(AELstmTF2, self).__init__(contamination=contamination)
        
        # if patience is not set, use patience equal to number of epochs
        self.patience = patience if patience is not None else epochs
        
        # for LSTM
        self.nr_timeseries = nr_timeseries
        self.nr_timesteps = nr_timesteps
        self.nr_lstm_units = nr_lstm_units
        self.plugin_names = None
        self.lstm_activation = lstm_activation
        self.recurrent_activation = recurrent_activation
        # from AE
        # self.hidden_neurons = hidden_neurons
        self.output_activation = output_activation
        self.loss = loss
        self.optimizer = optimizer
        self.epochs = epochs
        self.batch_size = batch_size
        self.dropout_rate = dropout_rate
        self.l2_regularizer = l2_regularizer
        self.validation_size = validation_size
        self.preprocessing = preprocessing
        self.verbose = verbose
        self.random_state = random_state
        self.contamination = contamination

       
    def fit(self, X, y=None):
        """Fit the LSTM AE detector. y is ignored in unsupervised methods.

        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The input samples.
            The input for each entity should consist in a concatenation of the
            n timeseries related to the same entity. Each time series must
            start with the most recent timestamp.
            e.g. Given two timeseries of the same entity: ts1 and ts2
            where ts1_0 is the most recent data of ts1 and ts1_n is the n-th
            most recent data of ts1, we want the input to be like:
            ts1_0, ts1_1, ts1_2, ..., ts1_n, ts2_0, ts2_1, ts2_2, ..., ts2_n
        y : Ignored
            Not used, present for API consistency by convention.
        Returns
        -------
        self : object
            Fitted estimator.
        """
        columns = X.columns
        # remove the suffix from column name
        if self.plugin_names is None:
            self.plugin_names = list(set([x[:x.rfind("_")] for x in columns]))
        if len(self.plugin_names) != self.nr_timeseries:
            print("Nr timeseries (%i) doesn't match the actial "
                  "number of plugins" %
                  self.nr_timeseries)
            [print("> ", plg) for plg in self.plugin_names]

        # reshape the images
        X_image = np.reshape(X.values,
                             (len(X),
                              self.nr_timeseries, self.nr_timesteps))
        # NOTE THAT WE HAVE ONLY 3 DIMENSIONS HERE
        X_image = np.nan_to_num(X_image, nan=0, posinf=0, neginf=0)
        # Standardize data for better performance
        # https://stackoverflow.com/questions/42460217/how-to-normalize-a-4d-numpy-array
        if self.preprocessing:
            # compute the max and min for every image
            self.v_min = X_image.min(axis=(0, 2), keepdims=True)
            self.v_max = X_image.max(axis=(0, 2), keepdims=True)
            denominator = (self.v_max - self.v_min)
            denominator[denominator == 0] = 1
            X_image = (X_image - self.v_min) / denominator

        # modification required for LSTM,
        # a particular direction for the timesteps axis is required
        X_image = X_image.swapaxes(1, 2)        
        
        # train
        self.model_ = \
            self._build_model(nr_lstm_units=self.nr_lstm_units,
                              lstm_activation=self.lstm_activation,
                              recurrent_activation=self.recurrent_activation)
        
        early_stop = EarlyStopping(monitor = 'val_loss', patience = self.patience)
        
        self.history_ = self.model_.fit(X_image, X_image,
                                        epochs=self.epochs,
                                        batch_size=self.batch_size,
                                        shuffle=True,
                                        validation_split=self.validation_size,
                                        verbose=self.verbose,
                                        callbacks = [early_stop]).history
        pred_scores = self.model_.predict(X_image)
        nr_rows = pred_scores.shape[0]
        pred_scores_flat = np.reshape(pred_scores,
                                      (nr_rows,
                                       self.nr_timeseries * self.nr_timesteps))
        original_images = np.reshape(X_image,
                                     (nr_rows,
                                      self.nr_timeseries * self.nr_timesteps))

        # print("pred_scores_flat:", pred_scores_flat.shape)
        # print("original_images:", original_images.shape)
        self.decision_scores_ = \
            pairwise_distances_no_broadcast(original_images, pred_scores_flat)
        self._process_decision_scores()
        return self

    def _build_model(self,
                     nr_lstm_units,
                     lstm_activation,
                     recurrent_activation):
        """Create the LSTM model."""
        
        if self.random_state is not None:
            fix_seeds(self.random_state)
            
        model = Sequential()
        model.add(LSTM(nr_lstm_units, input_shape=(self.nr_timesteps,
                                                   self.nr_timeseries), return_sequences=True))
        #model.add(RepeatVector(self.nr_timesteps))
        model.add(Dropout(self.dropout_rate)) 
        model.add(LSTM(nr_lstm_units, return_sequences=True))
        model.add(Dropout(self.dropout_rate)) 
        model.add(TimeDistributed(Dense(self.nr_timeseries)))
        model.compile(loss=self.loss,
                      optimizer=self.optimizer)
        model.summary()
        return model

    def decision_function(self, X):
        """Predict raw anomaly score of X using the fitted detector.

        The anomaly score of an input sample is computed based on different
        detector algorithms. For consistency, outliers are assigned with
        larger anomaly scores.
        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The training input samples. Sparse matrices are accepted only
            if they are supported by the base estimator.
        Returns
        -------
        anomaly_scores : numpy array of shape (n_samples,)
            The anomaly score of the input samples.
        """
        check_is_fitted(self, ['model_', 'history_'])
        X = check_array(X)

        X_image = np.reshape(X,
                             (len(X),
                              self.nr_timeseries, self.nr_timesteps))

        if self.preprocessing:
            denominator = (self.v_max - self.v_min)
            denominator[denominator == 0] = 1
            X_image = (X_image - self.v_min) / denominator

        # modification required for LSTM,
        # a particular direction for the timesteps axis is required
        X_image = X_image.swapaxes(1, 2)

        # Predict on X and return the reconstruction errors
        pred_scores = self.model_.predict(X_image)
        nr_rows = pred_scores.shape[0]
        pred_scores_flat = np.reshape(pred_scores,
                                      (nr_rows,
                                       self.nr_timeseries * self.nr_timesteps))
        original_images = np.reshape(X_image,
                                     (nr_rows,
                                      self.nr_timeseries * self.nr_timesteps))
        # print("pred_scores_flat:", pred_scores_flat.shape)
        # print("original_images:", original_images.shape)
        return pairwise_distances_no_broadcast(original_images,
                                               pred_scores_flat)

class AEGruTF2(BaseDetector):
    def __init__(self,
                 #nr_timeseries,                 
                 nr_timesteps,    
                 patience=None,             
                 loss=mean_squared_error,
                 optimizer='adam',
                 gru_units=1,
                 epochs=10, 
                 batch_size=16, 
                 dropout_rate=0.25,
                 validation_size=0.1,
                 verbose=1, 
                 random_state=None,
                 contamination=0.1,
                 preprocessing=True):
        
        super(AEGruTF2, self).__init__(contamination=contamination)
        
        # if patience is not set, use patience equal to number of epochs
        self.patience = patience if patience is not None else epochs
        self.nr_timesteps = nr_timesteps

        self.loss = loss 
        self.optimizer = optimizer 
        self.epochs = epochs 
        self.batch_size = batch_size 
        self.dropout_rate = dropout_rate 

        self.validation_size = validation_size
        self.preprocessing = preprocessing
        self.verbose = verbose
        self.random_state = random_state
        self.contamination = contamination
        
        self.gru_units = gru_units
    
    # Create GRU model
    def _build_model(self, gru_units):
        
        if self.random_state is not None:
            fix_seeds(self.random_state)
            
        model = Sequential()
        # Input layer
        model.add(GRU (gru_units, 
                       return_sequences=True, 
                       #input_shape=self.x_shape
                       input_shape=(self.nr_timesteps, self.nr_timeseries)
                      )
                 )
        model.add(Dropout(self.dropout_rate)) 
        # Hidden layer
        model.add(GRU(gru_units,return_sequences=True)) 
        model.add(Dropout(self.dropout_rate))
        model.add(TimeDistributed(Dense(self.nr_timeseries)))
        #Compile model
        model.compile(optimizer=self.optimizer,
                      loss=self.loss)
        
        if self.verbose >= 1:
            print(model.summary())
        
        return model
    
    def fit(self, X, y=None):
        # validate inputs X and y (optional)
        
        print("The type of X is : ", type(X))
        print("Shape of X is : ", X.shape)
                
        #X = check_array(X)
        #self._set_n_classes(y)
        
        self.plugin_names = list(set([x[:x.rfind("_")] for x in X.columns]))
        self.nr_timeseries = len(self.plugin_names)
                
        X_image = np.reshape(X.values,
                             (len(X),
                              self.nr_timeseries, 
                              self.nr_timesteps))
        
        # NOTE THAT WE HAVE ONLY 3 DIMENSIONS HERE
        X_image = np.nan_to_num(X_image, nan=0, posinf=0, neginf=0)
        
        # Standardize data for better performance
        # https://stackoverflow.com/questions/42460217/how-to-normalize-a-4d-numpy-array
        if self.preprocessing:
            # compute the max and min for every image
            self.v_min = X_image.min(axis=(0, 2), keepdims=True)
            self.v_max = X_image.max(axis=(0, 2), keepdims=True)
            denominator = (self.v_max - self.v_min)
            denominator[denominator == 0] = 1
            X_image = (X_image - self.v_min) / denominator            
       
        X_image = X_image.swapaxes(1, 2)
       
        self.model_ = self._build_model(gru_units=self.gru_units)
        early_stop = EarlyStopping(monitor = 'val_loss', patience = self.patience)
       
        self.history_ = self.model_.fit(X_image, X_image,
                                        epochs=self.epochs,
                                        batch_size=self.batch_size,
                                        shuffle=True,
                                        validation_split=self.validation_size,
                                        verbose=self.verbose,
                                        callbacks = [early_stop]).history
            
        pred_scores = self.model_.predict(X_image)
        nr_rows = pred_scores.shape[0]
        pred_scores_flat = np.reshape(pred_scores,
                                      (nr_rows,
                                       self.nr_timeseries * self.nr_timesteps))
        original_images = np.reshape(X_image,
                                     (nr_rows,
                                      self.nr_timeseries * self.nr_timesteps))

        print("pred_scores_flat:", pred_scores_flat.shape)
        print("original_images:", original_images.shape)
        self.decision_scores_ = \
            pairwise_distances_no_broadcast(original_images, pred_scores_flat)
        self._process_decision_scores()
        
        return self
    
    def decision_function(self, X):
        check_is_fitted(self, ['model_', 'history_'])        
      
        X_image = np.reshape(X.values,
                             (len(X),
                              self.nr_timeseries, 
                              self.nr_timesteps))
            
        # NOTE THAT WE HAVE ONLY 3 DIMENSIONS HERE
        X_image = np.nan_to_num(X_image, nan=0, posinf=0, neginf=0)   
        
        # Standardize data for better performance
        # https://stackoverflow.com/questions/42460217/how-to-normalize-a-4d-numpy-array
        if self.preprocessing:
            denominator = (self.v_max - self.v_min)
            denominator[denominator == 0] = 1
            X_image = (X_image - self.v_min) / denominator
            
        
        X_image = X_image.swapaxes(1, 2)
            
        # Predict on X and return the reconstruction errors
        pred_scores = self.model_.predict(X_image)
        nr_rows = pred_scores.shape[0]
        pred_scores_flat = np.reshape(pred_scores,
                                      (nr_rows,
                                       self.nr_timeseries * self.nr_timesteps))
        original_images = np.reshape(X_image,
                                     (nr_rows,
                                      self.nr_timeseries * self.nr_timesteps))

        return pairwise_distances_no_broadcast(original_images,
                                               pred_scores_flat)