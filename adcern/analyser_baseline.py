"""Baseline Methods to compare."""
# Author: Matteo Paltenghi <matteo.paltenghi@cern.ch> # noqa

import scipy  # noqa
import numpy as np

from sklearn.utils import check_array

from pyod.models.base import BaseDetector


class Percentile(BaseDetector):

    def __init__(self, nr_timeseries, nr_timesteps,
                 percentile=10, percentage_above=0.6,
                 random_state=None, contamination=0.1):
        """Percentile method: how far data are from percentile distribution.

        We monitor every time series and see how far they are from the mean
        in terms of percentile. The percentile model comes form the
        emprical distribution of data at training time.

        Parameters
        ----------
        nr_timeseries: (int), mandatory
            The number of time series taken as input.
        nr_timesteps: (int), optional
            the number of timesteps present for each timeseries. this can be
            inferred by the length of the input and the nr of timeseries
        percentile: (int) defualt 10
            it must be between 0 and 50. e.g. percentile = 5 means that
            we flag as anomalous all the machine in which one of their metric
            below the 10th percentile or above 90th (100 - 10) percentile.
        percentage_above: (float) [0,1]
            percentage of timesteps that have to be above threshold in order
            to trigger the anoamaly
        """
        # method specific
        self.nr_timeseries = nr_timeseries
        self.nr_timesteps = nr_timesteps
        self.percentile = int(percentile)
        self.percentage_above = percentage_above

        self.plugin_names = None
        # general
        self.random_state = random_state
        self.contamination = contamination

    def fit(self, X, y=None):
        """Learn the Percentile distributions.

        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The input samples.
            The input for each entity should consist in a concatenation of the
            n timeseries related to the same entity. Each time series must
            start with the most recent timestamp.
            e.g. Given two timeseries of the same entity: ts1 and ts2
            where ts1_0 is the most recent data of ts1 and ts1_n is the n-th
            most recent data of ts1, we want the input to be like:
            ts1_0, ts1_1, ts1_2, ..., ts1_n, ts2_0, ts2_1, ts2_2, ..., ts2_n
        y : Ignored
            Not used, present for API consistency by convention.
        Returns
        -------
        self : object
            Fitted estimator.
        """
        columns = X.columns
        # remove the suffix from column name
        if self.plugin_names is None:
            self.plugin_names = list(set([x[:x.rfind("_")] for x in columns]))
        if len(self.plugin_names) != self.nr_timeseries:
            print("""Nr timeseries (%i) doesn't match the actual number
                     of plugins""" %
                  self.nr_timeseries)
            [print("> ", plg) for plg in self.plugin_names]

        # reshape the images
        X_image = np.reshape(X.values,
                             (len(X),
                              self.nr_timeseries, self.nr_timesteps))

        # group the data coming from the same metric/time series
        # regardless of the provenance machine and timestep
        percentiles_all_metric = []
        for metric_pos in range(self.nr_timeseries):
            metric_slice = X_image[:, metric_pos, :]
            metric_values = metric_slice.flatten()
            percentiles_current_metric = []
            for i in range(101):
                value_i_th_percentile = np.percentile(metric_values, i)
                percentiles_current_metric.append(value_i_th_percentile)
            percentiles_all_metric.append(percentiles_current_metric)

        # create percentile matrix
        # every row is a metric (first is the 1st in alphabetical order)
        # every col is a value of the percentile of that metric
        # e.g. 11 row x 101 columns
        # so in col 0 and col 100 we have the min and max resepctively
        self.matrix_percentiles = np.vstack(percentiles_all_metric)

        print("MATRIX: ", self.matrix_percentiles)

        # check negative overflow - if the values are below the low-percentile
        self.low_vector = self.matrix_percentiles[:, self.percentile]
        print("low_vector: ", self.low_vector)

        # check positive overflow - if the values are below the top-percentile
        opposite_percentile = 100 - self.percentile
        self.top_vector = self.matrix_percentiles[:, opposite_percentile]
        print("top_vector: ", self.top_vector)

    def decision_function(self, X):
        """Predict raw anomaly score of X using the fitted detector.

        The anomaly score of an input sample is computed based on different
        detector algorithms. For consistency, outliers are assigned with
        larger anomaly scores.
        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The training input samples. Sparse matrices are accepted only
            if they are supported by the base estimator.
        Returns
        -------
        anomaly_scores : numpy array of shape (n_samples,)
            The anomaly score of the input samples.
        """
        X = check_array(X)

        X_image = np.reshape(X,
                             (len(X),
                              self.nr_timeseries, self.nr_timesteps))

        # ----------------------------------------------------------------
        # CHECK HOW MANY GO BELOW THE LOW PERCENTILE
        # prepare the vector to be broadcasted and subtracted to all
        # images at the same time
        low_vector_2d = np.expand_dims(self.low_vector, axis=0)
        low_vector_3d = np.expand_dims(low_vector_2d, axis=2)

        # remove the lower percentile (keep original shape 3d)
        image_minus_low_perc = X_image - low_vector_3d

        # count how many are below the lower percentile (danger area)
        values_below_low_threshold = image_minus_low_perc < 0

        # covert bool to int so that we can count how many timestep
        # every metric was below the threshold
        int_values_below_low_threshold = \
            values_below_low_threshold.astype('int')

        # count how many timestps were below the percentile for every metrics
        below_perc = int_values_below_low_threshold.sum(axis=2)
        # sum over the timesteps (3rd dimension)
        # output shape = (len(X), self.nr_timeseries)

        # ----------------------------------------------------------------
        # CHECK HOW MANY GO ABOVE THE TOP PERCENTILE
        # prepare the vector to be broadcasted and subtracted to all
        # images at the same time
        top_vector_2d = np.expand_dims(self.top_vector, axis=0)
        top_vector_3d = np.expand_dims(top_vector_2d, axis=2)

        # remove the toper percentile (keep original shape 3d)
        image_minus_top_perc = X_image - top_vector_3d

        # count how many are above the toper percentile (danger area)
        values_above_top_threshold = image_minus_top_perc > 0

        # covert bool to int so that we can count how many timestep
        # every metric was above the threshold
        int_values_above_top_threshold = \
            values_above_top_threshold.astype('int')

        # count how many timestps were above the percentile for every metrics
        above_perc = int_values_above_top_threshold.sum(axis=2)
        # sum over the timesteps (3rd dimension)
        # output shape = (len(X), self.nr_timeseries)

        # ----------------------------------------------------------------
        # sum all the timestep that were either above or below the thresholds
        dangerous_timesteps = np.add(below_perc, above_perc)
        # output shape = (len(X), self.nr_timeseries)

        # get the number of timesteps that have to be out of range to
        # raise an alarm. This is derivated from the total no. of steps
        # and the percentage defined during initialization
        num_percentage_dangerous_zone = \
            self.nr_timesteps * self.percentage_above
        print("""We need %.2f timesteps in dangerous interval to define \
                 a window as an anomalous: """
              % num_percentage_dangerous_zone)

        # compute how many images are below the threshold
        # np.any is used to raise an allarm if any of the metrics was about the
        # threshold. (i.e. any over the timeseries (2nd dimension))
        anomalous_image = \
            np.any((dangerous_timesteps >= num_percentage_dangerous_zone),
                   axis=1)

        # return an integer version
        return anomalous_image.astype('int')


class PercScore(BaseDetector):

    def __init__(self, nr_timeseries, nr_timesteps,
                 percentage_above=0.85,
                 random_state=None, contamination=0.1):
        """Percentile Score method.

        We monitor every time series and see how far they are from the mean
        in terms of percentile. The percentile model comes form the
        emprical distribution of data at training time.

        Parameters
        ----------
        nr_timeseries: (int), mandatory
            The number of time series taken as input.
        nr_timesteps: (int), optional
            the number of timesteps present for each timeseries. this can be
            inferred by the length of the input and the nr of timeseries
        percentile: (int) defualt 10
            it must be between 0 and 50. e.g. percentile = 5 means that
            we flag as anomalous all the machine in which one of their metric
            below the 10th percentile or above 90th (100 - 10) percentile.
        percentage_above: (float) [0,1]
            percentage of timesteps that have to be above threshold in order
            to trigger the anoamaly
        """
        # method specific
        self.nr_timeseries = nr_timeseries
        self.nr_timesteps = nr_timesteps
        self.percentage_above = float(percentage_above)
        if percentage_above > 0.9999:
            raise Exception("Invalid value for percentage_above: %.4f" %
                            percentage_above)

        self.plugin_names = None
        # general
        self.random_state = random_state
        self.contamination = contamination

    def fit(self, X, y=None):
        """Learn the Percentile distributions.

        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The input samples.
            The input for each entity should consist in a concatenation of the
            n timeseries related to the same entity. Each time series must
            start with the most recent timestamp.
            e.g. Given two timeseries of the same entity: ts1 and ts2
            where ts1_0 is the most recent data of ts1 and ts1_n is the n-th
            most recent data of ts1, we want the input to be like:
            ts1_0, ts1_1, ts1_2, ..., ts1_n, ts2_0, ts2_1, ts2_2, ..., ts2_n
        y : Ignored
            Not used, present for API consistency by convention.
        Returns
        -------
        self : object
            Fitted estimator.
        """
        print("No train required ;) - Z-score in Spark was enough.")

    def decision_function(self, X):
        """Predict raw anomaly score of X using the fitted detector.

        The anomaly score of an input sample is computed based on different
        detector algorithms. For consistency, outliers are assigned with
        larger anomaly scores.
        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The training input samples. Sparse matrices are accepted only
            if they are supported by the base estimator.
        Returns
        -------
        anomaly_scores : numpy array of shape (n_samples,)
            The anomaly score of the input samples.
        """
        X = check_array(X)

        X_image = np.reshape(X,
                             (len(X),
                              self.nr_timeseries, self.nr_timesteps))

        # ----------------------------------------------------------------
        # ABSOLUTE VALUE - DISTANCE FROM THE 50th PERCENTILE
        # Since the data were standardized with our previous week mean
        # it means that a very negative value or a very positve one are both
        # equally far from the save are (e.g. 5th and 95th percentile).
        # Taking the absolute value they both are weighted the same.
        X_image_pos = np.abs(X_image)

        # ----------------------------------------------------------------
        # PERCENTILE - UNDERSTAND IF x % OF DATA WERE THAT FAR FROM THE MEAN
        # Considering one window-machine and one of its metrics (e.g. CPU LOAD)
        # we want to be notified when it is for most of the time (e.g. 80 % of
        # the time) out of the safe interval.
        # If we list all the values: [1,2,3,1,,3,1,2,1,2,4] and taking the
        # 20th percentile [1,1,(1),1,2,2,2,3,3,4] -> (1) that is defining
        # if the values in that range were for 80% of the time out of the
        # safe range.
        # We compute the 100 - x th percentile for every metric, where
        # x = self.percentage_above
        perc_X_image = \
            np.percentile(X_image_pos,
                          q=1 - float(self.percentage_above),
                          axis=2)
        print("perc_X_image: ", perc_X_image.shape)

        # ----------------------------------------------------------------
        # MAX - SENSIBLE TO ONE METRIC
        # Since we want to be sensible to one metric, even if one metric is
        # out of the safe interval for x % of the time we want to be notified
        # so we care only of the metric that was furthest from the safe zone.
        scores = np.amax(perc_X_image, axis=1)
        print("scores:", scores.shape)

        return scores
