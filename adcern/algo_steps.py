"""Steps to iterate on the window data."""

from datetime import timedelta
from datetime import datetime
import time, re

from etl.spark_etl.utils import read_json_config

from adcern.publisher import create_id_for_metrics_dict
from adcern.publisher import create_id_for_hostgroups_and_plugins_start_end

from pyspark.sql.functions import col, from_unixtime

from etl.spark_etl.utils import convert_to_slash
from etl.spark_etl.utils import create_path_for_hostgroup  # noqa

# TESTED
def read_window_dataset(spark, config_filepath, config_dict_debug=None, select_slide_steps_cols=False, removing_half_nulls=False):
    """Read the window dataset at the pace specified by the slide step.

    Params
    ------
    spark: spark context
    config_filepath: path to json with the following keys:
        hdfs_out_folder (str), date_start (str), date_end_excluded(str),
        overwrite_on_hdfs (boolean), aggregate_every_n_minutes (int),
        history_steps (int), future_steps (int),
        hostgroups (list of str), selected_plugins (dict of dict)

    Return
    ------
    df_only_relevant_pace: SPARK Dataframe
        TO BE WRITTEN
    """
    print("Reading the window dataframe")
    if config_dict_debug:
        config_dict = config_dict_debug.copy()
    else:
        config_dict = read_json_config(config_filepath)
    
    if "removing_half_nulls" in config_dict:
        removing_half_nulls = config_dict["removing_half_nulls"]

    # create the interested timestamps
    timestamps = _compute_timestamps_in_seconds(config_dict=config_dict)
    min_ts = min(timestamps)
    print("Minimal timestamp in days: ", min_ts)
    print("Same number but human readable: ", time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(min_ts)))

    # create paths for every plugin
    inpath = config_dict["hdfs_out_folder"] + "/" + config_dict["code_project_name"]
    print("Reading a spark df in the path: " + inpath)
    df_all_windows = spark.read.parquet(inpath)
    min_ts_df = min(list(df_all_windows.select('timestamp').toPandas()['timestamp'].unique()))
    print("Minimal timestamp in the real dataframe: ", min_ts_df)
    print("Same number but human readable: ", time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(min_ts_df)))

    print("Removing first timestamp...")
    print("Initial dataset size: ", df_all_windows.count(), " x ", len(df_all_windows.columns))
    df_all_windows = df_all_windows.filter(col("timestamp") > min_ts)
    print("Obtained dataset size: ", df_all_windows.count(), " x ", len(df_all_windows.columns))

    min_ts_df = min(list(df_all_windows.select('timestamp').toPandas()['timestamp'].unique()))
    print("Minimal timestamp in the real dataframe: ", min_ts_df)
    print("Same number but human readable: ", time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(min_ts_df)))

    print("Removing the rows we don't want (if slide_steps=1 we don't remove anything).")
    slide_steps = config_dict["slide_steps"]
    window_length = slide_steps * 60 * config_dict["aggregate_every_n_minutes"]
    delta = 1 * 60 * config_dict["aggregate_every_n_minutes"]
    df = df_all_windows.filter(((col("timestamp") - int(min_ts_df)) % window_length) == 0)
    print("Obtained dataset size: ", df.count(), " x ", len(df.columns))

    df = df.withColumn("timestamp", col("timestamp") + window_length - delta)
    df = df.withColumn(
        "ts", 
        from_unixtime(col("timestamp"))
    )

    if "select_slide_steps_cols" in config_dict:
        select_slide_steps_cols = config_dict["select_slide_steps_cols"]
    if select_slide_steps_cols:
        print("select_slide_steps_cols is True. Removing the columns we don't need...")
        to_not_drop = int(config_dict['slide_steps'])
        columns_to_remove = list(filter(re.compile("_h[0-9]*$").search, df.schema.names))
        for i in range(to_not_drop):
            tmp = list(filter(re.compile("_h" + str(i) + "$").search, df.schema.names))
            for j in tmp:
                columns_to_remove.remove(j)
        df = df.drop(*columns_to_remove)
        print("Obtained dataset size: ", df.count(), " x ", len(df.columns))

    if removing_half_nulls:
        print("Removing rows where at least half of columns are NULL")
        df = df.withColumn("numNulls", sum(df[col].isNull().cast('int') for col in df.columns))
        df = df.filter(col("numNulls") <  int(len(df.columns)/2))
        df = df.drop(col("numNulls"))
        print("Obtained dataset size: ", df.count(), " x ", len(df.columns))

    print("Removing all the rows with at least 1 null value.")
    df = df.dropna('any')
    print("Obtained dataset size: ", df.count(), " x ", len(df.columns))

    return df


def create_daily_path(config_dict, hostgroup,
                      year, month, day):
    """Create path for a single day.

    Params
    ------
    config_dict: dict with the following keys
        hdfs_out_folder (str), date_start (str), date_end_excluded(str),
        overwrite_on_hdfs (boolean), aggregate_every_n_minutes (int),
        history_steps (int), future_steps (int),
        hostgroups (list of str), selected_plugins (dict of dict)
    hostgroup: str
        hostgroup that you want to construct
    year: int
    month: int
    day: int
    """
    base_path = config_dict["hdfs_out_folder"]
    # frequency
    freq_path = \
        "freq=" + str(config_dict["aggregate_every_n_minutes"]) + "min"
    # hostgroups
    escaped_hostgroup = convert_to_slash(hostgroup)
    hostgroup_path = \
        "hostgroup=" + escaped_hostgroup
    # plugins
    plugins = config_dict["selected_plugins"]
    id_plugins = create_id_for_metrics_dict(plugins)
    plugins_path = \
        "plg=" + id_plugins
    # normalization
    hostgroups = config_dict["hostgroups"]
    normalization_id = \
        create_id_for_hostgroups_and_plugins_start_end(
            hostgroups=hostgroups,
            plugins=plugins,
            start=config_dict["date_start_normalization"],
            end=config_dict["date_end_normalization_excluded"])
    normalization_path = \
        "norm=" + normalization_id
    # history
    history_path = \
        "h=" + str(config_dict["history_steps"])
    # future
    future_path = \
        "f=" + str(config_dict["future_steps"])
    # create parts around the day
    first_part = \
        "/".join([base_path,
                  freq_path,
                  hostgroup_path])
    last_part = \
        "/".join([plugins_path,
                  normalization_path,
                  history_path,
                  future_path])

    str_year = "year=" + str(year)
    str_month = "month=" + str(month)
    str_day = "day=" + str(day)
    new_path = \
        first_part + \
        "/" + str_year + "/" + str_month + "/" + str_day + "/" + \
        last_part
    return new_path


def _compute_timestamps_in_seconds(config_dict):
    """Compute the timestamp that have to be inspected with this pace."""
    # since the data are dpwnloaded including the entire day of the
    # ending date (but the pd.datarange is not including the entire day
    # in the genration of intervals), we ad one day to the end date
    print("Computing the timestamps given the dates")
    ts_start_second = datetime.strptime(config_dict["date_start"], "%Y-%m-%d")
    ts_end_second = datetime.strptime(
        config_dict["date_end_excluded"], 
        "%Y-%m-%d"
    ) + timedelta(days=1)
    print(str(ts_start_second) + " - " + str(ts_end_second))

    return list(
        range(int(ts_start_second.timestamp()),
        int(ts_end_second.timestamp()),
        60 * config_dict["aggregate_every_n_minutes"])
    )


class WindowIterator(object):

    def __init__(self, spark, config_filepath, slide_step, hostgroup,
                 keep_all_in_cache=False):
        """Class to iterate over the windows.

        Params
        ------
        spark: spark context
        config_filepath: path to json with the following keys:
            hdfs_out_folder (str), date_start (str), date_end_excluded(str),
            overwrite_on_hdfs (boolean), aggregate_every_n_minutes (int),
            history_steps (int), future_steps (int),
            hostgroups (list of str), selected_plugins (dict of dict)
        slide_step: int
            nr of steps between two time windows, a timestep is as long as
            specified in the aggregate_every_n_minutes key of the config_file
            dictionary.
        hostgroup: str
            hostgroup that you want to get, it must be in the config file too,
            as an element in the list "hostgroups" key
        keep_all_in_cache: bool
            if True all the windows will be persisted.
            if Flase you persist only one day window
        """
        self.spark = spark
        self.config_filepath = config_filepath
        self.slide_step = slide_step
        self.hostgroup = hostgroup
        self.keep_all_in_cache = keep_all_in_cache

        self.config_dict = read_json_config(config_filepath)
        self.current_window = 0
        self.last_path = ""
        self.last_window_day = None
        self.timestamps_available = \
            list(sorted(_compute_timestamps_in_seconds(self.config_dict)))

    def __next__(self):
        """Pass the next time window."""
        current_window = self.current_window
        if current_window >= len(self.timestamps_available):
            self.last_window_day.unpersist()
            raise StopIteration
        current_timestamp = self.timestamps_available[current_window]
        dt = datetime.fromtimestamp(current_timestamp)

        path = create_daily_path(config_dict=self.config_dict,
                                 hostgroup=self.hostgroup,
                                 year=dt.year, month=dt.month, day=dt.day)
        print(path)
        # reading
        df_window_day = self.spark.read.parquet(path)

        # when I get a new file, cache it and
        if path != self.last_path:
            if self.last_window_day is not None:
                self.last_window_day.unpersist()
            df_window_day.persist()
            self.last_window_day = df_window_day
        print("filtering")
        df_only_relevant_window = \
            df_window_day\
            .filter((col("timestamp") == current_timestamp))

        print("conversion to pandas")
        pdf_window = df_only_relevant_window.toPandas()
        print("converted")
        # window_X = pdf_window[~["timestamp", "hostname", "ts"]]
        # window_prov = pdf_window[~["hostname", "timestamp"]]
        self.current_window += 1
        return pdf_window

    def __iter__(self):
        """Iterate over the window."""
        return WindowIterator(spark=self.spark,
                              config_filepath=self.config_filepath,
                              slide_step=self.slide_step,
                              hostgroup=self.hostgroup,
                              keep_all_in_cache=self.keep_all_in_cache)
