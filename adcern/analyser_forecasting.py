"""Using Forecasting Methods for Outlier Detection."""
# Author: Matteo Paltenghi <matteo.paltenghi@cern.ch> # noqa

import scipy  # noqa
import numpy as np
import random
import pandas as pd

# new
import tensorflow as tf  # noqa
from tensorflow.keras.losses import mean_squared_error
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten

from sklearn.utils import check_array
from sklearn.utils.validation import check_is_fitted

from pyod.models.base import BaseDetector
from pyod.utils.stat_models import pairwise_distances_no_broadcast

# CNN
from tensorflow.keras.models import Model  # noqa
from tensorflow.keras.layers import Input  # noqa
from tensorflow.keras.layers import Conv2D  # noqa
from tensorflow.keras.layers import MaxPooling2D  # noqa
from tensorflow.keras.layers import UpSampling2D  # noqa

# TS Modelling
from statsmodels.tsa.api import VAR


class ForecastCNN(BaseDetector):

    def __init__(self, nr_timeseries, nr_timesteps, chunk_len,
                 receptive_field_len=3,
                 hidden_activation='relu', output_activation='sigmoid',
                 loss=mean_squared_error, optimizer='adam',
                 epochs=2, batch_size=32, dropout_rate=0.2,
                 l2_regularizer=0.1, validation_size=0.1,
                 verbose=1, random_state=None, contamination=0.1):
        """CNN predictor that is sliding acros multiple timeseries.

        Params
        ------
        nr_timeseries: (int), mandatory
            The number of time series taken as input.
        chunk_len: (int), mandatory
            The length of the history for each prediction. It must be shorter
            than nr_timesteps. (e.g. if nr_timesteps = 48, it can be 8, that
            means that we will slide a window of length 8 over the 48 timestep
            and predict the next sample)
        nr_timesteps: (int), optional
            the number of timesteps present for each timeseries. this can be
            inferred by the length of the input and the nr of timeseries
        receptive_field_len: (int), default 3
            filter dimension.
        """
        super(ForecastCNN, self).__init__(contamination=contamination)
        # for CNN
        self.nr_timeseries = nr_timeseries
        self.nr_timesteps = nr_timesteps
        self.chunk_len = chunk_len
        self.receptive_field_len = receptive_field_len
        self.plugin_names = None
        # from AE
        # self.hidden_neurons = hidden_neurons
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation
        self.loss = loss
        self.optimizer = optimizer
        self.epochs = epochs
        self.batch_size = batch_size
        self.dropout_rate = dropout_rate
        self.l2_regularizer = l2_regularizer
        self.validation_size = validation_size
        # self.preprocessing = preprocessing
        self.verbose = verbose
        self.random_state = random_state
        self.contamination = contamination

    def fit(self, X, y=None):
        """Fit the CNN detector. y is ignored in unsupervised methods.

        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The input samples.
            The input for each entity should consist in a concatenation of the
            n timeseries related to the same entity. Each time series must
            start with the most recent timestamp.
            e.g. Given two timeseries of the same entity: ts1 and ts2
            where ts1_0 is the most recent data of ts1 and ts1_n is the n-th
            most recent data of ts1, we want the input to be like:
            ts1_0, ts1_1, ts1_2, ..., ts1_n, ts2_0, ts2_1, ts2_2, ..., ts2_n
        y : Ignored
            Not used, present for API consistency by convention.
        Returns
        -------
        self : object
            Fitted estimator.
        """
        columns = X.columns
        # if y is None:
        #     raise Exception("Labels Required, use y parameter.")
        # remove the suffix from column name
        if self.plugin_names is None:
            self.plugin_names = list(set([x[:x.rfind("_")] for x in columns]))
        if len(self.plugin_names) != self.nr_timeseries:
            print("Nr timeseries (%i) doesn't match the actial "
                  "number of plugins" %
                  self.nr_timeseries)
            [print("> ", plg) for plg in self.plugin_names]
        # reshape the images
        X_image_original = np.reshape(X.values,
                                      (len(X),
                                       self.nr_timeseries,
                                       self.nr_timesteps, 1))

        self.model_ = self._build_model(self.receptive_field_len)
        self.model_.compile(optimizer='adadelta',
                            loss=mean_squared_error)
        errors = np.expand_dims(np.zeros(len(X)), axis=0)
        print("errors: ", errors.shape)

        total_epochs = 0

        # for every chunk (e.g. window of 8 steps)
        for start_chunk in range(1, self.nr_timesteps - self.chunk_len):
            end_chunk = start_chunk + self.chunk_len
            # the timesteps on the left/begin ar the h0
            # whereas at the end/tail there is h47
            X_image_past = X_image_original[:, :, start_chunk:end_chunk, :]
            # print(X_image_past.shape)
            # print(X_image_past[:5])
            X_image_future = X_image_original[:, :, start_chunk, 0]
            # print(X_image_future.shape)
            # print(X_image_future[:5])

            # make sure not to introduce a nan
            X_image = np.nan_to_num(X_image_past, nan=0, posinf=0, neginf=0)
            y = np.nan_to_num(X_image_future, nan=0, posinf=0, neginf=0)
            # print("X_image type: ", type(X_image))
            # print("y type: ", type(y))
            # train
            self.history_ = \
                self.model_.fit(X_image, y,
                                initial_epoch=total_epochs,
                                epochs=total_epochs + self.epochs,
                                batch_size=self.batch_size,
                                shuffle=True,
                                validation_split=self.validation_size,
                                verbose=self.verbose).history
            total_epochs += self.epochs
            # compute the error in every chunck after training
            pred_scores = self.model_.predict(X_image)
            error_on_this_chunk = \
                np.expand_dims(pairwise_distances_no_broadcast(y, pred_scores),
                               axis=0)
            # print("BEFORE STACK")
            # print("A: error_on_this_chunk: ", error_on_this_chunk.shape)
            # print("B: errors: ", errors.shape)
            errors = np.vstack((errors, error_on_this_chunk))
            # print("AFTER STACK")
            # print("errors: ", errors.shape)

        self.decision_scores_ = \
            np.sum(errors, axis=0)
        print("self.decision_scores_.shape: ", self.decision_scores_.shape)
        # slide on the same sample in prediction mode to compute the
        # decision scores

        self._process_decision_scores()
        return self

    def _build_model(self, receptive_field=3):
        """Create the CNN model."""
        input_img = Input(shape=(self.nr_timeseries, self.chunk_len, 1))
        # adapt this if using `channels_first` image data format

        x = Conv2D(4, (self.nr_timeseries, self.receptive_field_len),
                   activation=self.hidden_activation, padding='same')(input_img)  # A # noqa
        x = MaxPooling2D((1, 2), padding='same')(x)  # B
        x = Conv2D(2, (self.nr_timeseries, self.receptive_field_len),
                   activation=self.hidden_activation, padding='same')(x)  # C
        encoded = MaxPooling2D((1, 4), padding='same')(x)  # D
        flattened_encoded = Flatten()(encoded)
        dense_layer_1 = Dense(50, activation='tanh')(flattened_encoded)
        dense_layer_2 = Dense(20, activation='tanh')(dense_layer_1)
        output = Dense(self.nr_timeseries, activation='sigmoid')(dense_layer_2)

        cnn_predictor = Model(inputs=input_img,
                              outputs=output)
        cnn_predictor.summary()
        return cnn_predictor

    def decision_function(self, X, y=None):
        """Predict raw anomaly score of X using the fitted detector.

        The anomaly score of an input sample is computed based on different
        detector algorithms. For consistency, outliers are assigned with
        larger anomaly scores.
        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The training input samples. Sparse matrices are accepted only
            if they are supported by the base estimator.
        y : numpy array of shape (n_samples, n_features_to_predict)
            contains the value of the metrics at h0 timestamp
        Returns
        -------
        anomaly_scores : numpy array of shape (n_samples,)
            The anomaly score of the input samples.
        """
        check_is_fitted(self, ['model_', 'history_'])
        X = check_array(X)

        X_image_original = \
            np.reshape(X,
                       (len(X),
                        self.nr_timeseries, self.nr_timesteps, 1))

        errors = np.expand_dims(np.zeros(len(X)), axis=0)
        # print("errors: ", errors.shape)
        # for every chunk (e.g. window of 8 steps)
        for start_chunk in range(1, self.nr_timesteps - self.chunk_len):
            end_chunk = start_chunk + self.chunk_len
            # the timesteps on the left/begin ar the h0
            # whereas at the end/tail there is h47
            X_image_past = X_image_original[:, :, start_chunk:end_chunk, :]
            # print(X_image_past.shape)
            # print(X_image_past[:5])
            X_image_future = X_image_original[:, :, start_chunk, 0]
            # print(X_image_future.shape)
            # print(X_image_future[:5])

            # make sure not to introduce a nan
            X_image = np.nan_to_num(X_image_past, nan=0, posinf=0, neginf=0)
            y = np.nan_to_num(X_image_future, nan=0, posinf=0, neginf=0)
            # print("X_image type: ", type(X_image))
            # print("y type: ", type(y))
            # compute the error in every chunck after training
            pred_scores = self.model_.predict(X_image)
            error_on_this_chunk = \
                np.expand_dims(pairwise_distances_no_broadcast(y, pred_scores),
                               axis=0)
            # print("BEFORE STACK")
            # print("A: error_on_this_chunk: ", error_on_this_chunk.shape)
            # print("B: errors: ", errors.shape)
            errors = np.vstack((errors, error_on_this_chunk))
            # print("AFTER STACK")
            # print("errors: ", errors.shape)

        # make sure not to introduce a nan
        X_image = np.nan_to_num(X_image, nan=0, posinf=0, neginf=0)
        # print("X_image: ", X_image.shape)
        y = np.nan_to_num(y, nan=0, posinf=0, neginf=0)
        # print("y: ", y.shape)

        outlier_scores = \
            np.sum(errors, axis=0)
        # print("outlier_scores.shape: ", outlier_scores.shape)
        return outlier_scores


class ForecastVAR(BaseDetector):

    def __init__(self, nr_timeseries, nr_timesteps, chunk_len,
                 nr_estimators=2,
                 verbose=1, random_state=None, contamination=0.1):
        """VAR predictor that is sliding acros multiple timeseries.

        Params
        ------
        nr_timeseries: (int), mandatory
            The number of time series taken as input.
        nr_timesteps: (int), optional
            the number of timesteps present for each timeseries. this can be
            inferred by the length of the input and the nr of timeseries
        ar_order: (int), default 3
            order of the autoregression
        sub_window_length: (int)
            nr of timestep used for prediction
        """
        super(ForecastVAR, self).__init__(contamination=contamination)
        # for VAR ensemble
        self.nr_timeseries = nr_timeseries
        self.nr_estimators = nr_estimators
        self.nr_timesteps = nr_timesteps
        self.chunk_len = chunk_len
        self.plugin_names = None
        # self.preprocessing = preprocessing
        self.verbose = verbose
        self.random_state = random_state
        self.contamination = contamination

    def fit(self, X, prov, y=None):
        """Fit the VAR detector. y is ignored in unsupervised methods.

        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The input samples.
            The input for each entity should consist in a concatenation of the
            n timeseries related to the same entity. Each time series must
            start with the most recent timestamp.
            e.g. Given two timeseries of the same entity: ts1 and ts2
            where ts1_0 is the most recent data of ts1 and ts1_n is the n-th
            most recent data of ts1, we want the input to be like:
            ts1_0, ts1_1, ts1_2, ..., ts1_n, ts2_0, ts2_1, ts2_2, ..., ts2_n
        y : Ignored
            Not used, present for API consistency by convention.
        Returns
        -------
        self : object
            Fitted estimator.
        """
        columns = X.columns
        # if y is None:
        #     raise Exception("Labels Required, use y parameter.")
        # remove the suffix from column name
        if self.plugin_names is None:
            self.plugin_names = list(set([x[:x.rfind("_")] for x in columns]))
        if len(self.plugin_names) != self.nr_timeseries:
            print("Nr timeseries (%i) doesn't match the actial "
                  "number of plugins" %
                  self.nr_timeseries)
            [print("> ", plg) for plg in self.plugin_names]

        # create a bag of models VAR
        # each model is learnt form the full multivariate timeseries of
        # a different host
        self.bag_of_models = []
        sampling = random.choices(range(len(prov)), k=self.nr_estimators)
        selection = list(prov.iloc[sampling]["hostname"])
        print(selection)

        # for each host in the selection build a model
        for host in selection:
            plugin_names = sorted(set([x[:x.rfind("_h")] for x in X.columns]))
            print(plugin_names)
            mask = prov["hostname"] == host
            X_single_host = X[mask]
            prov_single_host = prov[mask]
            # join the two and order
            all_single_host = pd.concat([prov_single_host, X_single_host],
                                        axis=1)
            all_single_host.sort_values(by="timestamp", inplace=True)
            # drop prov columns
            all_single_host.drop(columns=["hostname", "ts", "timestamp"],
                                 inplace=True)
            df_windows = []
            for i in range(len(all_single_host)):
                # manipulate one row/window of 8 hours
                # convert it in a dataframe where:
                # columns = name of plugins (present just once)
                # rows = time instant. head = past, tail = future
                one_entity = \
                    X.iloc[i].values.reshape((self.nr_timeseries,
                                              self.nr_timesteps))
                new_values = np.flipud(one_entity.T)
                df_ts_chunk = pd.DataFrame(new_values, columns=plugin_names)
                df_windows += [df_ts_chunk]
            # concatenate all the new dataframes chunck
            # and create the overall history
            all_history_one_host = pd.concat(df_windows)
            # rshape
            print(all_history_one_host.shape)
            # train the VAR model
            this_host_model = VAR(all_history_one_host.fillna(value=0))
            fitte_this_host_model = this_host_model.fit(2)
            # add to the ensemble
            self.bag_of_models += [fitte_this_host_model]

        outlier_scores = self.decision_function(X)

        print("Nr outlier scores learnt during trianing: ",
              len(outlier_scores))

        self.decision_scores_ = outlier_scores

        self._process_decision_scores()
        return self

    def _build_model(self, receptive_field=3):
        """Create the CNN model."""
        input_img = Input(shape=(self.nr_timeseries, self.chunk_len, 1))
        # adapt this if using `channels_first` image data format

        x = Conv2D(4, (self.nr_timeseries, self.receptive_field_len),
                   activation=self.hidden_activation, padding='same')(input_img)  # A # noqa
        x = MaxPooling2D((1, 2), padding='same')(x)  # B
        x = Conv2D(2, (self.nr_timeseries, self.receptive_field_len),
                   activation=self.hidden_activation, padding='same')(x)  # C
        encoded = MaxPooling2D((1, 4), padding='same')(x)  # D
        flattened_encoded = Flatten()(encoded)
        dense_layer_1 = Dense(50, activation='tanh')(flattened_encoded)
        dense_layer_2 = Dense(20, activation='tanh')(dense_layer_1)
        output = Dense(self.nr_timeseries, activation='sigmoid')(dense_layer_2)

        cnn_predictor = Model(inputs=input_img,
                              outputs=output)
        cnn_predictor.summary()
        return cnn_predictor

    def decision_function(self, X, y=None):
        """Predict raw anomaly score of X using the fitted detector.

        The anomaly score of an input sample is computed based on different
        detector algorithms. For consistency, outliers are assigned with
        larger anomaly scores.
        Parameters
        ----------
        X : numpy array of shape (n_samples, n_features)
            The training input samples. Sparse matrices are accepted only
            if they are supported by the base estimator.
        y : numpy array of shape (n_samples, n_features_to_predict)
            contains the value of the metrics at h0 timestamp
        Returns
        -------
        anomaly_scores : numpy array of shape (n_samples,)
            The anomaly score of the input samples.
        """
        # reshape the images
        X_image_original = np.reshape(X.values,
                                      (len(X),
                                       self.nr_timeseries,
                                       self.nr_timesteps))

        outlier_scores = []
        # for every window/image
        # use the model and compute the error
        for i in range(len(X)):
            error_all_models = []
            # for every fitted model compute the errors vector
            for model_fitted in self.bag_of_models:
                errors_this_model = []
                window_image = X_image_original[i]
                # print("window_image:", window_image.shape)
                # create an array with:
                # columns = name of plugins (present just once)
                # rows = time instant. head = past, tail = future
                vertical_values = np.flipud(window_image.T)
                # print("vertical_values:", vertical_values.shape)
                # given the order of var = 2..
                # give 2 timesteps as input and predict the next one
                for start in range(len(vertical_values) - 3):
                    forecaster_input = vertical_values[start:start + 2]
                    # print("forecaster_input:", forecaster_input.shape)
                    expected_output = vertical_values[start + 2:start + 3]
                    # print("expected_output:", expected_output.shape)
                    # print(expected_output)
                    prediction = \
                        model_fitted.forecast(y=forecaster_input, steps=1)
                    # print("prediction: ", prediction.shape)
                    # print(prediction)
                    # compute the max of the diff
                    # error_this_chunk = np.max(expected_output-prediction)
                    # compute the euclidean distance
                    error_this_chunk = \
                        np.linalg.norm(expected_output - prediction)
                    # print("error_this_chunk: ", error_this_chunk)
                    errors_this_model.append(error_this_chunk)
                final_error_this_model = np.sum(errors_this_model)
                error_all_models.append(final_error_this_model)
            # ensemble strategy -> average
            outlier_score = np.mean(error_all_models)
            outlier_scores.append(outlier_score)
        return outlier_scores
