#!/usr/bin/env python
import click, sqlite3, json, yaml, shutil, copy
import time, sys, os, inspect, importlib, pytz

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  # noqa

from pathlib import Path
from datetime import datetime
from hashlib import blake2b

os.environ['PYSPARK_PYTHON'] = sys.executable

from adcern.sqlite3_backend import modify_db
from adcern.publisher import create_id_for_hostgroups_and_plugins_start_end  # noqa
import adcern.analyser as analyser

from etl.spark_etl.utils import read_json_config  # noqa
from etl.spark_etl.utils import read_local_window_dataset  # noqa
from etl.spark_etl.utils import hostgroup_mapping  # noqa
from etl.spark_etl.utils import print_exception
from etl.spark_etl.etl_pipeline import get_normalization_pandas  # noqa
from etl.spark_etl.etl_pipeline import pipeline_preparation_norm_coeff  # noqa
from etl.spark_etl.etl_pipeline import run_pipeline_all_in_one  # noqa
from etl.spark_etl.etl_pipeline import materialize_locally  # noqa

from etl.spark_etl import cluster_utils  # noqa

from pyspark import SparkConf  # noqa: E402

def create_instance(module_name, class_name, init_params_dict):
    """Create an instance of a class from the name and params.

    Params
    ------
    module_name: str
        name of the file containing the class
    class_name: str
        name of the class inside the file
    init_params_dict: dict(str)
        dictionary of parameters to initialize the class.
    """
    # import the module
    try:
        my_module_with_analyser = importlib.import_module(module_name)
    except Exception as e:
        print_exception('ERROR importing ' + module_name, e)
        raise

    # get analyser class
    try:
        my_analyser_class = getattr(my_module_with_analyser, class_name)
    except Exception as e:
        print_exception('ERROR with the specific class ' + class_name, e)
        raise

    # instantiate the class
    try:
        instance = my_analyser_class(**init_params_dict)
    except Exception as e:
        print_exception('ERROR with the parameters ' + init_params_dict, e)
        raise

    print("Analyser instance created: \n" + str(instance))
    return instance

def spark_preparation(config_file=None):
    """Preparation of the spark context.

    Params
    ------
    config_file: str
        name of the file containing the configuration

    Return
    ------
    sc: spark context
        context of spark
    spark: spark session
        session of spark
    conf: spark config
        configuration object of the spark context
    """
    try:
        # PREPARE SPARK
        print('PREPARING SPARK:')

        # Set spark_conf to None, if it remains None set_spark uses default parameters
        spark_conf = None
        debug = False

        if config_file is not None:
            config_dict = read_json_config(config_file)
            if 'spark_conf' in config_dict:
                spark_conf = SparkConf()
                attributes = config_dict['spark_conf']
                print("Spark attributes found in config_file: \n" + str(attributes))
                for a in attributes:
                    if a != 'debug':
                        spark_conf.set(a, attributes[a])
                if 'debug' in attributes:
                    debug = attributes['debug']

        if debug:
            print("SPARK CONFIGURATION: \n" + str(spark_conf))
        sc, spark, conf = cluster_utils.set_spark(spark_conf=spark_conf,
                                                  debug=debug)
        print('SPARK CONTEXT: ' + str(sc))
        print('SPARK OBJECT: ' + str(spark))
    except Exception as e:
        print_exception('ERROR WITH SPARK ACCESS!', e)
        sys.exit(1)
    return sc, spark, conf

def read_resource(resource_file, yaml=False):
    """Read, print and return the resource configuration file
    
    params
    ------
    resource_file: str
        path of the resource_file to be printed
    """
    try:
        print("RESOURCE DETAILS: %s" % resource_file)
        if yaml:
            with open(resource_file) as yaml_file:
                file_content = yaml.safe_load(yaml_file)
        else:
            file_content = read_json_config(resource_file)
        print(json.dumps(file_content, indent=4))
    except Exception as e:
        print_exception('ERROR READING THE FILE', e)
        sys.exit(1)
    return file_content

def save_scores_local_sqlite(analyser, score_folder, config_file,
                             scores, entities, end_window):
    """Save scores in a Sqlite3 database.

    Every tuple (score, entity, end_window) becomes a new record of the
    table scores. It also add columns for:
    - algorithm: name of the class representiong this analyser
    - hostgroup: name of the hostgroup
    - noramlization_id: condensing hostgroup, plugins, normalization start
        normalization end.

    Params
    ------
    score_folder: str
        folder where the database is (if not present it is created)
    config_file: str
        path to the JSON file contining the configuration of the test
    scores: list(float)
        scores of the server, each float is the score of one server
    entities: list(str)
        names of the servers, each string is the name of one server
    end_window: int
        the timestamp of the end of the window. it is broadcasted to all
        the records
    """
    # get general info for every line
    config_dictionary = read_json_config(config_file)
    hostgroup = config_dictionary["hostgroups"][0]
    algorithm_name = analyser.__class__.__name__
    if algorithm_name == "PyODWrapperAnalyzer":
        algorithm_name = analyser.pyod_detector.__class__.__name__

    normalization_id = create_id_for_hostgroups_and_plugins_start_end(
        hostgroups=config_dictionary["hostgroups"],
        plugins=config_dictionary["selected_plugins"],
        start=config_dictionary["date_start_normalization"],
        end=config_dictionary["date_end_normalization_excluded"]
    )

    # preapre result to inject
    df = pd.DataFrame({"hostgroup": str(hostgroup),
                       "hostname": entities,
                       "algorithm": str(algorithm_name),
                       "score": scores,
                       "end_window": str(int(end_window)),
                       "noramlization_id": str(normalization_id)})
    # connect to the db
    conn_score = sqlite3.connect(
        score_folder + '/scores_' + algorithm_name + '.db', 
        timeout=120
    )

    # ensure the table is there
    modify_db(
        conn=conn_score,
        query='''CREATE TABLE IF NOT EXISTS scores
                (hostgroup text, hostname text, algorithm text,
                score real, end_window int, noramlization_id text,
                PRIMARY KEY (hostgroup, hostname, algorithm, end_window,
                noramlization_id))''',
        upperbound=10)

    # add row by row
    num_rows = len(df)
    for i in range(num_rows):
        # Try inserting the row
        row = df.iloc[i]
        modify_db(
            conn=conn_score,
            query='''INSERT OR IGNORE INTO scores
                    VALUES (?, ?, ?, ?, ?, ?)''',
            upperbound=10,
            params=row)

    conn_score.close()


def save_scores_local_parquet(algorithm_name, algo_parameters, folder_score,
                              config_file, scores, normalized_scores, entities, end_window):
    """Save scores in a parquet file.

    Every tuple (score, entity, end_window) becomes a new record.
    It also add columns for:
    - algorithm: name of the class representiong this analyser
    - hostgroup: name of the hostgroup
    - noramlization_id: condensing hostgroup, plugins, normalization start
        normalization end.

    Params
    ------
    algorithm_name: str
        name used to save this data
    algo_parameters: dict(str)
        contains all the parameters of the raw initialized object
    score_folder: str
        folder where the database is (if not present it is created)
    config_file: str
        path to the JSON file contining the configuration of the test
    scores: list(float)
        scores of the server, each float is the score of one server
    entities: list(str)
        names of the servers, each string is the name of one server
    end_window: int
        the timestamp of the end of the window. it is broadcasted to all
        the records
    """
    
    print("Save scores to Parquet...") 
    # get general info for every line
    config_dictionary = read_json_config(config_file)
    hostgroup = config_dictionary["hostgroups"][0]

    normalization_id = create_id_for_hostgroups_and_plugins_start_end(
        hostgroups=config_dictionary["hostgroups"],
        plugins=config_dictionary["selected_plugins"],
        start=config_dictionary["date_start_normalization"],
        end=config_dictionary["date_end_normalization_excluded"]
    )

    # create initial data
    data = {"hostgroup": str(hostgroup),
            "hostname": entities,
            "algorithm": str(algorithm_name),
            "score": scores,
            "normalized_score": normalized_scores,
            "end_window": str(int(end_window)),
            "noramlization_id": str(normalization_id)}

    # append paramenters (if any)
    if algo_parameters is not None:
        data = {**data, **algo_parameters}

    # preapre result to inject
    df = pd.DataFrame(data)

    # algo identifier
    id_algo = "NOPARAMS"
    if algo_parameters is not None:
        h_algo = blake2b(digest_size=5)
        h_algo.update(str.encode(algorithm_name +
                                 json.dumps(algo_parameters)))
        id_algo = h_algo.hexdigest()
        
    # dump in a csv
    unix_to_date = datetime.fromtimestamp(end_window, pytz.timezone("Europe/Zurich")).strftime('%Y_%m_%d-%H:%M')
    algo_score_path = folder_score + '/' + str(unix_to_date) + '/'             
    Path(algo_score_path).mkdir(parents=True, exist_ok=True)
    
    df.to_parquet(algo_score_path + str(algorithm_name)
                  + '_ID_' + str(id_algo)
                  + '.parquet',
                  index=False)
    
def normalize_names(name):
    print("Replacing _ with - in " + name)
    return name.replace("_", "-")

@click.group(context_settings={"token_normalize_func": normalize_names})
def cli():
    print("******************************\n")
    print("Welcome in the Mining and Detection CLI.")
    print("\n******************************\n")

@cli.command()
@click.option('--resource_file', default="",
              help="""path to json file defining whcih normaliz to check.""")
def normalization_presence(resource_file):
    """Check for the presence of normalization coefficients."""
    sc, spark, conf = spark_preparation(resource_file)
    file_content = read_resource(resource_file=resource_file)

    # CHECK if the NORMALIZATION is already present
    try:
        try:
            print("Reading normalization_schema parameters.")
            schema_path = file_content["schemas"]["norm_path"]
        except Exception as e:
            print("Parameters not found. Setting schema_path to None.")
            schema_path = None
        norm_pdf = get_normalization_pandas(spark=spark,
                                            config_filepath=resource_file,
                                            schema_path=schema_path)
    except Exception as e:
        print_exception("FAILURE - NORMALIZATION COEFFICIENTS NOT PRESENT :/", e)
        sys.exit(1)
    print("NORMALIZATION COEFFICIENTS FOUND :)")
    print('Printing the first lines of the dataframe.')
    norm_pdf.head(40)


@cli.command()
@click.option('--resource_file', default="",
              help="""path to json file defining which coeff to compute.""")
def compute_normalization(resource_file):
    """Compute normalization coefficients."""
    sc, spark, conf = spark_preparation(resource_file)
    file_content = read_resource(resource_file=resource_file)
    
    # Compute the normalization coefficients ans save them
    try:
        norm_ret, norm_path = pipeline_preparation_norm_coeff(
            spark=spark,
            config_filepath=resource_file
        )
        print("Coefficient preparation shared: " + str(norm_ret))
        print("Dataframe path: " + norm_path)

        if norm_ret == 1:
            print_exception('FAILURE - the normalization returned 1')
            sys.exit(1)

        try:
            print("Reading normalization_schema parameters.")
            schema_path = file_content["schemas"]["norm_path"]
        except Exception as e:
            print("Parameters not found. Setting schema_path to None.")
            schema_path = None
        norm_pdf = get_normalization_pandas(spark=spark,
                                            config_filepath=resource_file,
                                            schema_path=schema_path)
    except Exception as e:
        print_exception('FAILURE - ERROR DURING COEFFICIENT MINING', e)
        sys.exit(1)
    print('Printing the first lines of the dataframe.')
    print(norm_pdf.head(40))


@cli.command()
@click.option('--resource_file', default="",
              help="""path to json file defining the data we need.""")
def data_presence(resource_file):
    read_resource(resource_file=resource_file)
    # CHECK IF DATA ARE ALREADY AVAILABLE LOCALLY
    try:
        X, prov, nr_timeseries = read_local_window_dataset(
            config_filepath=resource_file
        )
    except Exception as e:
        print_exception('FAILURE - NO DATA LOCALLY', e)
        sys.exit(1)
    print('SUCCESS - CACHE HIT - DATA ARE ALREADY AVAILABLE LOCALLY.')



@cli.command()
@click.option('--resource_file', default="",
              help="""path to json file defining what to download.""")
def transform_data(resource_file):
    sc, spark, conf = spark_preparation(resource_file)
    read_resource(resource_file=resource_file)

    # TRANSFORM THE DATA WITH A LONG MINING PROCESS
    try:
        # CREATE WINDOWS
        run_pipeline_all_in_one(spark=spark,
                                config_filepath=resource_file)
    except Exception as e:
        print_exception('FAILURE - PROBLEM OCCURRED IN SPARK MINING PROCEDURE', e)
        sys.exit(1)
    print('SUCCESS - DATA AGGREGATED IN HDFS.')


@cli.command()
@click.option('--resource_file', default="",
              help="""path to json file defining what to cache.""")
def copy_locally(resource_file):
    """Copy your data locally (aka move them from spark to local disk)."""
    # PREPARE SPARK
    sc, spark, conf = spark_preparation(resource_file)
    # READ RESOURCE FILE
    file_content = read_resource(resource_file=resource_file)

    print("ASSUMPTION: no data was in cache before and "
          "if anything is there, it will be deleted.")
    print("Deleting any old remainders...")
    dir_path = file_content["local_cache_folder"] + "/" + file_content["code_project_name"] + "/"
    try:
        shutil.rmtree(dir_path)
    except Exception as e_delete:
        print_exception('FAILURE - DURING CACHE CLEANING', e_delete)
        print("Continuing anyway!")
        
    # Create folders locally
    print("Creating the new folder")
    Path(file_content["local_cache_folder"] + "/").mkdir(parents=True, exist_ok=True)
    print("STARTING THE CACHE CREATION")
    try:
        # MATERIALIZE IN CACHE
        materialize_locally(spark=spark,
                            config_filepath=resource_file)
        # save metadata also
        new_config_file_name = file_content["local_cache_folder"] + "/" + \
            file_content["code_project_name"] + ".metadata"
        print("Save the config file locally: %s" % new_config_file_name)

        with open(new_config_file_name, 'w') as file:
            json.dump(file_content, file)
            file.close()
    except Exception as e:
        print('FAILURE - DURING CACHE CREATION:')
        print("Detail Error: ", e)
        sys.exit(1)
    print("SUCCESS - CACHED DATA AVAILABLE LOCALLY")

@cli.command()
@click.option('--analysis_file_path', default="",
              help='''path of the YAML to describe the ANALYSIS.''')
@click.option('--algorithms', default="",
              help='''Run only specific subset of algorithms described in resource file. String array of algo names separed by comma''')
@click.option('--retrain', '-r', is_flag=True, help="Retrain model even when saved model is available.")
def training(analysis_file_path, algorithms="", retrain=False):
    """Train models."""
        
    # read yaml
    with open(analysis_file_path) as yaml_file:
        analysis_dict = yaml.safe_load(yaml_file)
    
    training_file_path = analysis_dict['training_file_path']

    algo_and_params = analysis_dict['algo_and_params']
    algo_names = algo_and_params.keys()
    
    # select only subset of algorithms, otherwise run all algos specified in yaml
    if algorithms:
        algo_names_argument = algorithms.split(",")
        # intersection of algo names provided in yaml and from argument have no intersection
        algo_names = list(set(algo_names).intersection(algo_names_argument))
        if not algo_names:
            print("Invalid specification of algorithm names in argument, using original names from YAML.")
            algo_names = algo_and_params.keys()  
            
    folder_time = analysis_dict["folder_time"]
    folder_score = analysis_dict["folder_local_scores"]
    folder_models = analysis_dict["folder_models"] + "/"
    # create folder if they do not exist
    Path(folder_time).mkdir(parents=True, exist_ok=True)
    Path(folder_score).mkdir(parents=True, exist_ok=True)
    Path(folder_models).mkdir(parents=True, exist_ok=True)
        

    PUBLISH_PER_WINDOW = analysis_dict["publish_per_windows"]
    RANDOM_SEED = analysis_dict["random_seed"]
    # HISTORY_LEN = analysis_dict["history_steps"]
    SLIDE_STEPS = analysis_dict["slide_steps"]
    # ALGO_NAME = analysis_dict["algo_name"]    
                     
    print("Running training for following algorithms:", algo_names)
    
    for algo in algo_names:
        class_name = algo_and_params[algo]['import_path'].split(".")[-1]
        module_name = algo_and_params[algo]['import_path'].rsplit(".", 1)[0]
        alias_name = algo
        hyperparameters = algo_and_params[algo]['parameters']
        train_on_test = algo_and_params[algo]['train_on_test']
        subsample_for_train = algo_and_params[algo]['subsample_for_train']
            
        print("Start " + class_name)

        print("Hyperparameters Analyser:")
        print("type(hyperparameters) -> ", type(hyperparameters))
        print("hyperparameters:", hyperparameters)
        train_on_test = str(train_on_test)
        print("train_on_test: ", train_on_test)
        subsample_for_train = int(subsample_for_train)
        print("subsample_for_train: ", subsample_for_train)

        # Initialize ANALYZER INSTANCE
        core_analyser_instance = \
            create_instance(module_name=module_name,
                            class_name=class_name,
                            init_params_dict=hyperparameters)
        analyser_instance = \
            analyser.PyODWrapperAnalyzer(core_analyser_instance)
            
        if analyser_instance.model_exists(folder_models + alias_name + "_SAVED_MODEL") and not retrain:
            print("Trained model for", alias_name, "found at:", folder_models," - skipping training.")
            continue

        algo_name = class_name

        # SUBSAMPLE (IF SPECIFIED)
        SAMPLES_FOR_TRAIN = None
        if "max_samples_for_train" in analysis_dict.keys():
            SAMPLES_FOR_TRAIN = analysis_dict["max_samples_for_train"]

        # USE A LARGER SAMPLE IF DEEP LEARNING BASED
        if ((class_name in ["AEDenseTF2", "AECnnTF2", "AELstmTF2", "ForecastCNN"])
                and ("max_samples_for_train_deep" in analysis_dict.keys())):
            SAMPLES_FOR_TRAIN = analysis_dict["max_samples_for_train_deep"]

        # USE THE NUMEBR SPECIFIED AT THE ALGORITHM LEVEL
        if subsample_for_train > 0:
            SAMPLES_FOR_TRAIN = subsample_for_train

        # USE ALL THE SAMPLES
        if subsample_for_train == -1:
            SAMPLES_FOR_TRAIN = None

        # USE ONLY A SUBSET OF METRICS
        if "list_metrics_to_keep" in analysis_dict.keys():
            LIST_METRICS_TO_KEEP = analysis_dict["list_metrics_to_keep"]
        else:
            LIST_METRICS_TO_KEEP = None  

        # TRAIN
        X_train, prov_train, nr_timeseries = \
            read_local_window_dataset(config_filepath=training_file_path,
                                    list_metrics_to_keep=LIST_METRICS_TO_KEEP)
            
        print("Analyser instance before train: ", str(analyser_instance))

        # get name and parameters (if any)
        algo_parameters = None
        if analyser_instance.__class__.__name__ == "PyODWrapperAnalyzer":
            all_parameters = \
                inspect.getmembers(analyser_instance.pyod_detector,
                                lambda a: not(inspect.isroutine(a)))
            algo_parameters = \
                dict([("param_" + p[0], str(p[1]))
                    for p in all_parameters if p[0][0] != "_"])
        # use the alias to identify the algo (if available)
        # otherwise use the class name
        algorithm_name = class_name
        if alias_name is not None:
            algorithm_name = alias_name

        print("Algorithm name: ", algorithm_name)
        print("Algo str representation: ", str(analyser_instance))

        # skip the train if the algo is used to fit - predict directly
        # at test time on one single temporal window of the various machines
        # in the swarn
        if train_on_test == "False":
            print("Full model training (e.g. on the entire prev week)")
            algo = analyser_instance
            # TRAIN MODEL
            train_dataset = X_train

            algo_name = str(algo)

            start_fit = time.time()

            if SAMPLES_FOR_TRAIN is not None:
                if len(train_dataset) > SAMPLES_FOR_TRAIN:  # noqa
                    print("Subsampling: ", SAMPLES_FOR_TRAIN)
                    np.random.seed(RANDOM_SEED)
                    # randomly select the desired number of samples
                    train_dataset = \
                        X_train.sample(
                            n=SAMPLES_FOR_TRAIN,
                            random_state=np.random.RandomState(seed=RANDOM_SEED))

            if class_name == "ForecastVAR":
                # forward the provenance to the VAR learner because needs it to
                # reconstruct the time series starting from the windows of the
                # same machine
                algo.fit(train_dataset, prov=prov_train, forward_prov=True)
            else:
                # FIT THE METHOD
                print("Fitting the methods...")
                algo.fit(train_dataset, prov=prov_train)

            end_fit = time.time()
            # register time required by this algo to fit the data
            training_time = float(end_fit - start_fit)

            # SAVE TRAINING TIME
            with open(training_file_path) as json_file:
                data_dict_train = json.load(json_file)
            # connect to the db
            conn = sqlite3.connect(folder_time + '/time.db', timeout=120)

            modify_db(
                    conn = conn, 
                    query = '''CREATE TABLE IF NOT EXISTS training_time
                        (date_start text, date_end_excluded text,
                        long_algo_description text,
                        training_time real, measurement_time text,
                        PRIMARY KEY (date_start, date_end_excluded,
                                    long_algo_description,
                                    measurement_time))''',
                    upperbound = 10)

            modify_db(
                    conn = conn, 
                    query = '''INSERT INTO training_time
                        VALUES (?, ?, ?, ?, datetime('now', 'localtime'))''',
                    upperbound = 10,
                    params = [data_dict_train["date_start"],
                        data_dict_train["date_end_excluded"],
                        algo_name,
                        training_time])

            conn.close()

        algo = analyser_instance
        print(algo)
        
        # SAVE MODEL
        print("Saving model of", alias_name, "to: ", folder_models + alias_name + "_SAVED_MODEL")
        algo.save_model(folder_models + alias_name + "_SAVED_MODEL")
             
    pass

@cli.command()
@click.option('--analysis_file_path', default="",
              help='''path of the YAML to describe the ANALYSIS.''')
@click.option('--algorithms', default="",
              help='''Run only specific subset of algorithms described in resource file. String array of algo names separed by comma''')
def inference(analysis_file_path, algorithms=""):
    """Analyse data and produce anomaly score."""
        
    # read yaml
    with open(analysis_file_path) as yaml_file:
        analysis_dict = yaml.safe_load(yaml_file)
    
    inference_file_path = analysis_dict['inference_file_path']

    algo_and_params = analysis_dict['algo_and_params']
    algo_names = algo_and_params.keys()
    
    # select only subset of algorithms, otherwise run all algos specified in yaml
    if algorithms:
        algo_names_argument = algorithms.split(",")
        # intersection of algo names provided in yaml and from argument have no intersection
        algo_names = list(set(algo_names).intersection(algo_names_argument))
        if not algo_names:
            print("Invalid specification of algorithm names in argument, using original names from YAML.")
            algo_names = algo_and_params.keys()            
    
    folder_time = analysis_dict["folder_time"]
    folder_score = analysis_dict["folder_local_scores"]
    folder_models = analysis_dict["folder_models"] + "/"

    # create folder if they do not exist
    Path(folder_time).mkdir(parents=True, exist_ok=True)
    Path(folder_score).mkdir(parents=True, exist_ok=True)

    PUBLISH_PER_WINDOW = analysis_dict["publish_per_windows"]
    RANDOM_SEED = analysis_dict["random_seed"]
    # HISTORY_LEN = analysis_dict["history_steps"]
    SLIDE_STEPS = analysis_dict["slide_steps"]
    # ALGO_NAME = analysis_dict["algo_name"]      
                     
    print("Running inference for following algorithms:", algo_names)
    
    for algo in algo_names:
        class_name = algo_and_params[algo]['import_path'].split(".")[-1]
        module_name = algo_and_params[algo]['import_path'].rsplit(".", 1)[0]
        alias_name = algo
        hyperparameters = algo_and_params[algo]['parameters']
        train_on_test = algo_and_params[algo]['train_on_test']
        subsample_for_train = algo_and_params[algo]['subsample_for_train']
                    
        print("Start " + class_name)

        print("Hyperparameters Analyser:")
        print("type(hyperparameters) -> ", type(hyperparameters))
        print("hyperparameters:", hyperparameters)
        train_on_test = str(train_on_test)
        print("train_on_test: ", train_on_test)
        subsample_for_train = int(subsample_for_train)
        print("subsample_for_train: ", subsample_for_train)

        # Initialize ANALYZER INSTANCE
        core_analyser_instance = \
            create_instance(module_name=module_name,
                            class_name=class_name,
                            init_params_dict=hyperparameters)
        analyser_instance = \
            analyser.PyODWrapperAnalyzer(core_analyser_instance)
            
        print("Loading model of", alias_name,"from: ", folder_models + alias_name + "_SAVED_MODEL")    
        analyser_instance.load_model(folder_models + alias_name + "_SAVED_MODEL")
        
        analyser_instance.prepare_to_publish_config(
            config_file=inference_file_path,
            slide_steps=SLIDE_STEPS,
            algo_name = algo)


        algo_name = class_name

        # SUBSAMPLE (IF SPECIFIED)
        SAMPLES_FOR_TRAIN = None
        if "max_samples_for_train" in analysis_dict.keys():
            SAMPLES_FOR_TRAIN = analysis_dict["max_samples_for_train"]

        # USE A LARGER SAMPLE IF DEEP LEARNING BASED
        if ((class_name in ["AEDenseTF2", "AECnnTF2", "AELstmTF2", "ForecastCNN"])
                and ("max_samples_for_train_deep" in analysis_dict.keys())):
            SAMPLES_FOR_TRAIN = analysis_dict["max_samples_for_train_deep"]

        # USE THE NUMEBR SPECIFIED AT THE ALGORITHM LEVEL
        if subsample_for_train > 0:
            SAMPLES_FOR_TRAIN = subsample_for_train

        # USE ALL THE SAMPLES
        if subsample_for_train == -1:
            SAMPLES_FOR_TRAIN = None

        # USE ONLY A SUBSET OF METRICS
        if "list_metrics_to_keep" in analysis_dict.keys():
            LIST_METRICS_TO_KEEP = analysis_dict["list_metrics_to_keep"]
        else:
            LIST_METRICS_TO_KEEP = None

        # TEST
        X_test, prov_test, nr_timeseries = \
            read_local_window_dataset(config_filepath=inference_file_path,
                                    list_metrics_to_keep=LIST_METRICS_TO_KEEP)
            
        hg_map = hostgroup_mapping(config_filepath=inference_file_path)

        print("Analyser instance before train: ", str(analyser_instance))

        # get name and parameters (if any)
        algo_parameters = None
        if analyser_instance.__class__.__name__ == "PyODWrapperAnalyzer":
            all_parameters = \
                inspect.getmembers(analyser_instance.pyod_detector,
                                lambda a: not(inspect.isroutine(a)))
            algo_parameters = \
                dict([("param_" + p[0], str(p[1]))
                    for p in all_parameters if p[0][0] != "_"])
        # use the alias to identify the algo (if available)
        # otherwise use the class name
        algorithm_name = class_name
        if alias_name is not None:
            algorithm_name = alias_name

        print("Algorithm name: ", algorithm_name)
        print("Algo str representation: ", str(analyser_instance))

        algo = analyser_instance
        print(algo)

        # keep a reference copy of the not-trained model for the analyser
        # a brand new copy will be used for every window
        if train_on_test == "True":
            reference_copy_algo = copy.deepcopy(algo)
            print("Deep reference copy created")

        timestamps = sorted(prov_test["timestamp"].unique())

        start_inference = time.time()

        for ts in timestamps:

            print("Current window raw ts: ", ts)
            mask_current_window = prov_test["timestamp"] == ts
            X_window = X_test[mask_current_window]
            prov_window = prov_test[mask_current_window]

            # focus on a window it it is not empty
            if (len(X_window) > 1):
                print("Enough data: ok")

                print("-" * 60)
                print("Timestamp: ", prov_window.iloc[0, 1])
                print("=" * 60)

               # run algorithm
                if train_on_test == "True":
                    print("TRAIN ON TEST")
                    print("We test directly on new data at test time.")
                    print("Training a brandnew model for this window")
                    algo = copy.deepcopy(reference_copy_algo)
                    try:
                        algo.fit(X_window, prov=prov_window)
                        # backwards compatibility for PercScore to compute mean_train / std_train
                        algo.get_train_scores()
                    except AttributeError as e:
                        print(e)
                        print("No train scores saved during train...")
                        print("Never mind, we directly run on the same data")
                        algo.predict(X_window, prov=prov_window)
                else:
                    print("Predict with the trained model:")
                    algo.predict_std(X_window, prov=prov_window)
                # get most critical
                k = PUBLISH_PER_WINDOW
                critical_individuals = \
                    [(prov_window.iloc[i, :][0], algo.scores[i])
                        for i in np.argsort(algo.scores)[::-1]]
                print("hostname | anomaly score")
                [print(c) for c in critical_individuals[:k]]
                
                normalized_critical_individuals = \
                    [(prov_window.iloc[i, :][0], algo.std_scores[i])
                        for i in np.argsort(algo.std_scores)[::-1]]
                print("hostname | std anomaly score")
                [print(c) for c in normalized_critical_individuals[:k]]

                   
                ordered_hosts, ordered_scores = zip(*critical_individuals)
                normalized_ordered_hosts, normalized_ordered_scores = zip(*normalized_critical_individuals)   
                          
                algo.publish_top_k(ts_second_window_end=ts,
                                   hg_map = hg_map,
                                   top_k=PUBLISH_PER_WINDOW)


                # SAVE SCORES AND NORMALIZED SCORES TO PARQUET
                # ordered_hosts and normalized_ordered_hosts should be equal, so we use only ordered_hosts for entities
                save_scores_local_parquet(
                    algorithm_name=algorithm_name,
                    algo_parameters=algo_parameters,
                    folder_score=folder_score,
                    config_file=inference_file_path,
                    scores=ordered_scores,
                    normalized_scores=normalized_ordered_scores,
                    entities=ordered_hosts,
                    end_window=ts)

        print("Iteration on windows finished")

        end_inference = time.time()
        inference_time = float(end_inference - start_inference)

        # SAVE INFERENCE TIME
        with open(inference_file_path) as json_file:
            data_dict_inference = json.load(json_file)
        
        conn = sqlite3.connect(folder_time + '/time.db', timeout=120)

        modify_db(
                conn = conn, 
                query = '''CREATE TABLE IF NOT EXISTS inference_time
                    (date_start text, date_end_excluded text,
                    long_algo_description text,
                    inference_time real, measurement_time text,
                    PRIMARY KEY (date_start, date_end_excluded,
                                long_algo_description,
                                measurement_time))''',
            upperbound=10)

        modify_db(
                conn = conn, 
                query = '''INSERT INTO inference_time
                    VALUES (?, ?, ?, ?, datetime('now', 'localtime'))''',
                upperbound = 10,
                params = [data_dict_inference["date_start"],
                    data_dict_inference["date_end_excluded"],
                    algo_name,
                    inference_time])

        conn.close()

def main():
    cli()

if __name__ == '__main__':
    main()
