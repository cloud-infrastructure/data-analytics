import json
import datetime
from hashlib import blake2b

HASH_LENGTH = 20
SHORT_VER_HASH_LENGTH = 3

def generate_result(class_name, algo_name, class_version,
                    cell_name, hostgroup, plugins, granularity_min,
                    normalization,
                    entity, score, normalized_score, rank, percentile,
                    ts_second_window_start, ts_second_window_end,
                    slide_steps, history_len,
                    mean_train, std_train,
                    custom_id=None):
    """Emit the discovered anomalies in the result format.

    Return:
    - id: that will correspond to the _id with which it will be added to
        the monit infrastructure
    - result: dictionary containing all the info of the algorithm, ready
        to be transmitted to fluentd and Monit with a json via stdout:
        print(json.dumps(result))
    """
    global HASH_LENGTH
    result = {}
    
    # Algo name and version
    algo = {}
    algo["class_name"] = class_name
    algo["algo_name"] = algo_name
    algo["version"] = float(class_version)
    result["algo"] = algo

    # Data info
    entity_data = {}
    entity_data["entity"] = str(entity)
    entity_data["cell_name"] = str(cell_name)
    entity_data["hostgroup"] = str(hostgroup)
    result["entity_data"] = entity_data

    # Metrics info
    entity_metrics = {}
    entity_metrics["plugins"] = {}
    for k in plugins.keys():
        entity_metrics["plugins"][k] = plugins[k]
    result["entity_metrics"] = entity_metrics

    # Preprocessing
    preprocessing = {}
    preprocessing["type"] = str(normalization)
    result["preprocessing"] = preprocessing

    # Temporal - Window info
    temporal = {}
    dt_start = datetime.datetime.fromtimestamp(int(ts_second_window_start))
    ts_start_milli = int(ts_second_window_start) * 1000
    temporal["time_start_milli"] = ts_start_milli
    temporal["time_start"] = dt_start.strftime("%Y-%m-%dT%H:%M:%S.000Z")
    dt_end = datetime.datetime.fromtimestamp(int(ts_second_window_end))
    ts_end_milli = int(ts_second_window_end) * 1000
    temporal["time_end_milli"] = ts_end_milli
    temporal["time_end"] = dt_end.strftime("%Y-%m-%dT%H:%M:%S.000Z")
    temporal["window_length"] = int(history_len)
    temporal["slide_steps"] = int(slide_steps)
    temporal["step_resolution_minutes"] = int(granularity_min)
    result["temporal"] = temporal

    # Anomaly
    anomaly = {}
    anomaly["score"] = float(score)
    anomaly["normalized_score"] = float(normalized_score)    
    anomaly["rank_position"] = int(rank)
    anomaly["rank_percentile"] = float(percentile)
    explainalibilty = {}
    explainalibilty["mean_train"] = float(mean_train)
    explainalibilty["std_train"] = float(std_train)
    anomaly["explainalibilty"] = explainalibilty
    result["anomaly"] = anomaly

    # IDs
    ids = {}
    h_algo = blake2b(digest_size=HASH_LENGTH)
    h_algo.update(str.encode(json.dumps(algo)))
    ids["algo"] = h_algo.hexdigest()
    h_entity_data = blake2b(digest_size=HASH_LENGTH)
    h_entity_data.update(str.encode(json.dumps(entity_data)))
    ids["entity_data"] = h_entity_data.hexdigest()
    h_entity_metrics = blake2b(digest_size=HASH_LENGTH)
    h_entity_metrics.update(str.encode(json.dumps(entity_metrics)))
    ids["entity_metrics"] = h_entity_metrics.hexdigest()
    h_preprocessing = blake2b(digest_size=HASH_LENGTH)
    h_preprocessing.update(str.encode(json.dumps(preprocessing)))
    ids["preprocessing"] = h_preprocessing.hexdigest()
    h_temporal = blake2b(digest_size=HASH_LENGTH)
    h_temporal.update(str.encode(json.dumps(temporal)))
    ids["temporal"] = h_temporal.hexdigest()
    result["ids"] = ids

    # ID that will finish in the MONIT _ID
    h_app_id = blake2b(digest_size=HASH_LENGTH)
    h_app_id.update(str.encode(json.dumps(ids)))
    result["document_id"] = h_app_id.hexdigest()
    if custom_id is not None:
        result["document_id"] = str(custom_id)

    return result["document_id"], {'results': result}

# TESTED
def create_id_for_metrics_dict(plugins):
    """Create an unique id for the dictionary of metrics."""
    global HASH_LENGTH
    entity_metrics = {}
    entity_metrics["plugins"] = {}
    for k in plugins.keys():
        entity_metrics["plugins"][k] = plugins[k]
    h_entity_metrics = blake2b(digest_size=HASH_LENGTH)
    h_entity_metrics.update(str.encode(json.dumps(entity_metrics,
                                                  sort_keys=True)))
    return h_entity_metrics.hexdigest()

# TESTED
def create_id_for_hostgroups_and_plugins_start_end(hostgroups, plugins, 
                                                   start, end):
    """Get the hash of this hostgroups and plugins set."""
    global HASH_LENGTH
    global SHORT_VER_HASH_LENGTH
    hash_plugins = create_id_for_metrics_dict(plugins)
    h_hostgroups = blake2b(digest_size=HASH_LENGTH)
    # WE HAVE TO USE THE SAME REGEX STRING FOR THE SAME LIST OF HOSTGROUPS
    # IF NOT, WE WILL HAVE DIFFERENT HASH CODES (implying different list where they are the same)
    ordered_string = "-".join(list(sorted(hostgroups)))
    h_hostgroups.update(str.encode(ordered_string))
    hash_hostgroups = h_hostgroups.hexdigest()
    concatenation = hash_plugins + hash_hostgroups + str(start) + str(end)

    h_concatenation = blake2b(digest_size=SHORT_VER_HASH_LENGTH)
    h_concatenation.update(str.encode(concatenation))

    return h_concatenation.hexdigest()
