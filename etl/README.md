# Modules

The repository implements ETL (Extract, Transform, Load) modules for:
1. Grafana ETL
1. Spark ETL


## Grafana ETL

Here the UML class diagram of the module.

![use-case-diagram](documentation/images/grafana_etl_package.png)

## Spark ETL

(Work in progress)
