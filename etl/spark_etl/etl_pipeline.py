"""Entire ETL Pipeline."""

from etl.spark_etl.cluster_utils import load_schema, set_spark
from etl.spark_etl.cluster_utils import copy_to_local

from adcern.publisher import create_id_for_metrics_dict
from adcern.publisher import create_id_for_hostgroups_and_plugins_start_end

# ETL STEPS
from etl.spark_etl.etl_steps import data_reading
from etl.spark_etl.etl_steps import downsampling_and_aggregate
from etl.spark_etl.etl_steps import normalize
from etl.spark_etl.etl_steps import pivot_plugin_as_column
from etl.spark_etl.etl_steps import create_window
from etl.spark_etl.etl_steps import create_window_light  # noqa
from etl.spark_etl.etl_steps import data_writing
from etl.spark_etl.etl_steps import join_all

# ETL - NORMALIZATION SPECIFIC
from etl.spark_etl.etl_steps import compute_the_coefficients
from etl.spark_etl.etl_steps import save_normalization

# UTILS
from etl.spark_etl.utils import get_date_between
from etl.spark_etl.utils import read_json_config
from etl.spark_etl.utils import print_exception

# ALGO STEPS
from adcern.algo_steps import read_window_dataset
from pyspark.sql.utils import AnalysisException

import os, json, shutil, subprocess
from pyspark.sql.types import StructType

# NOT USED
def run_pipeline(spark, config_filepath):
    """Run the pipeline for the specified data.

    It produces window datasets in HDFS, divided by:
        - day
        - hostgroup
    Params
    ------
    spark: spark context
    config_filepath: path to json with the following keys:
        hdfs_out_folder (str), date_start (str), date_end_excluded(str),
        overwrite_on_hdfs (boolean), aggregate_every_n_minutes (int),
        history_steps (int), future_steps (int),
        hostgroups (list of str), selected_plugins (dict of dict)
    Return
    ------
    exit_code: int
        0 = success, 1 = error
    """
    config_dict = read_json_config(config_filepath)

    # get the days
    days = get_date_between(config_dict["date_start"],
                            config_dict["date_end_excluded"])
    # read normalization df
    plugins = config_dict["selected_plugins"]
    hostgroups = config_dict["hostgroups"]
    normalization_id = \
        create_id_for_hostgroups_and_plugins_start_end(
            hostgroups=hostgroups,
            plugins=plugins,
            start=config_dict["date_start_normalization"],
            end=config_dict["date_end_normalization_excluded"])

    norm_df =  \
        read_normalization_dataframe(
            spark=spark,
            normalization_folder=config_dict["normalization_out_folder"],
            normalization_id=normalization_id)
    # check that we have all the required noramlization constant for this task
    if not _are_compatible(df_normalization=norm_df, plugins_to_mine=plugins):
        return 1

    print("ORIGINAL: ", plugins)
    list_plugins = [{k: plugins[k]} for k in plugins.keys()]
    # print(list_plugins)
    # run the job separately for each plugin
    for p in list_plugins:
        # consider only this plugin
        plugins = p
        plugin_name = list(p.keys())[0]
        print("_" * 50)
        print("START: ", plugins)
        print("FOLDER NAME: ", plugin_name)

        # READ ALL DATA
        all_raw_data = data_reading(spark=spark,
                                    plugins=plugins,
                                    days=days,
                                    hostgroups=hostgroups)

        # AGGREGATE & DOWNSAMPLE
        every_n_minutes = config_dict["aggregate_every_n_minutes"]
        agg_data = downsampling_and_aggregate(df_all_plugins=all_raw_data,
                                              every_n_minutes=every_n_minutes)

        # NORMALIZE DATA
        normalized_data = normalize(df=agg_data,
                                    df_normalization=norm_df)
        '''
        # PIVOT & WINDOW - LIGHT
        history_steps = config_dict["history_steps"]
        future_steps = config_dict["future_steps"]
        windows_data = \
            create_window_light(spark=spark,
                                df_every_row_a_value=normalized_data,
                                plugin_names_list=list(plugins.keys()),
                                start_date=config_dict["date_start"],
                                end_date=config_dict["date_end_excluded"],
                                history_steps=history_steps,
                                future_steps=future_steps,
                                every_n_minutes=every_n_minutes)
        '''
        # PIVOT DATA - plugins as columns
        pivotted_data = \
            pivot_plugin_as_column(spark=spark,
                                   df=normalized_data,
                                   plugin_names_list=list(plugins.keys()))
        # keep them in memory
        # because I will trigger a collect function for resampling
        # DEBUG
        # pivotted_data.persist()
        # pivotted_data.show(5, truncate=False)
        # return pivotted_data

        # CREATE WINDOW OF DATA
        history_steps = config_dict["history_steps"]
        future_steps = config_dict["future_steps"]
        windows_data = create_window(spark=spark,
                                     df_plugins_as_col=pivotted_data,
                                     start_date=config_dict["date_start"],
                                     end_date=config_dict["date_end_excluded"],
                                     history_steps=history_steps,
                                     future_steps=future_steps,
                                     every_n_minutes=every_n_minutes)

        # windows_data.show(5, truncate=False)
        # return windows_data

        # WRITE THEM ON THE DISK
        id_plugins = \
            create_id_for_metrics_dict(config_dict["selected_plugins"])
        outfolder = config_dict["hdfs_out_folder"]
        try:
            if config_dict["overwrite_on_hdfs"] is True:
                mode = "overwrite"
            elif config_dict["overwrite_on_hdfs"] is False:
                mode = "append"
        except Exception as e:
            print_exception("Error getting the mode of writing, setting mode to \"overwrite\"", e)
            mode = "overwrite"
        '''
        # remove old
        for h in hostgroups:
            delete_files(spark=spark,
                        config_dict=config_dict,
                        hostgroup=h)
        '''
        print("plugin_name -> %s" % plugin_name)
        # print("Elements count: ", windows_data.count())

        partial_exit_code = \
            data_writing(spark=spark,
                         df_window=windows_data,
                         outfolder=outfolder + "/" + plugin_name + "/",
                         every_n_minutes=every_n_minutes,
                         history_steps=history_steps,
                         future_steps=future_steps,
                         id_plugins=id_plugins,
                         id_normalization=normalization_id,
                         mode=mode)
        print("Plugin %s -> Exit code: %i" % (p, partial_exit_code))
        # DEBUG

    # Read data from the plugin subfolders.
    outfolder = config_dict["hdfs_out_folder"]
    list_paths = [outfolder + "/" + p + "/"
                  for p in config_dict["selected_plugins"].keys()]
    print(list_paths)

    try:
        metrics_dfs = []
        for path in sorted(list_paths):
            df_plugin = spark.read.parquet(path)
            metrics_dfs += [df_plugin]
        # join
        final_window = join_all(dfs=metrics_dfs,
                                attributes=["timestamp",
                                            "hostname", "hostgroup", "ts"])
        final_window = final_window.drop('ts')
    except AnalysisException:
        print("path not found: %s" % list_paths)
        return

    try:
        if config_dict["overwrite_on_hdfs"] is True:
            mode = "overwrite"
        elif config_dict["overwrite_on_hdfs"] is False:
            mode = "append"
    except Exception as e:
        print_exception("Error getting the mode of writing, setting mode to \"overwrite\"", e)
        mode = "overwrite"

    id_plugins = create_id_for_metrics_dict(config_dict["selected_plugins"])
    exit_code = \
        data_writing(spark=spark,
                     df_window=final_window,
                     outfolder=outfolder + "/final/",
                     every_n_minutes=config_dict["aggregate_every_n_minutes"],
                     history_steps=config_dict["history_steps"],
                     future_steps=config_dict["future_steps"],
                     id_plugins=id_plugins,
                     id_normalization=normalization_id,
                     mode=mode)

    return exit_code

# TESTED
def run_pipeline_all_in_one(spark, config_filepath, local=False):
    """Run the pipeline for the specified data.

    It produces window datasets in HDFS, divided by:
        - day
        - hostgroup
    Params
    ------
    spark: spark context
    config_filepath: path to json with the following keys:
        hdfs_out_folder (str), date_start (str), date_end_excluded(str),
        overwrite_on_hdfs (boolean), aggregate_every_n_minutes (int),
        history_steps (int), future_steps (int),
        hostgroups (list of str), selected_plugins (dict of dict)
    local: boolean
        if True we add "file://" to spark paths

    Return
    ------
    exit_code: int
        0 = success, 1 = error
    """
    config_dict = read_json_config(config_filepath)

    # read normalization df
    plugins = config_dict["selected_plugins"]
    hostgroups = config_dict["hostgroups"]

    normalization_id = create_id_for_hostgroups_and_plugins_start_end(
        hostgroups=hostgroups,
        plugins=plugins,
        start=config_dict["date_start_normalization"],
        end=config_dict["date_end_normalization_excluded"]
    )

    try:
        schema_path = config_dict["schemas"]["norm_path"]    
    except Exception as e:
        print_exception("schema_path not found! Using schema_path=None ", e)
        schema_path = None

    norm_df = read_normalization_dataframe(
        spark=spark,
        normalization_folder=config_dict["normalization_out_folder"],
        normalization_id=normalization_id,
        schema_path=schema_path
    )
    
    # check that we have all the required noramlization constant for this task
    if not _are_compatible(df_normalization=norm_df, plugins_to_mine=plugins):
        return 1

    agg_data = get_aggregated_data(spark=spark,
                                   config_filepath=config_filepath,
                                   normalization=False,
                                   local=local)

    # NORMALIZE DATA
    normalized_data = normalize(df=agg_data,
                                df_normalization=norm_df)
    '''
    # PIVOT & WINDOW - LIGHT
    history_steps = config_dict["history_steps"]
    future_steps = config_dict["future_steps"]
    windows_data = \
        create_window_light(spark=spark,
                            df_every_row_a_value=normalized_data,
                            plugin_names_list=list(plugins.keys()),
                            start_date=config_dict["date_start"],
                            end_date=config_dict["date_end_excluded"],
                            history_steps=history_steps,
                            future_steps=future_steps,
                            every_n_minutes=every_n_minutes)
    '''
    # PIVOT DATA - plugins as columns
    pivotted_data = pivot_plugin_as_column(df=normalized_data,
                                           plugin_names_list=list(plugins.keys()))

    # CREATE WINDOW OF DATA
    history_steps = config_dict["history_steps"]
    windows_data = create_window(df_plugins_as_col=pivotted_data,
                                 steps=history_steps)

    # WRITE THEM ON THE DISK
    outfolder = config_dict["hdfs_out_folder"] + "/" + config_dict["code_project_name"]
    
    try:
        if config_dict["overwrite_on_hdfs"] is True:
            mode = "overwrite"
        elif config_dict["overwrite_on_hdfs"] is False:
            mode = "append"
    except Exception as e:
        print_exception("Error getting the mode of writing, setting mode to \"overwrite\"", e)
        mode = "overwrite"

    if not local:
        print("Deleting any previous/old remainders in %s ..." % outfolder)
        try:
            subprocess.call(["hdfs", "dfs", "-rm", "-R", "-skipTrash", outfolder])
        except Exception as e_delete:
            print('Error while deleting directory: ', e_delete)

    print("Saving the big guy (window dataframe) in: %s .." % outfolder)
    # print("Elements count: ", windows_data.count())
    exit_code, final_df = data_writing(
        df_window=windows_data,
        outfolder=outfolder,
        mode=mode
    )

    return exit_code, final_df

# TESTED
def pipeline_preparation_norm_coeff(spark, config_filepath, local=False):
    """Run the pipeline to get the coefficeint and create a normalization df.

    It produces normalization datasets in HDFS with noramlization coefficents
    (e.g. mean and stddev) for every pair of (hostgroup, plugin).
    Params
    ------
    spark: spark context
    config_filepath: path to json with the following keys:
        normalization_out_folder (str),
        date_start_normalization (str), date_end_normalization_excluded(str),
        overwrite_normalization (boolean), aggregate_every_n_minutes (int),
        hostgroups (list of str), selected_plugins (dict of dict)
    Return
    ------
    exit_code: int
        0 = success, 1 = error
    norm_path: str
        path where the normalization is stored
    """
    config_dict = read_json_config(config_filepath)
    
    plugins = config_dict["selected_plugins"]
    hostgroups = config_dict["hostgroups"]

    agg_data = get_aggregated_data(spark=spark,
                                   config_filepath=config_filepath,
                                   normalization=True,
                                   local=local)

    # COMPUTE NORMALIZATION COEFFICENTS
    normalized_data = compute_the_coefficients(df_aggregated=agg_data)

    # WRITE THEM ON THE DISK
    id_norm = create_id_for_hostgroups_and_plugins_start_end(
        hostgroups=hostgroups,
        plugins=plugins,
        start=config_dict["date_start_normalization"],
        end=config_dict["date_end_normalization_excluded"]
    )
    outfolder = config_dict["normalization_out_folder"]

    try:
        if config_dict["overwrite_on_hdfs"] is True:
            mode = "overwrite"
        elif config_dict["overwrite_on_hdfs"] is False:
            mode = "append"
    except Exception as e:
        print_exception("Error getting the mode of writing, setting mode to \"overwrite\"", e)
        mode = "overwrite"

    print("NEW id_norm: " + id_norm)
    print("Outfolder + id_norm = " + outfolder + "/" + id_norm)
        
    exit_code, norm_path = save_normalization(
        df_normalization=normalized_data,
        id_normalization=id_norm,
        outfolder=outfolder,
        mode=mode,
        local=local        
    )
    return exit_code, norm_path

# TESTED
def get_aggregated_data(spark, config_filepath, normalization=False, local=False):
    config_dict = read_json_config(config_filepath)
    plugins = config_dict["selected_plugins"]

    # get the days
    if normalization:
        print("Computing days for normalization.")
        days = get_date_between(config_dict["date_start_normalization"],
                                config_dict["date_end_normalization_excluded"])
    else:
        print("Computing days for real data.")
        days = get_date_between(config_dict["date_start"],
                                config_dict["date_end_excluded"])

    # READ ALL DATA
    hostgroups = config_dict["hostgroups"]
    try:
        schema_path = config_dict["schemas"]["raw_path"] 
    except Exception as e:
        print("raw_path paramter not found. Setting raw_path to None.")
        schema_path = None
    all_raw_data = data_reading(spark=spark,
                                plugins=plugins,
                                days=days,
                                hostgroups=hostgroups,
                                local=local,
                                schema_path=schema_path)

    # AGGREGATE & DOWNSAMPLE
    every_n_minutes = config_dict["aggregate_every_n_minutes"]
    agg_data = downsampling_and_aggregate(df_all_plugins=all_raw_data,
                                          every_n_minutes=every_n_minutes)
    return agg_data


def materialize_locally(spark, config_filepath, local=False):
    """Create a window dataset and move it locally."""
    config_dict = read_json_config(config_filepath)
    project_code = config_dict["code_project_name"]
    hdfs_outfolder = config_dict["hdfs_cache_folder"] + "/" + project_code
    local_outfolder = config_dict["local_cache_folder"] + "/" + project_code

    # TESTED!!
    df_win_train = read_window_dataset(spark=spark,
                                       config_filepath=config_filepath)
    
    if local:
        hdfs_outfolder = "file://" + hdfs_outfolder
    print("Writing the compressed data parquet in: " + hdfs_outfolder)
    df_win_train.coalesce(1).write.format(
        "parquet"
    ).mode("overwrite").save(hdfs_outfolder)

    if not local:
        copy_to_local(hdfs_path=hdfs_outfolder,
                    local_path=local_outfolder)
        
        raw_folder = config_dict["hdfs_out_folder"] + project_code
        print("Deleting the raw data saved in %s ..." % raw_folder)
        try:
            subprocess.call(["hdfs", "dfs", "-rm", "-R", "-skipTrash", raw_folder])
        except Exception as e_delete:
            print('Error while deleting raw_folder directory: ', e_delete)

        print("Deleting the raw data saved in %s ..." % hdfs_outfolder)
        try:
            subprocess.call(["hdfs", "dfs", "-rm", "-R", "-skipTrash", hdfs_outfolder])
        except Exception as e_delete:
            print('Error while deleting hdfs_outfolder directory: ', e_delete)

# TESTED
def get_normalization_path(config_filepath):
    """Given the config dictionary path get the normalization path.

    Params
    ------
    config_filepath: str
        path of the file containing the configuration

    Return
    ------
    to_return: str
        final path of the folder where the saved df must be
    """
    config_dict = read_json_config(config_filepath)
    id_norm = create_id_for_hostgroups_and_plugins_start_end(
        hostgroups=config_dict["hostgroups"],
        plugins=config_dict["selected_plugins"],
        start=config_dict["date_start_normalization"],
        end=config_dict["date_end_normalization_excluded"]
    )
    outfolder = config_dict["normalization_out_folder"]
    to_return = outfolder + "/" + id_norm
    print("Created normalization path: " + to_return)
    return to_return

# TESTED
def get_normalization_pandas(spark, config_filepath, local=False, schema_path=None):
    """Return the Pandas dataframe of normalization.

    Params
    ------
    spark: spark session
        session of spark
    config_filepath: str
        path of the file containing the configuration

    Return
    ------
    pdf: pandas dataframe
        dataframe containing mean and stddev
    """
    path = get_normalization_path(config_filepath=config_filepath)
    if local:
        print("Attention, local parameter is True. \"file://\" before paths will be used!")
        path = "file://" + path
    df = read_spark_df(spark=spark,
                       df_path=path,
                       schema_path=schema_path)
    pdf = df.toPandas()
    print("Shape of the dataframe: " + str(pdf.shape))
    print("First 2 rows of the dataframe ->")
    pdf.head(2)
    return pdf

# TESTED
def read_normalization_dataframe(spark,
                                 normalization_folder: str,
                                 normalization_id: str,
                                 schema_path=None):
    """Get the normalization dataframe with the corresponding id.

    NB the normalization coefficient are specific for every hostgroup and
    for every plugin.
    Params
    ------
    spark: spark context
    normalization_folder: str
        path to your normalization dataframes
    normalization_id: str
        id identifying uniquely the dataframe
    Return
    ------
    df_normalization: PySpark DataFrame
        It will contain the following columns:
        hostgroup | plugin | mean | stddev
    """
    print("Reading the dataframe in " + normalization_folder + "/" + normalization_id)
    df_normalization = read_spark_df(spark=spark,
                                     df_path=normalization_folder + "/" + normalization_id,
                                     schema_path=schema_path)
    return df_normalization


def _are_compatible(df_normalization, plugins_to_mine):
    """Check if we have all the coeffs for the plugins we want to mine.

    Return
    ------
        compatible: bool
            if ok = True; if problems = False
    """
    # TODO(avoid starting spark if norm df is not compatible with plugins)
    return True

# TESTED
def read_spark_df(spark, df_path, schema_path=None):
    """Reading a spark dataframe with the possibility of using the schema.

    Params
    ------
    spark: spark session
        session of spark
    df_path: str
        path of the dataframe  
    schema_path: str
        if not None we try to read the df using the schema

    Return
    ------
    df: spark dataframe
        the dataframe we read
    """
    print("Reading the spark df in the path: " + df_path)

    if schema_path is not None:
        print("We will use the schema, loading it.")
        schema = load_schema(schema_path)
        print("Using the following schema for reading: \n" + str(schema))
        try:
            df = spark.read.schema(schema).parquet(df_path)
        except Exception as e:
            print_exception("Error reading the parquet in " + df_path +
                            " using the schema in " + schema_path, e)
            raise
    else:
        print("We will not use any schema")
        try:
            df = spark.read.parquet(df_path)
        except Exception as e:
            print_exception("Error reading the parquet in " + df_path +
                            " without using any schema.", e)
            raise
    print("Reading opertion was sucessful.")
    try:
        df.show(5)
    except Exception as e:
        print_exception("Problems showing the dataframe.", e)
        raise
    return df