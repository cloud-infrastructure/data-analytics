#!/bin/bash

export LCG_VIEW=/cvmfs/sft.cern.ch/lcg/views/LCG_96python3/x86_64-centos7-gcc8-opt
source $LCG_VIEW/setup.sh  || fail 'Setting hadoop'
source /cvmfs/sft.cern.ch/lcg/etc/hadoop-confext/hadoop-swan-setconf.sh analytix 2.7 spark2
export PYTHONPATH=/usr/local/lib/python3.6/site-packages/:$PYTHONPATH

echo PYTHONPATH $PYTHONPATH
echo LD_LIBRARY_PATH $LD_LIBRARY_PATH
echo JAVA_HOME $JAVA_HOME
echo SPARK_HOME $SPARK_HOME
echo SPARK_DIST_CLASSPATH $SPARK_DIST_CLASSPATH
