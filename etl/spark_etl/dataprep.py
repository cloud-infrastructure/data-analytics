import pandas as pd
# from sklearn import preprocessing  # keep this import as it is
import numpy as np
from datetime import timedelta
from datetime import datetime
import os
from adcern.publisher import create_id_for_metrics_dict
import pyspark.sql.functions as F
from pyspark.sql.functions import col
from pyspark.sql import DataFrame
from functools import reduce  # For Python 3.x

MACRO_HOSTGROUP = 'cloud_compute/level2/'


class WindowDispatcher(object):
    """Create windows and dispatch them.

    it is an iterator over the windows created in cronological order.
    """

    def __init__(self, collectd_dataset,
                 folder_to_save_windows=None):
        """Load the data in the preprocessing engine.

        Paramters
        ---------
        collectd_dataset: CollectdDataset object
            data that you want to transform
        disk_folder: str
            folder where to save your windows. By default it will be saved to
            the same folder of the collectd dataset
        """
        global MACRO_HOSTGROUP
        if collectd_dataset is None:
            raise Exception("You passed an empty object as dataset.")
        data = collectd_dataset.get_data()
        metadata = collectd_dataset.get_metadata()
        # print(metadata["granularity_min"])
        self.original_data = data
        self.cell_name = metadata["cell_name"]
        self.plugins = metadata["plugins"]
        self.granularity_min = metadata["granularity_min"]
        self.short_hostgroup = \
            metadata["hostgroup"].replace(MACRO_HOSTGROUP, "")
        self.metadata = metadata
        start = collectd_dataset.get_current_start_date()
        end = collectd_dataset.get_current_end_date()
        self.set_range(start, end)
        self.set_window_start(start)
        self.window_dataset = None
        if folder_to_save_windows is not None:
            self.diskfolder = folder_to_save_windows
        else:
            self.diskfolder = collectd_dataset.get_location()

    def __iter__(self):
        """Get the iterator over the windows."""
        if self.window_dataset is not None:
            X, y, prov = self.window_dataset
            print("Cached result used: %i samples." %
                  len(X))
            return WindowIterator(X, y, prov,
                                  metadata=self.metadata,
                                  start_date=self.start_date_window,
                                  history_len=self.history_len,
                                  prediction_len=self.prediction_len,
                                  start_step=0,
                                  pace=self.pace)
        else:
            raise Exception("Create windows first.")

    def set_range(self, start_date: str, end_date: str):
        """Define the range of your preprocessing.

        start_date: start date of your dataset (format: "YYYY-mm-dd")
        end_date: end date of your dataset (format: "YYYY-mm-dd")
        """
        self.start = start_date
        self.end = end_date
        self.list_dates = self._dates_between(start_date, end_date)
        self.snapshot = self.original_data[self.start:self.end]

    def set_window_start(self, start_date_window):
        """Set the start of the window iterator.

        The data contained in this object will remain the same.
        """
        self.start_date_window = start_date_window

    def set_pace(self, pace):
        """Set the start of the window iterator.

        Parameters
        -------------
        pace : int
            nr of time instants to slide your window between each subsequent
            window.
        The data contained in this object will remain the same.
        """
        self.pace = pace
        # create the timestamps corresponding to data to be saved in cache
        self._compute_timestamps_at_this_pace()
        print("You will get in cache: windows every %i timesteps" % pace)

    def set_folder_for_disk_persistency(self, diskfolder):
        """Set a folder to store the computed window on disk."""
        self.diskfolder = diskfolder

    def window_creation(self,
                        on_machines=None,
                        on_metrics_history=None,
                        history_len=6,
                        pace=6,
                        on_metrics_prediction=None,
                        prediction_len=1,
                        scaler=None,
                        scaler_id=None,
                        verbose=False,
                        force_override=False,
                        use_cache=False):
        """Create dataset and label for a forecasting model.

        It returns three datasets:
         - feature_dataset: each row is a vector containing the values for the
             selected metrics for one host in a given timestamp.
             [load-2, load-1, load0, cpu-2, cpu-1, cpu0]
             load-t means the signal load t step in the past
         - label_dataset: each row is a vector containing the next n step
             for the selected metrics for the host on that row.
             [load+1, load+2, load+3, cpu+1, cpu+2, cpu+3, etc]
             load+t means the signal load t step in the future
         - provenance_dataset: to know to where each row is coming from in
             terms of host and timestamp (the current timestamp is part of
             the history)
            [host, temporal_index]

        NB in the trainset, the info about the Hypervisor each row is referring
        is discarded because for this setting we are only interested in the
        prediction. If we want to work on a single host just set only one
        host name in on_machines.

        on_machines: list of host names of the machines for which you want to
            consider as IID to train your model (any info about the initial
            host will be lost).
        on_metrics_history: list of signal names for the metrics that you want
            to consider as input.
        history_len: nr of steps that you want to consider in the past.
        pace: sliding step of your window.
        on_metrics_prediction: list of signal names for the metrics that you
            want to consider as output of the forecasting model.
        prediction_len: nr of steps that you want to forecast in the future.
        scaler: object with the sklearn scaler api
        use_cache: it will use the window created in the previous call of this
            function. This is useful if you compute the same windowing
            multiple times.

        """
        self.history_len = history_len
        self.prediction_len = prediction_len
        self.set_pace(pace)
        if use_cache and self.window_dataset is not None:
            X, y, prov = self.window_dataset
            print("Cached result used: %i samples." %
                  len(X))
            return X, y, prov

        # check to have a scaler and scaler id
        if scaler is None or scaler_id is None:
            raise Exception("You must pass a scaler and its" +
                            " relative scaler_id.")
        #  Look on the filesystem hdf5 file

        id_for_this_plugin_aggregate = create_id_for_metrics_dict(self.plugins)

        for index_today in range(1, len(self.list_dates)):
            # get the day before
            (year, month, day) = self.list_dates[index_today]
            (prev_year, prev_month, prev_day) = \
                self.list_dates[index_today - 1]
            dt_today = datetime(year=int(year), month=int(month), day=int(day))
            print("-" * 80)
            print("Window Crafting:", (year, month, day))
            print("-" * 80)
            today_base_path = self.diskfolder + "/" \
                + "/" + year + "/" + month + "/" + day \
                + "/" + str(self.granularity_min) + "min/" + \
                self.short_hostgroup
            hdf5_window = today_base_path + "/" + \
                "h" + str(self.history_len) + "_" + \
                "f" + str(self.prediction_len) + "_" + \
                "_plugins_" + id_for_this_plugin_aggregate + \
                "_scaler_" + scaler_id + ".hdf5"
            # load window directly
            if os.path.isfile(hdf5_window) and not force_override:
                X, y, prov = self._read_windows_on_disk(hdf5_window)
                self._add_to_cache(dt_today, X, y, prov)
                continue

            # not found - computation required

            # load raw datasets
            # dataset today
            hdf5_today_path = today_base_path + "/" + \
                id_for_this_plugin_aggregate + ".hdf5"
            with pd.HDFStore(hdf5_today_path) as store:
                print("Reading today: ", hdf5_today_path)
                pdf_today = store['dataset_raw'].reset_index(drop=True)
                store.close()
            # dataset yesterday
            hdf5_yesterday_path = self.diskfolder + "/" \
                + "/" + prev_year + "/" + prev_month \
                + "/" + prev_day \
                + "/" + str(self.granularity_min) + "min/" + \
                self.short_hostgroup + "/" + \
                id_for_this_plugin_aggregate + ".hdf5"
            with pd.HDFStore(hdf5_yesterday_path) as store:
                print("Reading yesterday: ", hdf5_yesterday_path)
                pdf_yesterday = store['dataset_raw'].reset_index(drop=True)
                store.close()
            # merged dataset
            two_days = pd.concat([pdf_today, pdf_yesterday], axis=0)
            two_days["index"] = pd.to_datetime(two_days["temporal_index"])
            two_days.set_index("index", inplace=True)
            print("Database loaded - lines: ", two_days.shape)

            df = two_days
            all_metrics = sorted(df["plugin"].unique())
            if on_metrics_history is None:
                on_metrics_history = all_metrics
            if on_metrics_prediction is None:
                on_metrics_prediction = all_metrics

            # filter data for this machine
            if on_machines is not None:
                df = df[df.host.isin(on_machines)]
            else:
                on_machines = sorted(df["host"].unique())
            # group data
            df_grouped = df.pivot_table(index=["host", "temporal_index"],
                                        columns=["plugin"],
                                        values="mean_value")
            df_grouped.reset_index(drop=False, inplace=True)

            # fill infinity. needed for the scaler
            df_cleaned = df_grouped.replace([np.inf, -np.inf], 0)

            data_pre_normalization = df_cleaned[df_cleaned.columns[2:]].values
            if scaler is not None:
                print("Columns pre-normalization: ")
                [print("-> ", x) for x in df_cleaned.columns[2:]]
                data_scaled = scaler.transform(data_pre_normalization)
            else:
                data_scaled = data_pre_normalization
            for i, c in enumerate(df_cleaned.columns[2:]):
                df_cleaned[c] = data_scaled[:, i]

            print("Nr single time instants: ", len(df_cleaned))
            # prepare final database to collect shifted data
            df_ensamble = pd.DataFrame()
            # create a windows of 3
            for host in on_machines:
                # columns past
                columns_history = []
                columns_future = []
                # group
                df_host = df_cleaned[df_cleaned["host"] == host]
                df_host = df_host.set_index("temporal_index")
                df_host.sort_index(inplace=True)
                # create shifted data for windows
                history_steps = range(history_len)
                future_steps = range(1, prediction_len + 1)
                # history steps
                for m in on_metrics_history:
                    for step in history_steps:
                        # create new columns with previous step(s)
                        new_column = m + "_h" + str(step)
                        columns_history += [new_column]
                        df_host[new_column] = df_host[m].shift(step)
                # future steps
                for m in on_metrics_prediction:
                    for step in future_steps:
                        # create new columns with future step(s)
                        new_column = m + "_f" + str(step)
                        columns_future += [new_column]
                        df_host[new_column] = df_host[m].shift(-step)
                # drop original columns
                df_host.drop(columns=all_metrics, inplace=True)
                before = len(df_host)
                # remove the rows that are with the earliest timestamp
                # because they do not have enought hystory step
                if history_len > 0:
                    df_host = df_host.iloc[history_len - 1:]
                # remove the rows that are with the latest timestamp
                # because they do not have enought future step
                if prediction_len > 0:
                    df_host = df_host.iloc[:-(prediction_len - 1)]
                after = len(df_host)
                diff = before - after
                if verbose:
                    print("%s dropped %i (from %i to %i)" %
                          (host, diff, before, after))
                # attach to the dataframe with all hosts
                df_ensamble = pd.concat([df_ensamble, df_host])

            # keep only data for today
            df_ensamble = df_ensamble.reset_index(drop=False)
            df_history_train = df_ensamble[columns_history]
            df_future_label = df_ensamble[columns_future]
            df_provenance = pd.DataFrame(
                df_ensamble[["host", "temporal_index"]].values)

            print("Nr samples to train your model:", len(df_ensamble))
            # save in cache of the object
            self._add_to_cache(dt_today, df_history_train,
                               df_future_label, df_provenance)
            print("SAVING ON DISK: %s" % hdf5_window)
            self._save_windows_on_disk(hdf5_window,
                                       X=df_history_train,
                                       y=df_future_label,
                                       prov=df_provenance)

        print("Done!")
        return self.window_dataset

    def _add_to_cache(self, dt_today, X, y, prov):
        """Add to cache only the timestamps defined by the pace."""
        print("Adding to cache: ", dt_today)
        datetimes_of_today = [dt for dt in self.dateidx
                              if dt >= dt_today]
        mask = (prov.loc[:, 1].isin(datetimes_of_today))
        # filter
        X = X[mask]
        y = y[mask]
        prov = prov[mask]
        if self.window_dataset is None:
            # create
            self.window_dataset = \
                X, y, prov
        else:
            # append
            old_X, old_y, old_prov = self.window_dataset
            self.window_dataset = \
                pd.concat([old_X, X], axis=0), \
                pd.concat([old_y, y], axis=0), \
                pd.concat([old_prov, prov], axis=0)

        print("New cache size: ", self.window_dataset[0].shape)

    def _compute_timestamps_at_this_pace(self):
        """Compute the timestamp that have to be inspected with this pace."""
        # since the data are dpwnloaded including the entire day of the
        # ending date (but the pd.datarange is not including the entire day
        # in the genration of intervals), we ad one day to the end date
        end_date_included = (datetime.strptime(self.end, "%Y-%m-%d")
                             + timedelta(days=1))
        self.dateidx = pd.date_range(start=self.start,
                                     end=end_date_included,
                                     freq=str(self.pace *
                                              self.granularity_min) + 'min')
        return self.dateidx

    def _save_windows_on_disk(self, hdf5_filepath, X, y, prov):
        """Save the window dataset to a local folder."""
        print("-----------------------------------------------------")
        print("---------- SAVING LOCAL WINDOWs HDF5 ----------------")
        store = pd.HDFStore(hdf5_filepath)
        store.put('dataset_window_X', X)
        store.put('dataset_window_y', y)
        store.put('dataset_window_prov', prov)
        metadata = self.metadata
        store.get_storer('dataset_window_X').attrs.metadata = metadata
        store.close()

    def _read_windows_on_disk(self, hdf5_filepath):
        """Load the windowss from the hdf5 file path."""
        print("-----------------------------------------------------")
        print("---------- READING LOCAL WINDOWs HDF5 ---------------")
        with pd.HDFStore(hdf5_filepath) as store:
            print(hdf5_filepath)
            X = store['dataset_window_X']
            y = store['dataset_window_y']
            prov = store['dataset_window_prov']
        store.close()
        print("WINDOWS LOADED FROM DISK SUCCESSFULLY")
        return X, y, prov

    def _dates_between(self, date_start, date_end):
        """Get all the dates between two date.

        The returned type will be a list of tuples like (year, month, day)
        """
        list_of_dates = []
        sdate = datetime.strptime(date_start, "%Y-%m-%d")   # start date
        edate = datetime.strptime(date_end, "%Y-%m-%d")   # end date
        delta = edate - sdate       # as timedelta
        for i in range(delta.days + 1):
            day = sdate + timedelta(days=i)
            day_tuple = (str(day.year),
                         str(day.month).zfill(2),
                         str(day.day).zfill(2))
            # print(day_tuple)
            list_of_dates.append(day_tuple)
        print("Start: %s - End %s" % (list_of_dates[0], list_of_dates[-1]))
        return list_of_dates


class WindowIterator(object):
    """Class to iterate over the windows."""

    def __init__(self, X, y, prov,
                 metadata,
                 history_len,
                 prediction_len,
                 start_date,
                 start_step=1,
                 pace=1):
        self.num = start_step
        self.pace = pace
        self.minutes_granularity = int(metadata["granularity_min"])
        self.history_len = history_len
        self.prediction_len = prediction_len
        self.X = X
        self.y = y
        self.prov = prov
        # since the data are dpwnloaded including the entire day of the
        # ending date (but the pd.datarange is not including the entire day
        # in the genration of intervals), we ad one day to the end date
        end_date_included = (datetime.strptime(metadata["end_date"],
                                               "%Y-%m-%d")
                             + timedelta(days=1))
        self.dateidx = pd.date_range(start=start_date,
                                     end=end_date_included,
                                     freq=str(self.minutes_granularity) + 'min')  # noqa

    def __next__(self):
        """Pass the next time window."""
        num = self.num
        if num >= len(self.dateidx):
            raise StopIteration
        mask = (self.prov.loc[:, 1] == self.dateidx[num])
        window_end = self.dateidx[num]
        window_start = window_end \
            - timedelta(minutes=self.minutes_granularity * self.history_len)  # noqa
        prediction_end = window_end \
            + timedelta(minutes=self.minutes_granularity * self.prediction_len)  # noqa
        window_X = self.X[mask]
        window_y = self.y[mask]
        window_prov = self.prov[mask]
        self.num += self.pace
        return window_X, window_y, window_prov, window_start, window_end, prediction_end  # noqa


# the code below is not used yet, it is meant to preprocess window in Spark


def read_day_hdfs(spark, csv_file_path):
    """Read the datae on HDFS in a PySpark DataFrame."""
    # csv_file = "/project/it_cloud_data_analytics/2020/02/12/10min/main/
    # gva_project_038/98292bb81a96d50c7ff90a3c7b31ca8c7afb88c9"
    df_compact = spark.read.csv(csv_file_path, header=True)
    df_compact = df_compact\
        .withColumn("ts",
                    F.unix_timestamp(F.to_timestamp(col("event_timestamp"))))
    df_compact = df_compact\
        .withColumn("mean_value", col("mean_value").cast("float"))
    return df_compact


def union_all(dfs):
    """Merge all datasets row-wise."""
    return reduce(DataFrame.unionAll, dfs)


def create_pivot(df, history_len,
                 unit_multiplier=1,
                 plugin_list=None):
    """Create the windows DF.

    Parameters
    ----------
    df: PySpark DataFrame
        original dataframe to shift
    history_len: int
        nr of timestep to create for every record (len of the window)
    unit_multiplier: int
        number of seconds that will represent your
    ts_first_full_window: int (seconds)
        timestamp in seconds of the first full window that you want to save.
    Return
    ------
    window_df: PySpark DataFrame
        windowed db
    """
    list_of_df_shifted = []
    # ge the list of plugins to speed up the performance x6
    unique_plugins = df.select("plugin").distinct().collect()
    print(unique_plugins)
    # get the min timestamp to understand which data to cut out
    min_ts = df.agg(F.min(col("ts"))).collect()[0][0]
    print("min value:", min_ts)
    ts_first_full_window = min_ts + history_len * unit_multiplier - 1
    print("ts_first_full_window value:", ts_first_full_window)

    # get the min timestamp to understand which data to cut out
    max_ts = df.agg(F.max(col("ts"))).collect()[0][0]
    print("max value:", max_ts)
    ts_last_full_window = max_ts
    print("ts_last_full_window value:", ts_last_full_window)

    new_lag_cols = [[p[0] + "_h" + str(i)
                     for i in range(history_len)]
                    for p in unique_plugins]
    flat_lags_cols = [item for sublist in new_lag_cols for item in sublist]
    print(flat_lags_cols)
    # create a df for each time shift
    for shift in range(history_len):
        df_shift = df.withColumn("group", col("ts") + shift * unit_multiplier)\
                     .withColumn("lag", F.lit(shift))
        list_of_df_shifted += [df_shift]
    # and concatenate them row-wise
    df_all = union_all(list_of_df_shifted)
    # create the intestation of the pivot
    df_column_merged = df_all. \
        withColumn("plugin_group",
                   F.concat(col("plugin"), F.lit("_h"), col("lag")))

    # if plugin_list is not None:
    # pass # imporve efficeincy by defining the value in the pivot column
    df_pivotted = \
        df_column_merged.groupBy(["group", "host"])\
                        .pivot("plugin_group", values=flat_lags_cols)\
                        .min("mean_value")\
                        .withColumnRenamed("group", "ts")
    df_filtered = df_pivotted.filter((col("ts") >= ts_first_full_window) &
                                     (col("ts") <= ts_last_full_window))
    df_fillna = df_filtered.fillna(0)
    return df_fillna
