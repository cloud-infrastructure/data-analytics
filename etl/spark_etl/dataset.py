import os
import hashlib
from datetime import datetime
from datetime import timedelta
import json

import numpy as np
import pandas as pd
import pyspark.sql.functions as F  # noqa : F401
from pyspark.sql.functions import lit  # noqa : F401

from etl.spark_etl.cluster_utils import hdfs_exist
from etl.spark_etl.cluster_utils import write_spark_df
from etl.spark_etl.cluster_utils import copy_to_local
from etl.spark_etl.cluster_utils import create_local_folder
from etl.spark_etl.cluster_utils import remove_local_csv
from etl.spark_etl.cluster_utils import find_csv_filenames
from adcern.publisher import create_id_for_metrics_dict
from pyspark.sql.utils import AnalysisException


MACRO_HOSTGROUP = 'cloud_compute/level2/'


class CollectdDataset(object):
    """Dataset containig metrics data of CERN Data Centre machines."""

    def __init__(self, cell=None, spark=None):
        """Initialize with a Spark context to access the cluster.

        cell: name of the cell (i.e. group of machines) under analysis
        spark: spark context
        """
        self.spark = spark
        self.cell = cell
        self.plugins = {}
        self.files_per_single_write = 1
        self.entire_pandas_df = None

    def from_config(self, spark=None, json_config_file='resources.json',
                    position_cell_to_read=0,
                    verbose=False):
        """Initialize you dataset form a json config file.

        Params:
        spark: (Spark Context) Deafult is None
        position_cell_to_read: (int) which cellto consider
        json_config_file: (str) path to the file with config

        The field of the JSON should be the following:
        "output_folder": (str) path in local folder
        "date_start": (str) YYYY-MM-DD "2020-05-10" start date of the analysis
        "date_end": (str) YYYY-MM-DD "2020-05-15" end date of the analysis
        "minutes": (int) granularity of the dataset
        "chunks": (int) nr of chnunks for the topandas method on Spark
        "cell_names": (list of str) only the element specified by
            position_cell_to_read will be considered
            WARING: use only one name
        "hostgroup": (str) only the str of the hostgroup
            e.g. "cloud_compute/level2/main/gva_project_038"
            then everything is saved in a path without "cloud_compute/level2/"
        "selected_plugins": (nested json) it represent the plugin dictionary
            as explained in the add_plugin function
        """
        global MACRO_HOSTGROUP
        with open(json_config_file) as json_file:
            data = json.load(json_file)
            if verbose:
                print(json.dumps(data, indent=4, sort_keys=True))
            self.cell = data["cell_names"][position_cell_to_read]
            self.hostgroup = data["hostgroup"]
            self.short_hostgroup = \
                data["hostgroup"].replace(MACRO_HOSTGROUP, "")
            self.set_download_options(hdfs_folder=data["hdfs_folder"],
                                      local_folder=data["local_folder"],
                                      override_on_hdfs=data["override_on_hdfs"],  # noqa
                                      num_chunks_pandas=data["chunks"])
            self.set_range(data["date_start"],
                           data["date_end"],
                           data["minutes"])
            self.add_plugins(data["selected_plugins"], override=True)

        self.acquire_data()

    def add_plugins(self, plugins, override=False):
        """Add info about the plugin you want in the dataset.

        plugins: dictionary of dict, each plugin is represented with a dict and
            its key is the signal name aka the name that you want your time
            serie to have after the aggregation process in the dataframe.
            This dict contains the following keys:
            - plugin_name: determines the table you will look at
            - plugin_instance, type, type_instance, value_instance (optional):
              they determine with which value to filter each column.
        override: if True the old list of plugin is overridden by the new list,
            if False the new plugins are appended (no check for duplicates)
        """
        if override:
            self.plugins = {}
            self.plugins.update(plugins)
        else:
            self.plugins.update(plugins)

    def set_download_options(self, hdfs_folder: str, local_folder: str,
                             override_on_hdfs=False, num_chunks_pandas=5):
        """Set options for download.

        hdfs_folder: the name of the folder on HDFS in your private space. The
            file scraped from the main db will be stored in this folder of your
            private space.
        local_folder: the name of the folder in your local environment. Here we
            will save the hdf5 file representing the aggregate you are
            analyzing.
        override_on_hdfs: if False (default behaviour) the method will use
            previously computed aggregates for the selected plugin, datasource
            and granularity of downsampling. This is recommended to avoid
            useful computation, if data aren't changed, there is no need to
            recompute. If True override the data already collected.
        num_chunks_pandas: increase this if obtaining out of memory error.
            it represents the  number of chunks to partition the conversion
            from spark to pands to avoid out of memory error (due to l
            imitation in the memory of the driver).
        """
        self.hdfs_folder = hdfs_folder
        self.local_folder = local_folder
        self.num_chunks_pandas = num_chunks_pandas
        self.override_on_hdfs = override_on_hdfs

    def set_file_per_single_write(self, nr_files):
        """Change the nr of files that are saved for each day."""
        self.files_per_single_write = nr_files

    def set_range(self, start_date: str, end_date: str, n_minutes: str):
        """Define the range of your analysis and downsampling frequency.

        start_date: start date of your dataset (format: "YYYY-mm-dd")
        end_date: end date of your dataset (format: "YYYY-mm-dd")
            NB: The extremes will be included. Replciate the same day to have
            one day only.
        n_minutes: this will be the interval of aggregation of your data that
            normally are stored with a frequence of 1 min.
        """
        self.n_minutes = n_minutes
        self.start = start_date
        self.end = end_date
        self.list_dates = self._dates_between(start_date, end_date)
        if self.entire_pandas_df is not None:
            self.pandas_df = self.entire_pandas_df[self.start:self.end]

    def get_data(self):
        """Return the Pandas Dataframe saved in this object."""
        return self.pandas_df

    def get_current_start_date(self):
        return self.start

    def get_current_end_date(self):
        return self.end

    def resample(self, periodicity_minutes):
        """Resample your data.

        periodicity_minutes: nr of minutes you want to resample.
        """
        # check on valid data
        periodicity_minutes = max(1, int(periodicity_minutes))
        temp = self.entire_pandas_df
        temp.index = pd.DatetimeIndex(temp.index)
        temp_grouped = temp.groupby(by=["submitter_hostgroup",
                                        "host", "plugin"]) \
            .resample(str(periodicity_minutes) + 'min').mean()
        temp_flat = temp_grouped.reset_index(drop=False)
        temp_flat.index = pd.to_datetime(temp_flat["index"])
        temp_flat = temp_flat.rename(columns={"index": "temporal_index"})
        temp_cleaned = temp_flat.dropna(axis=0, how="any")
        self.entire_pandas_df = temp_cleaned
        self.pandas_df = self.entire_pandas_df[self.start:self.end]
        self.metadata["granularity_min"] = periodicity_minutes
        print("Resampled with freq %i min" % periodicity_minutes)

    def get_metadata(self):
        """Return the dictionary with the metadata.

        'granularity_min': temporal granularity between two consecutive samples
        'start_date': start date of the data collected in this dataset
        'end_date': end date of the data collected in this dataset
        'plugins': list of plugins
        """
        return self.metadata

    def get_location(self):
        """Get the folder where the current object was created from."""
        return self.local_folder

    def acquire_data(self):
        """Get the data from HDFS (downsample query if needed).

        It uses the settings decided in the set_download_option function
        """
        print("-----------------------------------------------------")
        print("------------ PROCEDURE TO GET DATA ------------------")
        print("--------- CHECKING IF ALREADY DOWNLOADED ... --------")
        id_for_this_plugin_aggregate = create_id_for_metrics_dict(self.plugins)
        all_file_available = True
        missing_days = []
        for (year, month, day) in self.list_dates:
            hdf5_day_path = self.local_folder + "/" \
                + "/" + year + "/" + month + "/" + day \
                + "/" + str(self.n_minutes) + "min/" + \
                self.short_hostgroup + "/" + \
                id_for_this_plugin_aggregate + ".hdf5"
            if not os.path.isfile(hdf5_day_path):
                print("Local hdf5 File NOT found:", (year, month, day))
                all_file_available = False
                missing_days += [(year, month, day)]
        # scraping  HDFS
        if not all_file_available or self.override_on_hdfs:
            print("--------------- MINING PROCEDURE STARTED --------------")
            # scraping  HDFS
            self._create_in_HDFS(self.override_on_hdfs)
            print("--------------- DATA SAVED EFFECTIVELY ----------------")
        self.from_local_file()
        print("--------------- READY FOR ANALYSIS ------------------")
        print("-----------------------------------------------------")

    def anonymize(self, seed, forget_true_name=False,
                  drop_cell_name=False):
        """Anonymize all the machine in this pool.

        The new name will be overritten in all the columns of the pandas
        DataFrame representation.
        forget_true_name: if True the real name will be erased.
        """
        def anonymization_funct(real_name):
            return \
                str(hashlib.pbkdf2_hmac('sha256', b'' + str.encode(real_name),
                                        seed.to_bytes(64, 'big'), 100000,
                                        dklen=8)
                    .hex())
        if drop_cell_name is True:
            self.entire_pandas_df.drop(columns=["submitter_hostgroup"],
                                       inplace=True)
        if forget_true_name is False:
            # create backup column for old name
            self.entire_pandas_df["old_host"] = \
                self.entire_pandas_df["host"]
        if self.entire_pandas_df is not None:
            new_names = {}
            for old_name in self.entire_pandas_df.host.unique():
                new_names[old_name] = anonymization_funct(old_name)
            self.entire_pandas_df.replace({"host": new_names}, inplace=True)
            self.pandas_df = self.entire_pandas_df[self.start:self.end]
        else:
            print("No data have been loaded.")

    def from_local_file(self):
        """Read the hdf5 pandas file and create a new dataset object.

        Nothing is inferred apart frm the dates of your dataset. If you want
        to perform other analysis you need to input the plugins info from
        scratch.
        """
        print("-----------------------------------------------------")
        print("--------------- READING LOCAL HDF5 ------------------")
        id_for_this_plugin_aggregate = create_id_for_metrics_dict(self.plugins)
        pandas_of_all_days = []
        for (year, month, day) in self.list_dates:
            hdf5_day_path = self.local_folder + "/" \
                + "/" + year + "/" + month + "/" + day \
                + "/" + str(self.n_minutes) + "min/" + \
                self.short_hostgroup + "/" + \
                id_for_this_plugin_aggregate + ".hdf5"
            with pd.HDFStore(hdf5_day_path) as store:
                print("Reading: ", hdf5_day_path)
                pdf = store['dataset_raw']
                self.metadata = store.get_storer('dataset_raw').attrs.metadata
            store.close()
            pandas_of_all_days += [pdf.reset_index(drop=True)]
        self.entire_pandas_df = pd.concat(pandas_of_all_days,
                                          axis=0)
        self.entire_pandas_df["index"] = \
            pd.to_datetime(self.entire_pandas_df["temporal_index"])
        self.entire_pandas_df.set_index("index", inplace=True)
        self.pandas_df = self.entire_pandas_df[self.start:self.end]
        print("Database loaded")

    # PRIVATE METHODS

    def _create_in_HDFS(self, force_write=False):
        """Create an aggregate in HDFS."""
        """Create statistics for every mentioned plugins.
        """
        print("-----------------------------------------------------")
        print("---------------- AGGREGATE IN HDFS ------------------")

        # perform checks on the params
        if len(self.plugins) == 0:
            raise Exception("Empty plugin list")
        # output dir in user HDFS area
        # for each date
        for (year, month, day) in self.list_dates:
            for signal_name in self.plugins.keys():
                plugin_dict = self.plugins[signal_name]
                p = plugin_dict["plugin_name"]
                print(p)
                # print(plugin_dict)
                # mine data for that
                source_path = "/project/monitoring/collectd/" + p + \
                              "/" + year + "/" + month + "/" + day + "/"
                # /project/it_cloud_data_analytics/2020/02/14/10min/
                # batch@gva_project_14/load_longterm.parquet
                out_path = self.hdfs_folder + "/" \
                    + "/" + year + "/" + month + "/" + day \
                    + "/" + str(self.n_minutes) + "min/" \
                    + self.short_hostgroup + "/" + signal_name
                if not hdfs_exist(out_path) or force_write:
                    print("WORK - Scraping & Aggregating in HDFS: %s" %
                          out_path)
                    self._create_aggregate_specific_path(
                        source_path, out_path,
                        signal_name, plugin_dict)
                else:
                    print("GOOD (Folder already present): %s" % out_path)
            self._transfer_locally([(year, month, day)])

    def _create_aggregate_specific_path(self,
                                        inpath, outpath,
                                        signal_name,
                                        plugin_dict,
                                        timestamp_column='event_timestamp'):
        """It reads a parquet file at the inpath location.

        It implicitely considers only data of "coud_compute/level2/" in the
        'submitter_hostgroup' column. It filter out data that do not match
        the dictionary plugin and then perform a temporal aggregation based
        on the timestamp_column given. The aggregation creates the mean value.
        Finally the complete database is saved in the location "outpath"
        return: final database.
        """
        global MACRO_HOSTGROUP
        # load data
        try:
            full_df = self.spark.read.parquet(inpath)
        except AnalysisException:
            print("path not found: %s" % inpath)
            return
        # filter data by MACRO GROUP
        filtered_df = full_df.filter(
            (full_df.submitter_hostgroup.contains(MACRO_HOSTGROUP)))
        # filter only one cell
        filtered_df = filtered_df.filter(
            (filtered_df.submitter_hostgroup.contains(self.cell)))
        # filter based on dictionary
        if "plugin_instance" in plugin_dict.keys():
            filtered_df = filtered_df.filter(
                (filtered_df.plugin_instance == plugin_dict["plugin_instance"]))  # noqa
        if "type" in plugin_dict.keys():
            filtered_df = filtered_df.filter(
                (filtered_df.type == plugin_dict["type"]))
        if "type_instance" in plugin_dict.keys():
            filtered_df = filtered_df.filter(
                (filtered_df.type_instance == plugin_dict["type_instance"]))
        if "value_instance" in plugin_dict.keys():
            filtered_df = filtered_df.filter(
                (filtered_df.value_instance == plugin_dict["value_instance"]))
        # filtered_df.show(3)
        # keep only interesting column
        df_projection = filtered_df \
            .withColumn(
                'n_min_group',
                (filtered_df[timestamp_column] / 1000) -
                ((filtered_df[timestamp_column] / 1000) % (60 * self.n_minutes))  # noqa
            ).select(
                timestamp_column,
                'submitter_hostgroup',
                'host',
                'plugin',
                'type_instance',
                'value',
                'n_min_group'
            )
        # create aggregate columns
        df_projection.cache()
        statistics_df = df_projection\
            .groupBy([
                'submitter_hostgroup',
                'host',
                'plugin',
                'type_instance',
                'n_min_group'
            ]).agg(
                F.mean('value').alias('mean_value'),
                F.min(timestamp_column).alias(timestamp_column)
            ).orderBy(timestamp_column)

        timestamp_readded_df = statistics_df \
            .withColumn(
                timestamp_column,
                F.from_unixtime((statistics_df[timestamp_column] / 1000),
                                'YYYY-MM-dd HH:mm')
            )
        output_df = timestamp_readded_df
        # overrirde the plugin column
        output_df = output_df.withColumn('plugin',
                                         lit(signal_name))
        write_spark_df(output_df,
                       out_path=outpath,
                       num_files=self.files_per_single_write)
        return output_df

    def _transfer_locally(self, days_to_move):
        """Transfer file locally in csv and hdfs.

        Write a csv on hdfs and transfer it to local where it is converted
        in a hdf5.
        days_to_move: list of triples
            list with elements like this: (year, month, day)
        """
        paths = []
        id_for_this_plugin_aggregate = create_id_for_metrics_dict(self.plugins)
        for (year, month, day) in days_to_move:
            hdfs_this_day = self.hdfs_folder + "/" \
                + "/" + year + "/" + month + "/" + day \
                + "/" + str(self.n_minutes) + "min/" + \
                self.short_hostgroup + "/"
            local_folder_this_day = self.local_folder + "/" \
                + "/" + year + "/" + month + "/" + day \
                + "/" + str(self.n_minutes) + "min/" + \
                self.short_hostgroup + "/"
            paths = [hdfs_this_day + p for p in self.plugins.keys()]
            print("-----------------------------------------------------")
            print("------------ HDFS:  PARQUET -> DAILY CSV ------------")
            print("Reading from HDFS plugin of the day... " +
                  "/" + year + "/" + month + "/" + day)
            [print(x) for x in paths]
            df_on_hdfs = self.spark.read.parquet(*paths)
            df_on_hdfs.cache()
            print("COLUMNS: %s" % df_on_hdfs.schema.names)
            if "plugin" in df_on_hdfs.schema.names:
                plugins_presents = df_on_hdfs.select('plugin') \
                    .distinct().collect()
                plugins_presents = [p[0] for p in plugins_presents]
                print("Plugins in this DataFrame: %s" % plugins_presents)
            if "host" in df_on_hdfs.schema.names:
                different_hosts = df_on_hdfs.select('host') \
                    .distinct().collect()
                nr_of_different_hosts = len(different_hosts)
                print("Nr of Different Hosts present: %s"
                      % nr_of_different_hosts)

            # conversion to pandas in a partition fashion to stay in the limits
            # of the driver memory
            df_on_hdfs = df_on_hdfs.orderBy('n_min_group')
            # in hdfs
            # /project/it_cloud_data_analytics/2020/02/12/10min/main/gva_project_038/<id_plugins>/
            # - part-*.csv

            # locally
            # /tmp/vm-datalake/2020/02/12/10min/main/gva_project_038/<id_plugins>/
            # - part-*.csv

            new_hdfs_folder = \
                hdfs_this_day + id_for_this_plugin_aggregate
            df_on_hdfs.coalesce(1) \
                .write.format("com.databricks.spark.csv") \
                .mode('overwrite') \
                .option("header", "true") \
                .save(new_hdfs_folder)
            # move the copy the file from hdfs to the local machine with
            # cp command read it with the pandas locally

            print("-----------------------------------------------------")
            print("--------------- CSV: HDFS -> LOCAL ------------------")
            new_day_plg_local_folder = \
                local_folder_this_day + id_for_this_plugin_aggregate
            print("new_day_plg_local_folder: ", new_day_plg_local_folder)
            create_local_folder(new_day_plg_local_folder)
            copy_to_local(new_hdfs_folder,
                          new_day_plg_local_folder,
                          override=True)
            # look for the actual pandas csv with name part-0001-*.csv
            csv_name_local = find_csv_filenames(new_day_plg_local_folder)[0]
            complete_csv_path_local = \
                new_day_plg_local_folder + "/" + csv_name_local

            print("-----------------------------------------------------")
            print("------------- LOCALLY: READ DAILY CSV ---------------")
            print("complete_csv_path_local: ", complete_csv_path_local)
            # read local csv file in a pandas
            pdf = pd.read_csv(complete_csv_path_local)
            # skip the pandas conversion
            # pdf = self._to_pandas_oom_safe(df_on_hdfs, num_chunks=num_chunks)
            # cread the csv into pandas
            # create time series
            print("Datetime conversion ...")
            pdf["temporal_index"] = pd.to_datetime(pdf["event_timestamp"])
            # harmonize the discretized values of time, round to the n_minutes
            # or to the closer hour
            if self.n_minutes < 60:
                pdf["temporal_index"] = pd.to_datetime(pdf["temporal_index"]) \
                    .apply(lambda dt: datetime(
                        dt.year, dt.month, dt.day, dt.hour,
                        self.n_minutes * (dt.minute // self.n_minutes)))
            else:
                pdf["temporal_index"] = \
                    pd.to_datetime(pdf["temporal_index"]).dt.round("H")
            # transform scales
            for p in self.plugins.keys():
                c_dict_plugin = self.plugins[p]
                print(c_dict_plugin)
                if "scale" in c_dict_plugin.keys():
                    if c_dict_plugin["scale"] == "log":
                        print("log rescale")
                        mask = (pdf["plugin"] == p)
                        pdf_plugin = pdf[mask]
                        pdf.loc[mask, 'mean_value'] = \
                            np.log(pdf_plugin['mean_value'])
            print("Adding datetime index ...")
            pdf["index"] = pd.to_datetime(pdf["temporal_index"])
            print("Dropping a column ...")
            pdf = pdf.drop(columns=['n_min_group'])
            print("Resampling the frequency of %s" % self.n_minutes)
            # print(pdf.columns)
            # print(pdf.head())

            def resample_chunck(df):
                """Replicate all columns ffill except form mean_value."""
                frequency_str = str(int(self.n_minutes)) + 'min'
                # print(df.columns)
                # print(df.head())
                df_reindexed = df.drop_duplicates('index') \
                    .set_index('index')
                # all columns
                new_df_all_columns = df_reindexed \
                    .resample(frequency_str) \
                    .ffill()
                # print("ALL COLUMNS")
                # print(new_df_all_columns.head())
                # just the mean_value
                new_df_mean_only = df_reindexed["mean_value"] \
                    .resample(frequency_str) \
                    .mean()
                # print("MEAN ONLY")
                # print(new_df_mean_only.head())
                new_df_all_columns["mean_value"] = new_df_mean_only
                return new_df_all_columns

            pdf = (pdf.groupby(['host', 'plugin'])
                   .apply(resample_chunck))
            pdf.reset_index(level=["host", "plugin"], drop=True, inplace=True)
            pdf.reset_index(level=["index"], drop=False, inplace=True)
            pdf["temporal_index"] = pd.to_datetime(pdf["index"])
            pdf.set_index("index", inplace=True)
            pdf.dropna(subset=['plugin'], inplace=True)
            # save hdf5 locally
            new_local_hdf5_file = \
                local_folder_this_day + id_for_this_plugin_aggregate + ".hdf5"

            print("-----------------------------------------------------")
            print("------------- LOCALLY: CSV - hdf5 STORE -------------")
            print("Saving ... %s " % (new_local_hdf5_file))
            store = pd.HDFStore(new_local_hdf5_file)
            store.put('dataset_raw', pdf)
            metadata = {'granularity_min': int(self.n_minutes),
                        'start_date': self.start,
                        'end_date': self.end,
                        'cell_name': self.cell,
                        'hostgroup': self.hostgroup,
                        'short_hostgroup': self.short_hostgroup,
                        'plugins': self.plugins}
            store.get_storer('dataset_raw').attrs.metadata = metadata
            store.close()
            print("-----------------------------------------------------")
            print("---------------- LOCALLY: DELETE CSV ----------------")
            remove_local_csv(folder_local_path=local_folder_this_day)
        df_on_hdfs.unpersist()

    def _to_pandas_oom_safe(self, df, num_chunks=10):
        """To pandas safe against Out-of-memory error."""
        columns = df.schema.fieldNames()
        chunks = df.repartition(num_chunks). \
            rdd.mapPartitions(lambda iterator: [pd.DataFrame(list(iterator),
                                                             columns=columns)]).toLocalIterator()  # noqa
        df_pand = pd.concat(chunks)
        return df_pand

    def _dates_between(self, date_start, date_end):
        """Get all the dates between two date.

        The returned type will be a list of tuples like (year, month, day)
        """
        list_of_dates = []
        sdate = datetime.strptime(date_start, "%Y-%m-%d")   # start date
        edate = datetime.strptime(date_end, "%Y-%m-%d")   # end date
        delta = edate - sdate       # as timedelta
        for i in range(delta.days + 1):
            day = sdate + timedelta(days=i)
            day_tuple = (str(day.year),
                         str(day.month).zfill(2),
                         str(day.day).zfill(2))
            # print(day_tuple)
            list_of_dates.append(day_tuple)
        print("Start: %s - End %s" % (list_of_dates[0], list_of_dates[-1]))
        return list_of_dates
