"""Preprocessing steps entireliy done in Spark."""

import pyspark.sql.functions as F
from pyspark.sql import Window
from pyspark.sql.utils import AnalysisException
from pyspark.sql.functions import col
from pyspark.sql import DataFrame
from functools import reduce  # For Python 3.x
from pyspark.sql.types import StructType
import os, json, copy

from etl.spark_etl.utils import create_paths_for_plugins, print_exception
from etl.spark_etl.utils import keep_only_existing_paths

from etl.spark_etl.cluster_utils import load_schema

from datetime import datetime

# TESTED
def data_reading(spark, plugins, days, hostgroups, local=False, schema_path=None):
    """Read all plugin and all days.

    Params
    ------
    spark: spark context
    plugins: dict.
        Every key is the signal name. Then inside it we have all the value for
        the colums necesary to filter the correct data.
    days: list of tuples
        every tuple is (year, month, day)
    hostgroups: list of str
        full paths for the hostrgoroups you are interested in
    local: boolean
        if true we use file:// for getting data
    schema_path: str
        the string is the path of the json indicating the schema used for reading, 
        if None we don't use the schema

    Return
    ------
    all_data: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        timestamp | hostname | hostgroup | value | plugin
        NB timestamp will be in seconds
        NB hostgroup is the the extended version
    """
    plugins = copy.deepcopy(plugins)
    inpaths = create_paths_for_plugins(plugins=plugins,
                                       days=days)
    existing_inpaths = keep_only_existing_paths(spark=spark,
                                                paths=inpaths,
                                                local=local)
    [print(p) for p in sorted(existing_inpaths)]

    try:
        if local:
            print("local is True.")
            existing_inpaths = ["file://" + x for x in inpaths]

        if schema_path is not None:
            print("schema_path is not None")
            schema = load_schema(schema_path)
            df_raw = spark.read.schema(schema).parquet(*existing_inpaths)
        else:
            print("schema_path is None")
            df_raw = spark.read.parquet(*existing_inpaths)
    except AnalysisException:
        print("path not found: %s" % existing_inpaths)
        return

    all_data = filtered_plugins_hostgroups_df(df_raw,
                                              plugins,
                                              hostgroups)
    return all_data

# TESTED
def downsampling_and_aggregate(df_all_plugins, every_n_minutes):
    """Create the aggregate every x minutes.

    Example: if every 10 min it means that data between 15:10 and 15:20 are
    summarized with the mean statistic and they will get timestamp 15:20.
    This happens for each hostname and plugin separately.

    Params
    ------
    df_all_plugins: PySpark DataFrame
        timestamp | hostname | hostgroup | value | plugin
    every_n_minutes: int
        nr of minutes you want to aggregate. The aggregation function is the
        average.

    Return
    ------
    aggregated_data: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        timestamp | hostname | hostgroup | value | plugin
        NB timestamp will be in seconds
        Value contains the average values every x minutes for that host on that
        plugin.
    """
    print("Aggregating data...")
    
    aggregated_data = df_all_plugins.withColumn(
        'minutes_group',
        F.col("timestamp") - (F.col("timestamp") % (60 * every_n_minutes))
    ).withColumn(
        'timestamp',
        F.col("minutes_group") + (60 * every_n_minutes)
    ).groupBy(
        ['hostgroup',
         'hostname',
         'plugin',
         'minutes_group']
    ).agg(
        F.mean('value').alias('value'),
        F.max("timestamp").alias("timestamp")
    ).orderBy("timestamp")
    
    aggregated_data.show(5, False)

    return aggregated_data

# TESTED
def normalize(df, df_normalization):
    """Normalize the data given a dataframe with the coefficients.

    Remove the mean and divide by the std deviation to the value column.

    Params
    ------
    df: PySpark DataFrame
        timestamp | hostname | hostgroups | value | plugin
    df_normalization: PySpark DataFrame
        hostgroup | plugin | mean | stddev
        where mean and stddev are the mean and stddev of the relative plugin
        for the specified cell.

    Return
    ------
    normalized_data: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        timestamp | hostname | hostgroup | value | plugin
        NB hostgroup is the the extended version
    """
    print("Normalizing the data dataframe...")
    F.broadcast(df_normalization)
    # NOT WORKING WITHOUT PERSIST
    df = df.persist()
    df_normalization = df_normalization.persist()
    to_be_normalized = df.join(df_normalization, 'plugin')

    
    normalized_data = to_be_normalized.withColumn(
        "value", 
        (F.col("value") - F.col("mean")) / F.col("stddev")
    ).select("timestamp", "hostname", "hostgroup", "plugin", "value")

    normalized_data.show(5, False)

    return normalized_data

# TESTED
def pivot_plugin_as_column(df, plugin_names_list):
    """Pivot your dataframe and get the plugins as column.

     Params
    ------
    df: PySpark DataFrame
        timestamp | hostname | hostgroup | value | plugin
    plugin_names_list: list of str
        list of names of the signal present as plugins in this dataset.
        This is requiered boost performance

    Return
    ------
    plugins_as_col: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        time | hostname | hostgroup | load | cpu | memory | etc ...
        hostgroup is the the extended version
    """
    print("Pivoting...")
    plugins_as_col = df.groupBy(
        ["timestamp", "hostname", "hostgroup"]
    ).pivot(
        "plugin", 
        values=plugin_names_list
    ).min("value")

    plugins_as_col.show(5, False)

    print("Removing the columns with all NULL values.. if any...")
    nonNull_cols = [c for c in plugins_as_col.columns if plugins_as_col.filter(col(c).isNotNull()).count() > 0]
    plugins_as_col = plugins_as_col.select(*nonNull_cols)

    plugins_as_col.show(5, False)
    
    return plugins_as_col

# Not Used
def compute_the_coefficients_from_col_df(spark, df_plugins_as_col):
    """Compute from each of the plugin columns (from col DataFrame).

    col DataFrame means that plugins are the column of this dataframe.
    Params
    ------
    spark: spark context
    df_plugins_as_col: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        timestamp | hostname | hostgroup | load | cpu | memory | etc ...

    Return
    ------
    df_normalization: PySpark DataFrame
        hostgroup | plugin | mean | stddev
        where mean and stddev are the mean and stddev of the relative plugin
        for the specified cell.
    """
    plugins = df_plugins_as_col.columns
    [plugins.remove(x) for x in ['timestamp', 'hostname', 'hostgroup']]
    print(plugins)

    # create mean function for each column
    mean_functions = [F.mean(name).alias(name + "_mean") for name in plugins]
    # create stddev function for each column
    std_functions = [F.stddev(name).alias(name + "_std") for name in plugins]

    df_normalization = df_plugins_as_col\
        .groupBy(["hostgroup"])\
        .agg(
            *mean_functions,
            *std_functions
        )
    return df_normalization

# TESTED
def compute_the_coefficients(df_aggregated):
    """Compute from each of the plugin columns.

    Params
    ------
    df_aggregated: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        timestamp | hostname | hostgroup | plugin | value

    Return
    ------
    df_normalization: PySpark DataFrame
        hostgroup | plugin | mean | stddev
        where mean and stddev are the mean and stddev of the relative plugin
        for the specified cell.
    """
    print("Computing coefficients...")
    df_normalization = df_aggregated\
        .groupBy("plugin")\
        .agg(
            F.avg('value').alias("mean"),
            F.stddev('value').alias("stddev"))
    df_normalization.show(5, False)
    return df_normalization

# TESTED
def save_normalization(df_normalization, id_normalization,
                       outfolder, mode="overwrite", local=False):
    """Save the normalization database in the folder.

    Params
    ------
    df_normalization: PySpark DataFrame
        hostgroup | plugin | mean | stddev
        where mean and stddev are the mean and stddev of the relative plugin
        for the specified cell.
    outfolder: str
        absolute path in hdfs to write a single file as output
    id_normalization: str
        id to uniquely represent the normalization coefficients used to
        normalize this dataframe
    mode: str
        'overwrite': Overwrite existing data.
        'append': Append contents of this DataFrame to existing data.
    local: boolean
        if true we use file:// for getting data
    
    Return
    ------
    exit_code: int
        0 = success, 1 = error
    norm_path: str
        path where the normalization is stored
    """
    print("Saving normalization dataframe...")
    norm_path = outfolder + "/" + id_normalization
    if local:
        print("local is True.")
        norm_path = "file://" + norm_path
    try:
        df_normalization.write.format("parquet").mode(mode).save(norm_path)
    except Exception as e:
        print(e)
        return 1, None
    print("Normalization Saved successfully")
    return 0, norm_path

# TESTED
def create_window(df_plugins_as_col, steps):
    """Create a window with the timesteps for history and past.

    Create the lagged timesteps for each column (aka plugin). Do the same also
    for furture steps.
    NB beforhand all the timesteps missing have to be replaced with a null
    value.

    Params
    ------
    df_plugins_as_col: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        timestamp | hostname | hostgroup | load | cpu | memory | etc ...
    steps: int
        nr of lagged steps. New columns will be added with the name of the
        plugin followed by the suffix "_h0", "_h1", "_h2", etc. h0 is the
        current value.
        
    Return
    ------
    df_window: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        timestamp | hostname | hostgroup | load | cpu | memory | etc ...
    """
    plugins = df_plugins_as_col.columns
    [plugins.remove(x) for x in ['timestamp', 'hostname', 'hostgroup']]
    print(plugins)

    window_spec = Window.partitionBy("hostname").orderBy("timestamp")

    # DOUBLE LOOP FORMULATION
    '''
    for metric in sorted(plugins):
        for lag_step in range(history_steps):
            lag_col_name = metric + "_h" + str(lag_step)
            df_resampled = df_resampled\
                .withColumn(lag_col_name,
                            F.lag(metric, lag_step).over(window_spec))

    columns_to_keep = df_resampled.columns
    [columns_to_keep.remove(x) for x in plugins]

    keep only useful columns
    df_window = df_resampled\
        .select(columns_to_keep)
    '''

    # SINGLE LOOP + JOIN FORMULATION
    metrics_dfs = []
    for metric in sorted(plugins):
        df_metric = df_plugins_as_col
        for lag_step in range(steps):
            lag_col_name = metric + "_h" + str(lag_step)
            df_metric = df_metric.withColumn(
                lag_col_name,
                F.lag(
                    metric,
                    -lag_step
                ).over(window_spec)
            )
        # print(metric, " : ", df_metric.columns)
        columns_to_clone = [x for x in df_metric.columns
                            if x not in plugins]
        metrics_dfs.append(df_metric
                           .select(columns_to_clone))
        # .toDF(*df_metric.columns)
    # JOIN
    df_window = join_all(dfs=metrics_dfs,
                         attributes=["timestamp", "hostname", "hostgroup"])

    print("Windows dataset size: ", df_window.count(), " x ", len(df_window.columns))
    print("Columns of the dataset:")
    print(df_window.schema.names)

    return df_window

# TESTED
def union_all(dfs):
    """Merge all datasets row-wise."""
    return reduce(DataFrame.unionAll, dfs)

# Not Used
def create_window_light(spark, df_every_row_a_value,
                        plugin_names_list,
                        start_date, end_date,
                        history_steps, future_steps, every_n_minutes):
    """Create a window with the timesteps for history and past.

    Create the lagged timesteps for each column (aka plugin). Do the same also
    for furture steps.
    NB beforhand all the timesteps missing have to be replaced with a null
    value.

    Params
    ------
    spark: spark context
    df_every_row_a_value: PySpark DataFrame
        timestamp | hostname | hostgroup | value | plugin
    plugin_names_list: list of str
        list of names of the signal present as plugins in this dataset.
        This is requiered boost performance
    start_date: str
        it will be considered the 00:00 of the start day (so including all the
        24 h of the start date)
    end_date: str
        it will be considered the 00:00 of the end day (so including all the
        0 h of the end date)
    history_steps: int
        nr of lagged steps. New columns will be added with the name of the
        plugin followed by the suffix "_h0", "_h1", "_h2", etc. h0 is the
        current value.
    future_steps: int
        nr of future steps. New columns will be added with the name of the
        plugin followed by the suffix "_f1", "_f2", etc. f1 is the
        next value.
    every_n_minutes: int
        nr of minutes of your sampling frequency.
        NB the min value of the database will be take into account as
        starting point
    Return
    ------
    df_window: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        timestamp | hostname | hostgroup | load | cpu | memory | etc ...
    """
    print("columns: ", df_every_row_a_value.columns)
    unit_multiplier = every_n_minutes * 60
    start_ts_seconds = None
    end_ts_seconds = None
    df = df_every_row_a_value
    list_of_df_shifted = []
    # ge the list of plugins to speed up the performance x6
    unique_plugins = list(sorted(plugin_names_list))
    print(unique_plugins)
    # get the min timestamp to understand which data to cut out
    # get the min timestamp to understand which data to cut out
    min_ts, max_ts = \
        df.agg(F.min(col("timestamp")), F.max(col("timestamp"))).collect()[0]

    print("min value:", min_ts)
    ts_first_full_window = min_ts + history_steps * unit_multiplier - 1
    print("max value:", max_ts)
    ts_last_full_window = max_ts

    if start_ts_seconds is not None:
        ts_first_full_window = start_ts_seconds
    if end_ts_seconds is not None:
        ts_last_full_window = end_ts_seconds

    print("ts_first_full_window value:", ts_first_full_window)
    print("ts_last_full_window value:", ts_last_full_window)

    new_lag_cols = [[p + "_h" + str(i)
                     for i in range(history_steps)]
                    for p in unique_plugins]
    flat_lags_cols = [item for sublist in new_lag_cols for item in sublist]
    print(flat_lags_cols)
    # create a df for each time shift
    for shift in range(history_steps):
        df_shift = df.withColumn(
            "group",
            col("timestamp") +
            shift *
            unit_multiplier) .withColumn(
            "lag",
            F.lit(shift))
        list_of_df_shifted += [df_shift]
    # and concatenate them row-wise
    df_all = union_all(list_of_df_shifted)
    # create the intestation of the pivot
    df_column_merged = df_all. \
        withColumn("plugin_group",
                   F.concat(col("plugin"), F.lit("_h"), col("lag")))
    # if plugin_list is not None:
    # pass # imporve efficeincy by defining the value in the pivot column
    df_pivotted = \
        df_column_merged.groupBy(["group", "hostname", "hostgroup"])\
                        .pivot("plugin_group", values=flat_lags_cols)\
                        .min("value")\
                        .withColumnRenamed("group", "timestamp")
    plugin_columns = [c for c in df_pivotted.columns
                      if c not in ["timestamp", "hostname", "hostgroup"]]
    df_reordered = \
        df_pivotted.select("timestamp", "hostname", "hostgroup",
                           *sorted(plugin_columns))
    df_filtered = \
        df_reordered.filter((col("timestamp") >= ts_first_full_window) &
                            (col("timestamp") <= ts_last_full_window))
    # df_ts = \
    #     df_filtered.withColumn("timestamp", col("timestamp").cast("timestamp"))  # noqa
    # df_fillna = df_ts.fillna(0)
    return df_filtered

# TESTED
def data_writing(df_window, outfolder, mode='overwrite'):
    """Save the windows partitioned in the outfolder.

    Params
    ------
    df_window: PySpark DataFrame
        containing data from the plugins and cells in the following form:
        time | hostname | hostgroup | load | cpu | memory | etc ...
    outfolder: str
        absolute path in hdfs to save the partitoned output
    mode: str
        'overwrite': Overwrite existing data.
        'append': Append contents of this DataFrame to existing data.

    Return
    ------
    exit_code: int
        0 = success, 1 = error
    """

    try:
        #
        df_window.repartition(
            10
        ).write.format(
            "parquet"
        ).mode(mode).save(outfolder)
    except Exception as e:
        print_exception("", e)
        return 1
    print("Saved successfully: %s" % outfolder)
    return 0, df_window

# TESTED
def filtered_plugins_hostgroups_df(df, plugins, hostgroups):
    """Keep only the plugins and hostgroups.
       Rename the also with the serialized version.

    Params
    ------
    df: PySpark DataFrame
        containing raw data from the plugins and cells in the following form:
        dstype | host | interval | plugin | plugin_instance | time | type
    plugins: dict
        containing the plugins we want to filter
    hostgroups: list
        containing hostrgroups to filter
        
    Return
    ------
    exit_code: int
        0 = success, 1 = error
    """
    # filter hostgroups
    # only_my_hgs = df.filter(F.col("submitter_hostgroup").isin(hostgroups))
    # Let's use only the first element of the list supposing is a regex
    only_my_hgs = df.filter(F.col("submitter_hostgroup").rlike(hostgroups[0]))
    #repartitioned_only_my_hgs = only_my_hgs.repartition(30)
    # create the filter string
    filter_str = ""
    # for every plugin create the condition and then put them in or
    # df.filter('d<5 and (col1 <> col3 or (col1 = col3 and col2 <> col4))')
    or_conditions = []
    # filter all the plugin individually
    for k in plugins.keys():
        plugin_dict = plugins[k]
        plg_filter = "( " + plugin_dict['plugin_filter'] + " )"
        or_conditions.append(plg_filter)

    # filter all interested line
    filter_str = " or ".join(or_conditions)
    print("filter_str: " + filter_str)
    only_my_plugins = only_my_hgs.filter(filter_str)

    df_list_plugin_ranamed = []
    # filter line of each plugin to rename them
    for plg_name, condition in zip(plugins.keys(), or_conditions):
        df_current = only_my_plugins.filter(condition)
        df_current = df_current.withColumn('plugin', F.lit(plg_name))
        if "function" in plugins[plg_name]:
            function_name = plugins[plg_name]['function']
            print("There is a specific function to be applied to the time series values -> " + function_name)
            # I apply the function to the column value
            df_current = df_current.withColumn("value", getattr(F, function_name)(col("value")))
            # I check if there is negative values setting them to 0
            df_current = reduce(lambda df_current, x: df_current.withColumn(x, F.when(F.col(x) < 0, 0).otherwise(F.col(x))),
                                ['value'],
                                df_current)

        df_list_plugin_ranamed.append(df_current)

    only_my_plugins_renamed = union_all(df_list_plugin_ranamed)

    # project only relevant column
    # cast timestamp to int (seconds)
    relevant_cols = only_my_plugins_renamed\
        .select("event_timestamp",
                "plugin",
                "host",
                "submitter_hostgroup",
                "value")\
        .withColumnRenamed("event_timestamp", "timestamp")\
        .withColumnRenamed("host", "hostname")\
        .withColumnRenamed("submitter_hostgroup", "hostgroup")\
        .withColumn("timestamp", (col("timestamp") / 1000).cast("int"))
    
    return relevant_cols

# TESTED
def join_all(dfs, attributes):
    """Join the dataset on the attributes."""
    return reduce(lambda df_a, df_b:
                  df_a.join(df_b, attributes), dfs)

# TESTED -> TO NOT USE!
def resample(spark, df, start_date, end_date,
             every_n_minutes=10, fill_value=None):
    """Resample the DataFrame with the given frequency.

    NB missing timestamps are replaced with a fixed value.
    Params
    ------
    spark: spark context
    df: PySpark DataFrame
        containing data from the hosts in the following form:
        timestamp | hostname | hostgroup | load | cpu | memory | etc ...
        NB each row contains all the plugins
    every_n_minutes: int
        nr of minutes of resampling.
    fill_value: int (optional)
        the value with which replace null data of the newly introduced
        timestamps
    start_date: str
        it will be considered the 00:00 of the start day (so including all the
        24 h of the start date)
    end_date: str
        it will be considered the 00:00 of the end day (so including all the
        0 h of the end date)
    Return
    ------
    df_resampled: PySpark DataFrame
        containing all timestamps from min to max present before for every
        hostname:
        timestamp | hostname | hostgroup | load | cpu | memory | etc ...
    """
    unit_multiplier = 60 * every_n_minutes
    # create a dataset with all the timestamps for all the hosts
    sdate = datetime.strptime(start_date, "%Y-%m-%d")   # start date
    edate = datetime.strptime(end_date, "%Y-%m-%d")   # end date
    min_ts, max_ts = sdate.timestamp(), edate.timestamp()
    # inference
    # min_ts, max_ts = df\
    #     .agg(F.min(col("timestamp")), F.max(col("timestamp")))\
    #     .collect()[0]
    df_ts = spark\
        .range(min_ts,
               max_ts + unit_multiplier,  # excluded
               step=unit_multiplier)\
        .withColumnRenamed("id", "timestamp")
    df_host_names = df.select("hostname", "hostgroup").distinct()
    F.broadcast(df_host_names)
    # cross join them
    # create df to combine and get all the possible combinations
    df_host_ts = df_ts.crossJoin(df_host_names)
    # confront with the original and add the missing lines
    df_resampled = df_host_ts\
        .join(df,
              on=["timestamp", "hostname", "hostgroup"],
              how="full")
    if fill_value is not None:
        # replace null values created with resampling
        df_resampled = df_resampled.na.fill(fill_value)
    return df_resampled
