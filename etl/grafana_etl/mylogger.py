import logging


def getLogger(level=logging.DEBUG):
    logger = logging.getLogger(name="grafana-ETL")
    # the name of the logger has to be unique to avoid other apps
    # such as matplotlib to see the same logger and print
    # all their info debug there
    # https://stackoverflow.com/questions/56618739/matplotlib-throws-warning-message-because-of-findfont-python
    handlers = logger.handlers
    handler_console = None
    for h in handlers:
        if isinstance(h, logging.StreamHandler):
            handler_console = h
            break

    if handler_console is None:
        handler_console = logging.StreamHandler()

    if handler_console is not None:
        # first we need to remove to avoid duplication
        logger.removeHandler(handler_console)
        formatter = logging.Formatter(
            '''%(asctime)s - %(levelname)s- %(funcName)s - %(message)s''')
        handler_console.setFormatter(formatter)
        # then add it back
        logger.addHandler(handler_console)
    logger.setLevel(level)

    return logger
