from etl.grafana_etl import mylogger
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm

logger = mylogger.getLogger(mylogger.logging.INFO)


class TSAnalyserBase(object):
    """TimeSeries Analyser base class."""

    valid_args = []

    def __init__(self, **kwargs):
        self.pdfs = {}
        self.args = {}
        self.configure(**kwargs)

    def configure(self, **kwargs):
        for arg in self.valid_args:
            try:
                self.args[arg] = kwargs[arg]
            except KeyError:
                self.args[arg] = None
                logger.error('argument %s not defined' % arg)

    def setData(self, pdf):
        self.pdfs['input'] = pdf

    def getResponse(self):
        pdf = self.pdfs['response']
        return pdf.loc[pdf.last_valid_index()].to_dict()

    def getResults(self):
        return self.pdfs

    def runStrategy(self):
        pass

    def getPlots(self, flags=[], **pltargs):
        for flag in self.pdfs['input'].keys():
            if len(flags) == 0 or flag in flags:
                self.plotFlag(flag, **pltargs)

    def plotFlag(self, flag, **pltargs):
        print('plotFlag ' + flag)

        fig, axs = plt.subplots(2, 1, figsize=(32, 16))
        fig.suptitle(flag)

        ax = axs[0]
        ax.set_title('input Vs response')
        self.pdfs['input'][flag].plot(ax=ax, **pltargs)
        # (100*self.pdfs['response'][flag]+1).plot(ax=ax, **pltargs)
        # NB: workaround to show a heavyside in log scale

        # put a * at the top of the anomaly

        # need to reset index to avoid mismatch of indexes with the input pdf
        anomaly_pdf = self.pdfs['response'][flag] > 0
        self.pdfs['input'][flag][anomaly_pdf].plot(ax=ax, marker='o',
                                                   markersize=5,
                                                   fillstyle='none',
                                                   color='red',
                                                   linestyle='none')
        # ax = axs[1]
        # ax.set_title('input Vs model')
        # self.pdfs['input'][flag].plot(ax=ax, **pltargs)
        # self.pdfs['model'][flag].plot(ax=ax, **pltargs)

        ax = axs[1]
        ax.set_title('All derived quantities')
        for k, v in self.pdfs.items():
            if k == 'response':
                continue
            v[flag].plot(ax=ax, label=k, **pltargs)
        lines = ax.get_lines()
        ax.legend(lines, [label.get_label() for label in lines])

        return fig, axs


class RollingStatAnalyser(TSAnalyserBase):

    def __init__(self, **kwargs):
        """Rolling analyser."""
        TSAnalyserBase.valid_args = ['class', 'strategy', 'roll_win',
                                     'shift_win', 'th', 'alpha']
        TSAnalyserBase.__init__(self, **kwargs)
        logger.info('setting analyser %s' % __name__)

    def quantileStrategy(self):
        shift_win = self.args['shift_win']
        roll_win = self.args['roll_win']
        th = self.args['th']
        alpha = self.args['alpha']

        pdf = self.pdfs['input']

        # print 'shift data of %s' % shift_win
        # ldf = pdf.shift(shift_win)
        # print ldf.head()
        # self.pdfs['ldf'] = ldf

        print('diff data of %s' % shift_win)
        ldf = pdf.diff(shift_win)
        self.pdfs['ldf'] = ldf

        # https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.rolling.html
        print('Rolling of %s win' % roll_win)
        ldf_rolling = ldf.rolling(roll_win)

        # print 'evaluate rolling mean'  ;
        # self.pdfs['mean_ldf'  ] = ldf_rolling.mean()
        # print 'evaluate rolling median';
        # self.pdfs['median_ldf'] = ldf_rolling.median()
        # print 'evaluate rolling p80'   ;
        # self.pdfs['p80_ldf'   ] = ldf_rolling.quantile(.8)

        self.pdfs['model'] = alpha * ldf_rolling.quantile(1 - th)
        self.pdfs['response'] = np.heaviside(self.pdfs['ldf'] -
                                             self.pdfs['model'], 0.)

    @staticmethod
    def NormProb(x, th):
        """Gaussian Probability.

        x is an array of values
        calculate the threshold values for a gaussian distribution
        """
        N = len(x)
        # print "N", N
        if N < 36:
            return 1000.
        return norm(np.median(x[: N - 1]),
                    (np.percentile(x[: N - 1], 75) -
                     np.percentile(x[: N - 1], 25)
                     ) / 0.349
                    ).ppf(th)

    def probStrategy(self):
        shift_win = self.args['shift_win']
        roll_win = self.args['roll_win']
        th = self.args['th']

        pdf = self.pdfs['input']

        print('diff data of %s' % shift_win)
        ldf = pdf.diff(shift_win)
        # ldf = pdf.rolling(roll_win).apply(
        # lambda x:x[len(x)-1] - x[np.random.randint(0, len(x))])
        self.pdfs['ldf'] = ldf
        print(ldf.head())
        # https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.rolling.html
        print('Rolling of %s win' % roll_win)
        ldf_rolling = ldf.rolling(roll_win)

        self.pdfs['model_high'
                  ] = ldf_rolling.apply(RollingStatAnalyser.NormProb,
                                        kwargs={'th': 1 - th}  # , raw=True
                                        )
        self.pdfs['model_low'
                  ] = ldf_rolling.apply(RollingStatAnalyser.NormProb,
                                        kwargs={'th': th}
                                        )
        self.pdfs['response'
                  ] = (
                      np.heaviside(self.pdfs['ldf'] -
                                   self.pdfs['model_high'],
                                   0.)
                      +
                      np.heaviside(self.pdfs['model_low'] -
                                   self.pdfs['ldf'],
                                   0.)
        )

    @staticmethod
    def differential(x):
        """differential.

        x is an array of values of len(x) = N
        calculate the average differential for i=0..N-2 (x[N-1] - x[i])
        """
        N = len(x)
        return np.fabs(np.mean(map(lambda y: x[N - 1] - y, x[:N - 1])))

    def differentialStrategy(self):
        roll_win = self.args['roll_win']
        th = self.args['th']
        pdf = self.pdfs['input']

        self.pdfs['model'
                  ] = pdf.rolling(roll_win
                                  ).apply(RollingStatAnalyser.differential)
        self.pdfs['response'] = np.heaviside(self.pdfs['model'] - th, 0.)

    def runStrategy(self):
        print('evaluate model')
        if self.args['strategy'] == 'quantile':
            self.quantileStrategy()
        elif self.args['strategy'] == 'differential':
            self.differentialStrategy()
        elif self.args['strategy'] == 'gaussprob':
            self.probStrategy()
        else:
            self.pdfs['model'] = None
            raise Exception('''Not known strategy defined %s'''
                            % self.args['strategy'])
