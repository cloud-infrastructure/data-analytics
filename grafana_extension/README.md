This folder contains the extension to the Grafana Client JS needed to extend the Grafana Annotation functionalities,
allowing the automatic annotation of Anomalies.
In addition, all the templated variables are saved in the grafana annotation as tags.

The code in this folder is under the same license of Grafana
Apache License 2.0  https://github.com/grafana/grafana/blob/v7.1.3/LICENSE

### Build the extension
Procedure to extend the js code, when a new version of monit-grafana is released.
The js code to be extended can be found via browser developers tools in the folder `monit-grafana.cern.ch/public/build/app.*js`

1. Download it in this folder to extend it

```
curl -v -o app.grafana_7.3.3_extend_annotations.js https://monit-grafana.cern.ch/public/build/app.68bcddb017f3fbc49566.js  
```

1. Commit the code for reference

1. Replace in the js file the Annotation input form with what reported in [new_annotation_buttons.js_diff](new_annotation_buttons.js_diff)     

1. Add in the js file the Annotation button functions with what reported in [new_annotation_buttons_functionality.js_diff](new_annotation_buttons_functionality.js_diff)     

1. Expose the js file in some accessible url (see next section)

### Install advanced grafana annotation capabilities in the browser

- Install "Resource override" plugin for substituting the default javascript with our empowered version
    - CHROME: https://chrome.google.com/webstore/detail/resource-override/pkoacgokdfckfpndoffpifphamojphii
    - FIREFORX: https://addons.mozilla.org/en-US/firefox/addon/resourceoverride/
- Add this rule:
    - From: https://monit-grafana.cern.ch/public/build/app.*.js
    - To: <some_url_where_the_new_js_file_is_exported>
        - NB: you cannot serve this js directly from gitlab in raw format for security reasons (see https://gitlab.com/gitlab-org/gitlab-foss/-/issues/17234)
