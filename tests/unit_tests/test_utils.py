import unittest, os, yaml
import pandas as pd
import numpy as np
from etl.spark_etl.utils import get_date_between, create_paths_for_plugins, keep_only_existing_paths, read_local_window_dataset
from tests.unit_tests.utils import are_pandas_dfs_equal

class TestUtils(unittest.TestCase):
    def setUp(self):
        print()

        test_file = os.path.dirname(os.path.realpath(__file__)) + "/test.yaml"
        with open(test_file) as yaml_file:
            self.file_dict = yaml.safe_load(yaml_file)

        self.config_file = os.path.dirname(
            os.path.realpath(__file__)
        ) + "/config.json"
        
        self.REDUCED_config_file = os.path.dirname(
            os.path.realpath(__file__)
        ) + "/config_REDUCED.json"

    def test__get_date_between__compare(self):
        date_to_compare = get_date_between(
            start_date=self.file_dict['start_date'],
            end_date=self.file_dict['end_date']
        )
        expectation = self.file_dict['expected_utils']["get_date_between"]
        self.assertEqual(str(date_to_compare), expectation)

    def test__create_paths_for_plugins__compare(self):
        list_to_compare = create_paths_for_plugins(
            plugins=self.file_dict['selected_plugins'],
            days=get_date_between(
                start_date=self.file_dict['start_date'],
                end_date=self.file_dict['end_date']
            )
        )
        list_to_compare.sort()
        expectation = self.file_dict['expected_utils']["create_paths_for_plugins"]
        self.assertEqual(str(list_to_compare), expectation)    

    def test__keep_only_existing_paths__compare(self):
        more_paths = [
            "/eos/project/i/it-cloud-data-analytics/eos-datalake/dataset-tests/01_Original-Format_3-Days_2-Hostgroups_3-Plugins/it_doesnt_exist/",
            "/eos/project/i/it-cloud-data-analytics/eos-datalake/dataset-tests/02_testing_the_function/"
        ]
        paths = create_paths_for_plugins(
            plugins=self.file_dict['selected_plugins'],
            days=get_date_between(
                start_date=self.file_dict['start_date'],
                end_date=self.file_dict['end_date']
            )
        )
        paths.extend(more_paths)

        list_to_compare = keep_only_existing_paths(
            spark=None,
            paths=paths,
            local=True
        )
        list_to_compare.sort()
        expectation = self.file_dict['expected_utils']["keep_only_existing_paths"]
        self.assertEqual(str(list_to_compare), expectation)    

    def test__read_local_window_dataset__compare(self):
        a, b, c = read_local_window_dataset(
            config_filepath=self.config_file
        )

        outfolder_1 = self.file_dict['read_local_window_dataset']['feature_dataset'] + "/features.csv"
        outfolder_2 = self.file_dict['read_local_window_dataset']['provenance_dataset'] + "/provenance.csv"
        ### USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # a.to_csv(outfolder_1, index=False)
        # b.to_csv(outfolder_2, index=False)
        
        # EXPECTED OUTPUT
        expectation_1 = pd.read_csv(outfolder_1)
        expectation_2 = pd.read_csv(outfolder_2, dtype={'hostname': np.dtype('object'), 'ts': np.dtype('object'), 'timestamp': np.dtype('int32')})

        self.assertTrue(are_pandas_dfs_equal(a, expectation_1, ['cpu__percent_idle_h0', 'load_longterm_h1']))
        self.assertTrue(are_pandas_dfs_equal(b, expectation_2, ['hostname']))
        self.assertEqual(c, self.file_dict['read_local_window_dataset']['nr_timeseries'])

    def tearDown(self):
        pass