from pyspark.sql.functions import UserDefinedFunction
from pyspark.sql.types import DoubleType
import subprocess

def are_dfs_equal(df1, df2, print_schemas=True):
    if print_schemas:
        print("******\n", df1.schema, "\n", df2.schema, "\n******\n")
    names_1 = []
    names_2 = []
    types_1 = []
    types_2 = []
    for i in df1.schema.fields:
        names_1.append(i.jsonValue()['name'])
    for i in df2.schema.fields:
        names_2.append(i.jsonValue()['name'])
    for i in df1.schema.fields:
        types_1.append(i.jsonValue()['type'])
    for i in df2.schema.fields:
        types_2.append(i.jsonValue()['type'])
    if names_1 != names_2:
        return False
    if types_1 != types_2:
        return False


    for name in ['value', 'mean', 'stddev']:
        udf = UserDefinedFunction(lambda x: float( '%.3f'%(x) ), DoubleType())
        df1 = df1.select(*[udf(column).alias(name) if column == name else column for column in df1.columns])
        df2 = df2.select(*[udf(column).alias(name) if column == name else column for column in df2.columns])

    a = df1.collect()
    a.sort()
    b = df2.collect()
    b.sort()
    if a != b:
        print("Different values inside the dataframes!")
        return False
    return True

def overwrite_dataframe(df_to_compare, outfolder):
    try:
        subprocess.call(["rm", "-r", outfolder])
    except:
        pass
    df_to_compare.repartition(5).write.format("parquet").mode('overwrite').save("file://" + outfolder)

def are_pandas_dfs_equal(df1, df2, sorting_value):
    df1 = df1.sort_values(by=sorting_value)
    df2 = df2.sort_values(by=sorting_value)
    return (df1.round(2)).equals(df2.round(2))