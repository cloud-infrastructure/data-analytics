import unittest, os, yaml
from etl.spark_etl.cluster_utils import load_schema

class TestClusterUtils(unittest.TestCase):
    def setUp(self):
        test_file = os.path.dirname(os.path.realpath(__file__)) + "/test.yaml"
        with open(test_file) as yaml_file:
            self.file_dict = yaml.safe_load(yaml_file)

    def test__load_schema__compare(self):
        for schema in self.file_dict['json_load_schema_paths']:
            # INPUT
            input_json = self.file_dict['json_load_schema_paths'][schema]
            
            schema_to_compare = load_schema(
                json_schema=input_json
            )
            
            expectation = self.file_dict['expected_cluster_utils']['load_schema'][schema]
            
            self.assertTrue(str(schema_to_compare) == str(expectation))

    def tearDown(self):
        pass