import unittest, yaml, os
from adcern.publisher import create_id_for_metrics_dict
from adcern.publisher import create_id_for_hostgroups_and_plugins_start_end

class TestPublisher(unittest.TestCase):
    def setUp(self):
        collectd_path = "/project/monitoring/collectd/"
        self.plugin_order_1 = {
            'cpu__percent_idle': {
                'plugin_data_path': collectd_path + 'cpu',
                'plugin_filter': "type == 'percent' and type_instance == 'idle' and plugin == 'cpu'"
            }, 
            'load_longterm': {
                'plugin_data_path': collectd_path + 'load',
                'plugin_filter': "value_instance == 'longterm' and plugin == 'load'"
            }
        }
        self.plugin_order_2 = {
            'load_longterm': {
                'plugin_data_path': collectd_path + 'load',
                'plugin_filter': "value_instance == 'longterm' and plugin == 'load'"
            },
            'cpu__percent_idle': {
                'plugin_data_path': collectd_path + 'cpu',
                'plugin_filter': "type == 'percent' and type_instance == 'idle' and plugin == 'cpu'"
            }
        }
        #self.test_file = "work/tests/adcern/test.yaml"
        test_file = os.path.dirname(os.path.realpath(__file__)) + "/test_adcern.yaml"
        with open(test_file) as yaml_file:
            self.file_dict = yaml.safe_load(yaml_file)

        

    def test__create_id_for_metrics_dict__order(self):
        """Check if the function is invariant to the order."""
        hash_order_1 = create_id_for_metrics_dict(plugins=self.plugin_order_1)
        hash_order_2 = create_id_for_metrics_dict(plugins=self.plugin_order_1)
        self.assertEqual(hash_order_1, hash_order_2)

    def test__create_id_for_hostgroups_and_plugins_start_end__order(self):
        """Check if the function is invariant to order of hostsgroups."""
        hostgroups_order_1 = [
            "cloud_compute/level2/main/gva_shared_012",
            "cloud_compute/level2/batch/gva_project_014"
        ]
        hostgroups_order_2 = [
            "cloud_compute/level2/batch/gva_project_014",
            "cloud_compute/level2/main/gva_shared_012"
        ]
        
        start_date = "2020-07-12"
        end_date = "2020-07-17"
        hash_order_1 = \
            create_id_for_hostgroups_and_plugins_start_end(
                hostgroups=hostgroups_order_1,
                plugins=self.plugin_order_1,
                start=start_date, end=end_date)
        hash_order_2 = \
            create_id_for_hostgroups_and_plugins_start_end(
                hostgroups=hostgroups_order_2,
                plugins=self.plugin_order_1,
                start=start_date, end=end_date)
        self.assertEqual(hash_order_1, hash_order_2)

    def test__create_id_for_metrics_dict__compare(self):
        hash_to_compare = create_id_for_metrics_dict(
            plugins=self.file_dict["selected_plugins"]
        )
        expectation = self.file_dict['expected_publisher']["create_id_for_metrics_dict"]
        self.assertEqual(hash_to_compare, expectation)

    def test__create_id_for_hostgroups_and_plugins_start_end__compare(self):
        hash_to_compare = create_id_for_hostgroups_and_plugins_start_end(
            hostgroups=self.file_dict["hostgroups"],
            plugins=self.file_dict["selected_plugins"], 
            start=self.file_dict["start_date"], 
            end=self.file_dict["end_date"]
        )
        expectation = self.file_dict['expected_publisher']["create_id_for_hostgroups_and_plugins_start_end"]
        self.assertEqual(hash_to_compare, expectation)
    
    def tearDown(self):
        pass
    