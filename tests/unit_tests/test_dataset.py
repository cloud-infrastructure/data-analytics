import unittest
from etl.spark_etl.dataset import CollectdDataset

class TestDataset(unittest.TestCase):
    def setUp(self):
        pass

    def test_creation_dataset_manually(self):
        clds = CollectdDataset()
        clds.set_download_options(hdfs_folder="/project/it_cloud_data_analytics",  # noqa
                                  local_folder="/tmp/vm-datalake/",
                                  override_on_hdfs=False,
                                  num_chunks_pandas=5)
        clds.set_range(start_date="2020-05-20",
                       end_date="2020-05-30",
                       n_minutes=10)
        self.assertIsNotNone(clds)

    def tearDown(self):
        pass