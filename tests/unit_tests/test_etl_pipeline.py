import unittest, os, sys, pyspark, yaml
from etl.spark_etl.etl_pipeline import materialize_locally, pipeline_preparation_norm_coeff, get_normalization_path, get_normalization_pandas
from etl.spark_etl.etl_pipeline import get_aggregated_data, read_normalization_dataframe, run_pipeline_all_in_one, read_spark_df
from etl.spark_etl.utils import read_json_config
from .utils import are_dfs_equal, overwrite_dataframe

class TestEtlPipeline(unittest.TestCase):
    def setUp(self):
        print()

        os.environ['PYSPARK_PYTHON'] = sys.executable
        os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable

        number_cores = 1
        memory_gb = 3

        conf = (
            pyspark.SparkConf()
                .setMaster('local[{}]'.format(number_cores))
                .set('spark.driver.memory', '{}g'.format(memory_gb))
        )
        self.sc = pyspark.SparkContext(conf=conf)
        self.spark = pyspark.sql.SparkSession(self.sc)
        
        test_file = os.path.dirname(os.path.realpath(__file__)) + "/test.yaml"
        with open(test_file) as yaml_file:
            self.file_dict = yaml.safe_load(yaml_file)
        
        self.config_file = os.path.dirname(
            os.path.realpath(__file__)
        ) + "/config.json"
        
        self.REDUCED_config_file = os.path.dirname(
            os.path.realpath(__file__)
        ) + "/config_REDUCED.json"

    def test__local_spark_master_name__compare(self):
        name_to_compare = self.sc.getConf().get('spark.master')
        expectation = self.file_dict['expected_etl_pipeline']["local_spark_master_name"]
        self.assertEqual(name_to_compare, expectation)

    def test__pipeline_preparation_norm_coeff__check(self):
        result_to_compare, path_to_compare = pipeline_preparation_norm_coeff(
            spark=self.spark,
            config_filepath=self.config_file,
            local=True
        )
        
        self.assertEqual(result_to_compare, 0)
        self.assertEqual(path_to_compare, 
                         "file://" + 
                         self.file_dict['expected_etl_pipeline']['pipeline_preparation_norm_coeff'])

    def test__REDUCED_pipeline_preparation_norm_coeff__check(self):
        result_to_compare, path_to_compare = pipeline_preparation_norm_coeff(
            spark=self.spark,
            config_filepath=self.REDUCED_config_file,
            local=True
        )
        
        self.assertEqual(result_to_compare, 0)
        self.assertEqual(path_to_compare, 
                         "file://" + 
                         self.file_dict['expected_etl_pipeline']['REDUCED_pipeline_preparation_norm_coeff'])
    
    def test__pipeline_all_in_one__check(self):
        result_to_compare, df_to_compare = run_pipeline_all_in_one(
            spark=self.spark,
            config_filepath=self.config_file,
            local=True
        )
        
        outfolder = self.file_dict['pipeline_all_in_one_path']
        ### USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)

        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertEqual(result_to_compare, 0)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__REDUCED_pipeline_all_in_one__check(self):
        result_to_compare, df_to_compare = run_pipeline_all_in_one(
            spark=self.spark,
            config_filepath=self.REDUCED_config_file,
            local=True
        )
        
        outfolder = self.file_dict['REDUCED_pipeline_all_in_one_path']
        ### USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertEqual(result_to_compare, 0)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__REDUCED_get_normalization_path__compare(self):
        path_to_compare = get_normalization_path(
            config_filepath=self.REDUCED_config_file
        )
        
        self.assertEqual(path_to_compare, 
                         self.file_dict['expected_etl_pipeline']['REDUCED_pipeline_preparation_norm_coeff'])
        
    def test__REDUCED_get_normalization_pandas__check(self):
        df_to_compare = get_normalization_pandas(
            spark=self.spark,
            config_filepath=self.REDUCED_config_file,
            local=True,
            schema_path=self.file_dict['json_load_schema_paths']['norm_coeffs']
        )

        self.assertTupleEqual(df_to_compare.shape, 
                              eval(self.file_dict['expected_etl_pipeline']
                                   ['REDUCED_get_normalization_pandas']['shape']))
        
        row = df_to_compare.iloc[0]
        self.assertEqual(row['hostgroup'],
                         self.file_dict['expected_etl_pipeline']
                         ['REDUCED_get_normalization_pandas']['hostgroup'])
        self.assertEqual(row['plugin'],
                         self.file_dict['expected_etl_pipeline']
                         ['REDUCED_get_normalization_pandas']['plugin'])
        self.assertEqual(row['mean'],
                         self.file_dict['expected_etl_pipeline']
                         ['REDUCED_get_normalization_pandas']['mean'])
        self.assertEqual(row['stddev'],
                         self.file_dict['expected_etl_pipeline']
                         ['REDUCED_get_normalization_pandas']['stddev'])
    
    def test__REDUCED_get_aggregated_data__compare(self):
        df_to_compare = get_aggregated_data(
            spark=self.spark,
            config_filepath=self.REDUCED_config_file,
            normalization=True,
            local=True
        )  
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_aggregated_paths']['_2'])
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
    
    def test__REDUCED_read_normalization_dataframe__compare(self):
        df_to_compare = read_normalization_dataframe(
            spark=self.spark,
            normalization_folder=self.file_dict['REDUCED_df_norm_coeffs_CHECK_path'],
            normalization_id=self.file_dict['REDUCED_read_normalization_dataframe_id'],
            schema_path=self.file_dict['json_load_schema_paths']['norm_coeffs']
        )

        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + 
                                              self.file_dict['expected_etl_pipeline']
                                              ['REDUCED_read_normalization_dataframe'])
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__read_spark_df__compare(self):
        df_to_compare = read_spark_df(
            spark=self.spark,
            df_path="file://" + self.file_dict['df_pivot_plugin_as_column_path'],
            schema_path=None
        )
        
        outfolder = self.file_dict['df_pivot_plugin_as_column_path']
        ### USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        ## overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__read_spark_df_SCHEMA__compare(self):
        df_to_compare = read_spark_df(
            spark=self.spark,
            df_path="file://" + self.file_dict['REDUCED_df_raw_path'],
            schema_path=self.file_dict["json_load_schema_paths"]["raw"]
        )
        
        outfolder = self.file_dict['REDUCED_df_raw_path']
        ### USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        ## overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__materialize_locally__check(self):
        materialize_locally(
            spark=self.spark,
            config_filepath=self.config_file,
            local=True
        )
        
        tmp = read_json_config(self.config_file)
        outfolder = tmp["hdfs_cache_folder"] + "/" + tmp["code_project_name"]
        df_to_check = self.spark.read.parquet("file://" + outfolder)
        self.assertEqual(len(df_to_check.columns), 10)
        self.assertEqual(df_to_check.count(), 12482)

    def tearDown(self):
        self.sc.stop()