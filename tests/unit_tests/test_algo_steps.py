from threading import local
from adcern.algo_steps import _compute_timestamps_in_seconds, read_window_dataset
import unittest, os, sys, pyspark, yaml

from etl.spark_etl.utils import read_json_config
from .utils import are_dfs_equal, overwrite_dataframe

class TestAlgoSteps(unittest.TestCase):
    def setUp(self):
        print()
        
        os.environ['PYSPARK_PYTHON'] = sys.executable
        os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable
        number_cores = 1
        memory_gb = 3
        conf = (
            pyspark.SparkConf()
                .setMaster('local[{}]'.format(number_cores))
                .set('spark.driver.memory', '{}g'.format(memory_gb))
        )
        self.sc = pyspark.SparkContext(conf=conf)
        self.spark = pyspark.sql.SparkSession(self.sc)
        
        test_file = os.path.dirname(os.path.realpath(__file__)) + "/test_adcern.yaml"
        with open(test_file) as yaml_file:
            self.file_dict = yaml.safe_load(yaml_file)

        self.config_file = os.path.dirname(
            os.path.realpath(__file__)
        ) + "/config.json"
        
        self.REDUCED_config_file = os.path.dirname(
            os.path.realpath(__file__)
        ) + "/config_REDUCED.json"

        self.APPENDING_config_file = os.path.dirname(
            os.path.realpath(__file__)
        ) + "/config_APPENDING.json"

    def test__compute_timestamps_in_seconds__compare(self):
        to_compare = _compute_timestamps_in_seconds(
            config_dict=read_json_config(self.config_file)
        )
        
        self.assertEqual(str(to_compare), self.file_dict["expected_algo_steps"]["compute_timestamps_in_seconds"])
    
    def test__read_window_dataset__compare(self):
        for steps in [1, 12, 24]:
            # TRANSFORMATION
            config_dict = read_json_config(self.config_file)
            config_dict["slide_steps"] = steps
            config_dict["aggregate_every_n_minutes"] = 60
            config_dict["history_steps"] = 24
            config_dict["hdfs_out_folder"] = "/eos/project-i/it-cloud-data-analytics/eos-datalake/dataset-tests"
            config_dict["code_project_name"] = "11_create_window_24_steps"

            df_to_compare = read_window_dataset(
                spark=self.spark,
                config_filepath=self.config_file,
                config_dict_debug=config_dict
            )
            
            outfolder = self.file_dict['read_window_dataset_path_' + str(steps)]
            # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
            # overwrite_dataframe(df_to_compare, outfolder)

            # EXPECTED OUTPUT
            expectation = self.spark.read.parquet("file://" + outfolder)
            self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__read_window_dataset_APPENDING_compare(self):
        for steps in [1, 12, 24]:
            # TRANSFORMATION
            config_dict = read_json_config(self.APPENDING_config_file)
            config_dict["slide_steps"] = steps
            config_dict["hdfs_out_folder"] = "/eos/project-i/it-cloud-data-analytics/eos-datalake/dataset-tests"
            config_dict["code_project_name"] = "11_create_window_24_steps"

            df_to_compare = read_window_dataset(
                spark=self.spark,
                config_filepath=self.config_file,
                config_dict_debug=config_dict
            )
            
            outfolder = self.file_dict['read_window_dataset_appending_path_' + str(steps)]
            # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
            # overwrite_dataframe(df_to_compare, outfolder)

            # EXPECTED OUTPUT
            expectation = self.spark.read.parquet("file://" + outfolder)
            self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def tearDown(self):
        self.sc.stop()