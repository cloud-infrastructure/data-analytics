import shutil
from threading import local
from adcern.publisher import create_id_for_hostgroups_and_plugins_start_end
import unittest, os, sys, pyspark, yaml
from etl.spark_etl.etl_steps import filtered_plugins_hostgroups_df, data_reading, downsampling_and_aggregate
from etl.spark_etl.etl_steps import compute_the_coefficients, save_normalization, normalize, pivot_plugin_as_column
from etl.spark_etl.etl_steps import resample, create_window, data_writing, union_all, join_all
from etl.spark_etl.utils import get_date_between
from .utils import are_dfs_equal, overwrite_dataframe

class TestEtlSteps(unittest.TestCase):
    def setUp(self):
        print()

        os.environ['PYSPARK_PYTHON'] = sys.executable
        os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable
        number_cores = 1
        memory_gb = 3
        conf = (
            pyspark.SparkConf()
                .setMaster('local[{}]'.format(number_cores))
                .set('spark.driver.memory', '{}g'.format(memory_gb))
        )
        self.sc = pyspark.SparkContext(conf=conf)
        self.spark = pyspark.sql.SparkSession(self.sc)
        
        test_file = os.path.dirname(os.path.realpath(__file__)) + "/test.yaml"
        with open(test_file) as yaml_file:
            self.file_dict = yaml.safe_load(yaml_file)
    
    def test__filtered_plugins_hostgroups_df__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['df_raw_path'])
        # TRANSFORMATION
        df_to_compare = filtered_plugins_hostgroups_df(
            df=df,
            plugins=self.file_dict['selected_plugins'],
            hostgroups=self.file_dict['filtered_plugins_hostgroups_df_HOSTGROUPS_TO_FILTER']
        )
        
        outfolder = self.file_dict['df_filtered_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)

        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__PLUGIN_FUNCTIONS_filtered_plugins_hostgroups_df__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['df_raw_path'])
        # TRANSFORMATION
        df_to_compare = filtered_plugins_hostgroups_df(
            df=df,
            plugins=self.file_dict['PLUGIN_FUNCTIONS_selected_plugins'],
            hostgroups=self.file_dict['filtered_plugins_hostgroups_df_HOSTGROUPS_TO_FILTER']
        )
        
        outfolder = self.file_dict['df_filtered_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)

        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def test__REDUCED_filtered_plugins_hostgroups_df__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_raw_path'])
        # TRANSFORMATION
        df_to_compare = filtered_plugins_hostgroups_df(
            df=df,
            plugins=self.file_dict['REDUCED_selected_plugins'],
            hostgroups=self.file_dict['REDUCED_filtered_plugins_hostgroups_df_HOSTGROUPS_TO_FILTER']
        )
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_filtered_path'])
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def test__data_reading__compare(self):
        df_to_compare = data_reading(
            spark=self.spark,
            plugins=self.file_dict["selected_plugins"],
            days=get_date_between(
                start_date=self.file_dict["start_date"],
                end_date=self.file_dict["end_date"]
            ),
            hostgroups=self.file_dict["data_reading_HOSTGROUPS_TO_FILTER"],
            local=True,
            schema_path=self.file_dict['json_load_schema_paths']['raw']
        )
        
        outfolder = self.file_dict['df_filtered_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def test__REDUCED_data_reading__compare(self):
        df_to_compare = data_reading(
            spark=self.spark,
            plugins=self.file_dict["REDUCED_selected_plugins"],
            days=get_date_between(
                start_date=self.file_dict["REDUCED_start_date"],
                end_date=self.file_dict["REDUCED_end_date"]
            ),
            hostgroups=self.file_dict["REDUCED_data_reading_HOSTGROUPS_TO_FILTER"],
            local=True,
            schema_path=self.file_dict['json_load_schema_paths']['raw']
        )
        outfolder = self.file_dict['REDUCED_df_filtered_path']
        
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT, THEN CHECK ON SWAN
        # overwrite_dataframe(df_to_compare, outfolder)
  
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__REDUCED_data_reading_NO_SCHEMA__compare(self):
        df_to_compare = data_reading(
            spark=self.spark,
            plugins=self.file_dict["REDUCED_selected_plugins"],
            days=get_date_between(
                start_date=self.file_dict["REDUCED_start_date"],
                end_date=self.file_dict["REDUCED_end_date"]
            ),
            hostgroups=self.file_dict["REDUCED_data_reading_HOSTGROUPS_TO_FILTER"],
            local=True,
            schema_path=None
        )
        outfolder = self.file_dict['REDUCED_df_filtered_path']
        
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT, THEN CHECK ON SWAN
        # overwrite_dataframe(df_to_compare, outfolder)
  
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
    
    def test__downsampling_and_aggregate__compare(self):
        for aggregation in self.file_dict['aggregation_minutes']:
            # INPUT
            df = self.spark.read.parquet("file://" + self.file_dict['df_filtered_path'])
            # TRANSFORMATION
            df_to_compare = downsampling_and_aggregate(
                df_all_plugins=df,
                every_n_minutes=self.file_dict['aggregation_minutes'][aggregation]
            )
            
            outfolder = self.file_dict['df_aggregated_paths'][aggregation]
            # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
            # overwrite_dataframe(df_to_compare, outfolder)
            
            # EXPECTED OUTPUT
            expectation = self.spark.read.parquet("file://" + outfolder)
            self.assertTrue(are_dfs_equal(df_to_compare, expectation))
            
    def test__REDUCED_downsampling_and_aggregate__compare(self):
        for aggregation in self.file_dict['REDUCED_aggregation_minutes']:
            # INPUT
            df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_filtered_path'])
            # TRANSFORMATION
            df_to_compare = downsampling_and_aggregate(
                df_all_plugins=df,
                every_n_minutes=self.file_dict['REDUCED_aggregation_minutes'][aggregation]
            )
            
            outfolder = self.file_dict['REDUCED_df_aggregated_paths'][aggregation]
            # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
            # overwrite_dataframe(df_to_compare, outfolder)
            
            # EXPECTED OUTPUT
            expectation = self.spark.read.parquet("file://" + outfolder)
            self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__compute_the_coefficients__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['df_aggregated_paths']['_10'])
        # TRANSFORMATION
        df_to_compare = compute_the_coefficients(df_aggregated=df)
        
        outfolder = self.file_dict['df_norm_coeffs_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__REDUCED_compute_the_coefficients__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_aggregated_paths']['_2'])
        # TRANSFORMATION
        df_to_compare = compute_the_coefficients(df_aggregated=df)
        
        outfolder = self.file_dict['REDUCED_df_norm_coeffs_path'] 
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
    
    def test__save_normalization__check(self):
        df = self.spark.read.parquet("file://" + self.file_dict['df_norm_coeffs_path'])
        result_to_compare, path_to_compare = save_normalization(
            df_normalization=df,
            id_normalization=create_id_for_hostgroups_and_plugins_start_end(
                hostgroups=self.file_dict['filtered_plugins_hostgroups_df_HOSTGROUPS_TO_FILTER'],
                plugins=self.file_dict['selected_plugins'],
                start=self.file_dict["start_date"],
                end=self.file_dict['end_date']
            ),
            outfolder=self.file_dict['df_norm_coeffs_CHECK_path'],
            local=True
        )
        
        self.assertEqual(result_to_compare, 0)
        self.assertEqual(path_to_compare, 
                         "file://" + 
                         self.file_dict['expected_etl_steps']['save_normalization'])
        
    def test__REDUCED_save_normalization__check(self):
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_norm_coeffs_path'])
        result_to_compare, path_to_compare = save_normalization(
            df_normalization=df,
            id_normalization=create_id_for_hostgroups_and_plugins_start_end(
                hostgroups=self.file_dict['REDUCED_filtered_plugins_hostgroups_df_HOSTGROUPS_TO_FILTER'],
                plugins=self.file_dict['REDUCED_selected_plugins'],
                start=self.file_dict["REDUCED_start_date"],
                end=self.file_dict['REDUCED_end_date']
            ),
            outfolder=self.file_dict['REDUCED_df_norm_coeffs_CHECK_path'],
            local=True,
        )
        
        self.assertEqual(result_to_compare, 0)
        self.assertEqual(path_to_compare, 
                         "file://" + 
                         self.file_dict['expected_etl_steps']['REDUCED_save_normalization'])

    def test__normalize__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['df_aggregated_paths']['_60'])
        df_normalization = self.spark.read.parquet("file://" + 
                                                   self.file_dict['df_norm_coeffs_path'])
        # TRANSFORMATION
        df_to_compare = normalize(
            df=df,
            df_normalization=df_normalization
        )
        
        outfolder = self.file_dict['df_normalize_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def test__REDUCED_normalize__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_aggregated_paths']['_2'])
        df_normalization = self.spark.read.parquet("file://" + 
                                                   self.file_dict['REDUCED_df_norm_coeffs_path'])
        # TRANSFORMATION
        df_to_compare = normalize(
            df=df,
            df_normalization=df_normalization
        )
        
        outfolder = self.file_dict['REDUCED_df_normalize_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def test__pivot_plugin_as_column__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['df_normalize_path'])
        # TRANSFORMATION
        df_to_compare = pivot_plugin_as_column(
            df=df,
            plugin_names_list=list(self.file_dict['selected_plugins'].keys())
        )
        
        outfolder = self.file_dict['df_pivot_plugin_as_column_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def test__REDUCED_pivot_plugin_as_column__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_normalize_path'])
        # TRANSFORMATION
        df_to_compare = pivot_plugin_as_column(
            df=df,
            plugin_names_list=list(self.file_dict['REDUCED_selected_plugins'].keys())
        )
        
        outfolder = self.file_dict['REDUCED_df_pivot_plugin_as_column_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def test__REDUCED_pivot_plugin_as_column_with_NULL_values__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_normalize_with_NULL_values_path'])
        # TRANSFORMATION
        df_to_compare = pivot_plugin_as_column(
            df=df,
            plugin_names_list=list(self.file_dict['REDUCED_selected_plugins'].keys()).append('fake_plugin')
        )
        
        outfolder = self.file_dict['REDUCED_df_pivot_plugin_as_column_with_NULL_values_from_08_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))

    def test__resample__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['df_pivot_plugin_as_column_path'])
        # TRANSFORMATION
        df_to_compare = resample(
            spark=self.spark,
            df=df,
            start_date=self.file_dict["start_date"],
            end_date=self.file_dict['end_date'],
            every_n_minutes=self.file_dict['aggregation_minutes']['_60'],
            fill_value=None
        )
        
        outfolder = self.file_dict['df_resample_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def test__REDUCED_resample__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_pivot_plugin_as_column_path'])
        # TRANSFORMATION
        df_to_compare = resample(
            spark=self.spark,
            df=df,
            start_date=self.file_dict["REDUCED_start_date"],
            end_date=self.file_dict['REDUCED_end_date'],
            every_n_minutes=self.file_dict['REDUCED_aggregation_minutes']['_2'],
            fill_value=None
        )
        
        outfolder = self.file_dict['REDUCED_df_resample_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))
        
    def test__REDUCED_resample_with_NULL_values__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_pivot_plugin_as_column_with_NULL_values_path'])
        # TRANSFORMATION
        df_to_compare = resample(
            spark=self.spark,
            df=df,
            start_date=self.file_dict["REDUCED_start_date"],
            end_date=self.file_dict['REDUCED_end_date'],
            every_n_minutes=self.file_dict['REDUCED_aggregation_minutes']['_2'],
            fill_value=None
        )
        
        outfolder = self.file_dict['REDUCED_df_resample_with_NULL_values_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation, True))
        
    def test__REDUCED_resample_with_NULL_values_from_08__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_pivot_plugin_as_column_with_NULL_values_from_08_path'])
        # TRANSFORMATION
        df_to_compare = resample(
            spark=self.spark,
            df=df,
            start_date=self.file_dict["REDUCED_start_date"],
            end_date=self.file_dict['REDUCED_end_date'],
            every_n_minutes=self.file_dict['REDUCED_aggregation_minutes']['_2'],
            fill_value=None
        )
        
        outfolder = self.file_dict['REDUCED_df_resample_with_NULL_values_from_08_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation, True))

    def test__create_window__compare(self):
        for hystory_steps in self.file_dict['history_steps']:
            # INPUT
            df = self.spark.read.parquet("file://" + self.file_dict['df_pivot_plugin_as_column_path'])
            # TRANSFORMATION
            df_to_compare = create_window(
                df_plugins_as_col=df,
                steps=self.file_dict['history_steps'][hystory_steps]
            )
            
            outfolder = self.file_dict['df_create_window_paths'][hystory_steps]
            # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
            # overwrite_dataframe(df_to_compare, outfolder)
            
            # EXPECTED OUTPUT
            expectation = self.spark.read.parquet("file://" + outfolder)
            self.assertTrue(are_dfs_equal(df_to_compare, expectation))    
    
    def test__REDUCED_create_window__compare(self):
        for hystory_steps in self.file_dict['REDUCED_history_steps']:
            # INPUT
            df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_pivot_plugin_as_column_path'])
            # TRANSFORMATION
            df_to_compare = create_window(
                df_plugins_as_col=df,
                steps=self.file_dict['REDUCED_history_steps'][hystory_steps]
            )
            
            outfolder = self.file_dict['REDUCED_df_create_window_paths'][hystory_steps]
            # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
            # overwrite_dataframe(df_to_compare, outfolder)
            
            # EXPECTED OUTPUT
            expectation = self.spark.read.parquet("file://" + outfolder)
            self.assertTrue(are_dfs_equal(df_to_compare, expectation))    

    def test__REDUCED_create_window_with_NULL_values__compare(self):
        for hystory_steps in self.file_dict['REDUCED_history_steps']:
            # INPUT
            df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_pivot_plugin_as_column_with_NULL_values_from_08_path'])
            # TRANSFORMATION
            df_to_compare = create_window(
                df_plugins_as_col=df,
                steps=self.file_dict['REDUCED_history_steps'][hystory_steps]
            )
            
            outfolder = self.file_dict['REDUCED_df_create_window_with_NULL_values_paths'][hystory_steps]
            # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
            # overwrite_dataframe(df_to_compare, outfolder)
            
            # EXPECTED OUTPUT
            expectation = self.spark.read.parquet("file://" + outfolder)
            self.assertTrue(are_dfs_equal(df_to_compare, expectation))   

    def test__REDUCED_data_writing__compare_and_check(self):
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_create_window_with_NULL_values_paths']['_4'])
        
        outfolder = self.file_dict['REDUCED_df_data_writing_path']

        if os.path.isdir(outfolder):
            shutil.rmtree(outfolder)

        result_to_compare, df_to_compare = data_writing(
            df_window=df,
            outfolder="file://" + outfolder
        )
        
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertEqual(result_to_compare, 0)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation)) 

    def test__union_all__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['df_pivot_plugin_as_column_path'])
        df2 = self.spark.read.parquet("file://" + self.file_dict['df_pivot_plugin_as_column_path'])
        # TRANSFORMATION
        df_to_compare = union_all([df, df2])
        
        outfolder = self.file_dict['union_all_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))  

    def test__REDUCED_union_all__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_pivot_plugin_as_column_path'])
        df2 = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_pivot_plugin_as_column_path'])
        # TRANSFORMATION
        df_to_compare = union_all([df, df2])
        
        outfolder = self.file_dict['REDUCED_union_all_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))  

    def test__join_all__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['df_pivot_plugin_as_column_path'])
        df2 = self.spark.read.parquet("file://" + self.file_dict['df_filtered_path'])
        # TRANSFORMATION
        df_to_compare = join_all([df, df2], ["hostname", "timestamp", "hostgroup"])
        
        outfolder = self.file_dict['join_all_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))  

    def test__REDUCED_join_all__compare(self):
        # INPUT
        df = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_pivot_plugin_as_column_path'])
        df2 = self.spark.read.parquet("file://" + self.file_dict['REDUCED_df_filtered_path'])
        # TRANSFORMATION
        df_to_compare = join_all([df, df2], ["hostname", "timestamp", "hostgroup"])
        
        outfolder = self.file_dict['REDUCED_join_all_path']
        # USE THIS PART OF THE CODE TO DELETE THE OUTPUT FOLDER AND TO RECREATE THAT
        # overwrite_dataframe(df_to_compare, outfolder)
        
        # EXPECTED OUTPUT
        expectation = self.spark.read.parquet("file://" + outfolder)
        self.assertTrue(are_dfs_equal(df_to_compare, expectation))  
        
    def tearDown(self):
        self.sc.stop()