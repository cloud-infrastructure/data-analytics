#!/bin/bash
# This script should run in an environment (eg: docker container) configured
# to run jupyter notebooks and pyspark and connect to Spark 
# E.g.
# export CVMFSDIR=/cvmfs
# export KRB5CCNAME=<kerberos active ticket cache>
# docker run -it --rm -e KRB5CCNAME=$KRB5CCNAME -v $CVMFSDIR:/cvmfs:shared -v /tmp:/tmp  -v `pwd`:/work --net host gitlab-registry.cern.ch/cloud-infrastructure/data-analytics/sparknotebook:latest

function fail(){
  echo -e "\n------------------------\nFailing '$@'\n------------------------\n" >&2
  echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo -e "\n$0 finished (NOT OK) at $(date)\n"
  echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
  exit 1
}

WORK_DIR=$(readlink -f $(dirname $0))
echo WORK_DIR $WORK_DIR

[[ (-z "$KRB5CCNAME") || ( ! -e "$KRB5CCNAME") ]] && fail "Please export KRB5CCNAME pointing to a valid Kerberos ticket cache file. EXIT" 
klist -c $KRB5CCNAME


echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\nSetting hadoop libs\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

source set_spark_analytix.sh

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\nVerify Env\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

echo PYTHONPATH $PYTHONPATH
echo LD_LIBRARY_PATH $LD_LIBRARY_PATH
echo JAVA_HOME $JAVA_HOME
echo SPARK_HOME $SPARK_HOME
echo SPARK_DIST_CLASSPATH $SPARK_DIST_CLASSPATH


sed -i "s|PLACEHOLDER|${CI_COMMIT_SHORT_SHA}_${CI_JOB_ID}|g" /work/tests/adcern/integration/adcern_cfg_train.json
sed -i "s|PLACEHOLDER|${CI_COMMIT_SHORT_SHA}_${CI_JOB_ID}|g" /work/tests/adcern/integration/adcern_cfg_inference.json
sed -i "s|PLACEHOLDER|${CI_COMMIT_SHORT_SHA}_${CI_JOB_ID}|g" /work/tests/adcern/integration/adcern_cfg_experiment.yaml


HDFS_ADCERN_TEST_DIR="/project/it_cloud_data_analytics/CI/adcern_${CI_COMMIT_SHORT_SHA}_${CI_JOB_ID}/"
EOS_ADCERN_TEST_DIR="/eos/project-i/it-cloud-data-analytics/CI/adcern_${CI_COMMIT_SHORT_SHA}_${CI_JOB_ID}/"


echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest access to hdfs\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

if $(hdfs dfs -test -d ${HDFS_ADCERN_TEST_DIR}); then
    echo "hdfs dir ${HDFS_ADCERN_TEST_DIR} exists. Cleaning it"
    hdfs dfs -ls ${HDFS_ADCERN_TEST_DIR}
    hdfs dfs -rm -R -skipTrash ${HDFS_ADCERN_TEST_DIR} || fail 'to remove old hdfs folders'
    hdfs dfs -ls ${HDFS_ADCERN_TEST_DIR}
fi

echo "hdfs dir ${HDFS_ADCERN_TEST_DIR} does not exist. Creating it"
hdfs dfs -mkdir ${HDFS_ADCERN_TEST_DIR}

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest data_mining normalization_presence train\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

data_mining normalization_presence --resource_file /work/tests/adcern/integration/adcern_cfg_train.json || echo -e '\n------------------------\nNormalization is not present \n------------------------\n'
#fail 'test data_mining normalization_presence'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest data_mining compute_normalization train\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

data_mining compute_normalization --resource_file /work/tests/adcern/integration/adcern_cfg_train.json || fail 'test data_mining compute_normalization'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest data_mining transform_data train\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

data_mining transform_data --resource_file /work/tests/adcern/integration/adcern_cfg_train.json || fail 'test data_mining transform_data'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest data_mining transform_data inference\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

data_mining transform_data --resource_file /work/tests/adcern/integration/adcern_cfg_inference.json || fail 'test data_mining transform_data'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest data_mining copy_locally train\n"
echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

data_mining copy_locally --resource_file /work/tests/adcern/integration/adcern_cfg_train.json || fail 'test data_mining copy_locally'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest data_mining copy_locally inference\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

data_mining copy_locally --resource_file /work/tests/adcern/integration/adcern_cfg_inference.json || fail 'test data_mining copy_locally'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest data_mining analysis\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

data_mining training --analysis_file_path /work/tests/adcern/integration/adcern_cfg_experiment.yaml --retrain || fail 'test data_mining training'


data_mining inference --analysis_file_path /work/tests/adcern/integration/adcern_cfg_experiment.yaml || fail 'test data_mining inference'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "the script is finishing, ${HDFS_ADCERN_TEST_DIR} must be deleted" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

hdfs dfs -ls ${HDFS_ADCERN_TEST_DIR}
hdfs dfs -rm -R -skipTrash ${HDFS_ADCERN_TEST_DIR} || fail 'to remove old hdfs folders'
hdfs dfs -ls ${HDFS_ADCERN_TEST_DIR}

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo "deleting EOS leftovers from ${EOS_ADCERN_TEST_DIR}"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

ls ${EOS_ADCERN_TEST_DIR}
rm -rf ${EOS_ADCERN_TEST_DIR} || fail 'to remove old EOS folders'
ls ${EOS_ADCERN_TEST_DIR}

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\n$0 finished (OK) at $(date)\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
