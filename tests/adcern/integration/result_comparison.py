import time
import json
import datetime
import random
from elasticsearch import Elasticsearch
from dictdiffer import diff
import sys
from hashlib import blake2b
import adcern.publisher as publisher

ELASTICSEARCH_ENDPOINT = "srv_elasticsearch:9200/"
FIELD_NAME_TO_QUERY = "document_id"
ES_ID = 123456
TIME_WAIT = 10
ES_INDEX = "openstack-generic-data-analytics"
FILENAME = "/local_folder/test_results.json"

def check_es_status(counts=0, index='', MAX_RETRY=10):
    print("check_es_status querying for indices: retry nr: %d" % counts)
    if counts > MAX_RETRY:
        raise Exception('Too many retries')
    try:
        elastic_client = Elasticsearch(hosts=[ELASTICSEARCH_ENDPOINT])
        if index == '':
            print("Found indices: %s" % elastic_client.cat.indices())
        else:
            print("Found indices: %s" %
                  elastic_client.cat.indices(index=index))
    except Exception as e:
        print(e)
        counts += 1
        time.sleep(TIME_WAIT)
        check_es_status(counts=counts, index=index)

def retrieve_es_doc(es_id):
    # domain name, or server's IP address, goes in the 'hosts' list
    elastic_client = Elasticsearch(hosts=[ELASTICSEARCH_ENDPOINT])
    dict_res = elastic_client.get(index=ES_INDEX, id=es_id)
    return dict_res

def differ(doc1, doc2, ignore_set=[]):
    result = list(diff(doc1, doc2, ignore=ignore_set))

    for entry in result:
        if len(entry[2]) == 1:
            print('\n\t %s :\n\t\t %s\t%s' % entry)
        else:
            print('\n\t %s :\n\t\t %s\n\t\t\t%s\n\t\t\t%s' %
                  (entry[0], entry[1], entry[2][0], entry[2][1]))

    return(len(result))

def run_read(read_time_wait=30):
    print("-" * 10, "STEP READ", "-" * 10)
    print("Waiting... %i sec, then query ES" % read_time_wait)
    time.sleep(read_time_wait)

    print("Reading generated results from %s" % FILENAME)
    with open(FILENAME) as f:
        generated_results = json.loads(f.read())
        f.close()

    id_my_doc = generated_results["results"]["document_id"]
    print(json.dumps(generated_results,
                     indent=4, sort_keys=True))

    # Wait for FluentD and ElasticSearch (ES) to have done their job
    check_es_status(index=ES_INDEX, MAX_RETRY=10)

    # Query the endpoint of ES
    es_output = retrieve_es_doc(id_my_doc)
    out_dict = es_output["_source"]["data"]

    # Get the json
    print("Retrieved from ES:")
    print(json.dumps(es_output,
                     indent=4, sort_keys=True))

    print("-" * 10, "STEP COMPARE", "-" * 10)
    print("Compare documents:")

    differ_out = differ(generated_results["results"],
                        out_dict["results"])

    if differ_out == 0:
        print("@@@@@ SUCCESS @@@@@ all the attributes saved correctly")
    else:
        print("@@@@@ FAILURE @@@@@")
    return differ_out

if __name__ == "__main__":
    print("NEW Start script for WRITE / READ from local ES")

    # execute only if run as a script
    if len(sys.argv) == 1:
        print("1st argument (file name): ", sys.argv[0])
        print("Only one argument... :( not enough")
        exit(1)
    if sys.argv[1] in ['read_result']:
        stage = sys.argv[1]
        print("2nd argument (stage): ", sys.argv[1])
    else:
        print("Unfortunately the 2nd argument is this ... :( instead " +
              "of read_result", sys.argv[1])
        exit(1)

    print("""
----------------------------------------------------------------------------------
This is the python scripts that %s Data-Analytics results
----------------------------------------------------------------------------------
""" % stage)

    try:
        if stage == "read_result":
            exit_code = run_read()
            time.sleep(20)
    except Exception as e:
        print(e)
        exit_code = 1

    exit(exit_code)
