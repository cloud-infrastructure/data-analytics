# Tests

- [anomaly_to_ES](#anomaly_to_ES)
- [grafana_etl](#grafana_etl)
- [spark_etl](#spark_etl)
- [adcern](#adcern)


Describe the test purpose, content and method to reproduce outside CI

These tests run in CI via the .gitlab-ci.yml. 
Therefore most of the indications about how to run can be found in the dedicated jobs.

The tests are implemented in a way easy to reproduce outside the CI, interactively, 
in the local development area.

Unit tests run with `tox`, already installed in the image $CI_REGISTRY_IMAGE/tox:latest
It's possible to run interactively the same tests using the tox image and bind mounting the repository
```
docker run -v `pwd`:`pwd` -w `pwd` --rm -it $CI_REGISTRY_IMAGE/tox:latest
scl enable rh-python36 'tox -ecover'
```

## anomaly_to_ES

Docker compose configuration to start Elasticsearch, fluentd, and test the insertion of JSON anomaly reports into ES 

More details in [anomaly_to_ES/README.md](anomaly_to_ES/README.md)

## grafana_etl

In addition to the unit tests (to be completed) running via tox, a demonstrator of how to run the grafana_etl library via 
jupyter notebook is added ( [test_ETL.ipynb](grafana_etl/test_ETL.ipynb)). This notebook shows how to use the grafana_etl 
and a simple declarative configuration (yml file) to extract data from influxDB and display it using pandas dataframes. 

## spark_etl

In addition to the unit tests (to be completed) running via tox, the access to HDFS and Spark data using the spark_etl python module is 
provided here. Details are provided in the [spark_etl/README.md](spark_etl/README.md)

## adcern

Unit tests (to be implemented) running via tox, and integration tests of the Anomaly Detection [steps](adcern/integration/test_adcern_cmd.sh)
deployed via docker-compose in the CI. The same [script](adcern/integration/ci_test_script.sh) can be run interactively 
