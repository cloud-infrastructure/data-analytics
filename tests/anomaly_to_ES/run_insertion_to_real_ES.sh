#!/bin/bash -e

# This script is used to run a 
# test of insertion of anomaly results, in JSON format into the REAL ElasticSearch instance
# using fluentd as transport system to connect to MONIT
# Requirements:
# 
# In order to run the same script manually, assuming only docker available, run
#
# docker run --rm     -v /tmp:/tmp -v /builds:/builds -v `pwd`:`pwd` -w `pwd` -v /var/run/docker.sock:/var/run/docker.sock gitlab-registry.cern.ch/cloud-infrastructure/data-analytics/compose:qa sh -c ". ./ci_run_script.sh; start_docker_compose; stop_docker_compose"

function start_docker_compose() {
    cd $WORK_DIR
    ls -l
    rm -f test_results.json;
    rm -f compose.log
    #Use docker-compose variable substitution as from
    #https://docs.docker.com/compose/compose-file/#variable-substitution
    docker-compose -f docker-compose-to-real-ES.yml down --remove-orphans --volumes
    docker-compose -f docker-compose-to-real-ES.yml -p anomaly_to_ES up --remove-orphans --renew-anon-volumes -d
    docker-compose logs -f 2>&1 >> compose.log &
    docker ps
}
function stop_docker_compose(){
    cd $WORK_DIR
    docker-compose down -f docker-compose-to-real-ES.yml --remove-orphans --volumes
    cat compose.log
}

export WORK_DIR=$(readlink -f $(dirname $BASH_SOURCE))
echo WORK_DIR $WORK_DIR

export CI_REGISTRY_IMAGE=gitlab-registry.cern.ch/cloud-infrastructure/data-analytics
export CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH:-qa}
export CI_COMMIT_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
export FLUENTD_IMAGE_TAG=${FLUENTD_IMAGE_TAG:-$CI_COMMIT_TAG}
export SPARKNB_IMAGE_TAG=${SPARKNB_IMAGE_TAG:-$CI_COMMIT_TAG}
