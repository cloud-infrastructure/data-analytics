#!/bin/bash

function myecho() {
    echo -e "-------------------------------------------------------------------
$@
-------------------------------------------------------------------"
}

function check_indices() {
    myecho "List ES indices: will retry 5 times, if ES service still not ready"
    counts=0
    indices=`curl -s -X GET "srv_elasticsearch:9200/_cat/indices"`
    STATUS=$?
    echo "Status $STATUS"
    while [[ ("${indices:-none}" == "none") && ($counts -lt 6) ]]; do
        sleep 5
        counts=$((counts + 1))
        echo counts $counts
        indices=`curl -s -X GET "srv_elasticsearch:9200/_cat/indices"`
        STATUS=$?
        echo "Status $STATUS"
    done
    echo "Indices: $indices"
}

check_indices

# myecho "inject a document in the index dataana to verify the functionality"
# #curl -X PUT "srv_elasticsearch:9200/dataana/_doc/1?timeout=5m&pretty" -H 'Content-Type: application/json' --data-binary @dataformat.json

# #check_indices

# myecho "inject data using docker application with fluentd as log-driver\n
# in directory $(pwd)\n
# running python script that prints JSON results to be intercepted
# by fluentd and sent to elasticsearch"

# #docker run --rm --net=anomaly_to_es_default -v $(pwd):/work --log-driver=fluentd estester:latest python /work/test_anomaly_to_es_via_fluentd.py

# check_indices

#sleep 10
#myecho "retrieve injected json"
#curl -X GET "srv_elasticsearch:9200/openstack-generic-data-analytics/_search?pretty=true&q=*:*" | jq

#sleep 5
myecho "retrieve injected json"
curl -X GET "srv_elasticsearch:9200/openstack-generic-data-analytics/_doc/12345" | jq
