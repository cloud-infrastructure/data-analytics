
from helper_functions import generate_result
import time
import json

# from spark_etl.publisher import generate_result

ES_ID = 12345


# Dummy content
my_plugins_used = {
    "cpu__percent_user": {
        "plugin_instance": "",
        "type": "percent",
        "type_instance": "user",
        "plugin_name": "cpu"},
    "load_longterm": {
        "value_instance": "longterm",
        "plugin_name": "load"},
    "swap_swapfile_percent_used": {
        "plugin_instance": "swapfile",
        "type": "percent",
        "type_instance": "used",
        "plugin_name": "swap"},
    "df_root_percent_bytes_used": {
        "plugin_instance": "root",
        "type": "percent_bytes",
        "type_instance": "used",
        "plugin_name": "df"},
}


my_hyperparameters_str = {}
my_hyperparameters_str["parameter_str"] = "standard"
my_hyperparameters_int = {}
my_hyperparameters_int["parameter_int"] = 34
my_hyperparameters_float = {}
my_hyperparameters_float["parameter_float"] = 1.7
my_hyperparameters_bool = {}
my_hyperparameters_bool["parameter_bool"] = True

dummy_start_date = int(time.time() - (60 * 15))
dummy_end_date = int(time.time() - (60 * 60))

dummy_explainalibilty_metrics = ["cpu__percent_user",
                                 "load_longterm",
                                 "swap_swapfile_percent_used"]

id_monit, package_for_monit = generate_result(
    class_name="Local Outlier Factor",
    class_version=1.7,
    hyperparameters_str=my_hyperparameters_str,
    hyperparameters_int=my_hyperparameters_int,
    hyperparameters_float=my_hyperparameters_float,
    hyperparameters_bool=my_hyperparameters_bool,
    cell_name="gva_shared_012",
    hostgroup="cloud_compute/level2/main/gva_shared_012",
    plugins=my_plugins_used,
    granularity_min=10,
    normalization="standard",
    entity="p06636663p85421",
    score=34.6,
    percentile=100.0,
    rank=1,
    ts_second_window_start=dummy_start_date,
    ts_second_window_end=dummy_end_date,
    slide_steps=3,
    history_len=6,
    nr_anomalous_metrics=3,
    nr_total_metrics=4,
    ordered_anomalous_metrics=dummy_explainalibilty_metrics,
    only_top_k=3,
    validity=False)

generated_results = package_for_monit
print(json.dumps(generated_results))
