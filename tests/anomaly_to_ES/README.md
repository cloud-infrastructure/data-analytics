# anomaly to ES 

Docker compose configuration to start Elasticsearch, fluentd, and test the insertion of JSON anomaly reports into ES 

How to run interactively: read the header of the script `ci_run_script.sh` 

The same script is used in the `CI`. Refer to the job `pipeline_anomaly_to_ES`
