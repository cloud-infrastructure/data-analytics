
import json
import datetime
from hashlib import blake2b


def generate_result(class_name, class_version,
                    hyperparameters_str,
                    hyperparameters_int,
                    hyperparameters_float,
                    hyperparameters_bool,
                    cell_name, hostgroup, plugins, granularity_min,
                    normalization,
                    entity, score, rank, percentile,
                    ts_second_window_start, ts_second_window_end,
                    slide_steps, history_len,
                    nr_anomalous_metrics, nr_total_metrics,
                    ordered_anomalous_metrics,
                    only_top_k=7,
                    validity=False,
                    custom_id=None):
    """Emit the discovered anomalies in the result format.

    Return:
    - id: that will correspond to the _id with which it will be added to
        the monit infrastructure
    - result: dictionary containing all the info of the algorithm, ready
        to be transmitted to fluentd and Monit with a json via stdout:
        print(json.dumps(result))
    """
    result = {}
    # Algo name and version
    algo = {}
    algo["class_name"] = class_name
    algo["version"] = float(class_version)
    algo["hyperparams_str"] = {}
    for k in hyperparameters_str.keys():
        algo["hyperparams_str"][k] = str(hyperparameters_str[k])
    algo["hyperparameters_int"] = {}
    for k in hyperparameters_int.keys():
        algo["hyperparameters_int"][k] = int(hyperparameters_int[k])
    algo["hyperparameters_float"] = {}
    for k in hyperparameters_float.keys():
        algo["hyperparameters_float"][k] = float(hyperparameters_float[k])
    algo["hyperparameters_bool"] = {}
    for k in hyperparameters_bool.keys():
        algo["hyperparameters_bool"][k] = hyperparameters_bool[k]
    result["algo"] = algo
    # Data info
    entity_data = {}
    entity_data["entity"] = str(entity)
    entity_data["cell_name"] = str(cell_name)
    entity_data["hostgroup"] = str(hostgroup)
    result["entity_data"] = entity_data
    # Metrics info
    entity_metrics = {}
    entity_metrics["plugins"] = {}
    for k in plugins.keys():
        entity_metrics["plugins"][k] = plugins[k]
    result["entity_metrics"] = entity_metrics
    # Preprocessing
    preprocessing = {}
    preprocessing["type"] = str(normalization)
    result["preprocessing"] = preprocessing
    # Temporal - Window info
    temporal = {}
    dt_start = datetime.datetime.fromtimestamp(int(ts_second_window_start))
    temporal["time_start"] = dt_start.strftime("%Y-%m-%dT%H:%M:%S.000Z")
    dt_end = datetime.datetime.fromtimestamp(int(ts_second_window_end))
    temporal["time_end"] = dt_end.strftime("%Y-%m-%dT%H:%M:%S.000Z")
    temporal["window_length"] = int(history_len)
    temporal["step_resolution_minutes"] = int(granularity_min)
    result["temporal"] = temporal
    # Anomaly
    anomaly = {}
    anomaly["score"] = float(score)
    anomaly["rank_position"] = int(rank)
    anomaly["rank_percentile"] = float(percentile)
    explainalibilty = {}
    for i, metric in enumerate(ordered_anomalous_metrics):
        explainalibilty["metric_" + str(i)] = str(metric)
    explainalibilty["nr_anomalous_metrics"] = int(nr_anomalous_metrics)
    explainalibilty["total_metrics"] = int(nr_total_metrics)
    anomaly["explainalibilty"] = explainalibilty
    result["anomaly"] = anomaly

    result["is_anomaly"] = True
    result["slide_steps"] = int(slide_steps)

    # IDs
    ids = {}
    HASH_LENGTH = 20
    h_algo = blake2b(digest_size=HASH_LENGTH)
    h_algo.update(str.encode(json.dumps(algo)))
    ids["algo"] = h_algo.hexdigest()
    h_entity_data = blake2b(digest_size=HASH_LENGTH)
    h_entity_data.update(str.encode(json.dumps(entity_data)))
    ids["entity_data"] = h_entity_data.hexdigest()
    h_entity_metrics = blake2b(digest_size=HASH_LENGTH)
    h_entity_metrics.update(str.encode(json.dumps(entity_metrics)))
    ids["entity_metrics"] = h_entity_metrics.hexdigest()
    h_preprocessing = blake2b(digest_size=HASH_LENGTH)
    h_preprocessing.update(str.encode(json.dumps(preprocessing)))
    ids["preprocessing"] = h_preprocessing.hexdigest()
    h_temporal = blake2b(digest_size=HASH_LENGTH)
    h_temporal.update(str.encode(json.dumps(temporal)))
    ids["temporal"] = h_temporal.hexdigest()
    result["ids"] = ids

    # ID that will finish in the MONIT _ID

    h_app_id = blake2b(digest_size=HASH_LENGTH)
    h_app_id.update(str.encode(json.dumps(ids)))
    result["document_id"] = h_app_id.hexdigest()
    if custom_id is not None:
        result["document_id"] = str(custom_id)

    result["validity"] = validity
    return result["document_id"], {'data-analytics-results': result}
