import time
import json
import datetime
import random
from elasticsearch import Elasticsearch
from dictdiffer import diff
import sys
from hashlib import blake2b
import adcern.publisher as publisher

ELASTICSEARCH_ENDPOINT = "srv_elasticsearch:9200/"
FIELD_NAME_TO_QUERY = "document_id"
ES_ID = 123456
TIME_WAIT = 10
ES_INDEX = "openstack-generic-data-analytics"
FILENAME = "/work/test_results.json"


def generate_message(_id):
    date = datetime.datetime.strptime('2020-04-30', "%Y-%m-%d")
    # ts = time.time()
    rnd_float = random.random()
    print("rnd_float: %s" % rnd_float)
    results = {}
    results["staticsearch"] = 3555
    results[FIELD_NAME_TO_QUERY] = str(_id)
    results["metric__fff"] = 14.5
    results["rnd_float"] = rnd_float
    results["a_string"] = "a string"
    results["a_date"] = str(date)
    # results["now_timestamp"] = int(ts)
    results["a_timestamp"] = int(date.timestamp())

    return {'results': results}


def check_es_status(counts=0, index='', MAX_RETRY=10):
    print("check_es_status querying for indices: retry nr: %d" % counts)
    if counts > MAX_RETRY:
        raise Exception('Too many retries')
    try:
        elastic_client = Elasticsearch(hosts=[ELASTICSEARCH_ENDPOINT])
        if index == '':
            print("Found indices: %s" % elastic_client.cat.indices())
        else:
            print("Found indices: %s" %
                  elastic_client.cat.indices(index=index))
    except Exception as e:
        print(e)
        counts += 1
        time.sleep(TIME_WAIT)
        check_es_status(counts=counts, index=index)


def retrieve_es_doc(es_id):
    # domain name, or server's IP address, goes in the 'hosts' list
    elastic_client = Elasticsearch(hosts=[ELASTICSEARCH_ENDPOINT])
    dict_res = elastic_client.get(index=ES_INDEX, id=es_id)
    return dict_res


def differ(doc1, doc2, ignore_set=[]):
    result = list(diff(doc1, doc2, ignore=ignore_set))

    for entry in result:
        if len(entry[2]) == 1:
            print('\n\t %s :\n\t\t %s\t%s' % entry)
        else:
            print('\n\t %s :\n\t\t %s\n\t\t\t%s\n\t\t\t%s' %
                  (entry[0], entry[1], entry[2][0], entry[2][1]))

    return(len(result))


def run_write():
    check_es_status()

    # Create a message
    # Log it i the standard output
    print("-" * 10, "STEP WRITE", "-" * 10)
    print("The container prints this in the std out " +
          "(data to push to fluentd and ES):")

    # Dummy content
    my_plugins_used = {
        "cpu__percent_user": {
            "plugin_instance": "",
            "type": "percent",
            "type_instance": "user",
            "plugin_name": "cpu"},
        "load_longterm": {
            "value_instance": "longterm",
            "plugin_name": "load"},
        "swap_swapfile_percent_used": {
            "plugin_instance": "swapfile",
            "type": "percent",
            "type_instance": "used",
            "plugin_name": "swap"},
        "df_root_percent_bytes_used": {
            "plugin_instance": "root",
            "type": "percent_bytes",
            "type_instance": "used",
            "plugin_name": "df"},
    }

    dummy_start_date = int(time.time())
    dummy_end_date = int(time.time() + 6000)

    dummy_explainalibilty_metrics = ["cpu__percent_user",
                                     "load_longterm",
                                     "swap_swapfile_percent_used"]

    id_monit, package_for_monit = publisher.generate_result(
        class_name="Local Outlier Factor",
        algo_name="LOF",
        class_version=1.7,
        cell_name="gva_shared_012",
        hostgroup="cloud_compute/level2/main/gva_shared_012",
        plugins=my_plugins_used,
        granularity_min=10,
        normalization="standard",
        entity="charizard.cern.ch",
        score=34.6,
        normalized_score=10,
        percentile=100.0,
        rank=1,
        ts_second_window_start=dummy_start_date,
        ts_second_window_end=dummy_end_date,
        slide_steps=3,
        history_len=6,
        mean_train=5, 
        std_train=4
        )

    generated_results = package_for_monit
    # generated_results = generate_message(ES_ID)
    print(json.dumps(generated_results))

    with open(FILENAME, "w") as f:
        f.write(json.dumps(generated_results, indent=4, sort_keys=True))
        f.close()

    print("Generated results are in %s" % FILENAME)
    # Printing only the data content, withouth key results,
    # to avoid retrigger of pipeline
    print("data are %s " %
          json.dumps(generated_results['results'],
                     indent=4, sort_keys=True))

    # FLUENTD
    # It should have received it through the driver of the docker
    # and forwarded it to ElasticSearch

    # ELASTICSEARCH
    # It should have received the document form FluentD and
    # stored it in a proper index
    return 0


def run_read(read_time_wait=30):
    print("-" * 10, "STEP READ", "-" * 10)
    print("Waiting... %i sec, then query ES" % read_time_wait)
    time.sleep(read_time_wait)

    print("Reading generated results from %s" % FILENAME)
    with open(FILENAME) as f:
        generated_results = json.loads(f.read())
        f.close()

    id_my_doc = generated_results["results"]["document_id"]
    print(json.dumps(generated_results,
                     indent=4, sort_keys=True))

    # Wait for FluentD and ElasticSearch (ES) to have done their job
    check_es_status(index=ES_INDEX, MAX_RETRY=10)

    # Query the endpoint of ES
    es_output = retrieve_es_doc(id_my_doc)
    out_dict = es_output["_source"]["data"]

    # Get the json
    print("Retrieved from ES:")
    print(json.dumps(es_output,
                     indent=4, sort_keys=True))

    print("-" * 10, "STEP COMPARE", "-" * 10)
    print("Compare documents:")

    differ_out = differ(generated_results["results"],
                        out_dict["results"])

    if differ_out == 0:
        print("@@@@@ SUCCESS @@@@@ all the attributes saved correctly")
    else:
        print("@@@@@ FAILURE @@@@@")
    return differ_out


if __name__ == "__main__":
    print("NEW Start script for WRITE / READ from local ES")

    # execute only if run as a script
    if len(sys.argv) == 1:
        print("1st argument (file name): ", sys.argv[0])
        print("Only one argument... :( not enough")
        exit(1)
    if sys.argv[1] in ['read', 'write']:
        stage = sys.argv[1]
        print("2st argument (stage): ", sys.argv[1])
    else:
        print("Unfortunately the 2nd argument is this ... :( instead " +
              "of read or write", sys.argv[1])
        exit(1)

    print("""
----------------------------------------------------------------------------------
This is the python scripts that %s Data-Analytics results
----------------------------------------------------------------------------------
""" % stage)

    try:
        if stage == "write":
            exit_code = run_write()
            time.sleep(20)
        else:
            exit_code = run_read()
    except Exception as e:
        print(e)
        exit_code = 1

    exit(exit_code)
