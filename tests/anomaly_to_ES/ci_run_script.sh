#!/bin/bash -e

# This script is used by the gitlab CI of this repository to run a 
# test of insertion of anomaly results, in JSON format into an ElasticSearch instance
# using fluentd as transport system
# Requirements:
# 
# In order to run the same script manually, assuming only docker available, run
#
# docker run --rm     -v /tmp:/tmp -v /builds:/builds -v `pwd`:`pwd` -w `pwd` -v /var/run/docker.sock:/var/run/docker.sock gitlab-registry.cern.ch/cloud-infrastructure/data-analytics/compose:qa sh -c ". ./ci_run_script.sh; start_docker_compose; run_reader; stop_docker_compose"

function start_docker_compose() {
    cd $WORK_DIR
    ls -l
    rm -f test_results.json;
    rm -f compose.log myfluent.log
    #Use docker-compose variable substitution as from
    #https://docs.docker.com/compose/compose-file/#variable-substitution
    echo -e "\n------- DUMP docker-compose.yml ----\n"
    docker-compose -f docker-compose.yml config
    echo -e "\n------- END docker-compose.yml ----\n"
    docker-compose -p anomaly_to_ES down --remove-orphans --volumes

    docker-compose pull
    echo -e "\n------- START docker-compose.yml ----\n"
    docker-compose -f docker-compose.yml -p anomaly_to_ES up --remove-orphans --renew-anon-volumes -d
    echo -e "\n------- Saving the logs in compose.log ----\n"
    docker-compose -p anomaly_to_ES logs -f 2>&1 >> compose.log &
    echo -e "\n------- Running Dockers ----\n"
    docker ps
}

function run_reader() {
    cd $WORK_DIR
    echo "going to run: docker run --rm --user=root --net=anomaly_to_es_default --name mytester_read -v $(pwd):/work ${CI_REGISTRY_IMAGE}/sparknotebook:${SPARKNB_IMAGE_TAG} python3 /work/test_anomaly_to_es_via_fluentd.py read"
    docker run --rm --user=root --net=anomaly_to_es_default --name mytester_read -v $(pwd):/work ${CI_REGISTRY_IMAGE}/sparknotebook:${SPARKNB_IMAGE_TAG} python3 /work/test_anomaly_to_es_via_fluentd.py read
}

function stop_docker_compose(){
    cd $WORK_DIR
    docker-compose -p anomaly_to_ES down --rmi all --remove-orphans --volumes
    docker container prune -f
    cat compose.log
}

export WORK_DIR=$(readlink -f $(dirname $BASH_SOURCE))
echo WORK_DIR $WORK_DIR

export CI_REGISTRY_IMAGE=gitlab-registry.cern.ch/cloud-infrastructure/data-analytics
export CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH:-qa}
export CI_COMMIT_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
export FLUENTD_IMAGE_TAG=${FLUENTD_IMAGE_TAG:-$CI_COMMIT_TAG}
export SPARKNB_IMAGE_TAG=${SPARKNB_IMAGE_TAG:-$CI_COMMIT_TAG}

echo CI_REGISTRY_IMAGE $CI_REGISTRY_IMAGE
echo CI_COMMIT_BRANCH  $CI_COMMIT_BRANCH
echo CI_COMMIT_TAG     $CI_COMMIT_TAG
echo FLUENTD_IMAGE_TAG $FLUENTD_IMAGE_TAG
echo SPARKNB_IMAGE_TAG $SPARKNB_IMAGE_TAG
