import unittest

from rallytester.cmd import rally_tester
from rallytester import notifier
from rallytester import rallytester
from rallytester.tests import fixtures

# Import mock compatible with Python2 and Python3
try:
    import mock
except ImportError:
    from unittest import mock


class TestRallyTester(unittest.TestCase):

    def test_get_args_default(self):
        self.assertEqual(
            fixtures.DEFAULT_ARGS_RALLYTESTER,
            rally_tester.get_args([
                '--task', 'empty/empty_task.json',
                '--deployment', 'empty_deployment']))

    def test_main_no_args(self):
        with self.assertRaises(SystemExit) as cm:
            rally_tester.main()
        self.assertEqual(cm.exception.code, 2)

    @mock.patch.object(rallytester, 'execute_task')
    def test_main_args_default(self, mock_obj):
        mock_obj.return_value = {'task': 'empty/empty_task.json',
                                 'succeed': 1}, None

        rally_tester.main([
            '--task', 'empty/empty_task.json',
            '--deployment', 'empty_deployment',
            '--debug'])

        mock_obj.assert_called_once_with(
            'empty_deployment',
            'empty/empty_task.json',
            None
        )

    @mock.patch.object(rallytester, 'execute_task')
    @mock.patch.object(rally_tester, 'get_args')
    def test_main_args_trimmer(self, mock_args, mock_obj):
        mock_args.return_value = {
            "task": 'empty/empty_task.json',
            "deployment": 'empty_deployment',
            "debug": True,
            "log_level": 'INFO',
            "log_file": '/var/log/rally-tester/rally-tester.log',
            "alert_to": 'cloud-infrastructure-3rd-level@cern.ch',
            "trimmerConfig": [
                ['fake_regex', '["(.*)", "xyz"]']
            ]
        }

        mock_obj.return_value = {'task': 'empty/empty_task.json',
                                 'succeed': 1}, None

        rally_tester.main([
            '--task', 'empty/empty_task.json',
            '--deployment', 'empty_deployment',
            '--debug'])

    @mock.patch.object(rallytester, 'execute_task')
    @mock.patch.object(notifier, 'send_etl')
    def test_main_args_execute_error(self, mock_notifier, mock_obj):
        mock_obj.return_value = {'task': 'empty/empty_task.json',
                                 'succeed': 0}, "ERROR"

        rally_tester.main([
            '--task', 'empty/empty_task.json',
            '--deployment', 'empty_deployment',
            '--debug'])

        mock_obj.assert_called_once_with(
            'empty_deployment',
            'empty/empty_task.json',
            None
        )

        mock_notifier.assert_called_once_with(
            'empty/empty_task.json',
            'empty_deployment',
            "ERROR",
            "cloud-infrastructure-3rd-level@cern.ch"
        )
