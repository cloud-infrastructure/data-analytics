from etl.grafana_etl import ETLDriver
from etl.grafana_etl import GHandler
from etl.grafana_etl import mylogger
import os
import unittest
import yaml

logger = mylogger.getLogger()


class TestETL(unittest.TestCase):

    def setUp(self):
        print("THIS IS " + __file__ + " and " + __package__)
        filepath, _ = os.path.split(__file__)
        self.configfile = os.path.normpath(
            os.path.join(filepath, 'etc/test_grafana_etl.yaml'))

        print("THIS IS DIR %s " % os.getcwd())
        # Change dir in order to find request and secret dirs
        os.chdir(os.path.join(filepath, '/..'))
        print("THIS IS new DIR %s" % os.getcwd())

    def test_succeed_create_ESLogHandler(self):
        with open(self.configfile) as f:
            configuration = f.read()
        cfg = yaml.safe_load(configuration)
        g = GHandler.ESLogHandler(**cfg['test_es']['handler'])
        self.assertIsNotNone(g)

    def test_succeed_create_InfluxDBHandler(self):
        with open(self.configfile) as f:
            configuration = f.read()
        cfg = yaml.safe_load(configuration)
        g = GHandler.InfluxDBHandler(**cfg['test_influx']['handler'])
        self.assertIsNotNone(g)

    def test_succeed_create_ETLDriver(self):
        d = ETLDriver.ETLDriver(config_name=self.configfile)
        self.assertIsNotNone(d)
