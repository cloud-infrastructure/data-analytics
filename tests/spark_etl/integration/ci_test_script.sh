#!/bin/bash -e

# This script is used by the gitlab CI of this repository to run a 
# test of access and processing of Spark data that is based on the script test_spark_connector.sh
# Requirements:
#     - spark_etl libs (distributed in docker image)
#     - Kerberos authentication (passed via CI)
#     - cvmfs spark libs (exposed via cvmfs)
 
# In order to access the needed libraries from cvmfs, 
# a cvmfs service is started with docker-compose 
# 
# In order to run the same script manually, assuming only docker available, run
#
# CI_PROJECT_DIR=`pwd | sed -e 's@/tests/spark_etl@@'`
# docker run --rm -e CI_USER=$CI_USER -e CI_USER_PASSWD=$CI_USER_PASSWD -e CI_PROJECT_DIR=${CI_PROJECT_DIR} -v /tmp:/tmp -v /builds:/builds -v `pwd`:/work -v /var/run/docker.sock:/var/run/docker.sock gitlab-registry.cern.ch/cloud-infrastructure/data-analytics/compose:qa bash -c '. /work/ci_test_script.sh; start_docker_compose'
#
# Consider to open the Spark connection ports in iptables
#
# sudo iptables -I INPUT -p tcp -m multiport --dports 5001:6000 -m comment --comment "00200 firewall for hadoop jobs" -j ACCEPT
# sudo iptables -I DOCKER-USER -p tcp -m multiport --dports 5001:6000 -m comment --comment "00200 firewall for hadoop jobs" -j ACCEPT
#

function start_docker_compose() {
    mkdir -p $KRB5DIR
    export KRB5CCNAME=$KRB5DIR/krb5cc_docker; kdestroy -c $KRB5CCNAME ; echo $CI_USER_PASSWD | kinit -c $KRB5CCNAME $CI_USER@CERN.CH; klist -c $KRB5CCNAME
    ls -l $KRB5DIR

    echo -e "\n------- DUMP docker-compose.yml ----\n"
    docker-compose -f docker-compose.yml config
    echo -e "\n------- END docker-compose.yml ----\n"
    docker-compose -f docker-compose.yml down --remove-orphans --volumes
    docker-compose pull
    docker-compose -f docker-compose.yml -p spark_etl up --remove-orphans --renew-anon-volumes --abort-on-container-exit --exit-code-from srv_spark_etl
    #docker-compose logs -f 2>&1 >> compose.log &
}

function stop_docker_compose(){
    cd $WORK_DIR
    docker-compose down --remove-orphans --volumes  # First remove containers
    docker container prune -f
    docker-compose down --rmi all --remove-orphans --volumes # Then remove images
}

export WORK_DIR=$(readlink -f $(dirname $BASH_SOURCE))
echo WORK_DIR $WORK_DIR

cd $WORK_DIR
export CI_JOB_ID=${CI_JOB_ID:-noCI}
export CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH:-noCI}
export COMMIT_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
export CVMFSDIR=/builds/${COMMIT_TAG}/cvmfs-${CI_JOB_ID}
export KRB5DIR=/builds/${COMMIT_TAG}/krb5-${CI_JOB_ID}
export IMAGE_TAG=${COMMIT_TAG}



