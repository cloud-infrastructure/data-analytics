#!/bin/bash
# This script should run in an environment (eg: docker container) configured
# to run jupyter notebooks and pyspark and connect to Spark 
# E.g.
# export CVMFSDIR=/cvmfs
# export KRB5CCNAME=<kerberos active ticket cache>
# docker run -it --rm -e KRB5CCNAME=$KRB5CCNAME -v $CVMFSDIR:/cvmfs:shared -v /tmp:/tmp  -v `pwd`:/work --net host gitlab-registry.cern.ch/cloud-infrastructure/data-analytics/sparknotebook:latest

function fail(){
  echo -e "\n------------------------\nFailing '$@'\n------------------------\n" >&2
  echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo -e "\n$0 finished (NOT OK) at $(date)\n"
  echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
  exit 1
}

WORK_DIR=$(readlink -f $(dirname $0))
echo WORK_DIR $WORK_DIR

[[ (-z "$KRB5CCNAME") || ( ! -e "$KRB5CCNAME") ]] && fail "Please export KRB5CCNAME pointing to a valid Kerberos ticket cache file. EXIT" 
klist -c $KRB5CCNAME


echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\nSetting hadoop libs\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

source set_spark_analytix.sh

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "Verify Env\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

echo PYTHONPATH $PYTHONPATH
echo LD_LIBRARY_PATH $LD_LIBRARY_PATH
echo JAVA_HOME $JAVA_HOME
echo SPARK_HOME $SPARK_HOME
echo SPARK_DIST_CLASSPATH $SPARK_DIST_CLASSPATH


echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest access to hdfs\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"

hdfs dfs -ls /project/monitoring || fail 'test access to hdfs'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\ntest python pyspark module\n" 
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
outbasepath="/project/it_cloud_data_analytics/CI/spark_etl/test_rally_errors_py"
hdfs dfs -test -d $outbasepath; [[ "$?" -eq "0" ]] && echo "have to remove folder $outbasepath" && hdfs dfs -rm -r $outbasepath
python3 ${WORK_DIR}/test_spark_connector.py

# This still does not work in the swan image (issue with nbconvert libraries)
#echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
#echo -e "\ntest jupyter notebook\n" 
jupyter nbconvert --version
#echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
outbasepath="/project/it_cloud_data_analytics/CI/spark_etl/test_rally_errors_ipynb"
hdfs dfs -test -d $outbasepath; [[ "$?" -eq "0" ]] && echo "have to remove folder $outbasepath" && hdfs dfs -rm -r $outbasepath
jupyter nbconvert --ExecutePreprocessor.timeout=600  --to notebook --execute ${WORK_DIR}/test_notebook_spark_connector.ipynb
