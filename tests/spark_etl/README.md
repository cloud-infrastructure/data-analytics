# Test folder for spark_etl library

The access to HDFS and Spark data using the spark_etl python module is tested here

## CI test

The CI test runs in QA and at every tag. 
The CI triggers the script integration/ci_test_script.sh that can be also executed manually for testing purposes.

The ci_test_script.sh starts docker compose services
- a srv_cvmfs container, in order to expose cvmfs
- the sparknotebook container that includes the spark_etl python module under test.